;;; Copyright 2020 Sandipan Razzaque
;;;
;;; Licensed under the Apache License, Version 2.0 (the "License");
;;; you may not use this file except in compliance with the License.
;;; You may obtain a copy of the License at
;;;
;;;     http://www.apache.org/licenses/LICENSE-2.0
;;;
;;; Unless required by applicable law or agreed to in writing, software
;;; distributed under the License is distributed on an "AS IS" BASIS,
;;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;;; See the License for the specific language governing permissions and
;;; limitations under the License.

(in-package #:cl-fix-validation)

(defstruct validation-error
  (path (error "Path must be specified") :type list)
  (err (error "Error code must be specified") :type (member :missing-field
                                                            :type
                                                            :unknown-field))
  (msg "" :type string))

(defun %normalize-fld (f)
  "Normalises a field spec to a list."
  (if (listp f)
      f
      (list f)))

(defun %get-fld-name (f)
  "Given a normalised field def F, return the field name."
  (find-if-not (lambda (x) (string= "GROUP" (symbol-name x)))
               f))

(defun %is-user-controlled-field (fld)
  "Is this field something that the user actually defines?"
  (let ((fld* (car (%normalize-fld fld))))
    (not (member fld* '(:check-sum :body-length :begin-string :msg-type)))))

(defun %is-required-field (fld)
  (and (listp fld)
       (typep (car fld) 'keyword)
       (getf (cdr fld) :required)))

(defun %is-missing (fld msg)
  (let ((fld* (if (listp fld) (car fld) fld)))
    (not (getf msg fld*))))

(defun %check-missing-required-fields (msg fields)
  (loop for f in fields
        when (and (%is-required-field f)
                  (%is-missing f msg))
          collect
          (make-validation-error :path (list (%get-fld-name f))
                                 :err :missing-field
                                 :msg (format nil "Required field for [msg-type=~W] is missing: ~W"
                                              (getf msg :msg-type)
                                              (%get-fld-name f)))))

(defun %get-val (msg f)
  (let* ((f* (%normalize-fld f))
         (key (if (string-equal "GROUP" (symbol-name (car f*)))
                  (cadr f*)
                  (car f*))))
    (getf msg key)))

(defun %is-sane-type (value field-meta)
  (let* ((field-type (getf field-meta :type))
         (field-type* (if (listp field-type)
                          (intern (symbol-name (car field-type)))
                          field-type)))
    (cond ((member field-type* '(string-enum char-enum int-enum) :test #'string-equal :key #'symbol-name)
           (getf (cdr field-type) value))
          (t
           (typep value field-type*)))))

(defun %get-field-type (field-meta)
  (cond
    ((and (listp field-meta)
          (string-equal "GROUP" (symbol-name (car field-meta))))
     'list)
    ((listp field-meta)
     (getf field-meta :type))))

(defun %make-type-err-msg (value field-meta)
  (format nil "value [~W] is not of type ~A"
          value
          (let ((field-type (getf field-meta :type)))
            (if (listp field-type)
                (format nil "~A (see dictionary for enum values)" (or (car field-type)
                                                                      "LIST"))
                field-type))))

(defun %check-invalid-types (msg msg-fields fields-by-name)
  (loop for f in msg-fields
        for field-meta = (gethash (car (%normalize-fld f))
                                  fields-by-name)
        for val = (%get-val msg f)
        when (and val
                  (not (%is-sane-type val field-meta)))
          collect
          (make-validation-error :path (list (%get-fld-name (%normalize-fld f)))
                                 :err :type
                                 :msg (%make-type-err-msg val field-meta))))

(defun %field-name-sym (field-spec)
  (cond
    ((and (listp field-spec)
          (string= (symbol-name (first field-spec)) "GROUP"))
     (cadr field-spec))
    ((listp field-spec)
     (car field-spec))
    (t
     field-spec)))

(defun %check-unknown-fields (msg msg-fields)
  (loop for x from 0 below (length msg) by 2
        for fld = (elt msg x)
        when
        (and
         (not (eq :msg-type fld))
         (not
          (find-if (lambda (x) (eq x fld))
                   msg-fields
                   :key #'%field-name-sym)))
        collect
        (make-validation-error :path (list fld)
                               :err :unknown-field
                               :msg (format nil "Unknown field for [msg-type=~W]: ~W"
                                            (getf msg :msg-type)
                                            fld))))

(defun validate-msg (dict msg &key skip-header skip-trailer (skip-non-user t) &allow-other-keys)
  "Given a dictionary (DICT), a FIX message plist (MSG), return a list of VALIDATION-ERROR.
   Does NOT currently support traversal into repeating groups."
  (let* ((msg-type (or (getf msg :msg-type)
                       (return-from validate-msg
                         (list
                          (make-validation-error :path '(:msg-type)
                                                 :err :missing-field
                                                 :msg "No :msg-type was specified.")))))
         (msg-fields (gethash msg-type (getf dict :messages-by-name)))
         (fields-by-name (getf dict :fields-by-name))
         (fields-list (concatenate 'list
                                   msg-fields
                                   (if (not skip-header) (getf dict :header-fields))
                                   (if (not skip-trailer) (getf dict :trailer-fields)))))
    (when skip-non-user
      (setf fields-list (remove-if-not #'%is-user-controlled-field fields-list)))
    (concatenate 'list
                 (%check-missing-required-fields msg fields-list)
                 (%check-invalid-types msg fields-list fields-by-name)
                 (%check-unknown-fields msg fields-list))))

