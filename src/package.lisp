;;; Copyright 2020 Sandipan Razzaque
;;;
;;; Licensed under the Apache License, Version 2.0 (the "License");
;;; you may not use this file except in compliance with the License.
;;; You may obtain a copy of the License at
;;;
;;;     http://www.apache.org/licenses/LICENSE-2.0
;;;
;;; Unless required by applicable law or agreed to in writing, software
;;; distributed under the License is distributed on an "AS IS" BASIS,
;;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;;; See the License for the specific language governing permissions and
;;; limitations under the License.

(defpackage #:cl-fix-types
  (:use #:cl)
  (:export currency
           country
           exchange
           monthyear
           qty
           price
           priceoffset
           amt
           percentage
           int
           len
           seqnum
           tagnum
           dayofmonth
           utctimestamp
           localmktdate
           utcdateonly
           utctimeonly
           data
           repeating-group
           numingroup
           get-from-string-converter
           get-to-string-converter
           make-utcdateonly
           make-localmktdate
           make-utctimeonly
           make-monthyear)
  (:import-from #:alexandria
                if-let
                when-let))

(defpackage #:cl-fix-serialization
  (:nicknames :cl-fix-rw)
  (:use #:cl
        #:arrow-macros)
  (:export read-fix-msg
           serialize-fix-msg
           parse-fix-msg))

(defpackage #:cl-fix-validation
  (:use #:cl #:alexandria)
  (:export validate-msg
           validation-error-path
           validation-error-err))

(defpackage #:cl-fix-dict
  (:use #:cl)
  (:nicknames #:dict)
  (:export *registry*
           defdict
           in-dict
           deffields
           defcomponent
           defmessage
           defheader
           deftrailer
           defreader
           defserializer
           defvalidator
           dict-reader-fn-sym
           dict-serializer-fn-sym
           fix-serializer-error
           fix-reader-error)
  (:import-from #:alexandria
                if-let)
  (:use #:arrow-macros))

(defpackage #:cl-fix-util
  (:use #:cl)
  (:nicknames #:util)
  (:export fix-field
           getff
           make-fix-msg
           fix-msg-type
           resolve-dict
           destroy-thread))

(defpackage #:fastbuffer
  (:use :cl)
  (:nicknames #:fb)
  (:export fb
           fb-process
           fb-reserve
           fb-elt
           fb-publish
           fb-reset
           fb-publish-one))

(defpackage #:cl-fix-fsm
  (:use #:cl)
  (:nicknames #:fsm)
  (:export fsm
           make-transition
           fsm-current-state
           fsm-process-event))

(defpackage #:cl-fix-connector
  (:use #:cl)
  (:nicknames #:conn)
  (:export connector
           add-session
           find-session
           get-sessions
           start-connector
           stop-connector
           sessions
           enqueue-msg
           connector-event
           event-data
           event-type
           event-raw-msg
           event-source-session
           event-timestamp
           host
           port
           fb-in
           fb-out
           connector-error
           socket-read-error 
           socket-write-error
           backpressure-error
           retries-exhausted
           reader-fn
           serialize-fn
           publish-tick
           dict
           serialize-and-send
           connector-state))

(defpackage #:cl-fix-initiator
  (:use #:cl)
  (:export initiator)
  (:import-from #:cl-fix-connector
                socket-read-error
                socket-write-error
                backpressure-error
                retries-exhausted)
  (:import-from #:cl-fix-dict
                fix-serializer-error
                fix-reader-error))

(defpackage #:cl-fix-acceptor
  (:use #:cl)
  (:export acceptor))

(defpackage #:cl-fix-message-store
  (:use #:cl)
  (:nicknames #:store)
  (:export make-file-store
           file-store
           file-store-first-msg-position
           rebuild-index
           close-store
           store-msg
           iterate))

(defpackage #:cl-fix-session
  (:use #:cl #:alexandria)
  (:export session
           session-id
           sender-comp-id
           sender-sub-id
           target-comp-id
           target-sub-id
           conn
           dict
           persist
           persist-inbound-msg
           persist-outbound-msg
           perform-resend
           read-state-from-store
           init-session
           on-connect
           stop-session
           stoppedp
           session-handler
           session-tick
           enqueue-msg
           reset-outgoing-seq-num
           reset-incoming-seq-num
           deftickhandler
           init-stores
           close-stores
           *current-time*
           validate-logon
           session-status)
  (:import-from #:cl-fix-util
                fix-field
                getff
                resolve-dict))

(defpackage #:cl-fix
  (:use #:cl #:cl-fix-types)
  (:export initiator
           acceptor
           session
           deftickhandler
           *current-time*
           enqueue-msg
           add-session
           start-connector
           stop-connector
           init-session
           stop-session
           sender-comp-id
           sender-sub-id
           target-comp-id
           target-sub-id
           validate-logon
           currency
           country
           exchange
           monthyear
           qty
           price
           priceoffset
           amt
           percentage
           int
           len
           seqnum
           tagnum
           dayofmonth
           utctimestamp
           localmktdate
           utcdateonly
           utctimeonly
           data
           numingroup
           fix-msg-type
           make-fix-msg
           make-monthyear
           make-utcdateonly
           make-localmktdate
           make-utctimeonly
           validation-error-err
           validation-error-path
           get-sessions
           session-status)
  (:import-from #:cl-fix-util
                fix-msg-type
                make-fix-msg)
  (:import-from #:cl-fix-connector
                add-session
                start-connector
                stop-connector
                get-sessions)
  (:import-from #:cl-fix-initiator
                initiator)
  (:import-from #:cl-fix-acceptor
                acceptor)
  (:import-from #:cl-fix-session
                session
                init-session
                stop-session
                deftickhandler
                enqueue-msg
                sender-comp-id
                sender-sub-id
                target-comp-id
                target-sub-id
                *current-time*
                validate-logon
                session-status)
  (:import-from #:cl-fix-validation
                validation-error-err
                validation-error-path))


