;;; Copyright 2020 Sandipan Razzaque
;;;
;;; Licensed under the Apache License, Version 2.0 (the "License");
;;; you may not use this file except in compliance with the License.
;;; You may obtain a copy of the License at
;;;
;;;     http://www.apache.org/licenses/LICENSE-2.0
;;;
;;; Unless required by applicable law or agreed to in writing, software
;;; distributed under the License is distributed on an "AS IS" BASIS,
;;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;;; See the License for the specific language governing permissions and
;;; limitations under the License.

(in-package #:cl-fix-initiator)

(defclass initiator (conn:connector fsm:fsm)
  ((on-connect-fn :initarg :on-connect-fn
                  :initform (lambda (conn) (declare (ignore conn))))
   (stream :initarg :stream
           :initform nil
           :documentation "Stops auto-connect to :host/:port and uses this stream for I/O.")
   (inbuf :initform (make-array 1024 :fill-pointer 0 :element-type 'character))
   (outbuf :initform (make-array 1024 :fill-pointer 0 :element-type 'character))
   (single-threaded-mode :initform nil :initarg :single-threaded-mode)
   (io-cleanup-fn :initform nil)
   (io-thread :initform nil)
   (worker-thread :initform nil)
   (io-state :initform nil)
   (worker-state :initform nil))
  (:default-initargs
   :fsm-name "initiator"
   :fsm-starting-state :init
   :fsm-transitions
   (list
    (fsm:make-transition :from-state :init     :event :start-requested :to-state :starting :fn #'%start-initiator)
    (fsm:make-transition :from-state :starting :event :worker-ready    :to-state :starting :fn #'%worker-ready)
    (fsm:make-transition :from-state :starting :event :io-ready        :to-state :starting :fn #'%io-ready)
    (fsm:make-transition :from-state :starting :event :all-ready       :to-state :running)
    (fsm:make-transition :from-state :starting :event :stop-requested  :to-state :stopping)
    (fsm:make-transition :from-state :running  :event :stop-requested  :to-state :stopping)
    (fsm:make-transition :from-state :stopping :event :io-stopped      :to-state :stopping :fn #'%io-stopped)
    (fsm:make-transition :from-state :stopping :event :worker-stopped  :to-state :stopping :fn #'%worker-stopped)
    (fsm:make-transition :from-state :stopping :event :force-stop      :to-state :stopping  :fn #'%force-stop)
    (fsm:make-transition :from-state :stopping :event :all-stopped     :to-state :stopped  :fn #'%reset-initiator-state)
    (fsm:make-transition :from-state :stopped  :event :start-requested :to-state :starting :fn #'%start-initiator))))

(defparameter *running-states* '(:starting :running))

(defmethod conn:start-connector ((conn initiator))
  (fsm:fsm-process-event conn :start-requested))

(defun %start-initiator (conn)
  (with-slots (io-thread worker-thread single-threaded-mode) conn
    (cond
      (single-threaded-mode
       (log:info "Starting initiator in single-threaded mode")
       (%initiate-io conn)
       (setf io-thread nil
             worker-thread nil)
       (fsm:fsm-process-event conn :all-ready))
      (t
       (setf io-thread (bt:make-thread
                        (lambda ()
                          (log:info "Initializing IO thread.")
                          (%start-io conn))
                        :name "io-thread")
             worker-thread (bt:make-thread
                            (lambda ()
                              (log:info "Initializing worker thread.")
                              (%start-worker conn))
                            :name "worker-thread"))))))

(defun %check-ready (conn)
  (with-slots (io-state worker-state) conn
    (when (and (eq :ready io-state)
               (eq :ready worker-state))
      (fsm:fsm-process-event conn :all-ready))))

(defun %worker-ready (conn)
  (with-slots (worker-state) conn
    (setf worker-state :ready)
    (%check-ready conn)))

(defun %io-ready (conn)
  (with-slots (io-state) conn
    (setf io-state :ready)
    (%check-ready conn)))

(defmethod conn:stop-connector ((conn initiator))
  (log:info "Initiator shutdown requested.")
  (fsm:fsm-process-event conn :stop-requested)
  (%await-stop conn))

(defun destroy-thread (thread)
  (when (and thread (bt:thread-alive-p thread))
    (log:info "Force killing ~A thread" (bt:thread-name thread))
    (handler-case (bt:destroy-thread thread)
      (error (e)
        (log:warn "Could not destroy thread ~A: ~A" (bt:thread-name thread) e)))))

(defun %check-stopped (conn)
  (with-slots (io-state worker-state) conn
    (when (and (eq :stopped io-state)
               (eq :stopped worker-state))
      (fsm:fsm-process-event conn :all-stopped))))

(defun %worker-stopped (conn)
  (with-slots (worker-state) conn
    (setf worker-state :stopped)
    (%check-stopped conn)))

(defun %io-stopped (conn)
  (with-slots (io-state) conn
    (setf io-state :stopped)
    (%check-stopped conn)))

(defun %await-stop (conn)
  (when (slot-value conn 'single-threaded-mode)
    (fsm:fsm-process-event conn :all-stopped))
  (loop
     repeat 1000
     while (not (eq (fsm:fsm-current-state conn) :stopped))
     do (sleep 0.001))
  (when (not (eq (fsm:fsm-current-state conn) :stopped))
    (fsm:fsm-process-event conn :force-stop)))

(defun %force-stop (conn)
  (log:warn "Forcing a shutdown.")
  (with-slots (io-thread worker-thread) conn
    (util:destroy-thread io-thread)
    (util:destroy-thread worker-thread)
    (fsm:fsm-process-event conn :all-stopped)))

(defun %reset-initiator-state (conn)
  (with-slots (io-cleanup-fn
               conn:fb-in
               conn:fb-out
               io-thread
               worker-thread
               io-state
               worker-state)
      conn
    (when io-cleanup-fn
      (funcall io-cleanup-fn))
    (fb:fb-reset conn:fb-in conn:fb-out)
    (setf io-thread nil
          worker-thread nil
          io-cleanup-fn nil
          io-state nil
          worker-state nil)))

(defun %run-loop (conn)
  (%run-single-io-loop conn)
  (%run-single-worker-loop conn)
  (%run-single-io-loop conn))

(defun %run-single-io-loop (conn)
  (declare (type initiator conn))
  (with-slots (conn:fb-out
               conn:fb-in
               conn:serialize-fn
               conn:reader-fn
               inbuf
               outbuf
               stream)
      conn
    (fb:fb-process conn:fb-out
                   #'%process-outbound-event
                   outbuf
                   conn:serialize-fn
                   stream)
    (%read-msgs-from-socket stream
                            inbuf
                            conn:reader-fn
                            conn:fb-in)
    (%check-liveness conn)
    (conn:publish-tick conn)))

(defun %shutdown-initiator (err)
  (log:error "[~A] Shutting down initiator due to error: ~A"
             (bt:thread-name (bt:current-thread))
             err)
  (invoke-restart 'shutdown-initiator))

(defun %handle-backpressure (err)
  (when (find-restart 'wait-then-retry)
    (invoke-restart 'wait-then-retry 0.1 5))
  (%shutdown-initiator err))

(defun %start-io (conn)
  (declare (type initiator conn))
  (with-slots (state conn:fb-in stream io-cleanup-fn) conn
    (handler-bind ((usocket:socket-error #'%shutdown-initiator)
                   (backpressure-error #'%handle-backpressure)
                   (retries-exhausted #'%shutdown-initiator)
                   (end-of-file #'%shutdown-initiator)
                   (stream-error #'%shutdown-initiator))
      (unwind-protect
           (restart-case
               (progn
                 (%initiate-io conn)
                 (fsm:fsm-process-event conn :io-ready)
                 (loop while (member (fsm:fsm-current-state conn) *running-states*) do
                      (%run-single-io-loop conn)
                      (sleep 0.000000001))
                 (log:debug "IO thread completing.")
                 (fsm:fsm-process-event conn :io-stopped))
             (shutdown-initiator ()
               (fsm:fsm-process-event conn :stop-requested)
               (fsm:fsm-process-event conn :io-stopped)))
        (progn
          (when io-cleanup-fn (funcall io-cleanup-fn)))))))

(defun %read-msgs-from-socket (stream inbuf reader-fn conn:fb-in)
  (let ((msg-batch (loop
                      while (listen stream)
                      for x = (multiple-value-bind (obj start-idx) (funcall reader-fn stream inbuf)
                                (when obj
                                  (let ((msg-str (subseq inbuf start-idx)))
                                    (log:debug "IN < ~A" msg-str)
                                    (list obj msg-str))))
                      when x
                      collect x))
        (num-retry 0))
    (tagbody
     loop-start
       (restart-case
           (loop
              repeat 1000
              while msg-batch do
                (multiple-value-bind (lower-idx upper-idx reserve-ok)
                    (fb:fb-reserve conn:fb-in (length msg-batch))
                  (when reserve-ok
                    (loop
                       for i from lower-idx below upper-idx
                       for x = (fb:fb-elt conn:fb-in i)
                       do
                         (destructuring-bind (msg msg-str) (pop msg-batch)
                           (setf (conn:event-type x) :message
                                 (conn:event-data x) msg
                                 (conn:event-raw-msg x) msg-str
                                 (conn:event-timestamp x) (local-time:now))))
                    (fb:fb-publish conn:fb-in upper-idx)))
              finally
                (when msg-batch
                  (log:warn "Backpressure from worker thread to IO thread.")
                  (error 'backpressure-error
                         :msg "Failed to publish a msg-batch to worker thread.")))
         (wait-then-retry (sleep-time max-retries)
           (when (> num-retry max-retries)
             (log:error "Retries exhausted on backpressure (max retries: ~A)" max-retries)
             (error 'retries-exhausted :msg "Backpressure retries exhausted."))
           (incf num-retry)
           (log:info "Waiting then retrying - sleeping for: ~A secs" sleep-time)
           (sleep sleep-time)
           (go loop-start)))
     loop-exit)))

(defun %check-liveness (conn)
  (with-slots (conn:sessions) conn
    (let ((has-live-session (loop for x being the hash-values in conn:sessions
                               if (not (cl-fix-session:stoppedp x))
                               return t)))
      (when (not has-live-session)
        (log:info "No live sessions - shutting down initiator.")
        (fsm:fsm-process-event conn :stop-requested)))))

(defun %process-inbound-event (event conn)
  "Worker thread uses this fn to process inbound events from the connector. This includes inbound messages."
  (declare (type conn:connector-event event)
           (type initiator conn))
  (with-slots (conn:sessions) conn
    (let ((type (conn:event-type event))
          (data (conn:event-data event))
          (cl-fix-session:*current-time* (conn:event-timestamp event))
          (sessions-to-persist nil))
      (ecase type
        (:init-session
         (let ((session (conn:event-source-session event)))
           (when session
             (cl-fix-session:init-session session data)
             (cl-fix-session:on-connect session)
             (push session sessions-to-persist))))
        (:message
         (let ((session (conn:find-session conn data))
               (raw-msg (conn:event-raw-msg event)))
           (when session
             (conn:enqueue-msg conn
                               :event-type :persist-session-inbound-msg
                               :source-session session
                               :data raw-msg)
             (funcall (cl-fix-session:session-handler session) session data)
             (push session sessions-to-persist))))
        (:tick
         (loop for x being the hash-values in conn:sessions
            when (funcall (cl-fix-session:session-tick x) x)
            do (push x sessions-to-persist))))
      (loop for s in sessions-to-persist
         do
           (conn:enqueue-msg conn
                             :event-type :persist-session
                             :source-session s)))))

(defun %run-single-worker-loop (conn)
  (declare (type initiator conn))
  (with-slots (conn:fb-in) conn
    (fb:fb-process conn:fb-in #'%process-inbound-event conn)))

(defun %start-worker (conn)
  (handler-bind ((backpressure-error #'%handle-backpressure)
                 (retries-exhausted #'%shutdown-initiator)
                 (error #'%shutdown-initiator))
    (with-slots (state) conn
      (restart-case
          (progn
            (fsm:fsm-process-event conn :worker-ready)
            (loop while (member (fsm:fsm-current-state conn) *running-states*) do
                 (%run-single-worker-loop conn)
                 (sleep 0.000000001))
            (log:debug "Worker thread completing.")
            (fsm:fsm-process-event conn :worker-stopped))
        (shutdown-initiator ()
          (fsm:fsm-process-event conn :stop-requested)
          (fsm:fsm-process-event conn :worker-stopped))))))

(defun %initiate-io (conn)
  (with-slots (conn:host conn:port stream conn:fb-in io-cleanup-fn) conn
    (when (not stream)
      (let* ((socket (usocket:socket-connect conn:host conn:port)))
        (log:info "Connected to ~A:~A" conn:host conn:port)
        (setf stream (usocket:socket-stream socket)
              io-cleanup-fn (lambda ()
                              (handler-case (usocket:socket-close socket)
                                (error (e)
                                  (log:warn "Could not gracefully close socket - it may already be closed. Error: ~A" e)))
                              (setf stream nil)))))
    (%init-sessions conn)))

(defun %init-sessions (conn)
  "For each session: (1) init-stores, and (2) read-state-from-store."
  (with-slots (conn:sessions conn:fb-in) conn
    (let ((sessions* (loop for s being the hash-values in conn:sessions collect s))
          (states ()))
      (dolist (s sessions*)
        (let* ((state (cl-fix-session:read-state-from-store s)))
          (push state states)
          (cl-fix-session:init-stores s state)))
      (setf states (reverse states))
      (loop
         with publish-success = nil
         repeat 1000
         until publish-success
         do
           (multiple-value-bind (low high ok) (fb:fb-reserve conn:fb-in (length sessions*))
             (when ok
               (loop for i from low below high
                  do
                    (let ((event (fb:fb-elt conn:fb-in i)))
                      (setf (conn:event-type event) :init-session
                            (conn:event-source-session event) (elt sessions* i)
                            (conn:event-data event) (elt states i)
                            (conn:event-timestamp event) (local-time:now))))
               (fb:fb-publish conn:fb-in high)
               (setf publish-success t)))
         finally
           (when (not publish-success)
             (error 'backpressure-error
                    :msg "Failed to publish init-session event to the worker thread."))))))

(defun %process-outbound-event (event outbuf serialize-fn stream)
  "IO thread uses this fn to process outbound events from the worker thread."
  (declare (type conn:connector-event event))
  (ecase (conn:event-type event)
    (:persist-session
     (let ((session (conn:event-source-session event)))
       (log:debug "Persisting: ~A" session)
       (cl-fix-session:persist session)))
    (:message
     (let ((msg (conn:event-data event)))
       (conn:serialize-and-send serialize-fn outbuf msg stream)
       (let ((session (conn:event-source-session event)))
         (cl-fix-session:persist-outbound-msg session
                                              (util:fix-field msg :msg-seq-num)
                                              outbuf))
       (force-output stream)))
    (:perform-resend
     (let ((session (conn:event-source-session event))
           (bounds (conn:event-data event)))
       (destructuring-bind (start . end) bounds
         (cl-fix-session:perform-resend session
                                        start
                                        end
                                        (lambda (msg)
                                          (conn:serialize-and-send serialize-fn outbuf msg stream)))
         (force-output stream))))
    (:persist-session-inbound-msg
     (let ((msg (conn:event-data event))
           (session (conn:event-source-session event)))
       (when session
         (cl-fix-session:persist-inbound-msg session msg))))
    (:close-session-stores
     (let ((session (conn:event-source-session event)))
       (when session
         (log:debug "Closing stores for session: ~A" session)
         (cl-fix-session:close-stores session))))))

(defmethod conn:connector-state ((connector initiator))
  (cl-fix-fsm:fsm-current-state connector))
