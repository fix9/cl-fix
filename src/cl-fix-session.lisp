;;; Copyright 2020 Sandipan Razzaque
;;;
;;; Licensed under the Apache License, Version 2.0 (the "License");
;;; you may not use this file except in compliance with the License.
;;; You may obtain a copy of the License at
;;;
;;;     http://www.apache.org/licenses/LICENSE-2.0
;;;
;;; Unless required by applicable law or agreed to in writing, software
;;; distributed under the License is distributed on an "AS IS" BASIS,
;;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;;; See the License for the specific language governing permissions and
;;; limitations under the License.

(in-package #:cl-fix-session)

(defgeneric validate-logon (session logon-msg)
  (:documentation
   "Override for custom LOGON validation. Should return NIL for no error, or the error message to send on a LOGOUT."))
(defgeneric reset-outgoing-seq-num (session new-seq-num))
(defgeneric reset-incoming-seq-num (session new-seq-num))
(defgeneric init-session (session &optional file-state))
(defgeneric stop-session (session &optional send-logout logout-text)
  (:documentation
   "Stop the session. SEND-LOGOUT (default is t) will cause a LOGOUT to be sent to the client.
LOGOUT-TEXT will populate the text field on that LOGOUT message (defaults to 'System-initiated logout.')"))
(defgeneric on-connect (session &optional logon-msg))
(defgeneric init-stores (session disk-state))
(defgeneric close-stores (session))
(defgeneric read-state-from-store (session))

(defparameter *next-session-id* 0)
(defparameter *current-time* nil
  "The current time in the FIX engine.")
(defparameter *next-test-req-id* 0)

(defclass session ()
  ((exp-incoming-seq-num :initarg :exp-incoming-seq-num
                         :initform 1)
   (next-outgoing-seq-num :initarg :exp-outgoing-seq-num
                          :initform 1)
   (sender-comp-id :initarg :sender-comp-id :accessor sender-comp-id
                   :initform (error "Must supply :sender-comp-id"))
   (sender-sub-id :initarg :sender-sub-id :accessor sender-sub-id :initform nil)
   (target-comp-id :initarg :target-comp-id :accessor target-comp-id
                   :initform (error "Must supply :target-comp-id"))
   (target-sub-id :initarg :target-sub-id :accessor target-sub-id :initform nil)
   (logon-fields :initarg :logon-fields :initform nil :documentation "plist of extra fields to stamp on logon message.")
   (session-store :initarg :session-store :initform nil)
   (session-store-streams :initform nil)
   (gap-fill-msgs :initform '(:logon :heartbeat) :initarg :gap-fill-msgs)
   (session-id :initform (incf *next-session-id*) :reader session-id)
   (dict-key :initarg :dict-key :initform (error ":dict-key must be supplied."))
   (heartbeat-interval-ms :initarg :heartbeat-interval-ms :initform 30000)
   (handler :initarg :handler :accessor session-handler :initform (error ":handler must be supplied."))
   (reset-seq-num :initform nil :initarg :reset-seq-num)
   (tick :initarg :tick-handler :accessor session-tick :initform #'%default-tick-fn)
   (dict :initform ())
   (conn :accessor conn)
   (last-sent-msg-timestamp :initform nil)
   (last-received-msg-timestamp :initform nil)
   (test-request-sent-timestamp :initform nil)
   (test-request-sent-id :initform nil)
   (logon-sent-timestamp :initform nil)
   (logout-sent-timestamp :initform nil)
   (grace-period-ms :initarg :grace-period-ms :initform 5000)
   (session-status :initform :init :type (member :init :awaiting-logon :started :awaiting-logout-response :stopped :gap-fill) :reader session-status)
   (queued-msgs :initform nil :documentation "Messages with high seq-nums that are waiting to be processed.")
   (gap-fill-target :initform -1 :type fixnum)))

(define-condition session-error (error) ())

(define-condition send-on-invalid-session-status (session-error)
  ((session :initarg :session)
   (status :initarg :status)
   (msg :initarg :msg))
  (:report (lambda (condition stream)
             (with-slots (session status msg) condition
               (format stream "Attempt to send a message ~S on session ~A whilst it was in status ~S"
                       msg
                       session
                       status)))))

(defmethod initialize-instance :after ((session session) &rest initargs &key &allow-other-keys)
  (declare (ignore initargs))
  (with-slots (dict-key dict handler) session
    (setf dict (resolve-dict dict-key)
          handler (%wrap-handler handler))))

(defmethod init-session ((session session) &optional file-state)
  (let ((*current-time* (or *current-time* (local-time:now))))
    (with-slots (session-status
                 session-store
                 reset-seq-num
                 last-sent-msg-timestamp
                 last-received-msg-timestamp
                 test-request-sent-id
                 test-request-sent-timestamp
                 exp-incoming-seq-num
                 next-outgoing-seq-num)
        session
      (when file-state
        (destructuring-bind (in-seq out-seq sender-comp target-comp first-msg-idx) file-state
          (declare (ignore sender-comp target-comp first-msg-idx))
          (setf exp-incoming-seq-num in-seq
                next-outgoing-seq-num out-seq)))
      (when reset-seq-num
        (%reset-seq-nums session))
      (setf last-sent-msg-timestamp nil
            last-received-msg-timestamp nil
            test-request-sent-id nil
            test-request-sent-timestamp nil
            session-status :init))))

(defmethod validate-logon ((session session) logon-msg)
  (declare (ignore session logon-msg))
  nil)

(defun %handle-initial-logon (session logon-msg)
  (with-slots (dict exp-incoming-seq-num) session
    (log:info "Received a logon message from: ~A" (util:fix-field logon-msg :sender-comp-id))
    (let ((msg-seq-num (util:fix-field logon-msg :msg-seq-num))
          (sender-comp-id (util:fix-field logon-msg :sender-comp-id))
          (requested-heartbeat (util:fix-field logon-msg :heart-bt-int))
          (reset-seq-num (util:fix-field logon-msg :reset-seq-num-flag)))
      (with-slots (heartbeat-interval-ms) session
        (when requested-heartbeat
          (log:info "Client ~A has connected - setting heartbeat to ~A seconds." sender-comp-id requested-heartbeat)
          (setf heartbeat-interval-ms (* 1000 requested-heartbeat)))
        (cond
          (reset-seq-num
           (log:info "Client ~A has requested a SeqNum reset. Obliging." sender-comp-id)
           (%reset-seq-nums session 2))
          ((not (%check-seq-num session logon-msg))
           (log:error "Client ~A sent a LOGON with an invalid Seq Num. Dropping client." sender-comp-id)
           (stop-session session t (format nil "Invalid SeqNum on LOGON. Expected ~A. Got ~A." exp-incoming-seq-num msg-seq-num)))
          (t
           (%reset-seq-nums session (1+ msg-seq-num))))))))

(defun %reset-seq-nums (session &optional (in 1) (out 1))
  (with-slots (exp-incoming-seq-num next-outgoing-seq-num) session
    (setf exp-incoming-seq-num in
          next-outgoing-seq-num out)))

(defmethod on-connect ((session session) &optional logon-msg)
  (cond
    (logon-msg
     ;; Acceptor flow - we've receive a (validated) logon msg.
     (%handle-initial-logon session logon-msg)
     (%send-logon session)
     (setf (slot-value session 'session-status) :started))
    (t
     ;; Initiator flow - we send the logon.
     (%send-logon session)
     (setf (slot-value session 'session-status) :awaiting-logon))))

(defun %request-close-stores (session)
  (with-slots (conn) session
    (cl-fix-connector:enqueue-msg conn
                                  :event-type :close-session-stores
                                  :data nil
                                  :source-session session)))

(defmethod stop-session ((session session) &optional (send-logout t) (logout-text "System-initiated logout."))
  (let ((*current-time* (or *current-time* (local-time:now))))
    (with-slots (session-status) session
      (cond
        (send-logout
         (%send-logout session logout-text)
         (with-slots (logout-sent-timestamp) session
           (setf session-status :awaiting-logout-response
                 logout-sent-timestamp *current-time*)))
        (t
         (setf session-status :stopped)))))
  (%request-close-stores session)
  (log:info "Stopped session ~A" session))

(defun %make-outbound-msg-store (session disk-state)
  (with-slots (session-store) session
    (let* ((stream (open (%determine-session-filename session-store session "out")
                         :direction :io
                         :element-type 'character
                         :if-exists :append
                         :if-does-not-exist :create))
           (store (store:make-file-store
                   :stream stream
                   :first-msg-position (if disk-state (elt disk-state 4) 0))))
      (store:rebuild-index store (slot-value session 'dict))
      store)))

(defmethod init-stores ((session session) disk-state)
  (close-stores session)
  (with-slots (session-store session-store-streams) session
    (when (and session-store (not session-store-streams))
      (setf session-store-streams
            (list :state (open (%determine-session-filename session-store session)
                               :direction :output
                               :element-type 'character
                               :if-exists :supersede
                               :if-does-not-exist :create)
                  :msgs-in (open (%determine-session-filename session-store session "in")
                                 :direction :output
                                 :element-type 'character
                                 :if-exists :append
                                 :if-does-not-exist :create)
                  :msgs-out (%make-outbound-msg-store session disk-state)))
      (persist session))))

(defmethod close-stores (session)
  (with-slots (session-store-streams) session
    (when session-store-streams
      (dolist (store (remove-if #'keywordp session-store-streams))
        (typecase store
          (stream (close store))
          (store:file-store (store:close-store store))))
      (setf session-store-streams nil))))

(defmethod read-state-from-store ((session session))
  (log:info "Reading state from disk for session ~A" session)
  (with-slots (session-store) session
    (when session-store
      (let ((filename (%determine-session-filename session-store session)))
        (handler-case
            (with-open-file (stream filename
                                    :direction :input
                                    :element-type 'character
                                    :if-does-not-exist :error)
              (let ((vals (loop repeat 5 collect (read stream))))
                (destructuring-bind (exp-incoming-seq-num next-outgoing-seq-num sender-comp-id target-comp-id first-msg-file-pos) vals
                  (list exp-incoming-seq-num next-outgoing-seq-num sender-comp-id target-comp-id first-msg-file-pos))))
          (end-of-file (e)
            (log:error "State file ~A was corrupted when we attempted to read it. RENAME OR DELETE THIS FILE" filename)
            (error e))
          (file-error ()
            (log:warn "State file doesn't exist yet for session ~A. It will be created upon next persist." session)))))))

(defun persist (session)
  (declare (type session session))
  (with-slots (sender-comp-id target-comp-id session-store-streams) session
    (when session-store-streams
      (let ((stream (getf session-store-streams :state)))
        (file-position stream 0)
        (write (slot-value session 'exp-incoming-seq-num) :stream stream)
        (write-char #\Space stream)
        (write (slot-value session 'next-outgoing-seq-num) :stream stream)
        (write-char #\Space stream)
        (write (slot-value session 'sender-comp-id) :stream stream)
        (write-char #\Space stream)
        (write (slot-value session 'target-comp-id) :stream stream)
        (write-char #\Space stream)
        (write (store:file-store-first-msg-position (getf session-store-streams :msgs-out))
               :stream stream)
        (write-char #\Space stream)
        (force-output stream)))))

(defun %determine-session-filename (location session &optional (extension "dat"))
  (let ((dir (uiop:directory-exists-p location)))
    (when (not dir) (error "Directory does not exist: ~A" location))
    (with-slots (sender-comp-id sender-sub-id target-comp-id target-sub-id) session
      (concatenate 'string
                   (directory-namestring dir)
                   (if sender-sub-id
                       (format nil "~A-~A" sender-comp-id sender-sub-id)
                       sender-comp-id)
                   "_"
                   (if target-sub-id
                       (format nil "~A-~A" target-comp-id target-sub-id)
                       target-comp-id)
                   "."
                   extension))))

(defmethod print-object ((object session) stream)
  (print-unreadable-object (object stream :type t :identity t)
    (format stream "(sender-comp-id: \"~A\", target-comp-id: \"~A\")"
            (slot-value object 'sender-comp-id)
            (slot-value object 'target-comp-id))))

(declaim (inline %pre-send))
(defun %pre-send (session msg)
  (with-slots (dict next-outgoing-seq-num sender-comp-id target-comp-id sender-sub-id target-sub-id) session
    (setf (util:fix-field msg :msg-seq-num) (prog1
                                                next-outgoing-seq-num
                                              (incf next-outgoing-seq-num))
          (util:fix-field msg :sender-comp-id) sender-comp-id
          (util:fix-field msg :target-comp-id) target-comp-id
          (util:fix-field msg :sending-time) *current-time*)
    (when sender-sub-id (setf (util:fix-field msg :sender-sub-id) sender-sub-id))
    (when target-sub-id (setf (util:fix-field msg :target-sub-id) target-sub-id))
    msg))

(defun enqueue-msg (session msg)
  (with-slots (conn last-sent-msg-timestamp session-status) session
    (cond
      ((or (and (eq :gap-fill session-status)
                (not (%is-admin msg)))
           (eq :stopped session-status))
       (error 'send-on-invalid-session-status :msg msg :status session-status :session session))
      (t
       (setf msg (%pre-send session msg))
       (cl-fix-connector:enqueue-msg conn
                                     :event-type :message
                                     :data msg
                                     :source-session session)
       (setf last-sent-msg-timestamp *current-time*)))))

(defmethod reset-outgoing-seq-num ((session session) new-seq-num)
  (setf (slot-value session 'next-outgoing-seq-num) new-seq-num))

(defmethod reset-incoming-seq-num ((session session) new-seq-num)
  (setf (slot-value session 'exp-incoming-seq-num) new-seq-num))

(defun persist-inbound-msg (session msg-str)
  (declare (type session session))
  (with-slots (session-store-streams) session
    (when session-store-streams
      (let ((f (getf session-store-streams :msgs-in)))
        (write-sequence msg-str f)
        (write-char #\Newline f)
        (force-output f)))))

(defun persist-outbound-msg (session idx msg-str)
  (with-slots (session-store-streams) session
    (when session-store-streams
      (let ((store (getf session-store-streams :msgs-out)))
        (store:store-msg store idx msg-str)))))

(defun %session-handle-msg (session msg user-fn)
  (with-slots (session-status last-received-msg-timestamp exp-incoming-seq-num logon-sent-timestamp) session
    (setf last-received-msg-timestamp *current-time*)
    (let ((seq-num-status (%check-seq-num session msg)))
      (ecase seq-num-status
        (:too-low
         (log:error "Received message with seq-num too low. Dropping message. Expected ~A. Message: ~A" exp-incoming-seq-num msg))
        (:too-high
         (%handle-seq-num-too-high session msg))
        ((:ok :sequence-reset)
         (incf exp-incoming-seq-num)
         (%session-process-msg session msg user-fn)
         (%process-queued-messages session user-fn))))))

(defun %session-process-msg (session msg user-fn)
  (let ((msg-type (util:fix-msg-type msg)))
    (with-slots (session-status logon-sent-timestamp) session
      (ecase session-status
        ((:init :stopped)
         (log:error "Received an out-of-session ~A message. DROPPING MESSAGE." msg-type))
        (:awaiting-logon
         (case msg-type
           (:logon
            (log:info "Received LOGON from remote end - session established for ~A" session)
            (setf session-status :started
                  logon-sent-timestamp nil)
            (funcall user-fn session msg))
           (:logout
            (log:info "Received LOGOUT when attempting to logon. for session ~A: ~A" session (util:fix-field msg :text))
            (%send-logout session "Responding to remote LOGOUT. Terminating session.")
            (setf session-status :stopped))
           (t
            (log:error "Received unexpected ~A message whilst awaiting LOGON from remote end. DROPPING MESSAGE." msg-type))))
        (:awaiting-logout-response
         (case msg-type
           (:logout
            (log:info "Received response to LOGOUT on session ~A: ~A" session (util:fix-field msg :text))
            (setf session-status :stopped))
           (t
            (log:warn "Received message ~A whilst awaiting LOGOUT response. DROPPING MESSAGE." msg-type))))
        ((:gap-fill :started)
         (if (%is-admin msg)
             (%process-admin-msg session msg)
             (funcall user-fn session msg)))))))

(defun %process-queued-messages (session user-fn)
  (with-slots (queued-msgs gap-fill-target session-status exp-incoming-seq-num) session
    (loop for msg in queued-msgs
          while (= exp-incoming-seq-num (util:fix-field msg :msg-seq-num))
          do
             (pop queued-msgs)
             (incf exp-incoming-seq-num)
             (%check-gap-fill-state session)
             (cond
               ((%is-gap-fill-msg msg)
                (%handle-gap-fill-msg session msg))
               ((%is-admin msg)
                (log:debug "Skipping admin message ~A during message processing of queued messages." msg))
               (t
                (%session-process-msg session msg user-fn)))))
  (%check-gap-fill-state session))

(defun %check-gap-fill-state (session)
  (with-slots (gap-fill-target exp-incoming-seq-num session-status) session
    (when (>= gap-fill-target exp-incoming-seq-num)
      (setf gap-fill-target -1
            session-status :started))))

(defun %is-standard-sequence-reset (msg)
  (and (eq (util:fix-msg-type msg) :sequence-reset)
       (not (util:fix-field msg :gap-fill-flag))))

(defun %is-gap-fill-msg (msg)
  (and (eq (util:fix-msg-type msg) :sequence-reset)
       (util:fix-field msg :gap-fill-flag)))

(defun %handle-seq-num-too-high (session msg)
  (with-slots (queued-msgs gap-fill-target session-status exp-incoming-seq-num) session
    (when (%is-admin msg)
      (%process-admin-msg session msg))
    (unless (%is-standard-sequence-reset msg)
      (log:debug "Received message with a too-high seq-num. Caching for future processing: ~A" msg)
      (push msg queued-msgs)
      (setf queued-msgs (sort queued-msgs #'< :key (lambda (x) (util:fix-field x :msg-seq-num))))
      (when (not (eq :gap-fill session-status))
        (let ((seq-num (util:fix-field msg :msg-seq-num)))
          (enqueue-msg session
                       (util:make-fix-msg :resend-request
                                          :begin-seq-no exp-incoming-seq-num
                                          :end-seq-no 0))
          (setf session-status :gap-fill
                gap-fill-target seq-num))))))

(defun %is-admin (msg)
  (let ((msg-type (util:fix-msg-type msg)))
    (case msg-type
      ((:heartbeat :test-request :resend-request :sequence-reset :logout) t)
      (t nil))))

(defun %process-admin-msg (session msg)
  (with-slots (session-status) session
    (let ((msg-type (util:fix-msg-type msg)))
      (ecase msg-type
        (:heartbeat (%handle-heartbeat session msg))
        (:test-request (%handle-test-request session msg))
        (:resend-request (%handle-resend-request session msg))
        (:sequence-reset (%handle-sequence-reset session msg))
        (:logout
         (log:info "Received LOGOUT on session ~A: ~A" session (util:fix-field msg :text))
         (%send-logout session "Responding to remote LOGOUT. Terminating session.")
         (setf session-status :stopped))))))

(defun %wrap-handler (user-fn)
  "Wraps the given user function with session-handling code."
  (lambda (session msg)
    (%session-handle-msg session msg user-fn)))

(defun %check-seq-num (session msg)
  (with-slots (exp-incoming-seq-num dict) session
    (let ((is-seq-reset-msg (equal "4" (util:fix-field msg :msg-type)))
          (msg-seq-num (util:fix-field msg :msg-seq-num)))
      (cond
        (is-seq-reset-msg :sequence-reset)
        ((= msg-seq-num exp-incoming-seq-num) :ok)
        ((< msg-seq-num exp-incoming-seq-num) :too-low)
        ((> msg-seq-num exp-incoming-seq-num) :too-high)))))

(defun %handle-sequence-reset (session msg)
  (with-slots (exp-incoming-seq-num dict session-status queued-msgs gap-fill-target) session
    (let ((new-seq-num (util:fix-field msg :new-seq-no)))
      (cond
        ((< new-seq-num exp-incoming-seq-num)
         (log:error "SequenceReset attempted to LOWER the value of exp-incoming-seq-num from ~A to ~A. DROPPING MESSAGE."
                    exp-incoming-seq-num
                    new-seq-num))
        ((%is-standard-sequence-reset msg)
         (log:info "Received sequence reset - reset exp-incoming-seq-num to ~A" new-seq-num)
         (setf exp-incoming-seq-num new-seq-num)
         (when (eq session-status :gap-fill)
           (log:warn "Remote end requested a sequence reset during gap-fill. Clearing all queued messages.")
           (setf session-status :started
                 queued-msgs nil
                 gap-fill-target -1)))
        ((and (%is-gap-fill-msg msg)
              (eq session-status :gap-fill))
         ;; Special logic here. Reason: we immediately process all other admin messages despite seq-num order.
         ;;                             But, this will break if the remote end sends gap-fill messages whilst we
         ;;                             are awaiting a resend. So, we ONLY process 'seq-num-messages-with-gap-fill-true' messages
         ;;                             if they have the correct/expected incoming seq num.
         (when (= exp-incoming-seq-num (util:fix-field msg :msg-seq-num))
           (%handle-gap-fill-msg session msg)))))))

(defun %handle-gap-fill-msg (session msg)
  (with-slots (exp-incoming-seq-num) session
    (let ((new-seq-num (util:fix-field msg :new-seq-no)))
      (log:debug "Processing gap-fill. Setting exp-incoming-seq-num from ~A to ~A" exp-incoming-seq-num new-seq-num)
      (setf exp-incoming-seq-num new-seq-num))))

(defun %handle-resend-request (session msg)
  (with-slots (conn dict next-outgoing-seq-num) session
    (let* ((begin-seq-no (util:fix-field msg :begin-seq-no))
           (end-seq-no (util:fix-field msg :end-seq-no))
           (end-seq-normalised (cond
                                 ((= end-seq-no 0) next-outgoing-seq-num)
                                 ((> end-seq-no 0) (min (1+ end-seq-no) next-outgoing-seq-num))
                                 (t end-seq-no))))
      (cond
        ((and (> end-seq-normalised 0)
              (> begin-seq-no 0)
              (< begin-seq-no end-seq-normalised))
         (log:info "Invoking resend for messages with sequence numbers from ~A to ~A" begin-seq-no end-seq-no)
         ;; Why hand this off to the other thread?
         ;; Resend requires disk and network I/O. Which we don't do on the worker thread.
         (conn:enqueue-msg conn
                           :event-type :perform-resend
                           :data (cons begin-seq-no end-seq-normalised)
                           :source-session session))
        (t
         (log:warn "Resend was requested for an invalid interval - begin-seq-no: ~A end-seq-no: ~A. DROPPING MESSAGE."
                   begin-seq-no
                   end-seq-no))))))

(defun perform-resend (session from-idx to-idx send-fn)
  (with-slots (conn dict gap-fill-msgs session-store-streams) session
    (with-slots (conn:reader-fn) conn
      (let ((store (getf session-store-streams :msgs-out)))
        (when store
          (store:iterate store from-idx to-idx
                         (lambda (msg-str)
                           (let ((msg (with-input-from-string (s msg-str)
                                        (funcall conn:reader-fn s))))
                             (when (member (util:fix-msg-type msg) gap-fill-msgs :test #'eq)
                               (setf msg (%create-gap-fill-msg session :orig-msg msg)))
                             (setf (util:fix-field msg :poss-dup-flag) t
                                   (util:fix-field msg :poss-resend) t
                                   (util:fix-field msg :orig-sending-time) (util:fix-field msg :sending-time)
                                   (util:fix-field msg :sending-time) (local-time:now))
                             (funcall send-fn msg)))
                         (lambda (gap-start next-seq)
                           (let ((msg (%create-gap-fill-msg session
                                                            :msg-seq-num gap-start
                                                            :new-seq-no next-seq)))
                             (funcall send-fn msg)))))
        (when (not store)
          (log:warn "User requested resend on a session without a store set up. DROPPING MESSAGE."))))))

(defun %create-gap-fill-msg (session &key orig-msg msg-seq-num new-seq-no)
  (with-slots (sender-comp-id target-comp-id sender-sub-id target-sub-id dict) session
    (let ((msg (util:make-fix-msg :sequence-reset
                                  :sender-comp-id sender-comp-id
                                  :target-comp-id target-comp-id
                                  :msg-seq-num (or msg-seq-num (util:fix-field orig-msg :msg-seq-num))
                                  :sending-time (if orig-msg (util:fix-field orig-msg :sending-time) (local-time:now))
                                  :new-seq-no (or new-seq-no (util:fix-field orig-msg :msg-seq-num))
                                  :gap-fill-flag t)))
      (when (not orig-msg)
        (when sender-sub-id
          (setf (util:fix-field msg :sender-sub-id) sender-sub-id))
        (when target-sub-id
          (setf (util:fix-field msg :target-sub-id) target-sub-id)))
      msg)))

(defun %handle-heartbeat (session msg)
  (with-slots (dict test-request-sent-timestamp test-request-sent-id) session
    (let ((test-req-id (util:fix-field msg :test-req-id)))
      (when (and test-req-id
                 (string= test-req-id (format nil "~A" test-request-sent-id)))
        (setf test-request-sent-id nil
              test-request-sent-timestamp nil)))))

(defun %handle-test-request (session msg)
  (with-slots (dict) session
    (enqueue-msg session
                 (%make-heartbeat session (util:fix-field msg :test-req-id)))))

(defun %default-tick-fn (session)
  (log:trace "on-tick: ~A" session)
  (%check-liveness session))

(defun %check-liveness (session)
  (with-slots (session-status
               heartbeat-interval-ms
               logon-sent-timestamp
               last-received-msg-timestamp
               last-sent-msg-timestamp
               grace-period-ms
               test-request-sent-timestamp
               test-request-sent-id
               logout-sent-timestamp)
      session
    (macrolet ((tsdiff (v)
                 `(if ,v (* 1000d0 (local-time:timestamp-difference *current-time* ,v)) -1)))
      (let ((elapsed-since-logon (tsdiff logon-sent-timestamp))
            (elapsed-since-last-recvd (tsdiff last-received-msg-timestamp))
            (elapsed-since-last-sent (tsdiff last-sent-msg-timestamp))
            (elapsed-since-test-req (tsdiff test-request-sent-timestamp))
            (elapsed-since-logout-sent (tsdiff logout-sent-timestamp)))
        (cond
          ((and (eq session-status :awaiting-logon)
                (> elapsed-since-logon grace-period-ms))
           (log:warn "No response to initial LOGON within ~A ms. TERMINATING SESSION" grace-period-ms)
           (%send-logout session "No response received from LOGON - logging out.")
           (setf session-status :stopped))
          ((and (eq session-status :awaiting-logout-response)
                (> elapsed-since-logout-sent grace-period-ms))
           (log:warn "No response to LOGOUT within ~A ms. TERMINATING SESSION" grace-period-ms)
           (setf session-status :stopped))
          ((and test-request-sent-id
                (not (eq session-status :awaiting-logout-response))
                (> elapsed-since-test-req grace-period-ms))
           (log:warn "No response to test request within ~A ms. TERMINATING SESSION." grace-period-ms)
           (%send-logout session "No response received from test-request - logging out.")
           (setf session-status :awaiting-logout-response
                 logout-sent-timestamp *current-time*))
          ((and (not test-request-sent-id)
                (> elapsed-since-last-recvd (+ heartbeat-interval-ms grace-period-ms)))
           (log:warn "~A: No messages received for ~A ms - sending test request." session (+ heartbeat-interval-ms grace-period-ms))
           (%send-test-request session))
          ((> elapsed-since-last-sent heartbeat-interval-ms)
           (enqueue-msg session (%make-heartbeat session))))))))

(defmacro deftickhandler (fn-name lambda-list &body body)
  "Define your own tick handler. Must return a truthy value to force the engine to do a persist after handling the tick (if required)."
  (when (not (= 1 (length lambda-list)))
    (error "Lambda list can only have 1 argument, representing the 'session' object."))
  (let ((session (car lambda-list)))
    `(defun ,fn-name ,lambda-list
       (log:trace "on-tick: ~A" ,session)
       (%check-liveness ,session)
       ,@body)))

(defun %send-test-request (session)
  (with-slots (dict test-request-sent-timestamp test-request-sent-id) session
    (let* ((next-id (incf *next-test-req-id*)))
      (enqueue-msg session
                   (util:make-fix-msg :test-request :test-req-id (format nil "~A" next-id)))
      (setf test-request-sent-timestamp *current-time*
            test-request-sent-id next-id))))

(defun %make-heartbeat (session &optional test-request-id)
  (with-slots (dict) session
    (let* ((msg (util:make-fix-msg :heartbeat)))
      (when test-request-id
        (setf (util:fix-field msg :test-req-id) test-request-id))
      msg)))

(defun %send-logon (session)
  (with-slots (heartbeat-interval-ms reset-seq-num dict logon-fields logon-sent-timestamp) session
    (let* ((msg (util:make-fix-msg :logon
                                   :encrypt-method :none ;; TODO: Support other encryption methods.
                                   :heart-int (/ heartbeat-interval-ms 1000))))
      (when reset-seq-num
        (setf (util:fix-field msg :reset-seq-num-flag) t))
      (when logon-fields
        (loop
           for i from 0 below (length logon-fields) by 2
           for fld = (elt logon-fields i)
           for val = (elt logon-fields (1+ i))
           do (setf (util:fix-field msg fld) val)))
      (enqueue-msg session msg)
      (setf logon-sent-timestamp *current-time*))))

(defun %send-logout (session reason)
  (with-slots (dict conn session-status) session
    (enqueue-msg session
                 (util:make-fix-msg :logout
                                    :text reason))))

(defun stoppedp (session)
  (eq :stopped (slot-value session 'session-status)))

(defmethod print-object ((session session) stream)
  (with-slots (sender-comp-id target-comp-id) session
    (print-unreadable-object (session
                              stream
                              :type t
                              :identity nil)
      (format stream "~A<->~A" sender-comp-id target-comp-id))))
