;;; Copyright 2020 Sandipan Razzaque
;;;
;;; Licensed under the Apache License, Version 2.0 (the "License");
;;; you may not use this file except in compliance with the License.
;;; You may obtain a copy of the License at
;;;
;;;     http://www.apache.org/licenses/LICENSE-2.0
;;;
;;; Unless required by applicable law or agreed to in writing, software
;;; distributed under the License is distributed on an "AS IS" BASIS,
;;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;;; See the License for the specific language governing permissions and
;;; limitations under the License.

(in-package #:cl-fix-acceptor)

(defclass acceptor (cl-fix-connector:connector fsm:fsm)
  ((on-new-client-fn)
   (listen-socket :initform nil)
   (listen-grace-period :initform 0.2)
   (client-sessions :initform (make-hash-table))
   (listener-thread)
   (listener-state :initform :stopped)
   (client-io-thread)
   (client-io-state :initform :stopped)
   (worker-thread)
   (worker-state :initform :stopped)
   (incoming-clients :initform (make-instance 'fb:fb
                                              :init-fn (lambda ()
                                                         (make-instance 'conn:connector-event))
                                              :size 128))
   (single-threaded-mode :initarg :single-threaded-mode
                         :initform nil
                         :documentation "Allows user to explicitly add client sockets, and run the IO/worker loop. Used for testing."))
  (:default-initargs
   :fsm-name "acceptor"
    :fsm-starting-state :init
    :fsm-transitions
    (list
     ;; States: :init -> :starting -> :running -> :stopping -> :stopped
     (fsm:make-transition :from-state :init     :event :start-requested   :to-state :starting :fn #'%start-acceptor)
     (fsm:make-transition :from-state :starting :event :listener-started  :to-state :starting :fn (%signal-start 'listener-state))
     (fsm:make-transition :from-state :starting :event :client-io-started :to-state :starting :fn (%signal-start 'client-io-state))
     (fsm:make-transition :from-state :starting :event :worker-started    :to-state :starting :fn (%signal-start 'worker-state))
     (fsm:make-transition :from-state :starting :event :stop-requested    :to-state :stopping :fn #'%check-stopped)
     (fsm:make-transition :from-state :starting :event :all-started       :to-state :running)
     (fsm:make-transition :from-state :running  :event :stop-requested    :to-state :stopping :fn #'%check-stopped)
     (fsm:make-transition :from-state :stopping :event :listener-stopped  :to-state :stopping :fn (%signal-stop 'listener-state))
     (fsm:make-transition :from-state :stopping :event :client-io-stopped :to-state :stopping :fn (%signal-stop 'client-io-state))
     (fsm:make-transition :from-state :stopping :event :worker-stopped    :to-state :stopping :fn (%signal-stop 'worker-state))
     (fsm:make-transition :from-state :stopping :event :force-stop        :to-state :stopping :fn #'%force-stop)
     (fsm:make-transition :from-state :stopping :event :all-stopped       :to-state :stopped  :fn #'%reset-state)
     (fsm:make-transition :from-state :stopped  :event :start-requested   :to-state :starting :fn #'%start-acceptor))))

(defun %signal-start (slot)
  (lambda (connector)
    (setf (slot-value connector slot) :started)
    (%check-running connector)))

(defun %signal-stop (slot)
  (lambda (connector)
    (setf (slot-value connector slot) :stopped)
    (%check-stopped connector)))

(defstruct client-session
  stream
  session
  (inbuf (make-array 1024 :fill-pointer 0 :element-type 'character))
  (outbuf (make-array 1024 :fill-pointer 0 :element-type 'character)))

(defmethod conn:start-connector ((conn acceptor))
  (fsm:fsm-process-event conn :start-requested))

(defmethod conn:stop-connector ((conn acceptor))
  (fsm:fsm-process-event conn :stop-requested)
  (%await-stop conn))

(defun %initiate-listener (connector)
  (with-slots (listen-socket conn:host conn:port single-threaded-mode) connector
    (unless single-threaded-mode
      (setf listen-socket (usocket:socket-listen conn:host conn:port)))))

(defun %shutdown-io (connector)
  (with-slots (client-sessions listen-socket) connector
    (when listen-socket (usocket:socket-close listen-socket))
    (loop for cs being the hash-values in client-sessions
       do
         (with-slots (session stream) cs
           (close stream)
           (remhash (cl-fix-session:session-id session) client-sessions)))))

(defun %reset-state (connector)
  (%shutdown-io connector)
  (with-slots (client-sessions
               listen-socket
               listener-thread
               listener-state
               client-io-thread
               client-io-state
               worker-thread
               worker-state)
      connector
    (setf client-sessions (make-hash-table)
          listen-socket nil
          listener-state :stopped
          client-io-state :stopped
          worker-state :stopped
          listener-thread nil
          client-io-thread nil
          worker-thread nil)))

(defun %check-states (connector desired-state)
  (with-slots (listener-state client-io-state worker-state) connector
    (and (eq listener-state desired-state)
         (eq client-io-state desired-state)
         (eq worker-state desired-state))))

(defun %check-running (connector)
  (when (%check-states connector :started)
    (fsm:fsm-process-event connector :all-started)))

(defun %check-stopped (connector)
  (when (or (slot-value connector 'single-threaded-mode)
            (%check-states connector :stopped))
    (fsm:fsm-process-event connector :all-stopped)))

(defun %make-client-buffer ()
  (make-array 1024 :fill-pointer 0 :element-type 'character))

(declaim (inline %read-msgs))
(defun %read-msgs (stream reader-fn inbuf max-msgs)
  (loop
     repeat max-msgs
     while (listen stream)
     for x = (multiple-value-bind (obj start-idx) (funcall reader-fn stream inbuf)
               (when obj
                 (let ((msg-str (subseq inbuf start-idx)))
                   (log:debug "IN < ~A" msg-str)
                   (list obj msg-str))))
     when x
     collect x))

(defun %stop-existing-sessions-if-any (connector session)
  (with-slots (conn:fb-in client-sessions) connector
    (let* ((id (cl-fix-session:session-id session))
           (cs (gethash id client-sessions)))
      (when cs
        (log:info "Session ~A already has a client connected on stream ~A. Disconnecting that stream."
                  session
                  (client-session-stream cs))
        (close (client-session-stream cs))
        (fb:fb-publish-one (conn:fb-in ev)
          (setf (conn:event-type ev) :force-stop-session
                (conn:event-source-session ev) session))
        (remhash id client-sessions)))))

(defun %make-logout-msg (session text)
  (with-slots (cl-fix:sender-comp-id
               cl-fix:sender-sub-id
               cl-fix:target-comp-id
               cl-fix:target-sub-id)
      session
    (let ((msg (list :msg-type :logout
                     :msg-seq-num 1
                     :sending-time (local-time:now)
                     :sender-comp-id cl-fix:sender-comp-id
                     :target-comp-id cl-fix:target-comp-id
                     :text text)))
      (when cl-fix:sender-sub-id
        (setf (util:fix-field msg :sender-sub-id) cl-fix:sender-sub-id))
      (when cl-fix:target-sub-id
        (setf (util:fix-field msg :target-sub-id) cl-fix:target-sub-id))
      msg)))

(defun %send-logout (error-msg stream session connector)
  (with-slots (conn:serialize-fn) connector
    (let* ((logout-msg (%make-logout-msg session error-msg))
           (logout-msg-str (with-output-to-string (s)
                             (funcall conn:serialize-fn logout-msg s))))
      (log:debug "OUT > ~A" logout-msg-str)
      (write-sequence logout-msg-str stream)
      (force-output stream))))

(defun %handle-new-client-connection (event connector)
  (let ((stream (conn:event-data event)))
    (log:debug "Handling incoming client connection from ~A" stream)
    (handler-case
        (with-slots (conn:reader-fn client-sessions conn:fb-in conn:dict) connector
          (let* ((cs (make-client-session :stream stream))
                 (msgs (%read-msgs stream conn:reader-fn (client-session-inbuf cs) 1)))
            (when (not msgs)
              (error "Client did not send any messages upon connecting. Expecting a LOGON. Got nothing."))
            (destructuring-bind (first-msg first-msg-raw) (pop msgs)
              (when first-msg
                (let ((msg-type (util:fix-msg-type first-msg)))
                  (when (not (eq msg-type :logon))
                    (error "For new client connection, expected a LOGON(A), but got ~A." msg-type)))
                (let ((session (conn:find-session connector first-msg)))
                  (when (not session)
                    (error "Received LOGON(A) for unknown session."))
                  (let ((error-msg (cl-fix-session:validate-logon session first-msg)))
                    (when error-msg
                      (%send-logout error-msg stream session connector)
                      (error "Invalid LOGON(A) message for session ~A (~A)" session error-msg)))
                  (%stop-existing-sessions-if-any connector session)
                  (setf (client-session-session cs) session
                        (gethash (cl-fix-session:session-id session) client-sessions) cs)
                  (let ((session-state (cl-fix-session:read-state-from-store session)))
                    (cl-fix-session:init-stores session session-state)
                    (fb:fb-publish-one (conn:fb-in ev)
                      (setf (conn:event-type ev) :init-session
                            (conn:event-source-session ev) session
                            (conn:event-data ev) (list session-state first-msg)
                            (conn:event-raw-msg ev) first-msg-raw
                            (conn:event-timestamp ev) (local-time:now)))))))))
      (t (err)
        (log:warn "Disconnecting client ~A. Error: ~A"
                  stream
                  err)
        (close stream :abort t)))))

(defun %check-for-incoming-clients (connector)
  (with-slots (incoming-clients) connector
    (fb:fb-process incoming-clients #'%handle-new-client-connection connector)))

(declaim (inline %notify-client))
(defun %notify-client (connector stream)
  (with-slots (incoming-clients) connector
    (fb:fb-publish-one (incoming-clients ev)
      (setf (conn:event-type ev) :client-connected
            (conn:event-data ev) stream))))

(defun %single-listener-loop (connector)
  (with-slots (listen-grace-period listen-socket incoming-clients) connector
    (multiple-value-bind (_ timeout-remaining) (usocket:wait-for-input listen-socket
                                                                       :timeout 0.5)
      (declare (ignore _))
      (when timeout-remaining
        (let ((client-socket (usocket:socket-accept listen-socket)))
          (multiple-value-bind (sck timeout-remaining) (usocket:wait-for-input client-socket :timeout listen-grace-period)
            (cond
              (timeout-remaining
               (%notify-client connector (usocket:socket-stream client-socket)))
              (t
               (log:warn "Disconnecting client from stream ~A - no data received in ~A seconds"
                         (usocket:socket-stream sck)
                         listen-grace-period)))))))))

(defun %remove-stopped-sessions (conn)
  (with-slots (conn:sessions client-sessions) conn
    (loop for x being the hash-values in conn:sessions
       when (cl-fix-session:stoppedp x)
       do (remhash (cl-fix-session:session-id x) client-sessions))))

(defun %start-listener-thread (connector)
  (handler-case
      (unless (slot-value connector 'single-threaded-mode)
        (%initiate-listener connector)
        (log:info "Calling :listener-started")
        (fsm:fsm-process-event connector :listener-started)
        (log:info "Called :listener-started")
        (loop
          while (not (eq :stopping (fsm:fsm-current-state connector)))
          do
             (%single-listener-loop connector)
             (sleep 0.000000001))
        (fsm:fsm-process-event connector :listener-stopped))
    (t (err)
      (log:error "Shutting down listener thread. Encountered: ~A" err)
      (setf (slot-value connector 'listener-state) :stopped)
      (fsm:fsm-process-event connector :stop-requested))))

(defun %read-msgs-from-stream (stream inbuf reader-fn conn:fb-in session)
  (let ((msg-batch (%read-msgs stream reader-fn inbuf 100))
        (num-retry 0))
    (tagbody
     loop-start
       (restart-case
           (loop
              repeat 1000
              while msg-batch do
                (multiple-value-bind (lower-idx upper-idx reserve-ok)
                    (fb:fb-reserve conn:fb-in (length msg-batch))
                  (when reserve-ok
                    (loop
                       for i from lower-idx below upper-idx
                       for x = (fb:fb-elt conn:fb-in i)
                       do
                         (destructuring-bind (msg msg-str) (pop msg-batch)
                           (setf (conn:event-type x) :message
                                 (conn:event-data x) msg
                                 (conn:event-raw-msg x) msg-str
                                 (conn:event-timestamp x) (local-time:now)
                                 (conn:event-source-session x) session)))
                    (fb:fb-publish conn:fb-in upper-idx)))
              finally
                (when msg-batch
                  (log:warn "Backpressure from worker thread to IO thread.")
                  (error 'conn:backpressure-error
                         :msg "Failed to publish a msg-batch to worker thread.")))
         (wait-then-retry (sleep-time max-retries)
           (when (> num-retry max-retries)
             (log:error "Retries exhausted on backpressure (max retries: ~A)" max-retries)
             (error 'retries-exhausted :msg "Backpressure retries exhausted."))
           (incf num-retry)
           (log:info "Waiting then retrying - sleeping for: ~A secs" sleep-time)
           (sleep sleep-time)
           (go loop-start)))
     loop-exit)))

(defun %remove-client-session (client-sessions session)
  (remhash (cl-fix-session:session-id session) client-sessions))

(defun %loop-through-client-sessions (connector)
  (with-slots (client-sessions conn:reader-fn conn:fb-in) connector
    (loop for cs being the hash-values in client-sessions
       do
         (with-slots (stream inbuf session) cs
           (handler-case
               (%read-msgs-from-stream stream
                                       inbuf
                                       conn:reader-fn
                                       conn:fb-in
                                       session)
             (t (err)
               (log:warn "Encountered error in client IO READ for ~A. Dropping this session & associated socket. Error: ~A" session err)
               (close stream)
               (%remove-client-session client-sessions session)
               (fb:fb-publish-one (conn:fb-in ev)
                 (setf (conn:event-type ev) :force-stop-session
                       (conn:event-source-session ev) session))))))))

(defun %find-client-session (client-sessions session)
  (gethash (cl-fix-session:session-id session) client-sessions))

(defun %process-outbound-event (event connector)
  (declare (type conn:connector-event event))
  (with-slots (conn:fb-in conn:serialize-fn client-sessions) connector
    (ecase (conn:event-type event)
      (:persist-session
       (let ((session (conn:event-source-session event)))
         (log:debug "Persisting: ~A" session)
         (cl-fix-session:persist session)))
      (:message
       (let* ((session (conn:event-source-session event))
              (client-session (%find-client-session client-sessions session)))
         (when client-session
           (with-slots (outbuf stream) client-session
             (handler-case
                 (let ((msg (conn:event-data event)))
                   (conn:serialize-and-send conn:serialize-fn outbuf msg stream)
                   (cl-fix-session:persist-outbound-msg session
                                                        (util:fix-field msg
                                                                        :msg-seq-num)
                                                        outbuf)
                   (force-output stream))
               (t (err)
                 (log:warn "Encountered error in client IO WRITE for ~A. Dropping this session. Error: ~A" session err)
                 (close stream)
                 (%remove-client-session client-sessions session)
                 (fb:fb-publish-one (conn:fb-in ev)
                   (setf (conn:event-type ev) :force-stop-session
                         (conn:event-source-session ev) session))))))))
      (:perform-resend
       (let* ((session (conn:event-source-session event))
              (client-session (%find-client-session client-sessions session))
              (bounds (conn:event-data event)))
         (when client-session
           (with-slots (outbuf stream) client-session
             (destructuring-bind (start . end) bounds
               (cl-fix-session:perform-resend session
                                              start
                                              end
                                              (lambda (msg)
                                                (conn:serialize-and-send conn:serialize-fn outbuf msg stream)))
               (force-output stream))))))
      (:persist-session-inbound-msg
       (let ((msg (conn:event-data event))
             (session (conn:event-source-session event)))
         (when session
           (cl-fix-session:persist-inbound-msg session msg))))
      (:close-session-stores
       (let ((session (conn:event-source-session event)))
         (when session
           (log:debug "Closing stores for session: ~A" session)
           (cl-fix-session:close-stores session)))))))

(defun %process-outbound-events (connector)
  (with-slots (conn:fb-out) connector
    (fb:fb-process conn:fb-out #'%process-outbound-event connector)))

(declaim (inline %single-client-io-loop))
(defun %single-client-io-loop (connector)
  (%check-for-incoming-clients connector)
  (%loop-through-client-sessions connector)
  (%process-outbound-events connector)
  (conn:publish-tick connector))

(defun %start-client-io-thread (connector)
  (log:info "Calling :client-io-started")
  (fsm:fsm-process-event connector :client-io-started)
  (log:info "Called :client-io-started")
  (handler-case
      (progn
        (loop
          while (not (eq :stopping (fsm:fsm-current-state connector)))
          do
             (%single-client-io-loop connector)
             (sleep 0.000000001))
        (fsm:fsm-process-event connector :client-io-stopped))
    (t (err)
      (log:error "Shutting down client-io thread. Encountered: ~A" err)
      (setf (slot-value connector 'client-io-state) :stopped)
      (fsm:fsm-process-event connector :stop-requested))))

(defun %process-inbound-event (event conn)
  (with-slots (conn:sessions) conn
    (let ((type (conn:event-type event))
          (data (conn:event-data event))
          (cl-fix-session:*current-time* (conn:event-timestamp event))
          (sessions-to-persist nil)
          (session))
      (ecase type
        (:init-session
         (setf session (conn:event-source-session event))
         (let* ((session-data (elt data 0))
                (logon-msg (elt data 1))
                (logon-msg-raw (conn:event-raw-msg event)))
           (when session
             (cl-fix-session:init-session session session-data)
             (cl-fix-session:on-connect session logon-msg)
             (conn:enqueue-msg conn
                               :event-type :persist-session-inbound-msg
                               :source-session session
                               :data logon-msg-raw)
             (push session sessions-to-persist))))
        (:force-stop-session
         (setf session (conn:event-source-session event))
         (when session
           (cl-fix-session:stop-session session nil)
           (push session sessions-to-persist)))
        (:message
         (setf session (conn:find-session conn data))
         (let ((raw-msg (conn:event-raw-msg event)))
           (when session
             (conn:enqueue-msg conn
                               :event-type :persist-session-inbound-msg
                               :source-session session
                               :data raw-msg)
             (funcall (cl-fix-session:session-handler session) session data)
             (push session sessions-to-persist))))
        (:tick
         (loop for x being the hash-values in conn:sessions
            when (funcall (cl-fix-session:session-tick x) x)
            do (push x sessions-to-persist))))
      (loop for s in sessions-to-persist
         do
           (conn:enqueue-msg conn
                             :event-type :persist-session
                             :source-session s)))))

(declaim (inline %single-worker-loop))
(defun %single-worker-loop (connector)
  (with-slots (conn:fb-in) connector
    (fb:fb-process conn:fb-in #'%process-inbound-event connector)))

(defun %start-worker-thread (connector)
  (log:info "Calling :worker-started")
  (fsm:fsm-process-event connector :worker-started)
  (log:info "Called :worker-started")
  (handler-case
      (progn
        (loop
          while (not (eq :stopping (fsm:fsm-current-state connector)))
          do
             (%single-worker-loop connector)
             (sleep 0.000000001))
        (fsm:fsm-process-event connector :worker-stopped))
    (t (err)
      (log:error "Shutting down acceptor. Encountered error: ~A" err)
      (setf (slot-value connector 'worker-state) :stopped)
      (fsm:fsm-process-event connector :stop-requested))))

(defun %start-threads (connector)
  (with-slots (listener-thread client-io-thread worker-thread) connector
    (setf listener-thread (bt:make-thread (lambda () (%start-listener-thread connector)) :name "listener")
          client-io-thread (bt:make-thread (lambda () (%start-client-io-thread connector)) :name "client-io")
          worker-thread (bt:make-thread (lambda () (%start-worker-thread connector)) :name "worker"))))

(defun %start-acceptor (connector)
  (with-slots (single-threaded-mode) connector
    (cond
      (single-threaded-mode
       (log:info "Starting acceptor in single-threaded mode.")
       (fsm:fsm-process-event connector :all-started))
      (t (%start-threads connector)))))

(defun %await-stop (conn)
  (loop
     repeat 1000
     while (not (eq (fsm:fsm-current-state conn) :stopped))
     do (sleep 0.001))
  (when (not (eq (fsm:fsm-current-state conn) :stopped))
    (fsm:fsm-process-event conn :force-stop)))

(defun %force-stop (conn)
  (log:warn "Forcing a shutdown.")
  (with-slots (listener-state
               listener-thread
               worker-state
               worker-thread
               client-io-state
               client-io-thread)
      conn
    (unless (eq :stopped listener-state)
      (util:destroy-thread listener-thread))
    (unless (eq :stopped client-io-state)
      (util:destroy-thread client-io-thread))
    (unless (eq :stopped worker-state)
      (util:destroy-thread worker-thread))
    (fsm:fsm-process-event conn :all-stopped)))

(defun %client-connected (connector stream)
  (with-slots (incoming-clients) connector
    (let ((success nil)
          (num-retry 0))
      (tagbody
       loop-start
         (restart-case
             (loop repeat 1000 while (not success)
                do
                  (multiple-value-bind (low high reserve-ok) (fastbuffer:fb-reserve incoming-clients 1)
                    (when reserve-ok
                      (let ((event (fastbuffer:fb-elt incoming-clients low)))
                        (setf (conn:event-type event) :client-connected
                              (conn:event-data event) stream))
                      (fastbuffer:fb-publish incoming-clients high)
                      (setf success t)))
                finally
                  (when (not success)
                    (error 'conn:backpressure-error
                           :msg "Failed to publish to the client-io thread.")))
           (wait-then-retry (sleep-time max-retries)
             (when (> num-retry max-retries)
               (log:error "Retries exhausted on backpressure (max retries: ~A)" max-retries)
               (error 'retries-exhausted :msg "Backpressure retries exhausted."))
             (incf num-retry)
             (log:info "Waiting then retrying - sleeping for: ~A secs" sleep-time)
             (sleep sleep-time)
             (go loop-start)))
       loop-exit))))

(defun %run-loop (connector)
  (%single-client-io-loop connector)
  (%single-worker-loop connector)
  (%single-client-io-loop connector))

(defmethod conn:connector-state ((connector acceptor))
  (cl-fix-fsm:fsm-current-state connector))
