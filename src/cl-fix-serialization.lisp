;;; Copyright 2020 Sandipan Razzaque
;;;
;;; Licensed under the Apache License, Version 2.0 (the "License");
;;; you may not use this file except in compliance with the License.
;;; You may obtain a copy of the License at
;;;
;;;     http://www.apache.org/licenses/LICENSE-2.0
;;;
;;; Unless required by applicable law or agreed to in writing, software
;;; distributed under the License is distributed on an "AS IS" BASIS,
;;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;;; See the License for the specific language governing permissions and
;;; limitations under the License.

(in-package #:cl-fix-serialization)

(define-condition serializer-error (error) ())

(define-condition checksum-mismatch (serializer-error)
  ((expected :initarg :expected)
   (actual :initarg :actual))
  (:report (lambda (condition stream)
             (format stream "Checksum mismatch. Expected: ~A Actual: ~A"
                     (slot-value condition 'expected)
                     (slot-value condition 'actual)))))

(define-condition invalid-data-length (serializer-error)
  ((tag :initarg :tag)
   (length :initarg :length)
   (message :initarg :message))
  (:report (lambda (condition stream)
             (format stream "Length for data field (tag ~A) in message ~A is invalid. Got length of ~A."
                     (slot-value condition 'tag)
                     (slot-value condition 'message)
                     (slot-value condition 'length)))))

(define-condition unknown-tag (warning)
  ((tag :initarg :tag)
   (message :initarg :message))
  (:report (lambda (condition stream)
             (format stream "Unknown tag '~A' received on message ~A. Updating dictionary."
                     (slot-value condition 'tag)
                     (slot-value condition 'message)))))

(defun read-fix-msg (stream buf dict)
  (restart-case
      (multiple-value-bind (start exp-csum act-csum)
          (%read-fix stream (getf dict :begin-str) buf)
        (unless (= exp-csum act-csum)
          (cerror "Ignore checksum error." 'checksum-mismatch :actual act-csum :expected exp-csum))
        (values (%parse-fix-str-to-plist buf start dict)
                start
                buf))
    (skip-message ()
      :report "Skip message."
      (return-from read-fix-msg nil))))

(defun %field-to-str (stream msg field-spec dict)
  (let* ((field (if (listp field-spec) (car field-spec) field-spec))
         (is-required (if (listp field-spec) (getf (cdr field-spec) :required)))
         (user-has-explicitly-set-this-field (member field msg))
         (value (getf msg field))
         (meta (gethash field (getf dict :fields-by-name)))
         (is-data-len-field (and (eq 'cl-fix-types:len (getf meta :type))
                                 (getf meta :data-field)))
         (is-data-field (and (eq 'cl-fix-types:data (getf meta :type))
                             (getf meta :len-field)))
         (tag (getf meta :tag))
         (convert-fn (getf meta :to-string)))
    (when (or is-required user-has-explicitly-set-this-field value)
      (unless is-data-len-field
        (when is-data-field
          (let ((len-field-meta (gethash (getf meta :len-field) (getf dict :fields-by-name))))
            (format stream "~A=~A" (getf len-field-meta :tag) (length value))))
        (format stream "~A=~A" tag (if convert-fn
                                         (funcall convert-fn value)
                                         value))))))

(defun %calc-checksum (str &optional (start 0) end)
  (let ((sum 0))
    (declare ((unsigned-byte 8) sum start))
    (loop for idx from start below (or end (length str))
       do
         (let ((v (char-code (elt str idx))))
           (declare ((integer 0) v))
           (setf sum (mod (+ sum v) 256))))
    sum))

(defun %group-p (fld)
  (and (listp fld)
       (string= (symbol-name (first fld)) "GROUP")))

(defun %group-to-str (stream msg-or-list grp-field msg-type dict)
  (let* ((grp-len-field-name (if (listp grp-field) (car grp-field) grp-field))
         (is-required (and (listp grp-field) (getf (cdr grp-field) :required)))
         (field-meta (gethash grp-len-field-name (getf dict :fields-by-name)))
         (group-list (getf msg-or-list grp-len-field-name))
         (group-flds (-<> field-meta
                       (getf <> :member-fields-by-message)
                       (gethash msg-type <>))))
    (when (or group-list is-required)
      (format stream "~A=~A" (getf field-meta :tag) (length group-list)))
    (dolist (grp group-list)
      (dolist (fld group-flds)
        (if (%group-p fld)
            (let ((len-field (cadr fld)))
              (%group-to-str stream grp len-field msg-type dict))
            (%field-to-str stream grp fld dict))))))

(defun serialize-fix-msg (msg stream dict)
  (let ((checksum 0)
        (tmp-str)
        (body-str)
        (message-fields (concatenate 'list
                                     (getf dict :header-fields)
                                     (gethash (getf msg :msg-type) (getf dict :messages-by-name))
                                     (getf dict :trailer-fields))))
    (setf tmp-str (format nil "8=~A" (getf dict :begin-str)))
    (write-sequence tmp-str stream)
    (incf checksum (%calc-checksum tmp-str))
    (setf body-str
          (with-output-to-string (s)
            (%field-to-str s msg :msg-type dict)
            (dolist (fld message-fields)
              (let ((fld-name (if (listp fld) (first fld) fld)))
                (cond
                  ((%group-p fld)
                   (%group-to-str s msg (cadr fld) (getf msg :msg-type) dict))
                  (t
                   (case fld-name
                     ((:begin-string :body-length :msg-type :check-sum) nil)
                     (t
                      (%field-to-str s msg fld-name dict)))))))))
    (setf tmp-str (format nil "9=~A" (length body-str)))
    (write-sequence tmp-str stream)
    (write-sequence body-str stream)
    (incf checksum (%calc-checksum tmp-str))
    (incf checksum (%calc-checksum body-str))
    (format stream "10=~3,'0d" (mod checksum 256))))

(defun validate (msg dict)
  (declare (ignore msg dict))
  ;; TODO write me.
  ;; Option 1: return multiple-values - ERROR-CODE and the point in the list that error'd out
  ;; Option 2: return some rich data structure with all errors.
  )

(defun %read-fix (stream expected-begin-str stringbuf)
  (declare (type (vector character) stringbuf))
  (let* ((state :INIT)
         (msg-start 0)
         (begin-str (format nil "8=~A" expected-begin-str))
         (begin-len (length begin-str))
         (begin-idx 0)
         (body-len-str (format nil "9="))
         (body-len-len (length body-len-str))
         (body-len-idx 0)
         (checksum-str "10=")
         (checksum-len (length checksum-str))
         (checksum-idx 0)
         (body-len-start 0)
         (checksum-fld-start 0)
         (checksum-start 0)
         (exp-len 0)
         (exp-checksum 0))
    (setf (fill-pointer stringbuf) 0)
    (loop
       for x = (read-char stream)
       do
         (vector-push-extend x stringbuf)
         (ecase state
           (:INIT
            (cond ((char= x (elt begin-str begin-idx)) (incf begin-idx))
                  (t (setf begin-idx 0)))
            (when (= begin-idx begin-len)
              (setf state :READ-HDR
                    begin-idx 0
                    msg-start (- (length stringbuf) (length begin-str)))))
           (:READ-HDR
            (cond ((char= x (elt body-len-str body-len-idx)) (incf body-len-idx))
                  (t (setf body-len-idx 0)))
            (when (= body-len-idx body-len-len)
              (setf state :READ-LEN
                    body-len-idx 0
                    body-len-start (length stringbuf))))
           (:READ-LEN
            (when (char= x #\Soh)
              (setf state :READ-BODY
                    exp-len (parse-integer stringbuf
                                           :start body-len-start
                                           :end (1- (length stringbuf)))
                    body-len-idx 0
                    body-len-start 0)))
           (:READ-BODY
            (cond ((> exp-len 0) (decf exp-len))
                  ((= 0 exp-len)
                   (cond ((char= x (elt checksum-str checksum-idx)) (incf checksum-idx))
                         (t (setf checksum-idx 0)))
                   (when (= checksum-idx checksum-len)
                     (setf state :READ-CSUM
                           checksum-fld-start (- (length stringbuf)
                                                 (length checksum-str))
                           checksum-start (length stringbuf)
                           checksum-idx 0)))
                  (t (cerror "Invalid length" 'invalid-length))))
           (:READ-CSUM
            (when (char= x #\Soh)
              (setf state :DONE
                    exp-checksum (parse-integer stringbuf
                                                :start checksum-start
                                                :end (1- (length stringbuf)))))))
         (when (eq state :DONE)
           (return-from %read-fix (values msg-start
                                          exp-checksum
                                          (%calc-checksum stringbuf msg-start checksum-fld-start)))))))

(defun %inner-push (item target levels)
  (if (= levels 0)
      (progn
        (push item target)
        target)
      (progn
        (setf (car target) (%inner-push item (car target) (1- levels)))
        target)))

(defun %recursive-reverse (ls)
  (if (listp ls)
      (mapcar #'%recursive-reverse (reverse ls))
      ls))

(defun %validate-data-length (str start data-tag len)
  ;; This is a very rudimentary/simple check. There are certainly edge cases that will trip it up for a false-positive.
  ;; But it should never give us a false-negative.
  (let ((end-pos (+ start (length data-tag) 1 len)))
    (and (< end-pos (length str))
         (eq (elt str end-pos) #\Soh)
         (member (elt str (1+ end-pos)) '(#\0 #\1 #\2 #\3 #\4 #\5 #\6 #\7 #\8 #\9)))))

(defun %get-data-field (str start data-tag len-tag len msg-type)
  (when (not (%validate-data-length str start data-tag len))
    (error 'invalid-data-length :tag len-tag :length len :message msg-type))
  (let* ((tag (subseq str start (+ start (length data-tag))))
         (data-start-pos (+ 1
                            start
                            (length data-tag)))
         (data-end-pos (min (+ data-start-pos len)
                            (length str)))
         (data (subseq str data-start-pos data-end-pos)))
    (values tag
            data
            (1+ data-end-pos))))

(defun %unknown-tag (tag msg-type dict)
  (warn 'unknown-tag :message msg-type :tag tag)
  ;; TODO: Construction of the 'synthetic field' list breaks the encapsulation of cl-fix-dict.
  ;;       If it gets annoying, will refactor as necessary.
  (let* ((name (intern (format nil "UNKNOWN-TAG-~A" tag) 'keyword))
         (synthetic-field (list :tag tag
                                :name name
                                :type 'string
                                :data-field nil
                                :len-field nil
                                :member-fields-by-message (make-hash-table :test 'eq)
                                :to-string nil
                                :from-string nil
                                :unknown-tag t)))
    (setf (gethash name (getf dict :fields-by-name)) synthetic-field
          (gethash tag (getf dict :fields-by-tag)) synthetic-field)
    synthetic-field))

(defun %get-next-pair (str start msg-type dict)
  (let ((fields-by-tag (getf dict :fields-by-tag))
        (fields-by-name (getf dict :fields-by-name)))
    (let* ((sep-pos (or (search "" str :start2 start) (length str)))
           (equal-pos (search "=" str :start2 start))
           (tag (subseq str start equal-pos))
           (val (subseq str (1+ equal-pos) sep-pos))
           (field-meta (gethash tag fields-by-tag))
           (data-field-name (getf field-meta :data-field))
           (is-len-field (and (eq 'cl-fix-types:len (getf field-meta :type))
                              data-field-name)))
      (if is-len-field
          (%get-data-field str (1+ sep-pos)
                           (getf (gethash data-field-name fields-by-name) :tag)
                           tag
                           (parse-integer val)
                           msg-type)
          (values tag
                  val
                  (1+ sep-pos))))))

(defun %recurse-plist (str groups result msg-type dict)
  ;; TODO edge-cases with lengths and badly formed FIX strings shouldn't drop us into the debugger, but be dealt with in 
  (let ((fields-by-tag (getf dict :fields-by-tag)))
    (macrolet ((push-result (item)
                 `(setf result (%inner-push ,item result nest-level)))
               (recurse ()
                 `(%recurse-plist (subseq str next-idx) groups result msg-type dict)))
      (if (= (length str) 0)
          (%recursive-reverse result)
          (multiple-value-bind (tag val-str next-idx) (%get-next-pair str 0 msg-type dict)
            (let* ((field-metadata (or (gethash tag fields-by-tag)
                                       (%unknown-tag tag msg-type dict)))
                   (is-unknown-tag (getf field-metadata :unknown-tag))
                   (name (getf field-metadata :name))
                   (type (getf field-metadata :type))
                   (nest-level (* 2 (length groups)))
                   (is-new-group (and (not is-unknown-tag)
                                      (> nest-level 0)
                                      (eq name
                                          (if (listp (caar groups))
                                              (car (caar groups))
                                              (caar groups)))))
                   (is-end-of-groups (and (not is-unknown-tag)
                                          (> nest-level 0)
                                          (not (member name (car groups)))))
                   (val* (alexandria:if-let (x (getf field-metadata :from-string))
                           (funcall x val-str)
                           val-str)))
              (when (and (not msg-type)
                         (eq name :msg-type))
                (setf msg-type val*))
              (cond
                (is-new-group
                 (setf result (%inner-push () result (1- nest-level)))
                 (push-result name)
                 (push-result val*)
                 (recurse))
                (is-end-of-groups
                 (decf nest-level 2)
                 (pop groups)
                 (push-result name)
                 (push-result val*)
                 (recurse))
                (t
                 (case type
                   (cl-fix-types:numingroup
                    (let ((member-fields (-<> field-metadata
                                           (getf <> :member-fields-by-message)
                                           (gethash msg-type <>))))
                      (push-result name)
                      (push-result ())
                      (push member-fields groups)
                      (recurse)))
                   (t
                    (push-result name)
                    (push-result val*)
                    (recurse)))))))))))

(defun %parse-fix-str-to-plist (buf start dict)
  (%recurse-plist (subseq buf start) nil nil nil dict))

