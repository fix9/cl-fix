;;; Copyright 2020 Sandipan Razzaque
;;;
;;; Licensed under the Apache License, Version 2.0 (the "License");
;;; you may not use this file except in compliance with the License.
;;; You may obtain a copy of the License at
;;;
;;;     http://www.apache.org/licenses/LICENSE-2.0
;;;
;;; Unless required by applicable law or agreed to in writing, software
;;; distributed under the License is distributed on an "AS IS" BASIS,
;;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;;; See the License for the specific language governing permissions and
;;; limitations under the License.

;; Basic file-store for FIX messages, that allows easy resend behaviour.
;; NOTE: This is NOT thread-safe. Particularly - 'iterate' will clobber the file-position.

(in-package #:cl-fix-message-store)

(defstruct file-store
  stream
  (first-msg-position 0)
  (msg-index (make-hash-table)))

(defun store-msg (store idx msg-str)
  (let* ((msg-index (file-store-msg-index store))
         (stream (file-store-stream store))
         (cur-pos (file-position stream)))
    (when (= 1 idx)
      (clrhash msg-index)
      (setf (file-store-first-msg-position store) cur-pos))
    (let ((ln (length msg-str)))
      (setf (gethash idx msg-index) (cons cur-pos ln))
      (write-sequence msg-str stream)
      (write-char #\Newline stream) ;; Just for tail -f. Newline not included when resending.
      (force-output stream))))

(defun close-store (store)
  (close (file-store-stream store)))

(defun iterate (store from-idx to-idx msg-fn gap-fn)
  "Iterate through the messages in STORE for all messages with seq-nums [FROM-IDX TO-IDX).
   MSG-FN should be a function accepting an array of 'CHARACTER
   GAP-FN should be a function accepting 2 arguments, starting index of a gap, and the next-seq-no that will be sent."
  (let* ((msg-index (file-store-msg-index store))
         (stream (file-store-stream store))
         (buffer (make-array 1024 :adjustable t :fill-pointer 0 :element-type 'character))
         (orig-pos (file-position stream))
         (gap-start -1))
    (unwind-protect
         (loop
            for i from from-idx below to-idx
            do
              (multiple-value-bind (val present) (gethash i msg-index)
                (cond
                  ((and (not present)
                        (= -1 gap-start))
                   (setf gap-start i))
                  (present
                   (when (not (= -1 gap-start))
                     (funcall gap-fn gap-start i)
                     (setf gap-start -1))
                   (destructuring-bind (pos . len) val
                     (setf (fill-pointer buffer) len)
                     (file-position stream pos)
                     (read-sequence buffer stream :start 0)
                     (funcall msg-fn buffer))))))
      (file-position stream orig-pos))))

(defun rebuild-index (store dict)
  (let* ((first-msg-pos (file-store-first-msg-position store))
         (stream (file-store-stream store))
         (cur-pos (file-position stream))
         (msg-index (file-store-msg-index store))
         (reader-fn (getf dict :reader-fn))
         (buffer (make-array 1024 :element-type 'character :fill-pointer 0)))
    (unwind-protect
         (progn
           (when (< first-msg-pos (1- cur-pos))
             (file-position stream first-msg-pos)
             (loop
                for p = (file-position stream)
                while (< p (1- cur-pos))
                do
                  (multiple-value-bind (msg start-idx) (funcall reader-fn stream buffer)
                    (when msg
                      (let ((seq-num (util:fix-field msg :msg-seq-num))
                            (start-pos (+ p start-idx))
                            (len (- (length buffer) start-idx)))
                        (setf (gethash seq-num msg-index) (cons start-pos len))))))))
      (file-position stream cur-pos))))
