;;; Copyright 2020 Sandipan Razzaque
;;;
;;; Licensed under the Apache License, Version 2.0 (the "License");
;;; you may not use this file except in compliance with the License.
;;; You may obtain a copy of the License at
;;;
;;;     http://www.apache.org/licenses/LICENSE-2.0
;;;
;;; Unless required by applicable law or agreed to in writing, software
;;; distributed under the License is distributed on an "AS IS" BASIS,
;;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;;; See the License for the specific language governing permissions and
;;; limitations under the License.

(in-package #:fastbuffer)

(defclass fb ()
  ((size :initarg :size :initform 1024)
   (type :initform t :initarg :type)
   (data :initform nil)
   (init-fn :initarg :init-fn)
   (writer-idx :initform 0)
   (reader-idx :initform 0)
   #-sbcl (writer-lock :initform (bt:make-lock))
   #-sbcl (reader-lock :initform (bt:make-lock))
   ))

(defmethod initialize-instance :after ((obj fb) &key)
  (with-slots (type size data init-fn) obj
    (setf data (make-array size :element-type type))
    (loop for i from 0 below size do (setf (elt data i) (funcall init-fn)))))

(declaim (inline %get-writer-idx))

(define-condition backpressure (error)
  ((msg :initarg :msg)))

#-sbcl
(defun %get-writer-idx (fb)
  (declare (type fb fb))
  (with-slots (writer-lock writer-idx) fb
      (bt:with-lock-held (writer-lock)
                         writer-idx)))
#+sbcl
(defun %get-writer-idx (fb)
  (declare (type fb fb))
  (slot-value fb 'writer-idx))

(defun fb-process (fb process-fn &rest additional-args)
  (declare (type fb fb))
  (with-slots (data writer-idx reader-idx reader-fn size #-sbcl writer-lock #-sbcl reader-lock) fb
    (let ((writer-idx-snapped (%get-writer-idx fb)))
      (when (< #-sbcl reader-idx
               #+sbcl (sb-thread:barrier (:read)
                                         reader-idx)
               writer-idx-snapped)
        (loop for i from reader-idx below writer-idx-snapped
           for elem = (svref data (mod i size))
           do
             (apply process-fn elem additional-args))
        #-sbcl
        (bt:with-lock-held (reader-lock)
                           (setf reader-idx writer-idx-snapped))
        #+sbcl
        (sb-thread:barrier (:write)
                           (setf reader-idx writer-idx-snapped))))))

(defun fb-reserve (fb n)
  (declare (type fixnum n))
  (with-slots (reader-idx writer-idx size) fb
    (when (> n size)
      (error "Requested reserve/batch size (~A) greater than the fastbuffer capacity (~A)" n size))
    (if (>= (- size (- writer-idx reader-idx) n) 0)
        (values writer-idx (+ writer-idx n) t)
        (values -1 -1 nil))))

#+sbcl
(defun fb-publish (fb idx)
  "Signals to the data structure (and therefore the reader-thread) that data is available up to IDX."
  (sb-thread:barrier (:memory) 
    (setf (slot-value fb 'writer-idx) idx)))

#-sbcl
(defun fb-publish (fb idx)
  (with-slots (writer-lock) fb
    (bt:with-lock-held (writer-lock)
      (setf (slot-value fb 'writer-idx) idx))))

(defun (setf fb-elt) (val obj idx)
  (declare (type fb obj))
  (with-slots (size data) obj
    (setf (elt data (mod idx size)) val)))

(defun fb-elt (obj idx)
  (declare (type fb obj))
  (with-slots (data size) obj
    (elt data (mod idx size))))

(defun fb-reset (&rest fbs)
  (loop for fb in fbs
     do
       (with-slots (writer-idx reader-idx) fb
           (setf writer-idx 0
                 reader-idx 0))))

(defmacro fb-publish-one ((fb event-sym) &body body)
  (let ((low (gensym "low"))
        (high (gensym "high"))
        (ok (gensym "ok")))
    `(multiple-value-bind (,low ,high ,ok) (fb-reserve ,fb 1)
       (cond
         (,ok
          (let ((,event-sym (fb-elt ,fb ,low)))
            ,@body
            (fb-publish ,fb ,high)))
         (t
          (error 'backpressure :msg ,(format nil "Could not publish event to ~A" fb)))))))
