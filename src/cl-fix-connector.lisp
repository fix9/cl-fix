;;; Copyright 2020 Sandipan Razzaque
;;;
;;; Licensed under the Apache License, Version 2.0 (the "License");
;;; you may not use this file except in compliance with the License.
;;; You may obtain a copy of the License at
;;;
;;;     http://www.apache.org/licenses/LICENSE-2.0
;;;
;;; Unless required by applicable law or agreed to in writing, software
;;; distributed under the License is distributed on an "AS IS" BASIS,
;;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;;; See the License for the specific language governing permissions and
;;; limitations under the License.

(in-package #:cl-fix-connector)

(defgeneric start-connector (connector))
(defgeneric stop-connector (connector))
(defgeneric add-session (connector session))
(defgeneric connector-state (connector))

(define-condition connector-error (error)
  ((msg :initarg :msg)))

(define-condition socket-read-error (connector-error)
  ())

(define-condition socket-write-error (connector-error)
  ())

;; TODO should be a fastbuffer error
(define-condition backpressure-error (connector-error)
  ())

(define-condition retries-exhausted (connector-error)
  ())

(defparameter *event-timestamp* nil)

(defclass connector ()
  ((host :initarg :host :type string :initform (error "Must supply :host"))
   (port :initarg :port :type (integer 0 65535) :initform (error "Must supply :port"))
   (fb-in :type fastbuffer:fb :initform (%make-default-fastbuffer))
   (fb-out :type fastbuffer:fb :initform (%make-default-fastbuffer))
   (dict-key :initarg :dict-key :initform (error "Must supply :dict-key (e.g. :fix44)"))
   (dict :initarg :dict)
   (reader-fn :initarg :reader-fn)
   (serialize-fn :initarg :serialize-fn)
   (sessions :initform (make-hash-table :test 'equal))
   (tick-resolution-ms :initarg :tick-resolution-ms
                       :initform 1000
                       :documentation "How often to publish a ':tick' event to each session object.")
   (last-tick :initform nil)))

;; TODO This should be a struct. There's quite a performance hit on setf'ing over slot-values, which
;; will happen a LOT on this data structure. Will fix this in it's own isolated refactor with no
;; other changes.
(defclass connector-event ()
  ((type :accessor event-type :initform nil)
   (data :accessor event-data :initform nil)
   (raw-msg :accessor event-raw-msg :initform nil)
   (source-session :accessor event-source-session :initform nil)
   (timestamp :accessor event-timestamp :initform nil)))

(defun %make-default-fastbuffer ()
  (make-instance 'fastbuffer:fb
                 :init-fn (lambda () (make-instance 'connector-event))))

(defmethod initialize-instance :after ((instance connector) &rest initargs &key &allow-other-keys)
  (declare (ignore initargs))
  (with-slots (dict-key dict reader-fn serialize-fn) instance
    (setf dict (util:resolve-dict dict-key))
    (unless (slot-boundp instance 'reader-fn)
      (setf reader-fn (getf dict :reader-fn)))
    (unless (slot-boundp instance 'serialize-fn)
      (setf serialize-fn (getf dict :serialize-fn)))))

(defun enqueue-msg (conn &key event-type data raw-msg source-session)
  (declare (type connector conn))
  (with-slots (fb-out) conn
    (let ((success nil)
          (num-retry 0))
      (tagbody
       loop-start
         (restart-case
             (loop repeat 1000 while (not success)
                do
                  (multiple-value-bind (low high reserve-ok) (fastbuffer:fb-reserve fb-out 1)
                    (when reserve-ok
                      (let ((event (fastbuffer:fb-elt fb-out low)))
                        (setf (event-type event) event-type
                              (event-data event) data
                              (event-raw-msg event) raw-msg
                              (event-source-session event) source-session))
                      (fastbuffer:fb-publish fb-out high)
                      (setf success t)))
                finally
                  (when (not success)
                    (error 'backpressure-error
                           :msg "Failed to publish to the IO thread.")))
           (wait-then-retry (sleep-time max-retries)
             (when (> num-retry max-retries)
               (log:error "Retries exhausted on backpressure (max retries: ~A)" max-retries)
               (error 'retries-exhausted :msg "Backpressure retries exhausted."))
             (incf num-retry)
             (log:info "Waiting then retrying - sleeping for: ~A secs" sleep-time)
             (sleep sleep-time)
             (go loop-start)))
       loop-exit))))

(defun find-session (conn msg)
  (with-slots (dict) conn
    (let* ((sender-comp-id (util:fix-field msg :sender-comp-id))
           (target-comp-id (util:fix-field msg :target-comp-id))
           (sender-sub-id (util:fix-field msg :sender-sub-id))
           (target-sub-id (util:fix-field msg :target-sub-id))
           (session (%lookup-session-by-key conn sender-comp-id sender-sub-id target-comp-id target-sub-id)))
      (when (not session)
        (log:warn "Unknown session: ~A-~A.~A-~A - dropping message."
                  sender-comp-id
                  sender-sub-id
                  target-comp-id
                  target-sub-id))
      session)))

(defun publish-tick (connector)
  (with-slots (fb-in tick-resolution-ms last-tick) connector
    (let ((time-now (local-time:now))
          (fb conn:fb-in))
      (when (and (> tick-resolution-ms 0)
                 (or (not last-tick)
                     (> (local-time:timestamp-difference time-now last-tick) (/ tick-resolution-ms 1000d0))))
        (setf last-tick time-now)
        (loop
           with publish-success = nil
           repeat 1000
           until publish-success
           do
             (multiple-value-bind (low high ok) (fb:fb-reserve fb 1)
               (when ok
                 (let ((event (fb:fb-elt fb low)))
                   (setf (conn:event-type event) :tick
                         (conn:event-data event) nil
                         (conn:event-timestamp event) time-now))
                 (fb:fb-publish fb high)
                 (setf publish-success t)))
           finally
             (when (not publish-success)
               (error 'backpressure-error
                      :msg "Failed to publish tick event to worker thread.")))))))

(declaim (inline serialize-and-send))
(defun serialize-and-send (serialize-fn outbuf msg stream)
  (setf (fill-pointer outbuf) 0)
  (with-output-to-string (s outbuf)
    (funcall serialize-fn msg s))
  (log:debug "OUT > ~A" outbuf)
  (write-sequence outbuf stream :end (length outbuf)))

(defun %lookup-session-by-key (conn sender-comp-id sender-sub-id target-comp-id target-sub-id)
  (with-slots (sessions) conn
    (let ((session-key (%session-key target-comp-id target-sub-id sender-comp-id sender-sub-id)))
      ;; NOTE: The target/sender is the other way around above compared to add-session below.
      ;;       That's because we store it with OUR sender and THEIR comp - but on an inbound
      ;;       msg this will be flipped.
      ;; TODO We shouldn't be consing up a new string every time we want to look up a session
      (gethash session-key sessions))))

(defmethod add-session ((conn connector) (session cl-fix-session:session))
  (with-slots (sessions) conn
    (let ((session-key (%session-key (cl-fix-session:sender-comp-id session)
                                     (cl-fix-session:sender-sub-id session)
                                     (cl-fix-session:target-comp-id session)
                                     (cl-fix-session:target-sub-id session))))
      (setf (gethash session-key sessions) session)))
  (setf (cl-fix-session:conn session) conn))


(defun %session-key-part (a b)
  (if b (format nil "~A-~A" a b) a))

(defun %session-key (sender-comp-id sender-sub-id target-comp-id target-sub-id)
  (format nil "~A.~A" (%session-key-part sender-comp-id sender-sub-id)
                      (%session-key-part target-comp-id target-sub-id)))

(defun get-sessions (connector)
  (loop for x being the hash-values in (slot-value connector 'sessions)
        collect x))

(defmethod print-object ((connector connector) stream)
  (print-unreadable-object (connector stream :type t)
    (format stream "~A:~A - ~A"
            (slot-value connector 'host)
            (slot-value connector 'port)
            (connector-state connector))))
