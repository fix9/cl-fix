;;; Copyright 2020 Sandipan Razzaque
;;;
;;; Licensed under the Apache License, Version 2.0 (the "License");
;;; you may not use this file except in compliance with the License.
;;; You may obtain a copy of the License at
;;;
;;;     http://www.apache.org/licenses/LICENSE-2.0
;;;
;;; Unless required by applicable law or agreed to in writing, software
;;; distributed under the License is distributed on an "AS IS" BASIS,
;;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;;; See the License for the specific language governing permissions and
;;; limitations under the License.

;;;; cl-fix-fsm.lisp

;;; Inspired by FSM code in Durenard's book "Professional Automated Trading". Key differences:
;;; - Thread safety
;;; - There is no concept of a sensor
;;; - Events are just keywords
;;; - No state change history

(in-package #:cl-fix-fsm)

(defclass fsm ()
  ((name :initarg :fsm-name :initform (error "Must supply :name"))
   (starting-state :initarg :fsm-starting-state :initform (error "Must supply :current-state"))
   (transitions :initarg :fsm-transitions :initform nil)
   (state-lock :initform (bt:make-recursive-lock "fsm-state-lock"))
   (current-state :initform nil :reader fsm-current-state)))

(defmethod initialize-instance :after ((fsm fsm) &rest initargs &key &allow-other-keys)
  (declare (ignore initargs))
  (setf (slot-value fsm 'current-state) (slot-value fsm 'starting-state)))

(defclass transition ()
  ((event :initarg :event :initform (error "Must supply :event"))
   (transition-fn :initarg :transition-fn :initform (error "Must supply :transition-fn"))
   (from-state :initarg :from-state :initform (error "Must supply :from-state"))
   (to-state :initarg :to-state :initform (error "Must supply :to-state"))))

(defun %find-transition (transitions current-state event)
  (find-if (lambda (x)
             (and (eq current-state (slot-value x 'from-state))
                  (eq event (slot-value x 'event))))
           transitions))

(defun fsm-process-event (fsm event)
  (with-slots (name current-state transitions state-lock) fsm
    (log:debug "~A: Waiting to process event ~A" name event)
    (bt:with-recursive-lock-held (state-lock)
      (log:debug "~A: Processing event ~A" name event)
      (let ((transition (%find-transition transitions current-state event)))
        (when transition
          (let ((next-state (slot-value transition 'to-state))
                (fn (slot-value transition 'transition-fn)))
            (when (not (eq current-state next-state))
              (log:info "~A: Transitioning from ~A to ~A" name current-state next-state))
            (setf current-state next-state)
            (when fn
              (funcall fn fsm))))
        (when (not transition)
          (log:warn "~A: Couldn't find a transition for event ~A in current-state ~A" name event current-state))))))

(defun %noop (fsm)
  (declare (ignore fsm)))

(defun make-transition (&key event from-state to-state (fn #'%noop))
  (make-instance 'transition
                 :event event
                 :from-state from-state
                 :to-state to-state
                 :transition-fn fn))



