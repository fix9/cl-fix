;;; Copyright 2020 Sandipan Razzaque
;;;
;;; Licensed under the Apache License, Version 2.0 (the "License");
;;; you may not use this file except in compliance with the License.
;;; You may obtain a copy of the License at
;;;
;;;     http://www.apache.org/licenses/LICENSE-2.0
;;;
;;; Unless required by applicable law or agreed to in writing, software
;;; distributed under the License is distributed on an "AS IS" BASIS,
;;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;;; See the License for the specific language governing permissions and
;;; limitations under the License.

(in-package #:cl-fix-dict)

(eval-when (:compile-toplevel :load-toplevel :execute)

  (defparameter *registry* (make-hash-table))
  (defparameter *dict* ())

  (defun make-dict (begin-str)
    (list
     :begin-str (or begin-str (error "Must supply a valid begin-str. Got: ~A" begin-str))
     :fields-by-tag (make-hash-table :test 'equal)
     :fields-by-name (make-hash-table :test 'eq)
     :header-fields ()
     :trailer-fields ()
     :components-by-name (make-hash-table :test 'eq)
     :messages-by-name (make-hash-table :test 'eq)
     :messages-by-id (make-hash-table :test 'equal)
     :reader-fn ()
     :serialize-fn ()
     :validate-fn ()))

  (defun make-field-metadata (&key name tag type data-field &allow-other-keys)
    (list
     :tag tag
     :name name
     :type type
     :data-field data-field ;; Corresponding 'data' field for 'len' fields (supplied in user definition)
     :len-field nil ;; Corresponding 'len' for 'data' fields (resolved in %resolve-data-len-fields)
     :member-fields-by-message (make-hash-table :test 'eq)
     :to-string nil
     :from-string nil))

  (define-condition dictionary-error (error) ())

  (define-condition field-name-clash (dictionary-error)
    ((name :initarg :name))
    (:report (lambda (condition stream)
               (format stream "Duplicate field name: '~A'"
                       (slot-value condition 'name)))))

  (define-condition tag-value-clash (dictionary-error)
    ((tag :initarg :tag))
    (:report (lambda (condition stream)
               (format stream "Duplicate tag value: '~A'"
                       (slot-value condition 'tag)))))

  (define-condition bad-enum-type (dictionary-error)
    ((field :initarg :field)
     (type :initarg :type)
     (value :initarg :value))
    (:report (lambda (condition stream)
               (with-slots (field value type) condition
                 (format stream "Bad enum value in field ~A: ~A - (type ~A). Must be of type STRING, FIXNUM or CHARACTER."
                         field
                         value
                         type)))))

  (define-condition differing-enum-types (dictionary-error)
    ((field :initarg :field)
     (value :initarg :value)
     (first :initarg :first))
    (:report (lambda (condition stream)
               (with-slots (field value first) condition
                 (format stream "In enum field ~A: value '~A' differs in type from the first value '~A'."
                         field
                         value
                         first)))))

  (defun add-to-string-converter (&rest field-meta &key type &allow-other-keys)
    (let* ((field-meta (copy-list field-meta))
           (to-string-fn
            (cond
              ((listp type)
               (let* ((type-kw (intern (symbol-name (first type)) 'keyword))
                      (values (cdr type)))
                 (case type-kw
                   ((:string-enum
                     :char-enum
                     :int-enum)
                    (lambda (val)
                      (getf values val val)))
                   (:multiple-value-string-enum
                    (lambda (val)
                      (let ((resolved-vals (loop for v in val collect (getf values v v))))
                        (format nil "~{~A~^ ~}" resolved-vals))))
                   (t (error "Field ~A: unknown type ~A" (getf field-meta :name) type-kw)))))
              (t
               (multiple-value-bind (converter found) (cl-fix-types:get-to-string-converter type)
                 (if found
                     converter
                     (error "Field ~A: unknown type ~A" (getf field-meta :name) type)))))))
      (when to-string-fn
        (setf (getf field-meta :to-string) to-string-fn))
      field-meta))

  (defun %plist-to-flipped-alist (plist)
    "(:one 1 :two 2) => ((1 . :one) (2 . :two))"
    (loop for x = (pop plist) for y = (pop plist) while x collect (list* y x)))

  (defun add-from-string-converter (&rest field-meta &key type &allow-other-keys)
    (let* ((field-meta (copy-list field-meta))
           (from-string-fn
            (cond
              ((listp type)
               (let* ((type-kw (intern (symbol-name (first type)) 'keyword))
                      (values (copy-list (cdr type)))
                      (flipped-alist (%plist-to-flipped-alist values)))
                 (case type-kw
                   (:string-enum
                    (lambda (val)
                      (or (cdr (assoc val flipped-alist :test #'equal)) val)))
                   (:char-enum
                    (lambda (val)
                      (when (> (length val) 0)
                        (or (cdr (assoc (elt val 0) flipped-alist :test #'eq)) val))))
                   (:int-enum
                    (lambda (val)
                      (alexandria:if-let (parsed-val (parse-integer val :junk-allowed t))
                        (or (cdr (assoc parsed-val flipped-alist :test #'=)) val))))
                   (:multiple-value-string-enum
                    (lambda (val)
                      (nreverse
                       (loop for s in (cl-ppcre:split "\\s+" val)
                          collect (or (cdr (assoc s flipped-alist :test #'equal)) val)))))
                   (t (error "Field ~A: unknown type ~A" (getf field-meta :name) type-kw)))))
              (t
               (multiple-value-bind (converter found) (cl-fix-types:get-from-string-converter type)
                 (if found
                     converter
                     (error "Field ~A: unknown type ~A" (getf field-meta :name) type)))))))
      (when from-string-fn
        (setf (getf field-meta :from-string) from-string-fn))
      field-meta))

  (defun add-field-to-dict (&rest field-meta &key tag name &allow-other-keys)
    (let ((fields-by-tag (getf *dict* :fields-by-tag))
          (fields-by-name (getf *dict* :fields-by-name))
          (updated-meta (->> field-meta
                          (apply #'add-to-string-converter)
                          (apply #'add-from-string-converter))))
      (setf (gethash tag fields-by-tag) updated-meta
            (gethash name fields-by-name) updated-meta)))

  (defun add-component-to-dict (identifier fields)
    (setf (gethash identifier (getf *dict* :components-by-name)) fields))

  (defun set-header (fields)
    (setf (getf *dict* :header-fields) fields))

  (defun set-trailer (fields)
    (setf (getf *dict* :trailer-fields) fields))

  (defun add-field (&rest field-metadata)
    (let ((field-meta (apply #'make-field-metadata field-metadata)))
      (apply #'add-field-to-dict field-meta)))

  (defun add-message (id-keyword id-str fields)
    (declare (optimize (debug 3)))
    (setf (gethash id-keyword (getf *dict* :messages-by-name)) fields
          (gethash id-str (getf *dict* :messages-by-id)) fields))

  (defun add-group (len-field-name msg-name fields)
    (let ((field-meta (gethash len-field-name (getf *dict* :fields-by-name))))
      (setf (gethash msg-name (getf field-meta :member-fields-by-message)) fields)))

  (defun register-dictionary (identifier begin-str)
    (multiple-value-bind (val found) (gethash identifier *registry* (make-dict begin-str))
      (when (not found)
        (setf (gethash identifier *registry*) val))
      val))

  (defun set-dictionary (identifier)
    (setf *dict* (gethash identifier *registry*)))

  (defun register-reader (&key (dict *dict*) fn)
    (setf (getf dict :reader-fn) fn))

  (defun register-serializer (&key (dict *dict*) fn)
    (setf (getf dict :serialize-fn) fn))

  (defun validate-field (name tag props)
    (declare (type keyword name)
             (type string tag))
    (when (gethash name (getf *dict* :fields-by-name))
      (error 'field-name-clash :name name))
    (when (gethash tag (getf *dict* :fields-by-tag))
      (error 'tag-value-clash :tag tag))
    (let ((type (getf props :type)))
      (when (and type
                 (listp type)
                 (member (symbol-name (car type)) '("ENUM" "STRING-ENUM" "CHAR-ENUM") :test #'string=))
        (let* ((enum-spec (cdr type))
               (first-val (cadr enum-spec)))
          (typecase first-val
            ((or fixnum character string)
             (loop
                for i from 1 below (length enum-spec) by 2
                for val = (elt enum-spec i)
                when (not (or (and (typep val 'fixnum) (typep first-val 'fixnum))
                              (and (typep val 'character) (typep first-val 'character))
                              (and (typep val 'string) (typep first-val 'string))))
                do (error 'differing-enum-types :field name :value val :first first-val)))
            (t (error 'bad-enum-type :field name :type (type-of first-val) :value first-val)))))))

  (defun resolve (spec)
    "Recursively expands all components to their field constituents."
    (cond
      ((listp spec)
       (cond
         ((string= "GROUP" (symbol-name (first spec)))
          (list
           (apply #'concatenate
                  'list
                  (list (first spec))
                  (mapcar #'resolve (rest spec)))))
         ((string= "COMPONENT" (symbol-name (first spec)))
          (let* ((kw (second spec))
                 (comp (gethash kw (getf *dict* :components-by-name))))
            (when (not comp)
              (error "Unknown component: ~A" kw))
            (apply #'concatenate 'list (mapcar #'resolve comp))))
         (t (list spec))))
      (t (list spec))))

  (defun define-groups-from-fields (msg-name resolved-fields)
    (labels ((recurse-groups (fields)
               (let ((groups (loop for f in fields
                                when (and (listp f)
                                          (string= "GROUP" (symbol-name (first f))))
                                collect f)))
                 (apply #'concatenate
                        'list
                        groups
                        (loop for g in groups collect (recurse-groups (cddr g)))))))
      (let ((all-groups (recurse-groups resolved-fields)))
        (loop for g in all-groups
           collect
             `(add-group ,(let ((x (cadr g)))
                            (if (listp x) (car x) x))
                         ,msg-name
                         ',(cddr g))))))

  (defun %make-reader (dict)
    (lambda (stream &optional (stringbuf (make-array 1024 :element-type 'character :fill-pointer 0)))
      (cl-fix-rw:read-fix-msg stream stringbuf dict)))

  (defun %make-serializer (dict)
    (lambda (msg stream)
      (cl-fix-rw:serialize-fix-msg msg stream dict)))

  (defun %make-serializer-str (dict)
    (lambda (msg)
      (with-output-to-string (s)
        (cl-fix-rw:serialize-fix-msg msg s dict))))

  (defun %resolve-data-len-fields ()
    (loop
       for x being the hash-values in (getf *dict* :fields-by-name)
       if (and (eq (getf x :type) 'cl-fix-types:len)
               (getf x :data-field))
       do
         (let* ((len-field (getf x :name))
                (data-field (getf x :data-field))
                (data-field-meta (gethash data-field (getf *dict* :fields-by-name))))
           (setf (getf data-field-meta :len-field) len-field))))

  (defun %make-validate-fn (dict)
    (lambda (msg &rest rest)
      (apply #'cl-fix-validation:validate-msg (list* dict msg rest))))

  (defun register-validator (&key dict fn)
    (setf (getf dict :validate-fn) fn)))

(defmacro defdict (dict-name begin-str)
  `(eval-when (:compile-toplevel :load-toplevel :execute)
     (register-dictionary ,dict-name ,begin-str)
     (set-dictionary ,dict-name)))

(defmacro in-dict (dict-name)
  `(eval-when (:compile-toplevel :load-toplevel :execute)
     (set-dictionary ,dict-name)))

(defmacro %deffield (name tag &rest props)
  (validate-field name tag props)
  (let ((name* (intern (symbol-name name) 'keyword))
        (type (getf props :type))
        (data-field (getf props :data-field)))
    `(add-field :name ,name* :tag ,tag :type ',type ,@(if data-field `(:data-field ,data-field)))))

(defmacro deffields (&rest specs)
  `(eval-when (:compile-toplevel :load-toplevel :execute)
     ,@(loop for s in specs collect `(%deffield ,@s))
     (%resolve-data-len-fields)))

(defmacro defcomponent (name options &rest fields)
  (declare (ignore options))
  (let ((kw (intern (symbol-name name) 'keyword)))
    `(eval-when (:compile-toplevel :load-toplevel :execute)
       (add-component-to-dict ,kw ',fields))))

(defmacro defheader (&rest fields)
  (let ((resolved-fields (apply #'concatenate 'list (loop for f in fields collect (resolve f)))))
    `(eval-when (:compile-toplevel :load-toplevel :execute)
       (set-header ',resolved-fields))))

(defmacro deftrailer (&rest fields)
  (let ((resolved-fields (apply #'concatenate 'list (loop for f in fields collect (resolve f)))))
    `(eval-when (:compile-toplevel :load-toplevel :execute)
       (set-trailer ',resolved-fields))))

(defmacro defmessage (name id-str options &rest fields)
  (declare (ignore options))
  (let* ((msg-name (intern (symbol-name name) 'keyword))
         (resolved-fields (apply #'concatenate 'list (loop for f in fields collect (resolve f))))
         (all-resolved-fields (apply #'concatenate 'list
                                     (mapcar #'resolve (concatenate 'list
                                                                    (getf *dict* :header-fields)
                                                                    fields
                                                                    (getf *dict* :trailer-fields))))))
    `(eval-when (:compile-toplevel :load-toplevel :execute)
       ,@(define-groups-from-fields msg-name all-resolved-fields)
       (add-message ,msg-name ,id-str ',resolved-fields))))

(defmacro defreader (&optional (name (intern "READ-FIX")))
  (let ((read-fix-fn-name (intern (symbol-name name))))
    `(progn
       (export ',read-fix-fn-name)
       (setf (symbol-function ',read-fix-fn-name) (%make-reader *dict*))
       (register-reader :dict *dict* :fn #',read-fix-fn-name))))

(defmacro defserializer (&optional (name (intern "SERIALIZE-FIX")))
  (let ((serialize-fix-fn-name (intern (symbol-name name))))
    `(progn
       (export ',serialize-fix-fn-name)
       (export ',(intern "STR"))
       (setf (symbol-function ',serialize-fix-fn-name) (%make-serializer *dict*))
       (setf (symbol-function ',(intern "STR")) (%make-serializer-str *dict*))
       (register-serializer :dict *dict* :fn #',serialize-fix-fn-name))))

(defmacro defvalidator (&optional (name (intern "VALIDATE")))
  (let ((validate-fn-name (intern (symbol-name name))))
    `(progn
       (export ',validate-fn-name)
       (setf (symbol-function ',validate-fn-name) (%make-validate-fn *dict*))
       (register-validator :dict *dict* :fn #',validate-fn-name))))
