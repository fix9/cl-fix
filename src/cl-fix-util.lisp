;;; Copyright 2020 Sandipan Razzaque
;;;
;;; Licensed under the Apache License, Version 2.0 (the "License");
;;; you may not use this file except in compliance with the License.
;;; You may obtain a copy of the License at
;;;
;;;     http://www.apache.org/licenses/LICENSE-2.0
;;;
;;; Unless required by applicable law or agreed to in writing, software
;;; distributed under the License is distributed on an "AS IS" BASIS,
;;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;;; See the License for the specific language governing permissions and
;;; limitations under the License.

(in-package :cl-fix-util)

(defun resolve-dict (dict-key)
  (or (gethash dict-key cl-fix-dict:*registry*)
      (error "Couldn't resolve dictionary for key ~A." dict-key)))

(defun getff (obj kw1 kw2)
  (getf (getf obj kw1) kw2))

(defun fix-field (msg key)
  (getf msg key))

(define-setf-expander fix-field (place prop &environment env)
  (multiple-value-bind (dummies vals newval setter getter)
      (get-setf-expansion place env)
    (declare (ignore newval setter))
    (let ((store (gensym)))
      (values dummies
              vals
              `(,store)
              `(setf (getf ,getter ,prop) ,store)
              `(,getter)))))

(defun make-fix-msg (msg-type &rest others)
  (list* :msg-type msg-type others))

(defun fix-msg-type (msg)
  (getf msg :msg-type))

(defun destroy-thread (thread)
  (when (and thread (bt:thread-alive-p thread))
    (log:info "Force killing ~A thread" (bt:thread-name thread))
    (handler-case (bt:destroy-thread thread)
      (error (e)
        (log:warn "Could not destroy thread ~A: ~A" (bt:thread-name thread) e)))))

