;;; Copyright 2020 Sandipan Razzaque
;;;
;;; Licensed under the Apache License, Version 2.0 (the "License");
;;; you may not use this file except in compliance with the License.
;;; You may obtain a copy of the License at
;;;
;;;     http://www.apache.org/licenses/LICENSE-2.0
;;;
;;; Unless required by applicable law or agreed to in writing, software
;;; distributed under the License is distributed on an "AS IS" BASIS,
;;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;;; See the License for the specific language governing permissions and
;;; limitations under the License.

(in-package #:cl-fix-types)

;; ===========================================================================================
;; Types start here
;; ===========================================================================================

(deftype currency () 'double-float)

(deftype country () 'string)

(deftype exchange () 'string)

(defstruct monthyear
  (year (error "Year must be specified") :type (integer 0 9999))
  (month (error "Month must be specified") :type (integer 1 12))
  ;; 0 means 'not set', for both the next fields.
  (day 0 :type (integer 0 31))
  (week-code 0 :type (integer 0 5)))

(deftype qty () 'double-float)

(deftype price () 'double-float)

(deftype priceoffset () 'double-float)

(deftype amt () 'double-float)

(deftype percentage () 'double-float)

(deftype int () 'fixnum)

(deftype len () '(integer 0))

(deftype seqnum () '(integer 0))

(deftype tagnum () '(integer 0))

(deftype dayofmonth () '(integer 0 31))

(deftype utctimestamp () 'local-time:timestamp)

(defstruct localmktdate
  (year (error "Year must be specified") :type (integer 0 9999))
  (month (error "Month must be specified") :type (integer 1 12))
  (day (error "Day must be specified") :type (integer 1 31)))

(defstruct utcdateonly
  (year (error "Year must be specified") :type (integer 0 9999))
  (month (error "Month must be specified") :type (integer 1 12))
  (day (error "Day must be specified") :type (integer 1 31)))

(defstruct utctimeonly
  (hour (error "Hour must be specified") :type (integer 0 23))
  (minute (error "Minute must be specified") :type (integer 0 59))
  (second (error "Second must be specified") :type (integer 0 60)) ;; 60 for leap seconds.
  (millisecond -1 :type (integer -1 999)))

(deftype data () 'string)

(deftype repeating-group () 'list)

(deftype numingroup () '(integer 0))

;; ===========================================================================================
;; Conversion functions start here
;; ===========================================================================================

(define-condition type-parse-error (error)
  ((raw-str :initarg :raw-str)
   (type :initarg :type)
   (underlying-error :initarg :error))
  (:report (lambda (condition stream)
             (format stream "Could not parse string '~A' to type ~A. Underlying error: ~A"
                     (slot-value condition 'raw-str)
                     (slot-value condition 'type)
                     (slot-value condition 'underlying-error)))))

(defun monthyear-to-string (my)
  (with-output-to-string (s)
    (format s "~4,'0d~2,'0d"
            (monthyear-year my)
            (monthyear-month my))
    (cond
      ((> (monthyear-day my) 0) (format s "~2,'0d" (monthyear-day my)))
      ((> (monthyear-week-code my) 0) (format s "w~d" (monthyear-week-code my))))))

(defun string-to-monthyear (str)
  (handler-case
      (progn
        (when (not (or (= 6 (length str))
                       (= 8 (length str))))
          (error "String must be either 6 or 8 characters."))
        (let ((val (make-monthyear :year (parse-integer (subseq str 0 4))
                                   :month (parse-integer (subseq str 4 6)))))
          (when-let ((last-part (and (= (length str) 8)
                                     (subseq str 6))))
            (if (eq (elt last-part 0) #\w)
                (setf (monthyear-week-code val) (parse-integer (subseq str 7)))
                (setf (monthyear-day val) (parse-integer last-part))))
          val))
    (t (err)
      (error 'type-parse-error :raw-str str :type 'monthyear :error err))))

(defun localmktdate-to-string (dt)
  (format nil "~4,'0D~2,'0D~2,'0D"
          (localmktdate-year dt)
          (localmktdate-month dt)
          (localmktdate-day dt)))

(defun %string-to-ymd-type (str type)
  (let ((fn (ecase type
              (localmktdate #'make-localmktdate)
              (utcdateonly #'make-utcdateonly))))
    (handler-case
        (progn
          (when (not (= 8 (length str)))
            (error "String should be exactly 8 characters."))
          (let ((parts (list :year (parse-integer (subseq str 0 4))
                             :month (parse-integer (subseq str 4 6))
                             :day (parse-integer (subseq str 6 8)))))
            (apply fn parts)))
      (t (err)
        (error 'type-parse-error :raw-str str :type type :error err)))))

(defun string-to-localmktdate (str)
  (%string-to-ymd-type str 'localmktdate))

(defun utcdateonly-to-string (dt)
  (format nil "~4,'0D~2,'0D~2,'0D"
          (utcdateonly-year dt)
          (utcdateonly-month dt)
          (utcdateonly-day dt)))

(defun string-to-utcdateonly (str)
  (%string-to-ymd-type str 'utcdateonly))

(defun utctimeonly-to-string (time)
  (with-output-to-string (s)
    (format s "~2,'0D:~2,'0D:~2,'0D"
            (utctimeonly-hour time)
            (utctimeonly-minute time)
            (utctimeonly-second time))
    (when (> (utctimeonly-millisecond time) -1)
      (format s ".~3,'0D" (utctimeonly-millisecond time)))))

(defun string-to-utctimeonly (str)
  ;; TODO: Ideally avoid dropping into cl-ppcre here, but until proven this will cause
  ;;       a performance hit will leave it alone.
  (handler-case
      (progn
        (when (not (cl-ppcre:scan "^\\d{2}:\\d{2}:\\d{2}(\\.\\d{3})?$" str))
          (error "A UTCTIMEONLY field must be in form HH:MM:SS[.mmm]"))
        (let ((val (make-utctimeonly :hour (parse-integer (subseq str 0 2))
                                     :minute (parse-integer (subseq str 3 5))
                                     :second (parse-integer (subseq str 6 8)))))
          (when-let ((millis (and (= (length str) 12)
                                  (parse-integer (subseq str 9)))))
            (setf (utctimeonly-millisecond val) millis))
          val))
    (t (err)
      (error 'type-parse-error :raw-str str :type 'utctimeonly :error err))))

(defun double-to-string (val)
  (format nil "~F" val))

(defun string-to-double (val)
  (handler-case
      (progn
        (when (not (cl-ppcre:scan "^\\-?\\d+(\\.(\\d)*)?$" val))
          (error "Not a valid decimal/float value."))
        (parse-number:parse-number val :float-format 'double-float))
    (t (err)
      (error 'type-parse-error :raw-str val :type 'double-float :error err))))

(defun string-to-int (val)
  (handler-case
      (progn
        (when (not (cl-ppcre:scan "^(\\-)?\\d+$" val))
          (error "Not a valid integer value."))
        (parse-integer val))
    (t (err)
      (error 'type-parse-error :raw-str val :type 'integer :error err))))

(defun string-to-char (val)
  (handler-case
      (progn
        (when (not (= 1 (length val)))
          (error "Expected a string of length 1"))
        (when (eq #\Soh (elt val 0))
          (error "Character cannot be the field delimiter."))
        (elt val 0))
    (t (err)
      (error 'type-parse-error :raw-str val :type 'integer :error err))))

(defun timestamp-to-string (ts)
  (local-time:format-timestring
   nil
   ts
   :format '(:year (:month 2) (:day 2)
             "-"
             (:hour 2) ":" (:min 2) ":" (:sec 2)
             "."
             (:msec 3))))

(defun string-to-timestamp (s)
  (handler-case
      (let* ((year (subseq s 0 4))
             (month (subseq s 4 6))
             (day (subseq s 6 8))
             (rest (subseq s 9))
             (timestr (format nil "~A-~A-~AT~A"
                              year month day rest)))
        ;; NOTE: Cannot use local-time:parse-timestring directly because
        ;;       it cannot deal with YYYYMMDD not having any delimiters.
        ;;       Hence the transform above.
        (local-time:parse-timestring timestr))
    (t (err)
      (error 'type-parse-error :raw-str s :type 'integer :error err))))

(defun boolean-to-string (val)
  (if val #\Y #\N))

(defun string-to-boolean (str)
  (cond
    ((string= "Y" str) t)
    ((string= "N" str) nil)
    (t (error 'type-parse-error
              :raw-str str
              :type 'boolean
              :error "Not a valid boolean. Must be 'Y' or 'N'"))))

(defun get-to-string-converter (type)
  (let ((found t))
    (let ((converter
           (case type
             ((double-float
               qty
               currency
               price
               priceoffset
               amt
               percentage)
              #'double-to-string)
             ((local-time:timestamp
               utctimestamp)
              #'timestamp-to-string)
             (boolean #'boolean-to-string)
             (utcdateonly #'utcdateonly-to-string)
             (localmktdate #'localmktdate-to-string)
             (utctimeonly #'utctimeonly-to-string)
             (monthyear #'monthyear-to-string)
             ((string
               fixnum
               character
               int
               len
               seqnum
               tagnum
               dayofmonth
               numingroup
               exchange
               country
               data)
              nil)
             (t (setf found nil)))))
      (values converter found))))

(defun get-from-string-converter (type)
  (let ((found t))
    (let ((converter
           (case type
             ((double-float
               qty
               currency
               price
               priceoffset
               amt
               percentage)
              #'string-to-double)
             ((local-time:timestamp
               utctimestamp)
              #'string-to-timestamp)
             ((fixnum
               int
               len
               seqnum
               tagnum
               dayofmonth
               numingroup)
              #'string-to-int)
             (boolean #'string-to-boolean)
             (character #'string-to-char)
             (localmktdate #'string-to-localmktdate)
             (utcdateonly #'string-to-utcdateonly)
             (utctimeonly #'string-to-utctimeonly)
             (monthyear #'string-to-monthyear)
             ((string
               exchange
               country
               data)
              nil)
             (t
              (setf found nil)
              nil))))
      (values converter found))))
