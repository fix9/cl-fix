;;; Copyright 2020 Sandipan Razzaque
;;;
;;; Licensed under the Apache License, Version 2.0 (the "License");
;;; you may not use this file except in compliance with the License.
;;; You may obtain a copy of the License at
;;;
;;;     http://www.apache.org/licenses/LICENSE-2.0
;;;
;;; Unless required by applicable law or agreed to in writing, software
;;; distributed under the License is distributed on an "AS IS" BASIS,
;;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;;; See the License for the specific language governing permissions and
;;; limitations under the License.

(ql:quickload '(:cl-fix :cl-fix/fix44))

(defpackage #:acceptor
  (:use :cl)
  (:export start
           stop))

(in-package #:acceptor)
(setf local-time:*default-timezone* local-time:+utc-zone+)
(log:config :debug) ;; Shows all IN/OUT messages in the console

(defparameter *acceptor* nil)
(defparameter *sessions* nil)

(defun my-handler (session msg)
  (log:info "Received message on session ~A yay: ~A" (slot-value session 'cl-fix:target-comp-id) msg))

(defclass app-session (cl-fix:session)
  ()
  (:default-initargs
   :heartbeat-interval-ms 30000
   :session-store (ensure-directories-exist #P"/tmp/fix-sessions/")
   :dict-key :fix44
   :tick-resolution-ms 100
   :handler #'my-handler
   :reset-seq-num t))

(defun start ()
  (setf *acceptor* (make-instance 'cl-fix:acceptor :host "127.0.0.1" :port 3838 :dict-key :fix44)
        *sessions*   (list (make-instance 'app-session :sender-comp-id "acceptor" :target-comp-id "initiator")
                           (make-instance 'app-session :sender-comp-id "acceptor" :target-comp-id "another-initiator")))
  (dolist (s *sessions*)
    (cl-fix:add-session *acceptor* s))
  (cl-fix:start-connector *acceptor*))

(defun stop ()
  (dolist (s *sessions*)
    (cl-fix:stop-session s))
  (cl-fix:stop-connector *acceptor*))
