;;; Copyright 2020 Sandipan Razzaque
;;;
;;; Licensed under the Apache License, Version 2.0 (the "License");
;;; you may not use this file except in compliance with the License.
;;; You may obtain a copy of the License at
;;;
;;;     http://www.apache.org/licenses/LICENSE-2.0
;;;
;;; Unless required by applicable law or agreed to in writing, software
;;; distributed under the License is distributed on an "AS IS" BASIS,
;;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;;; See the License for the specific language governing permissions and
;;; limitations under the License.

(ql:quickload '(:cl-fix :cl-fix/fix44))

(defpackage #:initiator
  (:use :cl)
  (:export start
           stop))

(in-package #:initiator)

(setf local-time:*default-timezone* local-time:+utc-zone+)

(log:config :debug) ;; Shows all IN/OUT messages in the console

(defparameter *initiator* nil)
(defparameter *session* nil)
(defparameter *received* nil)

(defun my-handler (session msg)
  (case (cl-fix:fix-msg-type msg)
    (:logon
     (log:info "Received LOGON. Sending a NOS.")
     (cl-fix:enqueue-msg session
                         (cl-fix:make-fix-msg :new-order-single
                                              :cl-ord-id "order-1"
                                              :side :buy
                                              :transact-time cl-fix:*current-time*
                                              :symbol "AMZN"
                                              :ord-type :market)))
    (:execution-report
     (log:info "Received ER: ~A" (util:fix-field msg :order-id))
     (push msg *received*))))

;; This is optional. You can safely omit this if you don't need it.
;; Just be sure the drop the corresponding :tick-handler initarg below.
;; This gets run every ':tick-resolution-ms' in the defined initiator.
(cl-fix:deftickhandler my-tick-handler (session)
  (log:trace "Time now is: ~A" cl-fix:*current-time*))

(defclass app-session (cl-fix:session)
  ()
  (:default-initargs
   :heartbeat-interval-ms 30000
   :session-store (ensure-directories-exist #P"/tmp/fix-sessions/")
   :sender-comp-id "initiator"
   :target-comp-id "acceptor"
   :dict-key :fix44
   :handler #'my-handler
   :reset-seq-num t
   :tick-handler #'my-tick-handler))

(defun start ()
  (setf *initiator* (make-instance 'cl-fix:initiator :dict-key :fix44 :host "localhost" :port 3838)
        *session*  (make-instance 'app-session)
        *received*  nil)
  (cl-fix:add-session *initiator* *session*)
  (cl-fix:start-connector *initiator*))

(defun stop ()
  (cl-fix:stop-session *session*)
  (cl-fix:stop-connector *initiator*))
