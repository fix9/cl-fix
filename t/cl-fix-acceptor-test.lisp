;;; Copyright 2020 Sandipan Razzaque
;;;
;;; Licensed under the Apache License, Version 2.0 (the "License");
;;; you may not use this file except in compliance with the License.
;;; You may obtain a copy of the License at
;;;
;;;     http://www.apache.org/licenses/LICENSE-2.0
;;;
;;; Unless required by applicable law or agreed to in writing, software
;;; distributed under the License is distributed on an "AS IS" BASIS,
;;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;;; See the License for the specific language governing permissions and
;;; limitations under the License.

(in-package #:cl-fix-acceptor-test)

(def-suite acceptor :in :cl-fix-suite)
(in-suite acceptor)

(defparameter *store-dir* #P"/tmp/cl-fix-test/acceptor-store/")
(defparameter *magic-number* 1234.0d0)

(defun handler (session msg)
  (with-slots (cl-fix:target-comp-id received-msgs) session
    (log:debug "~A: Received ~A" cl-fix:target-comp-id (fix44:str msg))
    (case (util:fix-msg-type msg)
      (:new-order-single
       (let ((qty (util:fix-field msg :order-qty)))
         (when (= *magic-number* qty)
           (cl-fix-session:enqueue-msg session (util:make-fix-msg :execution-report
                                                                  :order-id "1"
                                                                  :exec-id "1"
                                                                  :exec-type :fill
                                                                  :ord-status :filled
                                                                  :side (util:fix-field msg :side)
                                                                  :leaves-qty 0.0d0
                                                                  :cum-qty (util:fix-field msg :order-qty)
                                                                  :avg-px 1.47893d0))))))
    (push msg received-msgs)))

(defclass my-session (cl-fix:session)
  ((received-msgs :initarg :received-msgs :initform nil))
  (:default-initargs
   :dict-key :fix44
   :sender-comp-id "acceptor"
   :handler #'handler
   :session-store *store-dir*))

(defmethod cl-fix:validate-logon ((session my-session) msg)
  (unless
      (and (string= "bob" (util:fix-field msg :username))
           (string= "password" (util:fix-field msg :password)))
    "Invalid credentials"))

(def-fixture acceptor-test (&key (start-acceptor t))
  (let* ((acceptor (make-instance 'cl-fix:acceptor
                                  :dict-key :fix44
                                  :host "unused"
                                  :port 0
                                  :single-threaded-mode t))
         (session-1 (make-instance 'my-session :target-comp-id "client-1"))
         (session-2 (make-instance 'my-session :target-comp-id "client-2")))
    (cl-fix:add-session acceptor session-1)
    (cl-fix:add-session acceptor session-2)
    (when start-acceptor (cl-fix:start-connector acceptor))
    (symbol-macrolet ((session-1-msgs (slot-value session-1 'received-msgs))
                      (session-2-msgs (slot-value session-2 'received-msgs)))
      (labels ((run-loop ()
                 (cl-fix-acceptor::%run-loop acceptor))
               (mock-incoming-connection (stream)
                 (cl-fix-acceptor::%notify-client acceptor stream))
               (send-msg (stream msg &optional (run-loop t))
                 (let ((flipped (test-util:flip stream)))
                   (fix44:serialize-fix msg flipped)
                   (force-output flipped))
                 (when run-loop (run-loop))))
        (uiop:delete-directory-tree *store-dir* :validate t :if-does-not-exist :ignore)
        (ensure-directories-exist *store-dir*)
        (unwind-protect
             (&body)
          (uiop:delete-directory-tree *store-dir* :validate t :if-does-not-exist :ignore))))))

(defun make-logon (client seq-num &key (username "bob") (password "password"))
  (util:make-fix-msg :logon
                     :sending-time (local-time:now)
                     :sender-comp-id client
                     :target-comp-id "acceptor"
                     :msg-seq-num seq-num
                     :reset-seq-num-flag t
                     :encrypt-method :none
                     :heart-bt-int 30
                     :username username
                     :password password))

(defun make-logout (client seq-num)
  (util:make-fix-msg :logout
                     :sending-time (local-time:now)
                     :sender-comp-id client
                     :target-comp-id "acceptor"
                     :msg-seq-num seq-num
                     :text "Logging out."))

(defun make-nos (client seq-num &optional (qty 100.0d0))
  (util:make-fix-msg :new-order-single
                     :cl-ord-id "1"
                     :side :buy
                     :sender-comp-id client
                     :target-comp-id "acceptor"
                     :msg-seq-num seq-num
                     :sending-time (local-time:now)
                     :transact-time (local-time:now)
                     :symbol "AUDUSD"
                     :order-qty qty
                     :ord-type :market))

(def-test test-incoming-connection (:fixture acceptor-test)
  (let ((mock-stream (make-instance 'test-util:flip-stream)))
    (mock-incoming-connection mock-stream)
    (send-msg mock-stream (make-logon "client-1" 1))
    (send-msg mock-stream (make-nos "client-1" 2))
    (is (= 1 (length session-1-msgs)))
    (is (eq :new-order-single (util:fix-msg-type (first session-1-msgs))))))

(def-test test-two-incoming-connections (:fixture acceptor-test :depends-on test-incoming-connection)
  (let ((stream1 (make-instance 'test-util:flip-stream))
        (stream2 (make-instance 'test-util:flip-stream)))
    (mock-incoming-connection stream1)
    (mock-incoming-connection stream2)
    (send-msg stream1 (make-logon "client-1" 1) nil)
    (send-msg stream2 (make-logon "client-2" 1) nil)
    (send-msg stream1 (make-nos "client-1" 2) nil)
    (send-msg stream2 (make-nos "client-2" 2))
    (is (= 1 (length session-1-msgs)))
    (is (= 1 (length session-2-msgs)))    
    (is (eq :new-order-single (util:fix-msg-type (first session-1-msgs))))
    (is (eq :new-order-single (util:fix-msg-type (first session-2-msgs))))))

(def-test test-session-disconnect-graceful (:fixture acceptor-test :depends-on test-incoming-connection)
  (let ((stream (make-instance 'test-util:flip-stream)))
    (mock-incoming-connection stream)
    (send-msg stream (make-logon "client-1" 1))
    (send-msg stream (make-logout "client-1" 2))
    (is (cl-fix-session:stoppedp session-1))))

(def-test test-session-disconnect-ungraceful (:fixture acceptor-test :depends-on test-incoming-connection)
  (let ((stream (make-instance 'test-util:flip-stream)))
    (mock-incoming-connection stream)
    (send-msg stream (make-logon "client-1" 1))
    (send-msg stream (make-nos "client-1" 2) nil)
    (close stream)
    (run-loop)
    (is (cl-fix-session:stoppedp session-1))))

(def-test test-session-disconnect-is-isolated (:fixture acceptor-test :depends-on test-two-incoming-connections)
  (let ((stream1 (make-instance 'test-util:flip-stream))
        (stream2 (make-instance 'test-util:flip-stream)))
    (mock-incoming-connection stream1)
    (mock-incoming-connection stream2)
    (send-msg stream1 (make-logon "client-1" 1) nil)
    (send-msg stream2 (make-logon "client-2" 1))
    (send-msg stream1 (make-nos "client-1" 2) nil)
    (close stream1)
    (send-msg stream2 (make-nos "client-2" 2) nil)
    (run-loop)
    (is (cl-fix-session:stoppedp session-1))
    (is (not (cl-fix-session:stoppedp session-2)))
    (is (= 0 (length session-1-msgs)))
    (is (= 1 (length session-2-msgs)))))

(def-test duplicate-session-connection (:fixture acceptor-test)
  (let ((stream1 (make-instance 'test-util:flip-stream))
        (stream2 (make-instance 'test-util:flip-stream)))
    (mock-incoming-connection stream1)
    (send-msg stream1 (make-logon "client-1" 1))
    (mock-incoming-connection stream2)
    (send-msg stream2 (make-logon "client-1" 1))
    (is (test-util:closed-p stream1) "First stream should have been closed")
    (is (not (test-util:closed-p stream2)) "Second stream should be open")))

(def-test duplicate-session-invalid-logon (:fixture acceptor-test)
  (let ((stream1 (make-instance 'test-util:flip-stream))
        (stream2 (make-instance 'test-util:flip-stream)))
    (mock-incoming-connection stream1)
    (send-msg stream1 (make-logon "client-1" 1))
    (mock-incoming-connection stream2)
    (send-msg stream2 (make-logon "client-1" 1 :password "wrong"))
    (is (not (test-util:closed-p stream1)) "First stream should have been kept open")
    (is (test-util:closed-p stream2) "Second stream should have been closed due to invalid credentials")))

(def-test invalid-session-credentials (:fixture acceptor-test)
  (let ((stream (make-instance 'test-util:flip-stream)))
    (mock-incoming-connection stream)
    (send-msg stream (make-logon "client-1" 1 :username "bob" :password "wrong"))
    (test-util:unclose-stream stream)
    (let ((msg (fix44:read-fix (test-util:flip stream))))
      (is-true msg "Acceptor should send a LOGOUT.")
      (is (eq :logout (util:fix-msg-type msg)))
      (is (string= "Invalid credentials" (util:fix-field msg :text))))))

(def-test acceptor-resend (:fixture acceptor-test)
  (let* ((mock-stream (make-instance 'test-util:flip-stream))
         (fstr (test-util:flip mock-stream)))
    (mock-incoming-connection mock-stream)
    (send-msg mock-stream (make-logon "client-1" 1))
    (send-msg mock-stream (make-nos "client-1" 2 *magic-number*))
    (let ((msg1 (fix44:read-fix fstr))
          (msg2 (fix44:read-fix fstr)))
      (is (every #'eq
                 (mapcar #'util:fix-msg-type (list msg1 msg2))
                 '(:logon :execution-report))))
    (send-msg mock-stream (util:make-fix-msg :resend-request
                                             :msg-seq-num 3
                                             :sending-time (local-time:now)
                                             :sender-comp-id "client-1"
                                             :target-comp-id "acceptor"
                                             :begin-seq-no 1
                                             :end-seq-no 0))
    (let ((next-msg1 (fix44:read-fix fstr))
          (next-msg2 (fix44:read-fix fstr)))
      (is (every #'eq
                 (mapcar #'util:fix-msg-type (list next-msg1 next-msg2))
                 '(:sequence-reset :execution-report))
          "Expected a sequence reset then exec-rpt. Got: ~A"
          (mapcar #'util:fix-msg-type (list next-msg1 next-msg2)))
      (is (every (lambda (x)
                   (util:fix-field x :poss-resend))
                 (list next-msg1 next-msg2))))))
