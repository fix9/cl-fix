;;; Copyright 2020 Sandipan Razzaque
;;;
;;; Licensed under the Apache License, Version 2.0 (the "License");
;;; you may not use this file except in compliance with the License.
;;; You may obtain a copy of the License at
;;;
;;;     http://www.apache.org/licenses/LICENSE-2.0
;;;
;;; Unless required by applicable law or agreed to in writing, software
;;; distributed under the License is distributed on an "AS IS" BASIS,
;;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;;; See the License for the specific language governing permissions and
;;; limitations under the License.

(defpackage #:cl-fix-test
  (:use #:cl #:5am))

(defpackage #:cl-fix-dict-test
  (:use #:cl #:cl-fix #:5am))

(defpackage #:cl-fix-test-util
  (:use #:cl)
  (:nicknames :test-util)
  (:export array-stream
           flip-stream
           flip
           await-input
           with-fn-rebind
           unclose-stream
           closed-p)
  (:import-from #:trivial-gray-streams
                fundamental-output-stream
                fundamental-input-stream
                stream-force-output
                stream-listen
                stream-read-char
                stream-write-char
                stream-write-sequence
                stream-write-string))

(defpackage #:cl-fix-initiator-test
  (:use #:cl #:5am)
  (:import-from #:cl-fix
                initiator
                session
                enqueue-msg
                add-session
                start-connector
                stop-connector
                init-session
                stop-session)
  (:import-from #:cl-fix-test-util
                await-input
                flip-stream
                flip))

(defpackage #:cl-fix-acceptor-test
  (:use #:cl #:5am)
  (:import-from :cl-fix-test-util
                with-fn-rebind))

(defpackage #:cl-fix-integration-test
  (:use #:cl #:5am)
  (:import-from #:cl-fix
                initiator
                session
                enqueue-msg
                add-session
                start-connector
                stop-connector
                init-session
                stop-session))

(defpackage #:cl-fix-session-test
  (:use #:cl #:5am)
  (:import-from :cl-fix-test-util
                with-fn-rebind))

(defpackage #:cl-fix-message-store-test
  (:use #:cl #:5am))

(defpackage #:cl-fix-fsm-test
  (:use #:cl #:5am #:cl-fix-fsm))

(defpackage #:cl-fix-types-test
  (:use #:cl #:5am #:cl-fix-types))

(defpackage #:cl-fix-validation-test
  (:use #:cl #:5am #:arrow-macros))

(defpackage #:fastbuffer-test
  (:use #:cl #:5am #:fastbuffer))

