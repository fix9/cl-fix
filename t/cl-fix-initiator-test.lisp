;;; Copyright 2020 Sandipan Razzaque
;;;
;;; Licensed under the Apache License, Version 2.0 (the "License");
;;; you may not use this file except in compliance with the License.
;;; You may obtain a copy of the License at
;;;
;;;     http://www.apache.org/licenses/LICENSE-2.0
;;;
;;; Unless required by applicable law or agreed to in writing, software
;;; distributed under the License is distributed on an "AS IS" BASIS,
;;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;;; See the License for the specific language governing permissions and
;;; limitations under the License.

(in-package #:cl-fix-initiator-test)

(def-suite initiator :in :cl-fix-suite)
(in-suite initiator)

(defparameter *sender* "test-sender")
(defparameter *target* "test-target")
(defparameter *dir* #P"/tmp/cl-fix-test/")

;; Supporting functions & infrastructure start here

(defclass test-initiator (initiator)
  ()
  (:default-initargs
   :stream (make-instance 'flip-stream)
   :host "unused"
   :port 999
   :dict-key :fix44))

(defun handler (session msg)
  (with-slots (handler-received) session
    (push msg handler-received))
  (case (util:fix-field msg :msg-type)
    (:new-order-single
     (enqueue-msg session
                  (list :msg-type :execution-report
                        :order-id "order-id-1"
                        :exec-id "exec-id-1"
                        :exec-type :done-for-day
                        :ord-status :filled
                        :side :buy
                        :leaves-qty 0.0d0
                        :cum-qty 100.0d0
                        :avg-px 83.43d0)))))

(defclass test-session (session)
  ((handler-received :initform ()))
  (:default-initargs
   :heartbeat-interval-ms 30000
   :dict-key :fix44
   :session-store (ensure-directories-exist *dir*)
   :sender-comp-id *sender*
   :target-comp-id *target*
   :logon-fields '(:username "bob"
                   :password "password")
   :handler #'handler))

(defun dump-msgs (ls)
  (log:debug "~{~A~%~}" (mapcar #'fix44:str ls)))

(defun resend-fields-are-set (msg)
  (is (util:fix-field msg :msg-seq-num))
  (is (util:fix-field msg :poss-dup-flag))
  (is (util:fix-field msg :poss-resend))
  (is (util:fix-field msg :orig-sending-time)))

(defun make-serializer (sender-comp target-comp start-seq)
  (let ((msg-seq-num start-seq))
    (lambda (msg stream &optional seq-num-override)
      (when (not (util:fix-field msg :msg-seq-num))
        (setf (util:fix-field msg :msg-seq-num) (or seq-num-override (incf msg-seq-num))))
      (setf (util:fix-field msg :sender-comp-id) sender-comp
            (util:fix-field msg :target-comp-id) target-comp
            (util:fix-field msg :sending-time) (local-time:now))
      (fix44:serialize-fix msg stream))))

(defun make-logon ()
  (util:make-fix-msg :logon
                     :username "bob"
                     :password "password"
                     :encrypt-method 0
                     :heart-bt-int 30))

(defun make-resend-request (begin end)
  (util:make-fix-msg :resend-request
                     :begin-seq-no begin
                     :end-seq-no end))

(defun make-heartbeat ()
  (util:make-fix-msg :heartbeat))

(defun make-test-request (id)
  (util:make-fix-msg :test-request :test-req-id id))

(defun make-nos (sym side qty prc)
  (util:make-fix-msg :new-order-single
                     :cl-ord-id "1"
                     :side side
                     :transact-time (local-time:now)
                     :symbol sym
                     :order-qty qty
                     :ord-type (if prc :limit :market)))

(def-fixture initiator-environment ()
  (uiop:delete-directory-tree *dir* :validate t :if-does-not-exist :ignore)
  (let* ((app-sent ())
         (initiator (make-instance 'test-initiator :single-threaded-mode t))
         (session (let ((session (make-instance 'test-session)))
                    (cl-fix:add-session initiator session)
                    session))
         (stream (flip (slot-value initiator 'cl-fix-initiator::stream)))
         (outgoing-serializer (make-serializer *target* *sender* 0)))
    (labels ((%run-loop (&optional (reset-msgs t))
               (when reset-msgs
                 (setf (slot-value session 'handler-received) ()
                       app-sent ()))
               (cl-fix-initiator::%run-loop initiator)
               (loop while (listen stream)
                  do
                    (push (fix44:read-fix stream) app-sent)))
             (send-to-app (msg seq-num &optional (reset-msgs t))
               (funcall outgoing-serializer msg stream seq-num)
               (force-output stream)
               (%run-loop reset-msgs)))
      (with-slots (handler-received) session
        (unwind-protect
             (progn
               (cl-fix-connector:start-connector initiator)
               (%run-loop)
               (&body))
          (progn
            (cl-fix:stop-connector initiator)
            (uiop:delete-directory-tree *dir* :validate t :if-does-not-exist :ignore)))))))

(def-test initiator-basic (:fixture initiator-environment)
  "Basic logon then test request."
  (is (= 1 (length app-sent)) "App should have sent a logon-msg on start-up")
  (is (string= "bob" (util:fix-field (car app-sent) :username))
      "Username should have been set as per session's :logon-fields")
  (is (string= "password" (util:fix-field (car app-sent) :password))
      "Password should have been set as per session's :logon-fields")
  (send-to-app (make-logon) 1)
  (is (= 1 (length handler-received)) "App should have received logon-msg")
  (send-to-app (make-test-request "hi") 2)
  (is (= 0 (length handler-received)))
  (is (= 1 (length app-sent)))
  (is (eq :heartbeat (util:fix-field (first app-sent) :msg-type))))

(def-test initiator-resend (:fixture initiator-environment :depends-on initiator-basic)
  "The underlying session should request a resend if it receives a sequence number that is too high."
  (send-to-app (make-logon) 1)
  (send-to-app (make-heartbeat) 3)
  (is (= 0 (length handler-received)) "Our handler should never have received the message with an invalid seq num.")
  (is (= 1 (length app-sent)) "The app should have sent out a resend-request")
  (is (eq :resend-request (util:fix-msg-type (first app-sent))))
  (is (= 2 (util:fix-field (first app-sent) :begin-seq-no)))
  (is (= 0 (util:fix-field (first app-sent) :end-seq-no))))

(def-test initiator-being-requested-resend (:fixture initiator-environment :depends-on initiator-basic)
  "A session should be able to respond to a request to resend messages between given sequence numbers."
  (let ((captured ()))
    (send-to-app (make-logon) 1)
    (send-to-app (make-nos "AUDUSD" :buy 100d0 12.03) 2)
    (send-to-app (make-nos "AUDUSD" :sell 20d0 12.02) 3 nil)
    (send-to-app (make-nos "AUDUSD" :sell 20d0 12.02) 4 nil)
    (is (= 3 (length app-sent)))
    (setf captured (copy-list app-sent))
    (send-to-app (make-resend-request 1 0) 5)
    (is (= 4 (length app-sent)) "App should have resent 4 messages upon request. Got ~A." (length app-sent))
    (mapc #'resend-fields-are-set app-sent)
    (mapc (lambda (x y) (is (= (util:fix-field x :msg-seq-num) (util:fix-field y :msg-seq-num))
                            "Captured messages before/after resend should have identical seq nums"))
          captured
          app-sent)))

(def-test initiator-resend-with-gap-fill (:fixture initiator-environment
                                                   :depends-on initiator-being-requested-resend)
  (send-to-app (make-logon) 1)
  (send-to-app (make-test-request "test-1") 2)
  (send-to-app (make-test-request "test-2") 3)
  (send-to-app (make-resend-request 1 0) 4)
  (is (= 3 (length app-sent)) "App should have sent 3 messages in response to resend request. Got ~A." (length app-sent))
  (let ((types (reverse (mapcar #'util:fix-msg-type app-sent))))
    (is (eq :sequence-reset (elt types 0)))
    (is (eq :sequence-reset (elt types 1)))
    (is (eq :sequence-reset (elt types 2)))))

(def-test initiator-multithreaded ()
  "Initiator basic behaviour should work in the default (multithreaded) mode."
  (let* ((initiator (make-instance 'test-initiator))
         (stream (flip (slot-value initiator 'cl-fix-initiator::stream)))
         (outgoing-serializer (make-serializer *target* *sender* 0)))
    (cl-fix:add-session initiator (make-instance 'test-session))
    (flet ((send (msg)
             (funcall outgoing-serializer msg stream)
             (force-output stream)))
      (unwind-protect
           (progn
             (cl-fix:start-connector initiator)
             (send (make-logon))
             (send (make-test-request "test1"))
             (send (make-nos "AUDUSD" :buy 100d0 12.03))
             (sleep 1)
             (destructuring-bind (m1 m2 m3) (loop repeat 3 collect (fix44:read-fix stream))
               (is (equal :logon (util:fix-msg-type m1)))
               (is (equal :heartbeat (util:fix-msg-type m2)))
               (is (equal :execution-report (util:fix-msg-type m3)))))
        (when initiator (cl-fix:stop-connector initiator))))))
