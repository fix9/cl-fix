(in-package #:cl-fix-fsm-test)

(def-suite cl-fix-fsm :in :cl-fix-suite)

(in-suite cl-fix-fsm)

(defun %make-running-thread (fsm start-event end-event)
  (lambda ()
    (fsm-process-event fsm start-event)
    (loop
       repeat 1000
       while (not (eq (fsm-current-state fsm) :stopping))
       do (sleep 0.001))
    (fsm-process-event fsm end-event)))

(defun %start-threads (fsm)
  (list
   (bt:make-thread (%make-running-thread fsm :io-ready :io-stopped))
   (bt:make-thread (%make-running-thread fsm :worker-ready :worker-stopped))))

(defun %check-readiness (fsm)
  (with-slots (io-ready worker-ready) fsm
    (when (and io-ready worker-ready)
      (fsm-process-event fsm :all-ready))))

(defun %check-stopped (fsm)
  (log:debug "Checking stoppedness")
  (with-slots (io-stopped worker-stopped) fsm
    (when (and io-stopped worker-stopped)
      (log:debug "Yep, we're proper stopped")
      (fsm-process-event fsm :all-stopped)))
  (log:debug "Done checking stoppedness"))

(defclass app-fsm (fsm)
  ((io-ready :initform nil)
   (worker-ready :initform nil)
   (io-stopped :initform nil)
   (worker-stopped :initform nil))
  (:default-initargs
   :fsm-name "test"
    :fsm-starting-state :init
    :fsm-transitions
    (list
     (cl-fix-fsm:make-transition :from-state :init     :event :start-requested :to-state :starting)
     (cl-fix-fsm:make-transition :from-state :starting :event :worker-ready    :to-state :starting :fn (lambda (fsm)
                                                                                                         (setf (slot-value fsm 'worker-ready) t)
                                                                                                         (%check-readiness fsm)))
     (cl-fix-fsm:make-transition :from-state :starting :event :io-ready        :to-state :starting :fn (lambda (fsm)
                                                                                                         (setf (slot-value fsm 'io-ready) t)
                                                                                                         (%check-readiness fsm)))
     (cl-fix-fsm:make-transition :from-state :starting :event :all-ready       :to-state :running)
     (cl-fix-fsm:make-transition :from-state :running  :event :stop-requested  :to-state :stopping)
     (cl-fix-fsm:make-transition :from-state :stopping :event :io-stopped      :to-state :stopping :fn (lambda (fsm)
                                                                                                         (setf (slot-value fsm 'io-stopped) t)
                                                                                                         (log:info "IO stopped!")
                                                                                                         (%check-stopped fsm)))
     (cl-fix-fsm:make-transition :from-state :stopping :event :worker-stopped  :to-state :stopping :fn (lambda (fsm)
                                                                                                         (setf (slot-value fsm 'worker-stopped) t)
                                                                                                         (log:info "Worker stopped!")
                                                                                                         (%check-stopped fsm)))
     (cl-fix-fsm:make-transition :from-state :stopping :event :all-stopped     :to-state :stopped)
     (cl-fix-fsm:make-transition :from-state :stopped  :event :start-requested :to-state :starting))))

(def-test fsm-thread-safety ()
  (let ((app-fsm (make-instance 'app-fsm)))
    (fsm-process-event app-fsm :start-requested)
    (is (eq :starting (fsm-current-state app-fsm)))
    (let ((threads (%start-threads app-fsm)))
      (loop repeat 1000 while (eq (fsm-current-state app-fsm) :starting) do (sleep 0.01))
      (is (eq :running (fsm-current-state app-fsm)))
      (fsm-process-event app-fsm :stop-requested)
      (loop repeat 1000 while (eq (fsm-current-state app-fsm) :stopping) do (sleep 0.01))
      (is (eq :stopped (fsm-current-state app-fsm)))
      (mapc #'bt:join-thread threads))))

