;;; Copyright 2020 Sandipan Razzaque
;;;
;;; Licensed under the Apache License, Version 2.0 (the "License");
;;; you may not use this file except in compliance with the License.
;;; You may obtain a copy of the License at
;;;
;;;     http://www.apache.org/licenses/LICENSE-2.0
;;;
;;; Unless required by applicable law or agreed to in writing, software
;;; distributed under the License is distributed on an "AS IS" BASIS,
;;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;;; See the License for the specific language governing permissions and
;;; limitations under the License.

(in-package #:cl-fix-message-store-test)

(def-suite message-store :in :cl-fix-suite)
(in-suite message-store)

(defparameter *dir* #P"/tmp/cl-fix-test/")
(defparameter *store-file* #P"/tmp/cl-fix-test/store-test.dat")

(defun make-hb (seq-num)
  (fix44:str (util:make-fix-msg :heartbeat
                                :msg-seq-num seq-num
                                :sender-comp-id "sender"
                                :target-comp-id "target"
                                :sending-time (local-time:now))))

(defparameter *num-entries* 100)

(defun open-stream ()
  (open *store-file* :direction :io :if-exists :append :if-does-not-exist :create))

(def-fixture file-store-fixture ()
  (ensure-directories-exist *dir*)
  (uiop:delete-file-if-exists *store-file*)
  (let ((stream (open-stream))
        (store))
    (flet ((reset-store (&optional (first-pos 0))
             (close stream)
             (setf stream (open-stream))
             (setf store (store:make-file-store :stream stream
                                                :first-msg-position first-pos))))
      (unwind-protect
           (progn
             (reset-store)
             (&body))
        (progn
          (handler-case (store:close-store store) (t () nil))
          (close stream)
          (uiop:delete-file-if-exists *store-file*))))))

(def-test index-rebuild-test (:fixture file-store-fixture)
  (let ((messages (loop for i from 1 upto *num-entries* collect (list i (make-hb i))))
        (i 1)
        (retrieved))
    (dolist (m messages)
      (destructuring-bind (idx str) m
        (store:store-msg store idx str)))
    (is (> (file-length stream) 0) "Messages should have been written to the file.")
    (reset-store)
    (store:rebuild-index store (util:resolve-dict :fix44))
    (store:iterate store 1 (1+ *num-entries*)
                   (lambda (fix-str)
                     (push (list i (alexandria:copy-array fix-str)) retrieved)
                     (incf i))
                   (lambda (x y)
                     (declare (ignore x y))))
    (setf retrieved (reverse retrieved))
    (is (= (length retrieved) (length messages)))
    (is (every #'equalp messages retrieved) "Every retrieved message should be the same as every originally stored message.")
    (store:close-store store)))

(defparameter *num-entries-perf* 100000)

(def-test index-rebuild-test-perf (:fixture file-store-fixture :suite :cl-fix-perf)
  (loop for i from 1 upto *num-entries-perf* do
       (store:store-msg store i (make-hb i)))
  (reset-store)
  (let ((time-start (local-time:now))
        (time-end))
    (store:rebuild-index store (util:resolve-dict :fix44))
    (setf time-end (local-time:now))
    (let ((time-elapsed (local-time:timestamp-difference time-end time-start)))
      (is (< time-elapsed 5)
          "Index rebuild for 100k messages should take <5 secs on sbcl. (took ~A seconds)" time-elapsed))))

(def-test gap-fill-test (:fixture file-store-fixture)
  (let ((store (store:make-file-store :stream stream
                                      :first-msg-position 0))
        (received-seq-nums)
        (received-gap-start)
        (received-gap-end))
    (loop for i from 1 upto 10
       when (not (<= 3 i 7))
       do
         (store:store-msg store i (make-hb i)))
    (store:iterate store 1 11
                   (lambda (buf)
                     (push (util:fix-field (with-input-from-string (s buf)
                                             (fix44:read-fix s))
                                           :msg-seq-num)
                           received-seq-nums))
                   (lambda (x y)
                     (setf received-gap-start x
                           received-gap-end y)))
    (is (equalp '(1 2 8 9 10) (reverse received-seq-nums)))
    (is (= 3 received-gap-start))
    (is (= 8 received-gap-end))))
