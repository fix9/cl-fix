;;; Copyright 2020 Sandipan Razzaque
;;;
;;; Licensed under the Apache License, Version 2.0 (the "License");
;;; you may not use this file except in compliance with the License.
;;; You may obtain a copy of the License at
;;;
;;;     http://www.apache.org/licenses/LICENSE-2.0
;;;
;;; Unless required by applicable law or agreed to in writing, software
;;; distributed under the License is distributed on an "AS IS" BASIS,
;;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;;; See the License for the specific language governing permissions and
;;; limitations under the License.

(in-package #:fastbuffer-test)

(def-suite :fastbuffer-suite)
(in-suite :fastbuffer-suite)

(defparameter *strict-check-limit* (expt 2 12)
  "The stricter checks (i.e. every pub/sub value) can blow the control stack on larger tests.
This is due to the atomic-push in (defun reader) below.")

(defparameter *buffer-size* nil)
(defparameter *batch-size* nil)
(defparameter *num-published* nil)

(defun wait-thread (thread max-wait-secs)
  (bt:make-thread
   (lambda ()
     (sleep max-wait-secs)
     (when (bt:thread-alive-p thread)
       (log:warn "Needing to forcibly kill thread ~A" thread)
       (bt:destroy-thread thread))))
  (bt:join-thread thread))

(defun writer (fb failed-reserves num-published)
  (lambda ()
    (loop
       with n = *num-published*
       until (= n 0)
       do
         (let ((this-batch-size
                (min n
                     (if (listp *batch-size*)
                         (+ (car *batch-size*)
                            (random (- (cadr *batch-size*) (car *batch-size*))))
                         *batch-size*))))
           (multiple-value-bind (lower upper reserve-ok) (fb-reserve fb this-batch-size)
             (log:trace "Batch size: ~A" this-batch-size)
             (cond
               (reserve-ok (loop
                              for i from lower below upper
                              do
                                (decf n)
                                (log:trace "Publishing: ~A~%" (- *num-published* n))
                                (setf (fb-elt fb i) (- *num-published* n))
                                (#+sbcl sb-ext:atomic-incf #-sbcl incf (car num-published)))
                           (fb-publish fb upper))
               (t
                (log:trace "Awaiting our slow subscriber...")
                (#+sbcl sb-ext:atomic-incf #-sbcl incf (car failed-reserves)))))))))

(defun reader (fb read-values num-read)
  (lambda ()
    (let ((last-read nil))
      (flet ((consume (x)
               (when (not (> *num-published* *strict-check-limit*))
                 (#+sbcl sb-ext:atomic-push #-sbcl push x (car read-values))
                 (log:trace "Subscriber received: ~A ~A~%" x (car read-values)))
               (#+sbcl sb-ext:atomic-incf #-sbcl incf (car num-read))
               (setf last-read x)))
        (loop
           until (and last-read (= last-read *num-published*))
           do
             (fb-process fb #'consume)))
      (log:trace "Exiting at ~A. ~A" last-read (car read-values)))))

(defun no-skipped-msgs (ls)
  (not (loop for (a b) on ls
          while b
          when (not (= 1 (- a b)))
          return t)))

(defun run-test% (buff batch n)
  (setf *batch-size* batch
        *buffer-size* buff
        *num-published* n)
  (let* ((fb (make-instance 'fb :init-fn (lambda () 0) :size buff))
         (failed-reserves (cons 0 nil))
         (num-published (cons 0 nil))
         (num-read (cons 0 nil))
         (read-values (cons nil nil))
         (t1 (bt:make-thread (writer fb failed-reserves num-published)))
         (t2 (bt:make-thread (reader fb read-values num-read))))
    (wait-thread t1 10)
    (wait-thread t2 10)
    (log:trace "With batch size ~A, buffer size ~A, and n ~A - number of failed reserves: ~A"
               *batch-size*
               *buffer-size*
               *num-published*
               (car failed-reserves))
    (is (= (car num-published) (car num-read))
        "All published values must have been read by the subscriber. Published ~A - but only read ~A"
        (car num-published)
        (car num-read))
    (when (not (> *num-published* *strict-check-limit*))
      ;; We only do these checks on smaller batch sizes. On large batch sizes, it blows our allocation.
      (is (apply #'> (car read-values))
          "All read values should be monotonically increasing")
      (is (no-skipped-msgs (car read-values))
          "There should be no skipped messages in the subscriber: ~A" (car read-values)))))

(def-test basic (:profile t)
  (run-test% 1024 32 1024))

(def-test randomised-batch-size (:depends-on basic)
  (run-test% 1024 '(1 50) 2048))

(def-test large-num-published-with-random-batch-size (:depends-on basic)
  (run-test% 1024 32 (expt 2 18)))
