;;; Copyright 2020 Sandipan Razzaque
;;;
;;; Licensed under the Apache License, Version 2.0 (the "License");
;;; you may not use this file except in compliance with the License.
;;; You may obtain a copy of the License at
;;;
;;;     http://www.apache.org/licenses/LICENSE-2.0
;;;
;;; Unless required by applicable law or agreed to in writing, software
;;; distributed under the License is distributed on an "AS IS" BASIS,
;;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;;; See the License for the specific language governing permissions and
;;; limitations under the License.

(in-package #:cl-fix-integration-test)

(def-suite integration :in :cl-fix-suite)
(in-suite integration)

;; --------------------------------------------------------------------------
;; SERVER / ACCEPTOR

(defun handler (session msg)
  (case (util:fix-msg-type msg)
    (:new-order-single
     (cl-fix:enqueue-msg session (util:make-fix-msg :execution-report
                                                    :order-id "1"
                                                    :exec-id "1"
                                                    :exec-type :new
                                                    :ord-status :new
                                                    :side (util:fix-field msg :side)
                                                    :leaves-qty (util:fix-field msg :order-qty)
                                                    :cum-qty 0.0d0
                                                    :avg-px 0.0d0)))))

(defclass server-session (cl-fix:session)
  ()
  (:default-initargs
   :dict-key :fix44
    :sender-comp-id "server"
    :target-comp-id "client"
    :handler #'handler))

;; --------------------------------------------------------------------------
;; CLIENT / INITIATOR

(defun client-handler (session msg)
  (with-slots (received-exec-rpt) session
    (case (util:fix-msg-type msg)
      (:logon
       (cl-fix:enqueue-msg session (util:make-fix-msg :new-order-single
                                                      :cl-ord-id "1"
                                                      :side :buy
                                                      :transact-time cl-fix:*current-time*
                                                      :symbol "AUDUSD"
                                                      :ord-type :market
                                                      :order-qty 100.0d0)))
      (:execution-report
       (setf received-exec-rpt t)
       (cl-fix:stop-session session)))))

(defclass client-session (cl-fix:session)
  ((received-exec-rpt :initform nil))
  (:default-initargs
   :dict-key :fix44
    :sender-comp-id "client"
    :target-comp-id "server"
    :handler #'client-handler))

;; --------------------------------------------------------------------------
;; TEST

(def-test basic-integration-test ()
  (let ((server-session (make-instance 'server-session))
        (acceptor (make-instance 'cl-fix:acceptor :host "127.0.0.1" :port 8889 :dict-key :fix44))
        (client-session (make-instance 'client-session))
        (initiator (make-instance 'cl-fix:initiator :host "127.0.0.1" :port 8889 :dict-key :fix44)))
    (cl-fix:add-session acceptor server-session)
    (cl-fix:add-session initiator client-session)
    (unwind-protect
         (progn
           (cl-fix:start-connector acceptor)
           (loop repeat 10 while (not (eq :running (fsm:fsm-current-state acceptor))) do (sleep 0.5))
           (is (eq :running (fsm:fsm-current-state acceptor)) "Acceptor did not start up.")
           (cl-fix:start-connector initiator)
           (loop repeat 10 while (not (slot-value client-session 'received-exec-rpt)) do (sleep 0.5))
           (is (slot-value client-session 'received-exec-rpt)))
      (cl-fix:stop-connector acceptor)
      (cl-fix:stop-connector initiator))))

