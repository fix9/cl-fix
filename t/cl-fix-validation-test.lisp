;;; Copyright 2020 Sandipan Razzaque
;;;
;;; Licensed under the Apache License, Version 2.0 (the "License");
;;; you may not use this file except in compliance with the License.
;;; You may obtain a copy of the License at
;;;
;;;     http://www.apache.org/licenses/LICENSE-2.0
;;;
;;; Unless required by applicable law or agreed to in writing, software
;;; distributed under the License is distributed on an "AS IS" BASIS,
;;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;;; See the License for the specific language governing permissions and
;;; limitations under the License.

(in-package #:cl-fix-validation-test)

(def-suite validation :in :cl-fix-suite)
(in-suite validation)

(defun make-filter (type)
  (lambda (x)
    (eq type (cl-fix:validation-error-err x))))

(defun get-path ()
  (lambda (x)
    (car (cl-fix:validation-error-path x))))

(def-test validate-missing-fields ()
  (let* ((msg (util:make-fix-msg :logon
                                 :username "bob"
                                 :password "password"))
         (result (fix44:validate msg :validate-header t))
         (expected-missing
           '(:encrypt-method :heart-int :sender-comp-id :target-comp-id :msg-seq-num :sending-time))
         (actual-missing
           (->> result
             (remove-if-not (make-filter :missing-field))
             (mapcar (get-path)))))
    (setf expected-missing (sort expected-missing #'string< :key #'symbol-name))
    (setf actual-missing (sort actual-missing #'string< :key #'symbol-name))
    (is (equalp expected-missing
                actual-missing))))

(def-test validate-data-types ()
  (let* ((msg (util:make-fix-msg :logon
                                 :username "bob"
                                 :password 235
                                 :heart-int "2891"
                                 :sending-time "oops"
                                 :orig-sending-time (local-time:now)
                                 :encrypt-method 92
                                 :next-expected-msg-seq-num "wrong"
                                 :no-msg-types "this should be a list" ;; TODO should be validated that htis is a list
                                 ))
         (expected-type-errors '(:password :encrypt-method :next-expected-msg-seq-num :heart-int :sending-time :no-msg-types))
         (actual-type-errors
           (->> (fix44:validate msg :validate-header t)
             (remove-if-not (make-filter :type))
             (mapcar (get-path)))))
    (setf expected-type-errors (sort expected-type-errors #'string< :key #'symbol-name))
    (setf actual-type-errors (sort actual-type-errors #'string< :key #'symbol-name))
    (is (equalp expected-type-errors
                actual-type-errors))))

(def-test unknown-fields ()
  (let* ((msg (util:make-fix-msg :logon
                                 :username "bob"
                                 :gangsta "1234"
                                 :another-unknown '()
                                 :what? nil))
         (errors (->> (fix44::validate msg)
                      (remove-if-not (make-filter :unknown-field))
                   (mapcar (get-path)))))
    (is (equalp '(:gangsta :another-unknown :what?) errors))))

(def-test validate-nos ()
  (let* ((msg (util:make-fix-msg :new-order-single
                                 :cl-ord-id "2891"
                                 :sending-time (local-time:now)
                                 :sender-comp-id "123"
                                 :side :BUY
                                 :transact-time (local-time:now)
                                 :ord-type :MARKET
                                 :target-comp-id "target"
                                 :msg-seq-num 32))
         (result (fix44::validate msg :validate-header t)))
    (is (not result)
        "Expecting no validation errors for this NOS")))

(def-test validate-no-msg-type ()
  (let ((errs (fix44:validate '())))
    (is (= 1 (length errs)))
    (is (equalp (cl-fix:validation-error-path (car errs)) '(:msg-type)))
    (is (equalp (cl-fix:validation-error-err (car errs)) ':missing-field))))
