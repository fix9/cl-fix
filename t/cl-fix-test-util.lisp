;;; Copyright 2020 Sandipan Razzaque
;;;
;;; Licensed under the Apache License, Version 2.0 (the "License");
;;; you may not use this file except in compliance with the License.
;;; You may obtain a copy of the License at
;;;
;;;     http://www.apache.org/licenses/LICENSE-2.0
;;;
;;; Unless required by applicable law or agreed to in writing, software
;;; distributed under the License is distributed on an "AS IS" BASIS,
;;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;;; See the License for the specific language governing permissions and
;;; limitations under the License.

(in-package #:cl-fix-test-util)

(defclass array-stream
    (fundamental-output-stream fundamental-input-stream)
  ((buffer :initform (make-array 1024 :element-type 'character))
   (write-idx :initform 0)
   (read-idx :initform 0)
   (publish-idx :initform 0)
   (lock :initform (bt:make-lock))
   (closed :initform nil)))

(defmethod stream-read-char ((s array-stream))
  (with-slots (closed buffer publish-idx read-idx lock) s
    (when closed
      (error 'stream-error :stream s))
    (bt:with-recursive-lock-held (lock)
      (if (>= read-idx publish-idx)
          :eof
          (elt buffer (mod (prog1 read-idx (incf read-idx)) 1024))))))

(defmethod stream-listen ((s array-stream))
  (with-slots (closed read-idx publish-idx lock) s
    (when closed
      (error 'stream-error :stream s))
    (bt:with-recursive-lock-held (lock)
      (> publish-idx read-idx))))

(defmethod stream-write-char ((s array-stream) chr)
  (with-slots (buffer write-idx lock) s
    (bt:with-recursive-lock-held (lock)
      (setf (elt buffer (mod (prog1 write-idx (incf write-idx)) 1024)) chr))))

(defmethod stream-write-sequence ((s array-stream) seq start end &key)
  (with-slots (lock) s
    (bt:with-recursive-lock-held (lock)
      (loop for x across (subseq seq (or start 0) (or end (length seq)))
         do (stream-write-char s x)))))

(defmethod stream-write-string ((s array-stream) string &optional start end)
  (stream-write-sequence s string start end))

(defmethod stream-force-output ((s array-stream))
  (with-slots (lock write-idx publish-idx) s
    (bt:with-recursive-lock-held (lock)
      (setf publish-idx write-idx))))

(defclass flip-stream
    (fundamental-output-stream fundamental-input-stream)
  ((in :initform (make-instance 'array-stream) :initarg :in)
   (out :initform (make-instance 'array-stream)  :initarg :out)))

(defgeneric flip (stream))
(defgeneric unclose-stream (stream))

(defmethod stream-read-char ((s flip-stream))
  (stream-read-char (slot-value s 'in)))

(defmethod stream-listen ((s flip-stream))
  (stream-listen (slot-value s 'in)))

(defmethod stream-write-char ((s flip-stream) chr)
  (stream-write-char (slot-value s 'out) chr))

(defmethod stream-write-sequence ((s flip-stream) seq start end &key)
  (stream-write-sequence (slot-value s 'out) seq start end))

(defmethod stream-write-string ((s flip-stream) string &optional start end)
  (stream-write-string (slot-value s 'out) string start end))

(defmethod stream-force-output ((s flip-stream))
  (stream-force-output (slot-value s 'out)))

(defmethod close-stream ((s flip-stream))
  (with-slots (in out) s
    (close-stream in)
    (close-stream out)))

(defmethod unclose-stream ((s flip-stream))
  "Purely for testing. To verify if a message was sent before the remote end closed the stream."
  (with-slots (in out) s
    (setf (slot-value in 'closed) nil
          (slot-value out 'closed) nil)))

(defmethod close ((s flip-stream) &key &allow-other-keys)
  (close-stream s))

(defmethod close-stream ((a array-stream))
  (with-slots (lock closed) a
    (bt:with-recursive-lock-held (lock)
      (setf closed t))))

(defun closed-p (fs)
  (declare (type flip-stream fs))
  (with-slots (in out) fs
    (or (slot-value in 'closed)
        (slot-value out 'closed))))

(defmethod flip ((s flip-stream))
  (with-slots (in out) s
    (make-instance 'flip-stream :in out :out in)))

(defun await-input (stream)
  (loop repeat 1000 do
       (sleep 0.001)
       (when (listen stream)
         (return-from await-input)))
  (error "Timed out waiting for input on stream."))

(defmacro with-fn-rebind ((fn-name new) &body body)
  (let ((orig-fn (gensym "orig-fn")))
    `(let ((,orig-fn (symbol-function ,fn-name)))
       (unwind-protect
            (progn
              (setf (symbol-function ,fn-name) ,new)
              ,@body)
         (setf (symbol-function ,fn-name) ,orig-fn)))))
