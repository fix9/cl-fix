;;; Copyright 2020 Sandipan Razzaque
;;;
;;; Licensed under the Apache License, Version 2.0 (the "License");
;;; you may not use this file except in compliance with the License.
;;; You may obtain a copy of the License at
;;;
;;;     http://www.apache.org/licenses/LICENSE-2.0
;;;
;;; Unless required by applicable law or agreed to in writing, software
;;; distributed under the License is distributed on an "AS IS" BASIS,
;;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;;; See the License for the specific language governing permissions and
;;; limitations under the License.

(in-package #:cl-fix-session-test)

(def-suite session :in :cl-fix-suite)
(in-suite session)

(defparameter *dir* #P"/tmp/tst1/")

(defparameter *expected-filename* #P"/tmp/tst1/TEST-sender_TEST-target.dat")
(defparameter *msgs-out-filename* #P"/tmp/tst1/TEST-sender_TEST-target.out")
(defparameter *msgs-in-filename* #P"/tmp/tst1/TEST-sender_TEST-target.in")

(defparameter *expected-filename-sub-ids* #P"/tmp/tst1/TEST-sender-ssub_TEST-target-tsub.dat")
(defparameter *msgs-out-filename-sub-ids* #P"/tmp/tst1/TEST-sender-ssub_TEST-target-tsub.out")
(defparameter  *msgs-in-filename-sub-ids* #P"/tmp/tst1/TEST-sender-ssub_TEST-target-tsub.in")

(defun noop-handler (session msg)
  (declare (ignore session msg)))

(def-fixture session-persist-test ()
  (dolist (x (list *expected-filename* *msgs-out-filename* *msgs-in-filename*
                   *expected-filename-sub-ids* *msgs-out-filename-sub-ids* *msgs-in-filename-sub-ids*))
    (uiop:delete-file-if-exists x))
  (unwind-protect (progn
                    (&body))
    (uiop:delete-directory-tree *dir* :validate t :if-does-not-exist :ignore)))

(def-test restore-session-initial (:fixture session-persist-test)
  "Restore a session without a prior session having been persisted."
  (let* ((session (make-instance 'cl-fix-session:session
                                :sender-comp-id "TEST-sender"
                                :target-comp-id "TEST-target"
                                :session-store (ensure-directories-exist #P"/tmp/tst1/")
                                :dict-key :fix44
                                :handler #'noop-handler))
         (file-state (cl-fix-session:read-state-from-store session)))
    (is (not file-state) "File state should be nil, but got ~A" file-state)
    (cl-fix-session:init-session session nil)
    (is (= 1 (slot-value session 'cl-fix-session::exp-incoming-seq-num)))
    (is (= 1 (slot-value session 'cl-fix-session::next-outgoing-seq-num)))))

(def-test persist-session (:fixture session-persist-test)
  "Persist a session to disk."
  (let ((session (make-instance 'cl-fix-session:session
                                :sender-comp-id "TEST-sender"
                                :target-comp-id "TEST-target"
                                :session-store (ensure-directories-exist #P"/tmp/tst1/")
                                :dict-key :fix44
                                :handler #'noop-handler)))
    (unwind-protect
         (progn
           (cl-fix-session:init-stores session nil)
           (cl-fix-session:persist session)
           (is (uiop:file-exists-p *expected-filename*)))
      (cl-fix-session:close-stores session))))

(def-test restore-session (:fixture session-persist-test
                           :depends-on persist-session)
  "Restoring from a previously persisted session"
  (let ((session (make-instance 'cl-fix-session:session
                                :sender-comp-id "TEST-sender"
                                :target-comp-id "TEST-target"
                                :exp-incoming-seq-num 47
                                :exp-outgoing-seq-num 388
                                :session-store (ensure-directories-exist #P"/tmp/tst1/")
                                :dict-key :fix44
                                :handler #'noop-handler))
        (restored (make-instance 'cl-fix-session:session
                                 :sender-comp-id "TEST-sender"
                                 :target-comp-id "TEST-target"
                                 :session-store (ensure-directories-exist #P"/tmp/tst1/")
                                 :dict-key :fix44
                                 :handler #'noop-handler)))
    (unwind-protect
         (progn
           (cl-fix-session:init-stores session nil)
           (cl-fix-session:persist session)
           (cl-fix-session:close-stores session)
           (cl-fix-session:init-session restored (cl-fix-session:read-state-from-store session))
           (is (= 47 (slot-value restored 'cl-fix-session::exp-incoming-seq-num)))
           (is (= 388 (slot-value restored 'cl-fix-session::next-outgoing-seq-num))))
      (cl-fix-session:close-stores session))))

(defun contents-same (filename contents)
  (with-open-file (f filename :direction :input)
    (string= (read-line f) contents)))

(def-test persist-msgs (:fixture session-persist-test)
  "Inbound and outbound messages can be persisted."
  (let ((session (make-instance 'cl-fix-session:session
                                 :sender-comp-id "TEST-sender"
                                 :target-comp-id "TEST-target"
                                 :exp-incoming-seq-num 47
                                 :exp-outgoing-seq-num 388
                                 :session-store (ensure-directories-exist #P"/tmp/tst1/")
                                 :dict-key :fix44
                                 :handler #'noop-handler))
        (in-msg (format nil "in-msg-~A" (random most-positive-fixnum)))
        (out-msg (format nil "out-msg-~A" (random most-positive-fixnum))))
    (unwind-protect
         (progn
           (cl-fix-session:init-stores session nil)
           (cl-fix-session:persist-inbound-msg session in-msg)
           (cl-fix-session:persist-outbound-msg session 1 out-msg)
           (is (contents-same *msgs-in-filename* in-msg) "Persisted in-msg was not as expected.")
           (is (contents-same *msgs-out-filename* out-msg) "Persisted out-msg was not as expected."))
      (cl-fix-session:close-stores session))))

(def-test correct-file-names-when-using-sub-ids (:fixture session-persist-test
                                                 :depends-on (and persist-session
                                                                  persist-msgs))
  (let ((session (make-instance 'cl-fix-session:session
                                :sender-comp-id "TEST-sender"
                                :target-comp-id "TEST-target"
                                :sender-sub-id "ssub"
                                :target-sub-id "tsub"
                                :exp-incoming-seq-num 47
                                :exp-outgoing-seq-num 388
                                :session-store (ensure-directories-exist #P"/tmp/tst1/")
                                :dict-key :fix44
                                :handler #'noop-handler))
        (in-msg (format nil "in-msg-~A" (random most-positive-fixnum)))
        (out-msg (format nil "out-msg-~A" (random most-positive-fixnum))))
    (unwind-protect
         (progn
           (cl-fix-session:init-stores session nil)
           (cl-fix-session:persist session)
           (cl-fix-session:persist-inbound-msg session in-msg)
           (cl-fix-session:persist-outbound-msg session 1 out-msg)
           (is (uiop:file-exists-p *expected-filename-sub-ids*) "Session file should have been created.")
           (is (uiop:file-exists-p *msgs-in-filename-sub-ids*) "Msgs out should have been created.")
           (is (uiop:file-exists-p *msgs-out-filename-sub-ids*) "Msgs in should have been created.")
           (is (contents-same *msgs-in-filename-sub-ids* in-msg) "Persisted in-msg was not as expected.")
           (is (contents-same *msgs-out-filename-sub-ids* out-msg) "Persisted out-msg was not as expected."))
      (cl-fix-session:close-stores session))))

(defun merge-plists (overrides defaults)
  (let ((result (copy-list defaults)))
    (loop
       with x = (copy-list overrides)
       while x
       do (let ((key (pop x))
                (val (pop x)))
            (setf (getf result key) val)))
    result))

(def-fixture session-behaviour-test (&optional (session-class 'cl-fix:session) &rest session-opts)
  (let ((out-events ())
        (next-seq 1)
        (mock-connector (make-instance 'cl-fix-connector:connector :host "unused" :port 8922 :dict-key :fix44))
        (session (apply 'make-instance
                        session-class
                        (merge-plists session-opts
                                      (list :sender-comp-id "TEST-sender"
                                            :target-comp-id "TEST-target"
                                            :session-store (ensure-directories-exist #P"/tmp/cl-fix-test/")
                                            :dict-key :fix44
                                            :handler #'noop-handler))))
        (time (local-time:parse-timestring "2020-04-19T05:46:31.192373Z")))
    (with-fn-rebind ('cl-fix-connector:enqueue-msg (lambda (conn &rest plist &key &allow-other-keys)
                                                     (declare (ignore conn))
                                                     (push plist out-events)))
      (labels ((advance-time (amount unit &key (preserve-msgs nil))
                 (unless preserve-msgs (setf out-events nil))
                 (loop repeat amount
                      do
                      (setf time (local-time:timestamp+ time 1 unit))
                      (tick t)))
               (send-msg (msg &key (time time) (preserve-msgs nil) (seq-override nil))
                 (when (not preserve-msgs) (setf out-events nil))
                 (setf (util:fix-field msg :msg-seq-num) (or seq-override next-seq))
                 (when (not seq-override)
                   (incf next-seq))
                 (let ((cl-fix-session:*current-time* time))
                   (funcall (cl-fix-session:session-handler session) session msg)))
               (msgs-out ()
                 (reverse
                  (loop for x in out-events
                     when (eq :message (getf x :event-type))
                     collect (getf x :data))))
               (tick (&optional (preserve-msgs nil))
                 (unless preserve-msgs (setf out-events nil))
                 (let ((cl-fix-session:*current-time* time))
                   (funcall (cl-fix-session:session-tick session) session)))
               (default-init ()
                 (let ((cl-fix-session:*current-time* time))
                   (cl-fix-session:init-session session)
                   (cl-fix-session:on-connect session)
                   (send-msg (util:make-fix-msg :logon)))))
        (cl-fix-connector:add-session mock-connector session)
        (&body)))))

(defmacro is-received (types)
  `(progn
     (let ((msgs (msgs-out)))
       (is (= (length ,types) (length msgs))
           ,(format nil "Expected ~A messages - ~A - but got ~~A" (length (cadr types)) types) (length msgs))
       (is (every #'eq
                  (mapcar #'util:fix-msg-type msgs)
                  ,types)
           ,(format nil "Expected the following message types: ~A, but got: ~~A" types)
           msgs))))

(def-test logon-is-sent-on-connect (:fixture session-behaviour-test)
  (let ((cl-fix-session:*current-time* time))
    (cl-fix-session:init-session session)
    (cl-fix-session:on-connect session))
  (is-received '(:logon)))

(def-test heartbeat-is-sent-on-no-msgs (:fixture session-behaviour-test)
  (default-init)
  (advance-time 31 :sec)
  (is-received '(:heartbeat)))

(def-test test-request-behaviour (:fixture (session-behaviour-test 'cl-fix:session
                                                                   :heartbeat-interval-ms 10000
                                                                   :grace-period-ms 1000))
  (default-init)
  (advance-time 12 :sec)
  (is-received '(:heartbeat :test-request)))

(def-test logout-after-no-test-request-response (:fixture session-behaviour-test
                                                 :depends-on test-request-behaviour)
  (default-init)
  (advance-time 42 :sec)
  (is-received '(:heartbeat :test-request :logout)))

(def-test no-logout-if-test-request-response-received (:fixture session-behaviour-test
                                                       :depends-on test-request-behaviour)
  (default-init)
  (advance-time 36 :sec)
  (send-msg (util:make-fix-msg :heartbeat
                               :test-req-id (util:fix-field (elt (msgs-out) 1) :test-req-id)))
  (advance-time 6 :sec)
  (is (= 0 (length (msgs-out)))
      "Response to test message should have prevented logout. Got: ~A" (msgs-out)))

(def-test logon-response-sent-on-init (:fixture (session-behaviour-test 'cl-fix:session
                                                                        :heartbeat-interval-ms 30000
                                                                        :grace-period-ms 5000))
  (let ((logon-msg (util:make-fix-msg :logon
                                      :msg-seq-num 1
                                      :sender-comp-id "sender"
                                      :target-comp-id "target"
                                      :heart-bt-int 30))
        (cl-fix-session:*current-time* time))
    (cl-fix-session:init-session session)
    (cl-fix-session:on-connect session logon-msg))
  (advance-time 31 :sec :preserve-msgs t)
  (is-received '(:logon :heartbeat)))

(def-test heartbeat-interval-gets-set-from-logon (:fixture (session-behaviour-test 'cl-fix:session
                                                                                   :heartbeat-interval-ms 30000
                                                                                   :grace-period-ms 5000))
  (let ((logon-msg (util:make-fix-msg :logon
                                      :msg-seq-num 1
                                      :sender-comp-id "sender"
                                      :target-comp-id "target"
                                      :heart-bt-int 10))
        (cl-fix-session:*current-time* time))
    (cl-fix-session:on-connect session logon-msg)
    (advance-time 11 :sec :preserve-msgs t)
    (is-received '(:logon :heartbeat))
    (advance-time 11 :sec)
    (is-received '(:heartbeat))))

(def-test logout-if-no-response-to-logon (:fixture session-behaviour-test)
  (let ((cl-fix-session:*current-time* time))
    (cl-fix-session:on-connect session))
  (is-received '(:logon))
  (advance-time 6 :sec)
  (is-received '(:logout))
  (is (cl-fix-session:stoppedp session)))

(defun make-nos (cl-ord-id)
  (util:make-fix-msg :new-order-single
                     :cl-ord-id cl-ord-id
                     :side :buy
                     :transact-time (local-time:now)
                     :symbol "AUDUSD"
                     :order-qty 100.0d0
                     :ord-type :market))

(defun make-exec-rpt (nos)
  (util:make-fix-msg :execution-report
                     :order-id (util:fix-field nos :cl-ord-id)
                     :exec-id (format nil "exec-id-for-~A" (util:fix-field nos :cl-ord-id))
                     :exec-type :new
                     :ord-status :new
                     :side (util:fix-field nos :side)
                     :leaves-qty (util:fix-field nos :order-qty)
                     :cum-qty 0.0d0
                     :avg-px 0.0d0))

(defun resend-handler (session msg)
  (case (util:fix-msg-type msg)
    (:new-order-single
     (cl-fix:enqueue-msg session (make-exec-rpt msg)))))


(defun make-resend-request (begin end)
  (util:make-fix-msg :resend-request
                     :begin-seq-no begin
                     :end-seq-no end))

(def-test basic-resend (:fixture (session-behaviour-test 'cl-fix:session :handler #'resend-handler))
  "Test that a session can respond to a basic resend request."
  (default-init)
  (send-msg (make-nos "ord1"))
  (send-msg (make-nos "ord2") :preserve-msgs t)
  (is-received '(:execution-report :execution-report))
  (is (equalp '(2 3) (mapcar (lambda (x) (util:fix-field x :msg-seq-num)) (msgs-out))))
  (send-msg (make-resend-request 2 2))
  (let ((perform-resend-event (find :perform-resend out-events :key (lambda (x) (getf x :event-type)))))
    (is-true perform-resend-event "A :perform-resend event should have been requested on the out buffer")
    (is (equalp (cons 2 3) (getf perform-resend-event :data))
        "Requesting a specific message should result in the correct bounds being requested on the msg store"))
  (send-msg (make-resend-request 2 0))
  (let ((perform-resend-event (find :perform-resend out-events :key (lambda (x) (getf x :event-type)))))
    (is-true perform-resend-event "A :perform-resend event should have been requested on the out buffer")
    (is (equalp (cons 2 4) (getf perform-resend-event :data))))
  (send-msg (make-resend-request 2 50))
  (let ((perform-resend-event (find :perform-resend out-events :key (lambda (x) (getf x :event-type)))))
    (is-true perform-resend-event "A :perform-resend event should have been requested on the out buffer")
    (is (equalp (cons 2 4) (getf perform-resend-event :data))
        "Should not be able to request a resend of a seq-num that's too high")))

(def-test resend-invalid-request (:fixture (session-behaviour-test 'cl-fix:session :handler #'resend-handler))
  "Test that session does NOT action an invalid resend request."
  (default-init)
  (send-msg (make-nos "ord-0"))
  (dotimes (n 100)
    (send-msg (make-nos (format nil "ord-~A" n)) :preserve-msgs t))
  (is (equalp (loop for i from 2 upto 102 collect i)
              (mapcar (lambda (x) (util:fix-field x :msg-seq-num))
                      (msgs-out))))
  (flet ((resend-exists ()
           (find :perform-resend out-events :key (lambda (x) (getf x :event-type)))))
    (send-msg (make-resend-request -1 2))
    (is (not (resend-exists)) "Resend shouldn't work if either boundary is negative")
    (send-msg (make-resend-request 2 -1))
    (is (not (resend-exists)) "Resend shouldn't work if either boundary is negative")
    (send-msg (make-resend-request 10 5))
    (is (not (resend-exists)) "Resend shouldn't work with begin-seq-no > end-seq-no")
    (send-msg (make-resend-request 3 4))
    (is (resend-exists) "Resend should work with begin-seq-no <= end-seq-no")
    (send-msg (make-resend-request 3 3))
    (is (resend-exists) "Resend should work with begin-seq-no < end-seq-no")))

(defclass seq-num-session (cl-fix:session)
  ((processed-msgs :initform nil)))

(defun processing-handler (session msg)
  (with-slots (processed-msgs) session
    (log:debug "Received: ~A" msg)
    (push msg processed-msgs)))

(defun get-processed-seq-nums (session)
  (reverse (loop for x in (slot-value session 'processed-msgs) collect (util:fix-field x :msg-seq-num))))

(def-test receive-out-of-order-messages-during-resend (:fixture (session-behaviour-test 'seq-num-session :handler #'processing-handler))
  "Test that when a session is gap-filling, the order of processing remains unaffected."
  (default-init)
  (send-msg (make-nos "ord-2") :seq-override 2)
  (send-msg (make-nos "ord-5") :seq-override 5)
  (is-received '(:resend-request))
  (let ((resend-msg (car (last (msgs-out)))))
    (is (= 3 (util:fix-field resend-msg :begin-seq-no)))
    (is (= 0 (util:fix-field resend-msg :end-seq-no))))
  (send-msg (make-nos "ord-6") :seq-override 6)
  (send-msg (make-nos "ord-7") :seq-override 7)
  (send-msg (make-nos "ord-3") :seq-override 3)
  (send-msg (make-nos "ord-4") :seq-override 4)
  (is (equalp '(1 2 3 4 5 6 7) (get-processed-seq-nums session))))

(def-test heartbeats-are-still-handled-during-gap-fill (:fixture (session-behaviour-test 'seq-num-session :handler #'processing-handler))
  "While gap-filling, continue processing heartbeats. Do not process twice."
  (default-init)
  (send-msg (make-nos "ord-2") :seq-override 2)
  (send-msg (make-nos "ord-5") :seq-override 5)
  (is-received '(:resend-request))
  (send-msg (make-nos "ord-6") :seq-override 6)
  (send-msg (make-nos "ord-7") :seq-override 7)
  (send-msg (make-nos "ord-3") :seq-override 3)
  (send-msg (util:make-fix-msg :heartbeat) :seq-override 4)
  (is (equalp '(1 2 3 5 6 7) (get-processed-seq-nums session))))

(def-test resend-requests-are-still-handled-during-gap-fill (:fixture (session-behaviour-test 'seq-num-session :handler #'processing-handler))
  "While gap-filling, honour resend-requests regardless of seq-num. Do not process twice."
  (default-init)
  (send-msg (make-nos "ord-2") :seq-override 2)
  (send-msg (make-nos "ord-5") :seq-override 5)
  (is-received '(:resend-request))
  (send-msg (make-nos "ord-6") :seq-override 6)
  (send-msg (make-nos "ord-7") :seq-override 7)
  (send-msg (make-resend-request 1 0) :seq-override 3)
  (let ((perform-resend-event (find :perform-resend out-events :key (lambda (x) (getf x :event-type)))))
    (is-true perform-resend-event "Resend request should be actioned, despite being out of order and during gap-fill."))
  (send-msg (make-nos "ord-3") :seq-override 4)
  (is (equalp '(1 2 4 5 6 7) (get-processed-seq-nums session))))

(def-test resend-requests-are-still-handled-during-gap-fill (:fixture (session-behaviour-test 'seq-num-session :handler #'processing-handler))
  "While gap-filling, heartbeats should be allowed to be sent."
  (default-init)
  (send-msg (make-nos "ord-2") :seq-override 2)
  (send-msg (make-nos "ord-5") :seq-override 5)
  (is-received '(:resend-request))
  (advance-time 31 :sec)
  (is-received '(:heartbeat)))

(def-test sequence-reset-during-gap-fill-should-clear-gap-fill-state (:fixture (session-behaviour-test 'seq-num-session :handler #'processing-handler))
  "Receiving a 'non gap-fill' sequence reset whilst in gap-fill should clear queued msgs, and resume normal processing."
  (default-init)
  (send-msg (make-nos "ord-2") :seq-override 2)
  (send-msg (make-nos "ord-5") :seq-override 5)
  (is-received '(:resend-request))
  (send-msg (util:make-fix-msg :sequence-reset :new-seq-no 10) :seq-override 9)
  (send-msg (make-nos "ord-10") :seq-override 10)
  (send-msg (make-nos "ord-11") :seq-override 11)
  (send-msg (make-nos "ord-12") :seq-override 12)
  (is (equalp '(1 2 10 11 12) (get-processed-seq-nums session)))
  (is (not (slot-value session 'cl-fix-session::queued-msgs)) "Queued messages should have been cleared")
  (is (not (eq :gap-fill (slot-value session 'cl-fix-session::session-status))) "Gap fill state should have been cleared"))

(def-test gap-fill-messages-during-gap-fill-should-be-handled-correctly (:fixture (session-behaviour-test 'seq-num-session :handler #'processing-handler))
  "Receiving a 'gap fill' sequence reset should be handled correctly."
  (default-init)
  (send-msg (make-nos "ord-2") :seq-override 2)
  (send-msg (make-nos "ord-5") :seq-override 5)
  (is-received '(:resend-request))
  (send-msg (util:make-fix-msg :sequence-reset :new-seq-no 4 :gap-fill-flag t) :seq-override 3)
  (send-msg (util:make-fix-msg :sequence-reset :new-seq-no 5 :gap-fill-flag t) :seq-override 4)
  (is (not (slot-value session 'cl-fix-session::queued-msgs)) "Queued messages should have been cleared")
  (is (not (eq :gap-fill (slot-value session 'cl-fix-session::session-status))) "Gap fill state should have been cleared")
  (send-msg (make-nos "ord-6") :seq-override 6)
  (is (equalp '(1 2 5 6) (get-processed-seq-nums session))))
