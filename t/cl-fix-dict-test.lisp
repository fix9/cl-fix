;;; Copyright 2020 Sandipan Razzaque
;;;
;;; Licensed under the Apache License, Version 2.0 (the "License");
;;; you may not use this file except in compliance with the License.
;;; You may obtain a copy of the License at
;;;
;;;     http://www.apache.org/licenses/LICENSE-2.0
;;;
;;; Unless required by applicable law or agreed to in writing, software
;;; distributed under the License is distributed on an "AS IS" BASIS,
;;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;;; See the License for the specific language governing permissions and
;;; limitations under the License.

(in-package #:cl-fix-dict-test)

(def-suite serialize :in :cl-fix-suite)
(in-suite serialize)

(test serialize-simple-msg
  (let ((msg (list :msg-type :logon
                   :sender-comp-id "larry"
                   :target-comp-id "bob"
                   :msg-seq-num 324
                   :sending-time (local-time:parse-timestring "2019-07-09T12:16:36.721")
                   :encrypt-method :none
                   :heart-int 1000)))
    (is (string= "8=FIX.4.49=6735=A49=larry56=bob34=32452=20190709-12:16:36.72198=0108=100010=047"
                (with-output-to-string (s)
                  (fix44:serialize-fix msg s))))))

(test serialize-msg-with-repeating-group
  (let ((msg (list :msg-type :logon
                   :sender-comp-id "larry"
                   :target-comp-id "bob"
                   :msg-seq-num 324
                   :sending-time (local-time:parse-timestring "2019-07-09T12:16:36.721")
                   :encrypt-method :none
                   :heart-int 1000
                   :no-msg-types '((:ref-msg-type "8" :msg-direction :send)
                                   (:ref-msg-type "D" :msg-direction :receive)))))
    (is (string= "8=FIX.4.49=9735=A49=larry56=bob34=32452=20190709-12:16:36.72198=0108=1000384=2372=8385=S372=D385=R10=210"
                 (fix44:str msg)))))

(test serialize-msg-with-enum
  (let ((msg (list :msg-type :market-data-request
                   :sender-comp-id "larry"
                   :target-comp-id "bob"
                   :msg-seq-num 324
                   :sending-time (local-time:parse-timestring "2019-07-09T12:16:36.721")
                   :md-req-id "a-request"
                   :subscription-request-type :snapshot-plus-updates
                   :market-depth 0
                   :no-md-entry-types ())))
    (is (string= "8=FIX.4.49=9135=V49=larry56=bob34=32452=20190709-12:16:36.721262=a-request263=1264=0267=0146=010=099"
                 (fix44:str msg)))))

(test serialize-msg-with-enum-without-corresponding-value
  (let ((msg (util:make-fix-msg :market-data-request
                                :sender-comp-id "larry"
                                :target-comp-id "bob"
                                :msg-seq-num 324
                                :sending-time (local-time:parse-timestring "2019-07-09T12:16:36.721")
                                :md-req-id "a-request"
                                :subscription-request-type 40289
                                :market-depth 0
                                :no-md-entry-types ())))
    (is (string= "8=FIX.4.49=9535=V49=larry56=bob34=32452=20190709-12:16:36.721262=a-request263=40289264=0267=0146=010=061"
                 (fix44:str msg)))))

(test serialize-msg-with-booleans
  (let ((msg-boolean-true (util:make-fix-msg :logon
                                             :encrypt-method :none
                                             :heart-int 20
                                             :reset-seq-num-flag t))
        (msg-boolean-false (util:make-fix-msg :logon
                                              :encrypt-method :none
                                              :heart-int 20
                                              :reset-seq-num-flag nil)))
    (is (string= "8=FIX.4.49=2335=A98=0108=20141=Y10=041"
                 (fix44:str msg-boolean-true)))
    (is (string= "8=FIX.4.49=2335=A98=0108=20141=N10=030"
                 (fix44:str msg-boolean-false)))))

(test serialize-msg-with-md-entry-types
  (let* ((msg (util:make-fix-msg :market-data-incremental-refresh
                                 :msg-seq-num 76
                                 :sender-comp-id "my-broker"
                                 :sender-sub-id "quotes"
                                 :target-comp-id "me"
                                 :target-sub-id "quotes"
                                 :sending-time (local-time:parse-timestring "2020-07-03t02:46:32.615000Z")
                                 :no-md-entries
                                 '((:md-update-action :new :md-entry-type :offer :md-entry-id "461072857" :symbol "1" :md-entry-px 1.12466d0 :md-entry-size 3000000)
                                   (:md-update-action :delete :md-entry-id "461072187" :symbol "1")
                                   (:md-update-action :new :md-entry-type :bid :md-entry-id "461072853" :symbol "1" :md-entry-px 1.12464d0 :md-entry-size 2000000)
                                   (:md-update-action :delete :md-entry-id "461072186" :symbol "1") (:md-update-action :delete :md-entry-id "461072183" :symbol "1")))))
    (is (string= "8=FIX.4.49=27035=X49=my-broker56=me34=7650=quotes57=quotes52=20200703-02:46:32.615268=5279=0269=1278=46107285755=1270=1.12466271=3000000.0279=2278=46107218755=1279=0269=0278=46107285355=1270=1.12464271=2000000.0279=2278=46107218655=1279=2278=46107218355=110=033"
                 (fix44:str msg)))))

(test serialize-multi-value-string
  (let ((msg (util:make-fix-msg :new-order-single
                                :exec-inst '(:not-held :ok-to-cross :peg-to-vwap))))
    (is (string= "8=FIX.4.49=1435=D18=1 B W10=152"
                 (fix44:str msg)))))

(test serialize-data-field
  (let ((msg (util:make-fix-msg :new-order-single
                                :signature "abc123")))
    (is (string= "8=FIX.4.49=2035=D93=689=abc12310=047"
                 (fix44:str msg)))))

(test serialize-data-field-with-delims
  (let ((msg (util:make-fix-msg :new-order-single
                                :signature "abc123=8293")))
    (is (string= "8=FIX.4.49=2935=D93=1489=abc123=829310=125"
                 (fix44:str msg)))))

(def-suite deserialize :in :cl-fix-suite)
(in-suite deserialize)

(test deserialize-simple-msg
  (let ((msg "8=FIX.4.49=7035=A49=larry56=bob34=32452=20190709-12:16:36.72100098=0108=100010=185")
        (msg-obj))
    (with-input-from-string (s msg)
      (setf msg-obj (fix44:read-fix s))
      (is (string= "FIX.4.4" (util:fix-field msg-obj :begin-string)))
      (is (eq :logon (util:fix-field msg-obj :msg-type))))))

(test deserialize-msg-with-repeating-group
  (let* ((msg "8=FIX.4.49=10035=A49=larry56=bob34=32452=20190709-12:16:36.72100098=0108=1000384=2372=8385=S372=D385=R10=131")
         (msg-obj))
    (with-input-from-string (s msg)
      (setf msg-obj (fix44:read-fix s))
      (is (string= "FIX.4.4" (util:fix-field msg-obj :begin-string)))
      (is (eq :logon (util:fix-field msg-obj :msg-type)))
      (is (= 2 (length (util:fix-field msg-obj :no-msg-types))))
      (is (string= "8" (util:fix-field (first (util:fix-field msg-obj :no-msg-types)) :ref-msg-type))))))

(test deserialize-simple-msg-with-surrounding-junk
  (let ((msg "xxxifij3298j9jasdpofj8=FIX.4.49=7035=A49=larry56=bob34=32452=20190709-12:16:36.72100098=0108=100010=185hahahhajajaja"))
    (with-input-from-string (s msg)
      (let ((msg-obj (fix44:read-fix s)))
        (is (string= "FIX.4.4" (util:fix-field msg-obj :begin-string)))
        (is (eq :logon (util:fix-field msg-obj :msg-type)))))))

(test deserialize-simple-msg-with-junk-filling-up-original-buffer-size
  (let ((buffer (make-array 128 :element-type 'character :fill-pointer 0))
        (msg "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX8=FIX.4.49=7035=A49=larry56=bob34=32452=20190709-12:16:36.72100098=0108=100010=185"))
    (with-input-from-string (s msg)
      (let ((msg-obj (fix44:read-fix s buffer)))
        (is (string= "FIX.4.4" (util:fix-field msg-obj :begin-string)))
        (is (eq :logon (util:fix-field msg-obj :msg-type)))))))

(test deserialize-eof-before-msg-end
  (let ((msg "xxxifij3298j9jasdpofj8=FIX.4.49=7035=A49=larry56=bob34=32452=20190709-12:16:36.72100098=0")
        (seen-condition nil))
    (with-input-from-string (s msg)
      (handler-bind ((end-of-file (lambda (e)
                                    (setf seen-condition e)
                                    (invoke-restart 'cl-fix-serialization::skip-message))))
        (is-false (fix44:read-fix s)))
      (is-true seen-condition))))

(test deserialize-with-enum
  (let ((msg (with-input-from-string (s "8=FIX.4.49=8235=V49=larry56=bob34=32452=20190709-12:16:36.721000262=a-request263=1264=010=221")
               (fix44:read-fix s))))
    (is (eq :market-data-request (util:fix-field msg :msg-type)))
    (is (eq :snapshot-plus-updates (util:fix-field msg :subscription-request-type)))))

(test deserialize-with-more-repeating-groups
  (let ((msg (with-input-from-string (s "8=FIX.4.49=26935=X49=my-broker56=me34=7650=quotes57=quotes52=20200703-02:46:32.615000268=5279=0269=1278=46107285755=1270=1.12466271=3000000279=2278=46107218755=1279=0269=0278=46107285355=1270=1.12464271=2000000279=2278=46107218655=1279=2278=46107218355=110=253")
               (fix44:read-fix s))))
    (is (= 5 (length (getf msg :no-md-entries))))))

(test deserialize-multi-value-string
  (let ((msg (with-input-from-string (s "8=FIX.4.49=1435=D18=1 B W10=152")
               (fix44:read-fix s))))
    (is (equalp '(:not-held :ok-to-cross :peg-to-vwap)
                (util:fix-field msg :exec-inst)))))

(test deserialize-data-field-basic
  (let ((msg (with-input-from-string (s "8=FIX.4.49=2035=D93=689=abc12310=047")
               (fix44:read-fix s))))
    (is (equalp "abc123"
                (util:fix-field msg :signature)))))

(test deserialize-data-field-with-delims
  (let ((msg (with-input-from-string (s "8=FIX.4.49=2935=D93=1489=abc123=829310=125")
               (fix44:read-fix s))))
    (is (equalp "abc123=8293"
                (util:fix-field msg :signature)))))

(test deserialize-data-field-with-invalid-length-field
  (let ((caught-err nil))
    (handler-bind ((cl-fix-serialization::serializer-error
                    (lambda (err)
                      (declare (ignore err))
                      (setf caught-err t)
                      (invoke-restart 'cl-fix-serialization::skip-message))))
      (let ((msg (with-input-from-string (s "8=FIX.4.49=2935=D93=1389=abc123=829310=124")
                   (fix44:read-fix s))))
        (is-false msg)
        (is-true caught-err)))))

(test deserialize-with-unknown-tag
  (let ((msg (with-input-from-string (s "8=FIX.4.49=1535=D11022=abc10=66")
               (fix44:read-fix s))))
    (is (equalp "abc"
                (util:fix-field msg :unknown-tag-11022)))))

(test deserialize-with-unknown-tag-in-rpt-group
  (let ((msg (with-input-from-string (s "8=FIX.4.49=5435=A384=2372=D385=S11029=abc372=81023=haha385=R10=127")
               (fix44:read-fix s))))
    (is (equalp "abc"  (getf (car (util:fix-field msg :no-msg-types)) :unknown-tag-11029)))
    (is (equalp "haha" (getf (cadr (util:fix-field msg :no-msg-types)) :unknown-tag-1023)))))

