;;; Copyright 2020 Sandipan Razzaque
;;;
;;; Licensed under the Apache License, Version 2.0 (the "License");
;;; you may not use this file except in compliance with the License.
;;; You may obtain a copy of the License at
;;;
;;;     http://www.apache.org/licenses/LICENSE-2.0
;;;
;;; Unless required by applicable law or agreed to in writing, software
;;; distributed under the License is distributed on an "AS IS" BASIS,
;;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;;; See the License for the specific language governing permissions and
;;; limitations under the License.

(in-package #:cl-fix-types-test)

(def-suite types :in :cl-fix-suite)
(in-suite types)

;; ===========================================================================================
;; Helper functions start here
;; ===========================================================================================

(defmacro is-monthyear (str &key year month (day 0) (week-code 0))
  `(is (equalp (cl-fix-types::string-to-monthyear ,str)
               (make-monthyear :year ,year :month ,month :day ,day :week-code ,week-code))))

(defun monthyear-err-p (str)
  (not
   (handler-case
       (cl-fix-types::string-to-monthyear str)
     (t ()
       nil))))

(defmacro is-ymd (str type &key year month day)
  (let ((parse-fn (ecase type
                    (localmktdate #'cl-fix-types::string-to-localmktdate)
                    (utcdateonly #'cl-fix-types::string-to-utcdateonly)))
        (make-fn (ecase type
                   (localmktdate #'cl-fix-types::make-localmktdate)
                   (utcdateonly #'cl-fix-types::make-utcdateonly))))
    `(is (equalp (funcall ,parse-fn ,str)
                 (apply ,make-fn (list :year ,year :month ,month :day ,day))))))

(defun ymd-err-p (str type)
  (let ((fn (ecase type
              (localmktdate #'cl-fix-types::string-to-localmktdate)
              (utcdateonly #'cl-fix-types::string-to-utcdateonly))))
    (not
     (handler-case
         (funcall fn str)
       (t ()
         nil)))))

(defmacro is-hms (str &key hour minute second (millisecond 0))
  `(is (equalp (cl-fix-types::string-to-utctimeonly ,str)
               (make-utctimeonly :hour ,hour
                                 :minute ,minute
                                 :second ,second
                                 :millisecond ,millisecond))))

(defun %err-p (fn str)
  (let ((saw-err))
    (handler-case
        (funcall fn str)
      (t ()
        (setf saw-err t)))
    saw-err))

(defun hms-err-p (str)
  (%err-p #'cl-fix-types::string-to-utctimeonly str))

(defun double-err-p (str)
  (%err-p #'cl-fix-types::string-to-double str))

(defun int-err-p (str)
  (%err-p #'cl-fix-types::string-to-int str))

(defun char-err-p (str)
  (%err-p #'cl-fix-types::string-to-char str))

(defun parsed-timestamp-equalp (str1 str2)
  (= 0
     (local-time:timestamp-difference (cl-fix-types::string-to-timestamp str1)
                                      (local-time:parse-timestring str2))))

(defun timestamp-err-p (str)
  (%err-p #'cl-fix-types::string-to-timestamp str))

(defun boolean-err-p (str)
  (%err-p #'cl-fix-types::string-to-boolean str))

;; ===========================================================================================
;; Tests start here
;; ===========================================================================================

(test monthyear-parsing
  (is-monthyear "201005" :year 2010 :month 5)
  (is-monthyear "20100504" :year 2010 :month 5 :day 4)
  (is-monthyear "201004w4" :year 2010 :month 4 :week-code 4))

(test monthyear-errors
  (is (monthyear-err-p ""))
  (is (monthyear-err-p "  "))
  (is (monthyear-err-p "201013"))
  (is (monthyear-err-p "2010-1"))
  (is (monthyear-err-p "-28311"))
  (is (monthyear-err-p "20104w8"))
  (is (monthyear-err-p "201005w6")))

(test monthyear-to-string
  (is-every equalp
       "202005" (cl-fix-types::monthyear-to-string
                 (cl-fix-types::make-monthyear :year 2020 :month 5))
       "201912" (cl-fix-types::monthyear-to-string
                 (cl-fix-types::make-monthyear :year 2019 :month 12))))

(test localmktdate-parsing
  (is-ymd "20190518" localmktdate :year 2019 :month 5 :day 18)
  (is-ymd "20201218" localmktdate :year 2020 :month 12 :day 18)
  (is-ymd "99991231" localmktdate :year 9999 :month 12 :day 31)
  (is-ymd "00000101" localmktdate :year 0 :month 1 :day 1))

(test localmktdate-errors
  (is (ymd-err-p "20190535" 'localmktdate))
  (is (ymd-err-p "2019-219" 'localmktdate))
  (is (ymd-err-p "    20190520" 'localmktdate))
  (is (ymd-err-p "20190520    " 'localmktdate)))

(test localmktdate-to-string
  (is-every equalp
    "20200512" (cl-fix-types::localmktdate-to-string
                (cl-fix-types::make-localmktdate :year 2020
                                                 :month 5
                                                 :day 12))
    "20201231" (cl-fix-types::localmktdate-to-string
                (cl-fix-types::make-localmktdate :year 2020
                                                 :month 12
                                                 :day 31))))

(test utcdateonly-parsing
  (is-ymd "20190518" utcdateonly :year 2019 :month 5 :day 18)
  (is-ymd "20201218" utcdateonly :year 2020 :month 12 :day 18)
  (is-ymd "99991231" utcdateonly :year 9999 :month 12 :day 31)
  (is-ymd "00000101" utcdateonly :year 0 :month 1 :day 1))

(test utcdateonly-errors
  (is (ymd-err-p "20190535" 'utcdateonly))
  (is (ymd-err-p "2019-219" 'utcdateonly))
  (is (ymd-err-p "    20190520" 'utcdateonly))
  (is (ymd-err-p "20190520    " 'utcdateonly)))

(test utcdateonly-to-string
  (is-every equalp
    "20200512" (cl-fix-types::utcdateonly-to-string
                (cl-fix-types::make-utcdateonly :year 2020
                                                :month 5
                                                :day 12))
    "20201231" (cl-fix-types::utcdateonly-to-string
                (cl-fix-types::make-utcdateonly :year 2020
                                                :month 12
                                                :day 31))))

(test utctimeonly-parsing
  (is-hms "12:32:37.333" :hour 12 :minute 32 :second 37 :millisecond 333)
  (is-hms "00:00:00.000" :hour 0 :minute 0 :second 0 :millisecond 0)
  (is-hms "23:59:60.999" :hour 23 :minute 59 :second 60 :millisecond 999))

(test utctimeonly-errors
  (is (hms-err-p ""))
  (is (hms-err-p "    "))
  (is (hms-err-p "12:32:37.333    "))
  (is (hms-err-p "12:99:37.333"))
  (is (hms-err-p "-1:32:37.333"))
  (is (hms-err-p "-1:32:37."))
  (is (hms-err-p "12:32:37.77"))
  (is (hms-err-p "12:32:37. 77"))
  (is (hms-err-p "12:32:37. 77")))

(test utctimeonly-to-string
  (is-every equalp
    "12:32:38.123" (cl-fix-types::utctimeonly-to-string
                    (cl-fix-types::make-utctimeonly :hour 12
                                                    :minute 32
                                                    :second 38
                                                    :millisecond 123))
    "12:32:38" (cl-fix-types::utctimeonly-to-string
                (cl-fix-types::make-utctimeonly :hour 12
                                                :minute 32
                                                :second 38))))

(test double-parsing
  (is-every =
    (cl-fix-types::string-to-double "0.5678") 0.5678d0
    (cl-fix-types::string-to-double "1.5678") 1.5678d0
    (cl-fix-types::string-to-double "1.5678000") 1.5678d0
    (cl-fix-types::string-to-double "0001.5678") 1.5678d0
    (cl-fix-types::string-to-double "1.") 1.0d0
    (cl-fix-types::string-to-double "1.000000000000000000000000000000000000000000000001") 1.000000000000000000000000000000000000000000000001d0
    (cl-fix-types::string-to-double "-27.2") -27.2d0))

(test double-errors
  (is (double-err-p "abcd"))
  (is (double-err-p ""))
  (is (double-err-p "   "))
  (is (double-err-p "23.32.42"))
  (is (double-err-p "23,32"))
  (is (double-err-p "23-032"))
  (is (double-err-p "      23.42"))
  (is (double-err-p "23.42  ")))

(test double-to-string
  (is-every equalp
    "1.234" (cl-fix-types::double-to-string 1.234d0)
    "0.0000001" (cl-fix-types::double-to-string 0.0000001d0)
    "0.00000000012" (cl-fix-types::double-to-string 1.2e-10)))

(test int-parsing
  (is-every =
    (cl-fix-types::string-to-int "1") 1
    (cl-fix-types::string-to-int "0") 0
    (cl-fix-types::string-to-int (format nil "~D" most-positive-fixnum)) most-positive-fixnum
    (cl-fix-types::string-to-int (format nil "~D" most-negative-fixnum)) most-negative-fixnum))

(test int-errors
  (is (int-err-p ""))
  (is (int-err-p "   "))
  (is (int-err-p "28.5"))
  (is (int-err-p "abcd"))
  (is (int-err-p "   89"))
  (is (int-err-p "89  ")))

(test char-parsing
  (is-every eq
    (cl-fix-types::string-to-char "A") #\A
    (cl-fix-types::string-to-char "B") #\B
    (cl-fix-types::string-to-char "c") #\c
    (cl-fix-types::string-to-char " ") #\Space))

(test char-err
  (is (char-err-p ""))
  (is (char-err-p "")))

(test timestamp-parsing
  (is-every parsed-timestamp-equalp
    "20200314-23:32:48" 	"2020-03-14T23:32:48"
    "20200314-23:32:48.367" 	"2020-03-14T23:32:48.367"
    "20200314-00:00:00" 	"2020-03-14T00:00:00.000"
    "20200229-00:00:00" 	"2020-02-29T00:00:00.000"))

(test timestamp-errors
  (loop for x in '(""
                   "random-chars"
                   "20200231-23:59:00.000"
                   "20200228-23:61:00.000"
                   "20200228-23:48:61.000"
                   "20200228-23:48:49.-32")
     do
       (is (timestamp-err-p x)
           "String '~A' should not have been parsed successfully as a timestamp."
           x)))

(test timestamp-to-string
  (is-every equalp
    "20200314-23:32:48.367" (cl-fix-types::timestamp-to-string
                             (local-time:parse-timestring "2020-03-14T23:32:48.367"))
    "20200314-23:32:48.000" (cl-fix-types::timestamp-to-string
                             (local-time:parse-timestring "2020-03-14T23:32:48"))))

(test boolean-parsing
  (is (cl-fix-types::string-to-boolean "Y"))
  (is (not (cl-fix-types::string-to-boolean "N"))))

(test boolean-errs
  (loop for x in '("n"
                   " "
                   "y"
                   "any"
                   "other"
                   "string."
                   "  Y"
                   "  N"
                   "Y  "
                   "N  ")
     do
       (is (boolean-err-p x)
           "String '~A' should not be considered a valid boolean value" x)))

(test boolean-to-string
  (is-every string=
    "Y" (cl-fix-types::boolean-to-string t)
    "N" (cl-fix-types::boolean-to-string nil)))
