;;; Copyright 2020 Sandipan Razzaque
;;;
;;; Licensed under the Apache License, Version 2.0 (the "License");
;;; you may not use this file except in compliance with the License.
;;; You may obtain a copy of the License at
;;;
;;;     http://www.apache.org/licenses/LICENSE-2.0
;;;
;;; Unless required by applicable law or agreed to in writing, software
;;; distributed under the License is distributed on an "AS IS" BASIS,
;;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;;; See the License for the specific language governing permissions and
;;; limitations under the License.

(asdf:defsystem #:cl-fix
  :description "A FIX (Financial Information eXchange) library for Common Lisp"
  :author "Sandipan Razzaque <z@fix9.net>"
  :license  "Apache 2.0"
  :version "0.2.0"
  :depends-on (:local-time
               :usocket
               :bordeaux-threads
               :alexandria
               :log4cl
               :parse-number
               :arrow-macros
               :cl-ppcre)
  :components ((:module "src"
                :serial t
                :components
                ((:file "package")
                 (:file "fastbuffer")
                 (:file "cl-fix-types")
                 (:file "cl-fix-serialization")
                 (:file "cl-fix-validation")
                 (:file "cl-fix-dict")
                 (:file "cl-fix-util")
                 (:file "cl-fix-fsm")
                 (:file "cl-fix-message-store")
                 (:file "cl-fix-session")
                 (:file "cl-fix-connector")
                 (:file "cl-fix-initiator")
                 (:file "cl-fix-acceptor"))))
  :in-order-to ((test-op (test-op "cl-fix/test"))))

(asdf:defsystem #:cl-fix/fix44
  :depends-on (:cl-fix)
  :components ((:module "dict"
                :serial t
                :components
                ((:file "fix44")))))

(asdf:defsystem #:cl-fix/test
    :depends-on (:cl-fix
                 :cl-fix/fix44
                 :fiveam
                 :trivial-gray-streams)
    :components ((:module "t"
                          :serial t
                          :components
                          ((:file "package")
                           (:file "fastbuffer-test")
                           (:file "cl-fix-test-util")
                           (:file "cl-fix-test")
                           (:file "cl-fix-dict-test")
                           (:file "cl-fix-initiator-test")
                           (:file "cl-fix-acceptor-test")
                           (:file "cl-fix-integration-test")
                           (:file "cl-fix-session-test")
                           (:file "cl-fix-fsm-test")
                           (:file "cl-fix-message-store-test")
                           (:file "cl-fix-types-test")
                           (:file "cl-fix-validation-test"))))
    :perform (test-op (o c)
                      (symbol-call :log :config :error)
                      (when (not (handler-bind ((warning #'muffle-warning))
                                   (if (string= (uiop:getenv "CL_FIX_PERF_TEST" ) "true")
                                       (symbol-call :fiveam :run! :cl-fix-perf)
                                       (symbol-call :fiveam :run! :cl-fix-suite))))
                        (error "There were test failures."))))
