;;;; quickfix-xml-converter.lisp
;;;; Helper to convert quickfix-style XML definitions into cl-fix dictionary definitions.
;;;; NOT intended as a full-resolution converter. The output still needs some hand-crafting afterwards.

;;; Copyright 2020 Sandipan Razzaque
;;;
;;; Licensed under the Apache License, Version 2.0 (the "License");
;;; you may not use this file except in compliance with the License.
;;; You may obtain a copy of the License at
;;;
;;;     http://www.apache.org/licenses/LICENSE-2.0
;;;
;;; Unless required by applicable law or agreed to in writing, software
;;; distributed under the License is distributed on an "AS IS" BASIS,
;;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;;; See the License for the specific language governing permissions and
;;; limitations under the License.

;;; This is a little helper utility to help get 80% of the way there when converting a quickfix
;;; xml dictionary to the s-exp style dictionary definition for cl-fix.

;;; It's not, at this stage, intended for a "full-auto" conversion, and the output still needs some
;;; hand-tweaking.

;; TODO: Still need to hand-fix "no-md-entry/ies" fields and groups

(ql:quickload '(:cxml :arrow-macros :cl-ppcre :optima))

(defpackage #:quickfix-xml-converter
  (:use #:cl #:arrow-macros)
  (:import-from :cl-ppcre
                regex-replace-all)
  (:import-from :optima
                match))

(in-package #:quickfix-xml-converter)

(defparameter *xml* nil)
(defparameter *fields* nil)
(defparameter *header* nil)
(defparameter *messages* nil)
(defparameter *trailer* nil)
(defparameter *components* nil)

(defun load-xml (xml-file)
  (cxml:parse-file xml-file (cxml-dom:make-dom-builder)))

(defun child (parent name)
  (loop for n across (dom:child-nodes parent)
     when (and (dom:element-p n)
               (string= name (dom:tag-name n)))
     return n))

(defun children (parent &rest names)
  (loop for n across (dom:child-nodes parent)
     when (and (dom:element-p n)
               (member (dom:tag-name n) names :test #'string=))
     collect n))

(defun nmt (ls)
  "nmt = 'not empty'"
  (remove-if-not #'identity ls))

(defun lisp-case (s)
  ;; This will obviously need successive refinement for all edge cases.
  (-<> s
    (regex-replace-all "^MD\([a-zA-Z]\)" <> "md-\\1")
    (regex-replace-all "NoMD\([a-zA-Z]\)" <> "no-md-\\1")
    (regex-replace-all "^CP\([a-zA-Z]\)" <> "cp-\\1")
    (regex-replace-all "\([a-z]\)CP\([a-zA-Z]\)" <> "\\1-cp-\\2")
    (regex-replace-all "^IOI\([a-zA-Z]\)" <> "ioi-\\1")
    (regex-replace-all "\([a-z]\)IOI\([a-zA-Z]\)" <> "\\1-ioi-\\2")
    (regex-replace-all "\([a-z]\)\(ID\)" <> "\\1-id")
    (regex-replace-all "\([a-z]\)\([A-Z][a-z]\)\+" <> "\\1-\\2")
    (string-downcase <>)))

(defun enum-p (fld)
  (and (not (match (dom:get-attribute fld "type")
              ((or "BOOLEAN" "NUMINGROUP") t)))
       (> (length (children fld "value")) 0)))

(defun fmt-string (fld tp)
  (let ((coerced-type (match (dom:get-attribute fld "name")
                        ((or "MiscFeeType" "MassCancelRejectReason")
                         ;; Unfortunately the FIX spec thinks '10' is a single character. This
                         ;; breaks if we treat these as char-enums.
                         "STRING")
                        (* tp))))
    
    (match coerced-type
      ("INT" "~A")
      ((or "STRING" "MULTIPLEVALUESTRING") "\"~A\"")
      ("CHAR" "#\\~A")
      (t "~A"))))

(defun lisp-type (fld s)
  ;; See https://btobits.com/fixopaedia/fixdic44/data_types_.html
  (cond
    ((enum-p fld)
     (match s
       ((or "INT" "CHAR" "STRING" "MULTIPLEVALUESTRING")
        (format nil "(~A ~{~A~^~%~})"
                (if (string= "MULTIPLEVALUESTRING" s)
                    "multiple-value-string-enum"
                    (format nil "~A-enum" (string-downcase s)))
                (mapcar (lambda (x) (format nil ":~A ~A"
                                            (-<> (dom:get-attribute x "description")
                                              (regex-replace-all "_" <> "-")
                                              (string-downcase <>))
                                            (format nil
                                                    (fmt-string fld s)
                                                    (dom:get-attribute x "enum"))))
                        (children fld "value"))))
       (* (error "Unhandled enum type: ~A" s))))
    (t (match s
         ("STRING" "string")
         ("NUMINGROUP" "cl-fix:numingroup")
         ("LENGTH" "cl-fix:len")
         ("BOOLEAN" "boolean")
         ("CHAR" "character")
         ("FLOAT" "double-float")
         ((or "CURRENCY" "COUNTRY" "EXCHANGE" "MONTHYEAR"
              "QTY" "PRICE" "PRICEOFFSET" "AMT" "PERCENTAGE"
              "INT" "SEQNUM" "TAGNUM" "DAYOFMONTH"
              "UTCTIMESTAMP" "LOCALMKTDATE"
              "DATA" "UTCDATEONLY" "UTCTIMEONLY")
          (format nil "cl-fix:~A" (string-downcase s)))
         (* (error "Unhandled type: ~A" s))))))

(defun field-def (x)
  (let ((tp (lisp-type x (dom:get-attribute x "type"))))
    (when tp
      (format nil "(:~A \"~A\" :type ~A)"
              (lisp-case (dom:get-attribute x "name"))
              (dom:get-attribute x "number")
              tp))))

(defun generate-field-defs (field-nodes)
  (format nil "(deffields~%~{  ~A~^~%~})~%"
          (nmt (mapcar #'field-def (children field-nodes "field")))))

(defun msg-field-def (field &optional (nest-level 1))
  (let ((tag (dom:tag-name field)))
    (match tag
      ("group"
       (format nil "(group ~%    ~A~%~{    ~A~^~%~})"
               (let ((count-field (lisp-case (dom:get-attribute field "name"))))
                 (format
                  nil
                  (if (string= (dom:get-attribute field "required") "Y")
                      "(:~A :required t)"
                      ":~A")
                  count-field))
               (mapcar (lambda (x) (msg-field-def x (1+ nest-level)))
                       (children field "field" "component" "group"))))
      ("component"
       (if (string= "Y" (dom:get-attribute field "required"))
           (format nil "(component :~A :required t)" (lisp-case (dom:get-attribute field "name")))
           (format nil "(component :~A)" (lisp-case (dom:get-attribute field "name")))))
      ("field"
       (if (string= "Y" (dom:get-attribute field "required"))
           (format nil "(:~A :required t)" (lisp-case (dom:get-attribute field "name")))
           (format nil ":~A" (lisp-case (dom:get-attribute field "name"))))))))

(defun generate-header-def (header-node)
  (format nil "(defheader~%~{  ~A~^~%~})~%"
          (nmt (mapcar #'msg-field-def (children header-node "field" "group" "component")))))

(defun generate-trailer-def (trailer-node)
  (format nil "(deftrailer~%~{  ~A~^~%~})~%"
          (nmt (mapcar #'msg-field-def (children trailer-node "field" "group" "component")))))

(defun component-def (cmp)
  (format nil "(defcomponent ~A ()~%~{  ~A~^~%~})~%"
          (lisp-case (dom:get-attribute cmp "name"))
          (mapcar #'msg-field-def (children cmp "field" "component" "group"))))

(defun generate-component-defs (component-root)
  (format nil "~{~A~%~}"
          (nmt (mapcar #'component-def (children component-root "component")))))

(defun message-def (msg)
  (format nil "(defmessage ~A \"~A\" ()~%~{  ~A~^~%~})~%"
          (lisp-case (dom:get-attribute msg "name"))
          (dom:get-attribute msg "msgtype")
          (nmt (mapcar #'msg-field-def (children msg "field" "component" "group")))))

(defun generate-message-defs (message-root)
  (format nil "~{~A~%~}" (nmt (mapcar #'message-def (children message-root "message")))))

(defun convert (xml-file &optional (output t))
  (setf *xml* (load-xml xml-file))
  (let ((root (child *xml* "fix")))
    (setf *fields* (child root "fields")
          *header* (child root "header")
          *messages* (child root "messages")
          *trailer* (child root "trailer")
          *components* (child root "components"))
    (log:info "From ~A, read: ~A fields (~A in header and ~A in trailer), ~A messages & ~A components"
              xml-file
              (length (dom:child-nodes *fields*))
              (length (dom:child-nodes *header*))
              (length (dom:child-nodes *trailer*))
              (length (dom:child-nodes *messages*))
              (length (dom:child-nodes *components*)))
    (format output (generate-field-defs *fields*))
    (terpri output)
    (format output (generate-header-def *header*))
    (terpri output)
    (format output (generate-trailer-def *trailer*))
    (terpri output)
    (format output (generate-component-defs *components*))
    (terpri output)
    (format output (generate-message-defs *messages*))
    (terpri output)))

(defun component-usage-count ()
  (flet ((get-name (x) (dom:get-attribute x "name")))
    (let ((counts
            (loop
               for c in (children *components* "component")
               for name = (dom:get-attribute c "name")
               collect (list name
                             (loop
                                for m in (concatenate 'list (children *messages* "message") (children *components* "component") (list *header* *trailer*))
                                for mc = (children m "component")
                                for mg = (children m "group")
                                summing (+ (count name mc :key #'get-name :test #'string=)
                                           (loop
                                              for g in mg
                                              summing (count name (children g "component") :key #'get-name :test #'string=))))
                             (length (children c "component" "field" "group"))))))
      (sort counts #'> :key #'cadr))))
