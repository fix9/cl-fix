;;;; fix44 - FIX.4.4 dictionary

;;; Copyright 2020 Sandipan Razzaque
;;;
;;; Licensed under the Apache License, Version 2.0 (the "License");
;;; you may not use this file except in compliance with the License.
;;; You may obtain a copy of the License at
;;;
;;;     http://www.apache.org/licenses/LICENSE-2.0
;;;
;;; Unless required by applicable law or agreed to in writing, software
;;; distributed under the License is distributed on an "AS IS" BASIS,
;;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;;; See the License for the specific language governing permissions and
;;; limitations under the License.

(defpackage #:fix44
  (:use #:cl #:cl-fix-dict))

(in-package #:fix44)

(defdict :fix44 "FIX.4.4")

(deffields
  (:account "1" :type string)
  (:adv-id "2" :type string)
  (:adv-ref-id "3" :type string)
  (:adv-side "4" :type (char-enum :buy #\B
                                  :sell #\S
                                  :cross #\X
                                  :trade #\T))
  (:adv-trans-type "5" :type (string-enum :new "N"
                                          :cancel "C"
                                          :replace "R"))
  (:avg-px "6" :type cl-fix:price)
  (:begin-seq-no "7" :type cl-fix:seqnum)
  (:begin-string "8" :type string :value "FIX.4.4")
  (:body-length "9" :type cl-fix:len)
  (:check-sum "10" :type string)
  (:cl-ord-id "11" :type string)
  (:commission "12" :type cl-fix:amt)
  (:comm-type "13" :type (char-enum :per-unit #\1
                                    :percentage #\2
                                    :absolute #\3
                                    :4 #\4
                                    :5 #\5
                                    :points-per-bond-or-contract-supply-contractmultiplier #\6))
  (:cum-qty "14" :type cl-fix:qty)
  (:currency "15" :type cl-fix:currency)
  (:end-seq-no "16" :type cl-fix:seqnum)
  (:exec-id "17" :type string)
  (:exec-inst "18" :type (multiple-value-string-enum :not-held "1"
                                                     :work "2"
                                                     :go-along "3"
                                                     :over-the-day "4"
                                                     :held "5"
                                                     :participate-dont-initiate "6"
                                                     :strict-scale "7"
                                                     :try-to-scale "8"
                                                     :stay-on-bidside "9"
                                                     :stay-on-offerside "0"
                                                     :no-cross "A"
                                                     :ok-to-cross "B"
                                                     :call-first "C"
                                                     :percent-of-volume "D"
                                                     :do-not-increase "E"
                                                     :do-not-reduce "F"
                                                     :all-or-none "G"
                                                     :reinstate-on-system-failure "H"
                                                     :institutions-only "I"
                                                     :reinstate-on-trading-halt "J"
                                                     :cancel-on-trading-halt "K"
                                                     :last-peg "L"
                                                     :mid-price-peg "M"
                                                     :non-negotiable "N"
                                                     :opening-peg "O"
                                                     :market-peg "P"
                                                     :cancel-on-system-failure "Q"
                                                     :primary-peg "R"
                                                     :suspend "S"
                                                     :customer-display-instruction "U"
                                                     :netting "V"
                                                     :peg-to-vwap "W"
                                                     :trade-along "X"
                                                     :try-to-stop "Y"
                                                     :cancel-if-not-best "Z"
                                                     :trailing-stop-peg "a"
                                                     :strict-limit "b"
                                                     :ignore-price-validity-checks "c"
                                                     :peg-to-limit-price "d"
                                                     :work-to-target-strategy "e"))
  (:exec-ref-id "19" :type string)
  (:handl-inst "21" :type (char-enum :automated-execution-order-private-no-broker-intervention #\1
                                     :automated-execution-order-public-broker-intervention-ok #\2
                                     :manual-order-best-execution #\3))
  (:security-id-source "22" :type (string-enum :cusip "1"
                                               :sedol "2"
                                               :quik "3"
                                               :isin-number "4"
                                               :ric-code "5"
                                               :iso-currency-code "6"
                                               :iso-country-code "7"
                                               :exchange-symbol "8"
                                               :consolidated-tape-association "9"
                                               :bloomberg-symbol "A"
                                               :wertpapier "B"
                                               :dutch "C"
                                               :valoren "D"
                                               :sicovam "E"
                                               :belgian "F"
                                               :common "G"
                                               :clearing-house "H"
                                               :isda-fpml-product-specification "I"
                                               :options-price-reporting-authority "J"))
  (:ioi-id "23" :type string)
  (:ioi-qlty-ind "25" :type (char-enum :low #\L
                                       :medium #\M
                                       :high #\H))
  (:ioi-ref-id "26" :type string)
  (:ioi-qty "27" :type (string-enum :small "S"
                                    :medium "M"
                                    :large "L"))
  (:ioi-trans-type "28" :type (char-enum :new #\N
                                         :cancel #\C
                                         :replace #\R))
  (:last-capacity "29" :type (char-enum :agent #\1
                                        :cross-as-agent #\2
                                        :cross-as-principal #\3
                                        :principal #\4))
  (:last-mkt "30" :type cl-fix:exchange)
  (:last-px "31" :type cl-fix:price)
  (:last-qty "32" :type cl-fix:qty)
  (:no-lines-text "33" :type cl-fix:numingroup)
  (:msg-seq-num "34" :type cl-fix:seqnum)
  (:msg-type "35" :type (string-enum :heartbeat "0"
                                     :test-request "1"
                                     :resend-request "2"
                                     :reject "3"
                                     :sequence-reset "4"
                                     :logout "5"
                                     :indication-of-interest "6"
                                     :advertisement "7"
                                     :execution-report "8"
                                     :order-cancel-reject "9"
                                     :logon "A"
                                     :news "B"
                                     :email "C"
                                     :new-order-single "D"
                                     :order-list "E"
                                     :order-cancel-request "F"
                                     :order-cancel-replace-request "G"
                                     :order-status-request "H"
                                     :allocation-instruction "J"
                                     :list-cancel-request "K"
                                     :list-execute "L"
                                     :list-status-request "M"
                                     :list-status "N"
                                     :allocation-instruction-ack "P"
                                     :dont-know-trade "Q"
                                     :quote-request "R"
                                     :quote "S"
                                     :settlement-instructions "T"
                                     :market-data-request "V"
                                     :market-data-snapshot-full-refresh "W"
                                     :market-data-incremental-refresh "X"
                                     :market-data-request-reject "Y"
                                     :quote-cancel "Z"
                                     :quote-status-request "a"
                                     :mass-quote-acknowledgement "b"
                                     :security-definition-request "c"
                                     :security-definition "d"
                                     :security-status-request "e"
                                     :security-status "f"
                                     :trading-session-status-request "g"
                                     :trading-session-status "h"
                                     :mass-quote "i"
                                     :business-message-reject "j"
                                     :bid-request "k"
                                     :bid-response "l"
                                     :list-strike-price "m"
                                     :xml-message "n"
                                     :registration-instructions "o"
                                     :registration-instructions-response "p"
                                     :order-mass-cancel-request "q"
                                     :order-mass-cancel-report "r"
                                     :new-order-s "s"
                                     :cross-order-cancel-replace-request "t"
                                     :cross-order-cancel-request "u"
                                     :security-type-request "v"
                                     :security-types "w"
                                     :security-list-request "x"
                                     :security-list "y"
                                     :derivative-security-list-request "z"
                                     :derivative-security-list "AA"
                                     :new-order-ab "AB"
                                     :multileg-order-cancel-replace "AC"
                                     :trade-capture-report-request "AD"
                                     :trade-capture-report "AE"
                                     :order-mass-status-request "AF"
                                     :quote-request-reject "AG"
                                     :rfq-request "AH"
                                     :quote-status-report "AI"
                                     :quote-response "AJ"
                                     :confirmation "AK"
                                     :position-maintenance-request "AL"
                                     :position-maintenance-report "AM"
                                     :request-for-positions "AN"
                                     :request-for-positions-ack "AO"
                                     :position-report "AP"
                                     :trade-capture-report-request-ack "AQ"
                                     :trade-capture-report-ack "AR"
                                     :allocation-report "AS"
                                     :allocation-report-ack "AT"
                                     :confirmation-ack "AU"
                                     :settlement-instruction-request "AV"
                                     :assignment-report "AW"
                                     :collateral-request "AX"
                                     :collateral-assignment "AY"
                                     :collateral-response "AZ"
                                     :collateral-report "BA"
                                     :collateral-inquiry "BB"
                                     :network-bc "BC"
                                     :network-bd "BD"
                                     :user-request "BE"
                                     :user-response "BF"
                                     :collateral-inquiry-ack "BG"
                                     :confirmation-request "BH"))
  (:new-seq-no "36" :type cl-fix:seqnum)
  (:order-id "37" :type string)
  (:order-qty "38" :type cl-fix:qty)
  (:ord-status "39" :type (char-enum :new #\0
                                     :partially-filled #\1
                                     :filled #\2
                                     :done-for-day #\3
                                     :canceled #\4
                                     :pending-cancel #\6
                                     :stopped #\7
                                     :rejected #\8
                                     :suspended #\9
                                     :pending-new #\A
                                     :calculated #\B
                                     :expired #\C
                                     :accepted-for-bidding #\D
                                     :pending-replace #\E))
  (:ord-type "40" :type (char-enum :market #\1
                                   :limit #\2
                                   :stop #\3
                                   :stop-limit #\4
                                   :with-or-without #\6
                                   :limit-or-better #\7
                                   :limit-with-or-without #\8
                                   :on-basis #\9
                                   :previously-quoted #\D
                                   :previously-indicated #\E
                                   :forex #\G
                                   :funari #\I
                                   :market-if-touched #\J
                                   :market-with-leftover-as-limit #\K
                                   :previous-fund-valuation-point #\L
                                   :next-fund-valuation-point #\M
                                   :pegged #\P))
  (:orig-ord-id "41" :type string)
  (:orig-time "42" :type cl-fix:utctimestamp)
  (:poss-dup-flag "43" :type boolean)
  (:price "44" :type cl-fix:price)
  (:ref-seq-num "45" :type cl-fix:seqnum)
  (:security-id "48" :type string)
  (:sender-comp-id "49" :type string)
  (:sender-sub-id "50" :type string)
  (:sending-time "52" :type cl-fix:utctimestamp)
  (:quantity "53" :type cl-fix:qty)
  (:side "54" :type (char-enum :buy #\1
                               :sell #\2
                               :buy-minus #\3
                               :sell-plus #\4
                               :sell-short #\5
                               :sell-short-exempt #\6
                               :undisclosed #\7
                               :cross #\8
                               :cross-short #\9
                               :cross-short-exempt #\A
                               :as-defined #\B
                               :opposite #\C
                               :subscribe #\D
                               :redeem #\E
                               :lend #\F
                               :borrow #\G))
  (:symbol "55" :type string)
  (:target-comp-id "56" :type string)
  (:target-sub-id "57" :type string)
  (:text "58" :type string)
  (:time-force "59" :type (char-enum :day #\0
                                     :good-till-cancel #\1
                                     :at-the-opening #\2
                                     :immediate-or-cancel #\3
                                     :fill-or-kill #\4
                                     :good-till-crossing #\5
                                     :good-till-date #\6
                                     :at-the-close #\7))
  (:transact-time "60" :type cl-fix:utctimestamp)
  (:urgency "61" :type (char-enum :normal #\0
                                  :flash #\1
                                  :background #\2))
  (:valid-until-time "62" :type cl-fix:utctimestamp)
  (:settl-type "63" :type (char-enum :regular #\0
                                     :cash #\1
                                     :next-day #\2
                                     :t-plus-2 #\3
                                     :t-plus-3 #\4
                                     :t-plus-4 #\5
                                     :future #\6
                                     :when-and-if-issued #\7
                                     :sellers-option #\8
                                     :t-plus-5 #\9))
  (:settl-date "64" :type cl-fix:localmktdate)
  (:symbol-sfx "65" :type string)
  (:list-id "66" :type string)
  (:list-seq-no "67" :type cl-fix:int)
  (:tot-orders "68" :type cl-fix:int)
  (:list-exec-inst "69" :type string)
  (:alloc-id "70" :type string)
  (:alloc-trans-type "71" :type (char-enum :new #\0
                                           :replace #\1
                                           :cancel #\2))
  (:ref-alloc-id "72" :type string)
  (:no-orders "73" :type cl-fix:numingroup)
  (:avg-precision "74" :type cl-fix:int)
  (:trade-date "75" :type cl-fix:localmktdate)
  (:position-effect "77" :type (char-enum :open #\O
                                          :close #\C
                                          :rolled #\R
                                          :fifo #\F))
  (:no-allocs "78" :type cl-fix:numingroup)
  (:alloc-account "79" :type string)
  (:alloc-qty "80" :type cl-fix:qty)
  (:process-code "81" :type (char-enum :regular #\0
                                       :soft-dollar #\1
                                       :step-in #\2
                                       :step-out #\3
                                       :soft-dollar-step-in #\4
                                       :soft-dollar-step-out #\5
                                       :plan-sponsor #\6))
  (:no-rpts "82" :type cl-fix:int)
  (:rpt-seq "83" :type cl-fix:int)
  (:cxl-qty "84" :type cl-fix:qty)
  (:no-dlvy-inst "85" :type cl-fix:numingroup)
  (:alloc-status "87" :type (int-enum :accepted 0
                                      :block-level-reject 1
                                      :account-level-reject 2
                                      :received 3
                                      :incomplete 4
                                      :rejected-by-intermediary 5))
  (:alloc-rej-code "88" :type (int-enum :unknown-account 0
                                        :incorrect-quantity 1
                                        :incorrect-average-price 2
                                        :unknown-executing-broker-mnemonic 3
                                        :commission-difference 4
                                        :unknown-orderid 5
                                        :unknown-listid 6
                                        :other 7
                                        :incorrect-allocated-quantity 8
                                        :calculation-difference 9
                                        :unknown-or-stale-execid 10
                                        :mismatched-data-value 11
                                        :unknown-clordid 12
                                        :warehouse-request-rejected 13))
  (:signature "89" :type cl-fix:data)
  (:secure-data-len "90" :type cl-fix:len :data-field :secure-data)
  (:secure-data "91" :type cl-fix:data)
  (:signature-length "93" :type cl-fix:len :data-field :signature)
  (:email-type "94" :type (char-enum :new #\0
                                     :reply #\1
                                     :admin-reply #\2))
  (:raw-data-length "95" :type cl-fix:len :data-field :raw-data)
  (:raw-data "96" :type cl-fix:data)
  (:poss-resend "97" :type boolean)
  (:encrypt-method "98" :type (int-enum :none 0
                                        :pkcs 1
                                        :des 2
                                        :pkcs-des 3
                                        :pgp-des 4
                                        :pgp-des-md5 5
                                        :pem-des-md5 6))
  (:stop-px "99" :type cl-fix:price)
  (:ex-destination "100" :type cl-fix:exchange)
  (:cxl-rej-reason "102" :type (int-enum :too-late-to-cancel 0
                                         :unknown-order 1
                                         :broker 2
                                         :order-already-in-pending-cancel-or-pending-replace-status 3
                                         :unable-to-process-order-mass-cancel-request 4
                                         :origordmodtime 5
                                         :duplicate-clordid 6
                                         :other 99))
  (:ord-rej-reason "103" :type (int-enum :broker 0
                                         :unknown-symbol 1
                                         :exchange-closed 2
                                         :order-exceeds-limit 3
                                         :too-late-to-enter 4
                                         :unknown-order 5
                                         :duplicate-order 6
                                         :duplicate-of-a-verbally-communicated-order 7
                                         :stale-order 8
                                         :trade-along-required 9
                                         :invalid-investor-id 10
                                         :unsupported-order-characteristic12-surveillence-option 11
                                         :incorrect-quantity 13
                                         :incorrect-allocated-quantity 14
                                         :unknown-account 15
                                         :other 99))
  (:ioi-qualifier "104" :type (char-enum :all-or-none #\A
                                         :market-on-close #\B
                                         :at-the-close #\C
                                         :vwap #\D
                                         :in-touch-with #\I
                                         :limit #\L
                                         :more-behind #\M
                                         :at-the-open #\O
                                         :taking-a-position #\P
                                         :at-the-market #\Q
                                         :ready-to-trade #\R
                                         :portfolio-shown #\S
                                         :through-the-day #\T
                                         :versus #\V
                                         :indication #\W
                                         :crossing-opportunity #\X
                                         :at-the-midpoint #\Y
                                         :pre-open #\Z))
  (:issuer "106" :type string)
  (:security-desc "107" :type string)
  (:heart-int "108" :type cl-fix:int)
  (:min-qty "110" :type cl-fix:qty)
  (:max-floor "111" :type cl-fix:qty)
  (:test-req-id "112" :type string)
  (:report-exch "113" :type boolean)
  (:locate-reqd "114" :type boolean)
  (:on-behalf-comp-id "115" :type string)
  (:on-behalf-sub-id "116" :type string)
  (:quote-id "117" :type string)
  (:net-money "118" :type cl-fix:amt)
  (:settl-curr-amt "119" :type cl-fix:amt)
  (:settl-currency "120" :type cl-fix:currency)
  (:forex-req "121" :type boolean)
  (:orig-sending-time "122" :type cl-fix:utctimestamp)
  (:gap-fill-flag "123" :type boolean)
  (:no-execs "124" :type cl-fix:numingroup)
  (:expire-time "126" :type cl-fix:utctimestamp)
  (:dkreason "127" :type (char-enum :unknown-symbol #\A
                                    :wrong-side #\B
                                    :quantity-exceeds-order #\C
                                    :no-matching-order #\D
                                    :price-exceeds-limit #\E
                                    :calculation-difference #\F
                                    :other #\Z))
  (:deliver-comp-id "128" :type string)
  (:deliver-sub-id "129" :type string)
  (:ioi-natural-flag "130" :type boolean)
  (:quote-req-id "131" :type string)
  (:bid-px "132" :type cl-fix:price)
  (:offer-px "133" :type cl-fix:price)
  (:bid-size "134" :type cl-fix:qty)
  (:offer-size "135" :type cl-fix:qty)
  (:no-misc-fees "136" :type cl-fix:numingroup)
  (:misc-fee-amt "137" :type cl-fix:amt)
  (:misc-fee-curr "138" :type cl-fix:currency)
  (:misc-fee-type "139" :type (char-enum :regulatory "1"
                                         :tax "2"
                                         :local-commission "3"
                                         :exchange-fees "4"
                                         :stamp "5"
                                         :levy "6"
                                         :other "7"
                                         :markup "8"
                                         :consumption-tax "9"
                                         :per-transaction "10"
                                         :conversion "11"
                                         :agent "12"))
  (:prev-close-px "140" :type cl-fix:price)
  (:reset-seq-num-flag "141" :type boolean)
  (:sender-location-id "142" :type string)
  (:target-location-id "143" :type string)
  (:on-behalf-location-id "144" :type string)
  (:deliver-location-id "145" :type string)
  (:no-related-sym "146" :type cl-fix:numingroup)
  (:subject "147" :type string)
  (:headline "148" :type string)
  (:urllink "149" :type string)
  (:exec-type "150" :type (char-enum :new #\0
                                     :done-for-day #\3
                                     :canceled #\4
                                     :replace #\5
                                     :pending-cancel #\6
                                     :stopped #\7
                                     :rejected #\8
                                     :suspended #\9
                                     :pending-new #\A
                                     :calculated #\B
                                     :expired #\C
                                     :restated #\D
                                     :pending-replace #\E
                                     :trade #\F
                                     :trade-correct #\G
                                     :trade-cancel #\H
                                     :order-status #\I))
  (:leaves-qty "151" :type cl-fix:qty)
  (:cash-order-qty "152" :type cl-fix:qty)
  (:alloc-avg-px "153" :type cl-fix:price)
  (:alloc-net-money "154" :type cl-fix:amt)
  (:settl-curr-rate "155" :type double-float)
  (:settl-curr-rate-calc "156" :type (char-enum :multiply #\M
                                                :divide #\D))
  (:num-days-interest "157" :type cl-fix:int)
  (:accrued-interest-rate "158" :type cl-fix:percentage)
  (:accrued-interest-amt "159" :type cl-fix:amt)
  (:settl-inst-mode "160" :type (char-enum :standing-instructions-provided #\1
                                           :specific-order-for-a-single-account #\4
                                           :request-reject #\5))
  (:alloc-text "161" :type string)
  (:settl-inst-id "162" :type string)
  (:settl-inst-trans-type "163" :type (char-enum :new #\N
                                                 :cancel #\C
                                                 :replace #\R
                                                 :restate #\T))
  (:email-thread-id "164" :type string)
  (:settl-inst-source "165" :type (char-enum :brokers-instructions #\1
                                             :institutions-instructions #\2
                                             :investor #\3))
  (:security-type "167" :type (string-enum :future "FUT"
                                           :option "OPT"
                                           :euro-supranational-coupons "EUSUPRA"
                                           :federal-agency-coupon "FAC"
                                           :federal-agency-discount-note "FADN"
                                           :private-export-funding "PEF"
                                           :usd-supranational-coupons "SUPRA"
                                           :corporate-bond "CORP"
                                           :corporate-private-placement "CPP"
                                           :convertible-bond "CB"
                                           :dual-currency "DUAL"
                                           :euro-corporate-bond "EUCORP"
                                           :indexed-linked "XLINKD"
                                           :structured-notes "STRUCT"
                                           :yankee-corporate-bond "YANK"
                                           :foreign-exchange-contract "FOR"
                                           :common-stock "CS"
                                           :preferred-stock "PS"
                                           :brady-bond "BRADY"
                                           :euro-sovereigns "EUSOV"
                                           :us-treasury-bond "TBOND"
                                           :interest-strip-from-any-bond-or-note "TINT"
                                           :treasury-inflation-protected-securities "TIPS"
                                           :principal-strip-of-a-callable-bond-or-note "TCAL"
                                           :principal-strip-from-a-non-callable-bond-or-note "TPRN"
                                           :us-treasury-note-ust "UST"
                                           :us-treasury-bill-ustb "USTB"
                                           :us-treasury-note-tnote "TNOTE"
                                           :us-treasury-bill-tbill "TBILL"
                                           :repurchase "REPO"
                                           :forward "FORWARD"
                                           :buy-sellback "BUYSELL"
                                           :securities-loan "SECLOAN"
                                           :securities-pledge "SECPLEDGE"
                                           :term-loan "TERM"
                                           :revolver-loan "RVLV"
                                           :revolver-term-loan "RVLVTRM"
                                           :bridge-loan "BRIDGE"
                                           :letter-of-credit "LOFC"
                                           :swing-line-facility "SWING"
                                           :debtor-in-possession "DINP"
                                           :defaulted "DEFLTED"
                                           :withdrawn "WITHDRN"
                                           :replaced "REPLACD"
                                           :matured "MATURED"
                                           :amended-restated "AMENDED"
                                           :retired "RETIRED"
                                           :bankers-acceptance "BA"
                                           :bank-notes "BN"
                                           :bill-of-exchanges "BOX"
                                           :certificate-of-deposit "CD"
                                           :call-loans "CL"
                                           :commercial-paper "CP"
                                           :deposit-notes "DN"
                                           :euro-certificate-of-deposit "EUCD"
                                           :euro-commercial-paper "EUCP"
                                           :liquidity-note "LQN"
                                           :medium-term-notes "MTN"
                                           :overnight "ONITE"
                                           :promissory-note "PN"
                                           :plazos-fijos "PZFJ"
                                           :short-term-loan-note "STN"
                                           :time-deposit "TD"
                                           :extended-comm-note "XCN"
                                           :yankee-certificate-of-deposit "YCD"
                                           :asset-backed-securities "ABS"
                                           :corp-mortgage-backed-securities "CMBS"
                                           :collateralized-mortgage-obligation "CMO"
                                           :ioette-mortgage "IET"
                                           :mortgage-backed-securities "MBS"
                                           :mortgage-interest-only "MIO"
                                           :mortgage-principal-only "MPO"
                                           :mortgage-private-placement "MPP"
                                           :miscellaneous-pass-through "MPT"
                                           :pfandbriefe "PFAND"
                                           :to-be-announced "TBA"
                                           :other-anticipation-notes-ban-gan-etc "AN"
                                           :certificate-of-obligation "COFO"
                                           :certificate-of-participation "COFP"
                                           :general-obligation-bonds "GO"
                                           :mandatory-tender "MT"
                                           :revenue-anticipation-note "RAN"
                                           :revenue-bonds "REV"
                                           :special-assessment "SPCLA"
                                           :special-obligation "SPCLO"
                                           :special-tax "SPCLT"
                                           :tax-anticipation-note "TAN"
                                           :tax-allocation "TAXA"
                                           :tax-exempt-commercial-paper "TECP"
                                           :tax-revenue-anticipation-note "TRAN"
                                           :variable-rate-demand-note "VRDN"
                                           :warrant "WAR"
                                           :mutual-fund "MF"
                                           :multi-leg-instrument "MLEG"
                                           :no-security-type "NONE"))
  (:effective-time "168" :type cl-fix:utctimestamp)
  (:stand-inst-type "169" :type (int-enum :other 0
                                          :dtc-sid 1
                                          :thomson-alert 2
                                          :a-global-custodian 3
                                          :accountnet 4))
  (:stand-inst-name "170" :type string)
  (:stand-inst-db-id "171" :type string)
  (:settl-delivery-type "172" :type (int-enum :versus-payment-deliver 0
                                              :free-deliver 1
                                              :tri-party 2
                                              :hold-in-custody 3))
  (:bid-spot-rate "188" :type cl-fix:price)
  (:bid-forward-points "189" :type cl-fix:priceoffset)
  (:offer-spot-rate "190" :type cl-fix:price)
  (:offer-forward-points "191" :type cl-fix:priceoffset)
  (:order-qty2 "192" :type cl-fix:qty)
  (:settl-date2 "193" :type cl-fix:localmktdate)
  (:last-spot-rate "194" :type cl-fix:price)
  (:last-forward-points "195" :type cl-fix:priceoffset)
  (:alloc-link-id "196" :type string)
  (:alloc-link-type "197" :type (int-enum :f-x-netting 0
                                          :f-x-swap 1))
  (:secondary-order-id "198" :type string)
  (:no-ioi-qualifiers "199" :type cl-fix:numingroup)
  (:maturity-month-year "200" :type cl-fix:monthyear)
  (:put-call "201" :type (int-enum :put 0
                                   :call 1))
  (:strike-price "202" :type cl-fix:price)
  (:covered-uncovered "203" :type (int-enum :covered 0
                                            :uncovered 1))
  (:opt-attribute "206" :type character)
  (:security-exchange "207" :type cl-fix:exchange)
  (:notify-broker-credit "208" :type boolean)
  (:alloc-handl-inst "209" :type (int-enum :match 1
                                           :forward 2
                                           :forward-and-match 3))
  (:max-show "210" :type cl-fix:qty)
  (:peg-offset-value "211" :type double-float)
  (:xml-data-len "212" :type cl-fix:len :data-field :xml-data)
  (:xml-data "213" :type cl-fix:data)
  (:settl-inst-ref-id "214" :type string)
  (:no-routing-ids "215" :type cl-fix:numingroup)
  (:routing-type "216" :type (int-enum :target-firm 1
                                       :target-list 2
                                       :block-firm 3
                                       :block-list 4))
  (:routing-id "217" :type string)
  (:spread "218" :type cl-fix:priceoffset)
  (:benchmark-curve-currency "220" :type cl-fix:currency)
  (:benchmark-curve-name "221" :type string)
  (:benchmark-curve-point "222" :type string)
  (:coupon-rate "223" :type cl-fix:percentage)
  (:coupon-payment-date "224" :type cl-fix:localmktdate)
  (:issue-date "225" :type cl-fix:localmktdate)
  (:repurchase-term "226" :type cl-fix:int)
  (:repurchase-rate "227" :type cl-fix:percentage)
  (:factor "228" :type double-float)
  (:trade-origination-date "229" :type cl-fix:localmktdate)
  (:ex-date "230" :type cl-fix:localmktdate)
  (:contract-multiplier "231" :type double-float)
  (:no-stipulations "232" :type cl-fix:numingroup)
  (:stipulation-type "233" :type (string-enum :amt "AMT"
                                              :auto-reinvestment-at-rate-or-better "AUTOREINV"
                                              :bank-qualified "BANKQUAL"
                                              :bargain-conditions-see "BGNCON"
                                              :coupon-range "COUPON"
                                              :iso-currency-code "CURRENCY"
                                              :custom-start-end-date "CUSTOMDATE"
                                              :geographics-and-range "GEOG"
                                              :valuation-discount "HAIRCUT"
                                              :insured "INSURED"
                                              :year-or-year-month-of-issue "ISSUE"
                                              :issuers-ticker "ISSUER"
                                              :issue-size-range "ISSUESIZE"
                                              :lookback-days "LOOKBACK"
                                              :explicit-lot-identifier "LOT"
                                              :lot-variance "LOTVAR"
                                              :maturity-year-and-month "MAT"
                                              :maturity-range "MATURITY"
                                              :maximum-substitutions "MAXSUBS"
                                              :minimum-quantity "MINQTY"
                                              :minimum-increment "MININCR"
                                              :minimum-denomination "MINDNOM"
                                              :payment-frequency-calendar "PAYFREQ"
                                              :number-of-pieces "PIECES"
                                              :pools-maximum "PMAX"
                                              :pools-per-million "PPM"
                                              :pools-per-lot "PPL"
                                              :pools-per-trade "PPT"
                                              :price-range "PRICE"
                                              :pricing-frequency "PRICEFREQ"
                                              :production-year "PROD"
                                              :call-protection "PROTECT"
                                              :purpose "PURPOSE"
                                              :benchmark-price-source "PXSOURCE"
                                              :rating-source-and-range "RATING"
                                              :type-of-redemption-values-are-noncallable-callable-prefunded-escrowedtomaturity-putable-convertible "REDEMPTION"
                                              :restricted "RESTRICTED"
                                              :market-sector "SECTOR"
                                              :securitytype-included-or-excluded "SECTYPE"
                                              :structure "STRUCT"
                                              :substitutions-frequency "SUBSFREQ"
                                              :substitutions-left "SUBSLEFT"
                                              :freeform-text "TEXT"
                                              :trade-variance "TRDVAR"
                                              :weighted-average-couponvalue-in-percent "WAC"
                                              :weighted-average-life-coupon-value-in-percent "WAL"
                                              :weighted-average-loan-age-value-in-months "WALA"
                                              :weighted-average-maturity-value-in-months "WAM"
                                              :whole-pool "WHOLE"
                                              :yield-range "YIELD"))
  (:stipulation-value "234" :type string)
  (:yield-type "235" :type (string-enum :after-tax-yield "AFTERTAX"
                                        :annual-yield "ANNUAL"
                                        :yield-at-issue "ATISSUE"
                                        :yield-to-average-maturity "AVGMATURITY"
                                        :book-yield "BOOK"
                                        :yield-to-next-call "CALL"
                                        :yield-change-since-close "CHANGE"
                                        :closing-yield "CLOSE"
                                        :compound-yield "COMPOUND"
                                        :current-yield "CURRENT"
                                        :true-gross-yield "GROSS"
                                        :government-equivalent-yield "GOVTEQUIV"
                                        :yield-with-inflation-assumption "INFLATION"
                                        :inverse-floater-bond-yield "INVERSEFLOATER"
                                        :most-recent-closing-yield "LASTCLOSE"
                                        :closing-yield-most-recent-month "LASTMONTH"
                                        :closing-yield-most-recent-quarter "LASTQUARTER"
                                        :closing-yield-most-recent-year "LASTYEAR"
                                        :yield-to-longest-average-life "LONGAVGLIFE"
                                        :mark-to-market-yield "MARK"
                                        :yield-to-maturity "MATURITY"
                                        :yield-to-next-refund "NEXTREFUND"
                                        :open-average-yield "OPENAVG"
                                        :yield-to-next-put "PUT"
                                        :previous-close-yield "PREVCLOSE"
                                        :proceeds-yield "PROCEEDS"
                                        :semi-annual-yield "SEMIANNUAL"
                                        :yield-to-shortest-average-life "SHORTAVGLIFE"
                                        :simple-yield "SIMPLE"
                                        :tax-equivalent-yield "TAXEQUIV"
                                        :yield-to-tender-date "TENDER"
                                        :true-yield "TRUE"
                                        :yield-value-of-1-32 "VALUE1/32"
                                        :yield-to-worst "WORST"))
  (:yield "236" :type cl-fix:percentage)
  (:total-takedown "237" :type cl-fix:amt)
  (:concession "238" :type cl-fix:amt)
  (:repo-collateral-security-type "239" :type string)
  (:redemption-date "240" :type cl-fix:localmktdate)
  (:underlying-coupon-payment-date "241" :type cl-fix:localmktdate)
  (:underlying-issue-date "242" :type cl-fix:localmktdate)
  (:underlying-repo-collateral-security-type "243" :type string)
  (:underlying-repurchase-term "244" :type cl-fix:int)
  (:underlying-repurchase-rate "245" :type cl-fix:percentage)
  (:underlying-factor "246" :type double-float)
  (:underlying-redemption-date "247" :type cl-fix:localmktdate)
  (:leg-coupon-payment-date "248" :type cl-fix:localmktdate)
  (:leg-issue-date "249" :type cl-fix:localmktdate)
  (:leg-repo-collateral-security-type "250" :type string)
  (:leg-repurchase-term "251" :type cl-fix:int)
  (:leg-repurchase-rate "252" :type cl-fix:percentage)
  (:leg-factor "253" :type double-float)
  (:leg-redemption-date "254" :type cl-fix:localmktdate)
  (:credit-rating "255" :type string)
  (:underlying-credit-rating "256" :type string)
  (:leg-credit-rating "257" :type string)
  (:traded-flat-switch "258" :type boolean)
  (:basis-feature-date "259" :type cl-fix:localmktdate)
  (:basis-feature-price "260" :type cl-fix:price)
  (:md-req-id "262" :type string)
  (:subscription-request-type "263" :type (char-enum :snapshot #\0
                                                     :snapshot-plus-updates #\1
                                                     :disable-previous-snapshot-plus-update-request #\2))
  (:market-depth "264" :type cl-fix:int)
  (:md-update-type "265" :type (int-enum :full-refresh 0
                                        :incremental-refresh 1))
  (:aggregated-book "266" :type boolean)
  (:no-md-entry-types "267" :type cl-fix:numingroup)
  (:no-md-entries "268" :type cl-fix:numingroup)
  (:md-entry-type "269" :type (char-enum :bid #\0
                                        :offer #\1
                                        :trade #\2
                                        :index-value #\3
                                        :opening-price #\4
                                        :closing-price #\5
                                        :settlement-price #\6
                                        :trading-session-high-price #\7
                                        :trading-session-low-price #\8
                                        :trading-session-vwap-price #\9
                                        :imbalance #\A
                                        :trade-volume #\B
                                        :open-interest #\C))
  (:md-entry-px "270" :type cl-fix:price)
  (:md-entry-size "271" :type cl-fix:qty)
  (:md-entry-date "272" :type cl-fix:utcdateonly)
  (:md-entry-time "273" :type cl-fix:utctimeonly)
  (:tick-direction "274" :type (char-enum :plus-tick #\0
                                          :zero-plus-tick #\1
                                          :minus-tick #\2
                                          :zero-minus-tick #\3))
  (:md-mkt "275" :type cl-fix:exchange)
  (:quote-condition "276" :type (multiple-value-string-enum :open "A"
                                                            :closed "B"
                                                            :exchange-best "C"
                                                            :consolidated-best "D"
                                                            :locked "E"
                                                            :crossed "F"
                                                            :depth "G"
                                                            :fast-trading "H"
                                                            :non-firm "I"))
  (:trade-condition "277" :type (multiple-value-string-enum :cash "A"
                                                            :average-price-trade "B"
                                                            :cash-trade "C"
                                                            :next-day "D"
                                                            :opening "E"
                                                            :intraday-trade-detail "F"
                                                            :rule-127-trade "G"
                                                            :rule-155-trade "H"
                                                            :sold-last "I"
                                                            :next-day-trade "J"
                                                            :opened "K"
                                                            :seller "L"
                                                            :sold "M"
                                                            :stopped-stock "N"
                                                            :imbalance-more-buyers "P"
                                                            :imbalance-more-sellers "Q"
                                                            :opening-price "R"))
  (:md-entry-id "278" :type string)
  (:md-update-action "279" :type (char-enum :new #\0
                                           :change #\1
                                           :delete #\2))
  (:md-entry-ref-id "280" :type string)
  (:md-req-rej-reason "281" :type (char-enum :unknown-symbol #\0
                                            :duplicate-mdreqid #\1
                                            :insufficient-bandwidth #\2
                                            :insufficient-permissions #\3
                                            :unsupported-subscriptionrequesttype #\4
                                            :unsupported-marketdepth #\5
                                            :unsupported-mdupdatetype #\6
                                            :unsupported-aggregatedbook #\7
                                            :unsupported-mdentrytype #\8
                                            :unsupported-tradingsessionid #\9
                                            :unsupported-scope #\A
                                            :unsupported-openclosesettleflag #\B
                                            :unsupported-mdimplicitdelete #\C))
  (:md-entry-originator "282" :type string)
  (:location-id "283" :type string)
  (:desk-id "284" :type string)
  (:delete-reason "285" :type (char-enum :cancelation #\0
                                         :error #\1))
  (:open-close-settl-flag "286" :type (multiple-value-string-enum :daily-open "0"
                                                                  :session-open "1"
                                                                  :delivery-settlement-entry "2"
                                                                  :expected-entry "3"
                                                                  :entry-from-previous-business-day "4"
                                                                  :theoretical-price-value "5"))
  (:seller-days "287" :type cl-fix:int)
  (:md-entry-buyer "288" :type string)
  (:md-entry-seller "289" :type string)
  (:md-entry-position-no "290" :type cl-fix:int)
  (:financial-status "291" :type (multiple-value-string-enum :bankrupt "1"
                                                             :pending-delisting "2"))
  (:corporate-action "292" :type (multiple-value-string-enum :ex-dividend "A"
                                                             :ex-distribution "B"
                                                             :ex-rights "C"
                                                             :new "D"
                                                             :ex-interest "E"))
  (:def-bid-size "293" :type cl-fix:qty)
  (:def-offer-size "294" :type cl-fix:qty)
  (:no-quote-entries "295" :type cl-fix:numingroup)
  (:no-quote-sets "296" :type cl-fix:numingroup)
  (:quote-status "297" :type (int-enum :accepted 0
                                       :canceled-for-symbol 1
                                       :canceled-for-security-type 2
                                       :canceled-for-underlying 3
                                       :canceled-all 4
                                       :rejected 5
                                       :removed-from-market 6
                                       :expired 7
                                       :query 8
                                       :quote-not-found 9
                                       :pending 10
                                       :pass 11
                                       :locked-market-warning 12
                                       :cross-market-warning 13
                                       :canceled-due-to-lock-market 14
                                       :canceled-due-to-cross-market 15))
  (:quote-cancel-type "298" :type (int-enum :cancel-for-symbol 1
                                            :cancel-for-security-type 2
                                            :cancel-for-underlying-symbol 3
                                            :cancel-all-quotes 4))
  (:quote-entry-id "299" :type string)
  (:quote-reject-reason "300" :type (int-enum :unknown-symbol 1
                                              :exchange 2
                                              :quote-request-exceeds-limit 3
                                              :too-late-to-enter 4
                                              :unknown-quote 5
                                              :duplicate-quote 6
                                              :invalid-bid-ask-spread 7
                                              :invalid-price 8
                                              :not-authorized-to-quote-security 9
                                              :other 99))
  (:quote-response-level "301" :type (int-enum :no-acknowledgement 0
                                               :acknowledge-only-negative-or-erroneous-quotes 1
                                               :acknowledge-each-quote-messages 2))
  (:quote-set-id "302" :type string)
  (:quote-request-type "303" :type (int-enum :manual 1
                                             :automatic 2))
  (:tot-quote-entries "304" :type cl-fix:int)
  (:underlying-security-id-source "305" :type string)
  (:underlying-issuer "306" :type string)
  (:underlying-security-desc "307" :type string)
  (:underlying-security-exchange "308" :type cl-fix:exchange)
  (:underlying-security-id "309" :type string)
  (:underlying-security-type "310" :type string)
  (:underlying-symbol "311" :type string)
  (:underlying-symbol-sfx "312" :type string)
  (:underlying-maturity-month-year "313" :type cl-fix:monthyear)
  (:underlying-put-call "315" :type cl-fix:int)
  (:underlying-strike-price "316" :type cl-fix:price)
  (:underlying-opt-attribute "317" :type character)
  (:underlying-currency "318" :type cl-fix:currency)
  (:security-req-id "320" :type string)
  (:security-request-type "321" :type (int-enum :request-security-identity-and-specifications 0
                                                :request-security-identity-for-the-specifications-provided 1
                                                :request-list-security-types 2
                                                :request-list-securities 3))
  (:security-response-id "322" :type string)
  (:security-response-type "323" :type (int-enum :accept-security-proposal-as-is 1
                                                 :accept-security-proposal-with-revisions-as-indicated-in-the-message 2
                                                 :reject-security-proposal 5
                                                 :can-not-match-selection-criteria 6))
  (:security-status-req-id "324" :type string)
  (:unsolicited-indicator "325" :type boolean)
  (:security-trading-status "326" :type (int-enum :opening-delay 1
                                                  :trading-halt 2
                                                  :resume 3
                                                  :no-open-no-resume 4
                                                  :price-indication 5
                                                  :trading-range-indication 6
                                                  :market-imbalance-buy 7
                                                  :market-imbalance-sell 8
                                                  :market-on-close-imbalance-buy 9
                                                  :market-on-close-imbalance-sell 10
                                                  :no-market-imbalance 12
                                                  :no-market-on-close-imbalance 13
                                                  :its-pre-opening 14
                                                  :new-price-indication 15
                                                  :trade-dissemination-time 16
                                                  :ready-to-trade 17
                                                  :not-available-for-trading 18
                                                  :not-traded-on-this-market 19
                                                  :unknown-or-invalid 20
                                                  :pre-open 21
                                                  :opening-rotation 22
                                                  :fast-market 23))
  (:halt-reason-char "327" :type (char-enum :order-imbalance #\I
                                            :equipment-changeover #\X
                                            :news-pending #\P
                                            :news-dissemination #\D
                                            :order-influx #\E
                                            :additional-information #\M))
  (:in-view-common "328" :type boolean)
  (:due-related "329" :type boolean)
  (:buy-volume "330" :type cl-fix:qty)
  (:sell-volume "331" :type cl-fix:qty)
  (:high-px "332" :type cl-fix:price)
  (:low-px "333" :type cl-fix:price)
  (:adjustment "334" :type (int-enum :cancel 1
                                     :error 2
                                     :correction 3))
  (:trad-ses-req-id "335" :type string)
  (:trading-session-id "336" :type string)
  (:contra-trader "337" :type string)
  (:trad-ses-method "338" :type (int-enum :electronic 1
                                          :open-outcry 2
                                          :two-party 3))
  (:trad-ses-mode "339" :type (int-enum :testing 1
                                        :simulated 2
                                        :production 3))
  (:trad-ses-status "340" :type (int-enum :unknown 0
                                          :halted 1
                                          :open 2
                                          :closed 3
                                          :pre-open 4
                                          :pre-close 5
                                          :request-rejected 6))
  (:trad-ses-start-time "341" :type cl-fix:utctimestamp)
  (:trad-ses-open-time "342" :type cl-fix:utctimestamp)
  (:trad-ses-pre-close-time "343" :type cl-fix:utctimestamp)
  (:trad-ses-close-time "344" :type cl-fix:utctimestamp)
  (:trad-ses-end-time "345" :type cl-fix:utctimestamp)
  (:number-orders "346" :type cl-fix:int)
  (:message-encoding "347" :type (string-enum :jis "ISO-2022-JP"
                                              :euc "EUC-JP"
                                              :for-using-sjis "Shift_JIS"
                                              :unicode "UTF-8"))
  (:encoded-issuer-len "348" :type cl-fix:len :data-field :encoded-issuer-len)
  (:encoded-issuer "349" :type cl-fix:data)
  (:encoded-security-desc-len "350" :type cl-fix:len :data-field :encoded-security-desc)
  (:encoded-security-desc "351" :type cl-fix:data)
  (:encoded-list-exec-inst-len "352" :type cl-fix:len :data-field :encoded-list-exec-inst)
  (:encoded-list-exec-inst "353" :type cl-fix:data)
  (:encoded-text-len "354" :type cl-fix:len :data-field :encoded-text)
  (:encoded-text "355" :type cl-fix:data)
  (:encoded-subject-len "356" :type cl-fix:len :data-field :encoded-subject)
  (:encoded-subject "357" :type cl-fix:data)
  (:encoded-headline-len "358" :type cl-fix:len :data-field :encoded-headline)
  (:encoded-headline "359" :type cl-fix:data)
  (:encoded-alloc-text-len "360" :type cl-fix:len :data-field :encoded-alloc-text)
  (:encoded-alloc-text "361" :type cl-fix:data)
  (:encoded-underlying-issuer-len "362" :type cl-fix:len :data-field :encoded-underlying-issuer)
  (:encoded-underlying-issuer "363" :type cl-fix:data)
  (:encoded-underlying-security-desc-len "364" :type cl-fix:len :data-field :encoded-underlying-security-desc)
  (:encoded-underlying-security-desc "365" :type cl-fix:data)
  (:alloc-price "366" :type cl-fix:price)
  (:quote-set-valid-until-time "367" :type cl-fix:utctimestamp)
  (:quote-entry-reject-reason "368" :type cl-fix:int)
  (:last-msg-seq-num-processed "369" :type cl-fix:seqnum)
  (:ref-tag-id "371" :type cl-fix:int)
  (:ref-msg-type "372" :type string)
  (:session-reject-reason "373" :type (int-enum :invalid-tag-number 0
                                                :required-tag-missing 1
                                                :tag-not-defined-for-this-message-type 2
                                                :undefined-tag 3
                                                :tag-specified-without-a-value 4
                                                :value-is-incorrect 5
                                                :incorrect-data-format-for-value 6
                                                :decryption-problem 7
                                                :signature-problem 8
                                                :compid-problem 9
                                                :sendingtime-accuracy-problem 10
                                                :invalid-msgtype 11
                                                :xml-validation-error 12
                                                :tag-appears-more-than-once 13
                                                :tag-specified-out-of-required-order 14
                                                :repeating-group-fields-out-of-order 15
                                                :incorrect-numingroup-count-for-repeating-group 16
                                                :non-data-value-includes-field-delimiter 17
                                                :other 99))
  (:bid-request-trans-type "374" :type (char-enum :new #\N
                                                  :cancel #\C))
  (:contra-broker "375" :type string)
  (:compliance-id "376" :type string)
  (:solicited-flag "377" :type boolean)
  (:exec-restatement-reason "378" :type (int-enum :gt-corporate-action 0
                                                  :gt-renewal 1
                                                  :verbal-change 2
                                                  :repricing-of-order 3
                                                  :broker-option 4
                                                  :partial-decline-of-orderqty 5
                                                  :cancel-on-trading-halt 6
                                                  :cancel-on-system-failure 7
                                                  :market 8
                                                  :canceled-not-best 9
                                                  :warehouse-recap 10
                                                  :other 99))
  (:business-reject-ref-id "379" :type string)
  (:business-reject-reason "380" :type (int-enum :other 0
                                                 :unkown-id 1
                                                 :unknown-security 2
                                                 :unsupported-message-type 3
                                                 :application-not-available 4
                                                 :conditionally-required-field-missing 5
                                                 :not-authorized 6
                                                 :deliverto-firm-not-available-at-this-time 7))
  (:gross-trade-amt "381" :type cl-fix:amt)
  (:no-contra-brokers "382" :type cl-fix:numingroup)
  (:max-message-size "383" :type cl-fix:len)
  (:no-msg-types "384" :type cl-fix:numingroup)
  (:msg-direction "385" :type (char-enum :send #\S
                                         :receive #\R))
  (:no-trading-sessions "386" :type cl-fix:numingroup)
  (:total-volume-traded "387" :type cl-fix:qty)
  (:discretion-inst "388" :type (char-enum :related-to-displayed-price #\0
                                           :related-to-market-price #\1
                                           :related-to-primary-price #\2
                                           :related-to-local-primary-price #\3
                                           :related-to-midpoint-price #\4
                                           :related-to-last-trade-price #\5
                                           :related-to-vwap #\6))
  (:discretion-offset-value "389" :type double-float)
  (:bid-id "390" :type string)
  (:client-bid-id "391" :type string)
  (:list-name "392" :type string)
  (:tot-related-sym "393" :type cl-fix:int)
  (:bid-type "394" :type (int-enum :non-disclosed-style 1
                                   :disclosed-style 2
                                   :no-bidding-process 3))
  (:num-tickets "395" :type cl-fix:int)
  (:side-value1 "396" :type cl-fix:amt)
  (:side-value2 "397" :type cl-fix:amt)
  (:no-bid-descriptors "398" :type cl-fix:numingroup)
  (:bid-descriptor-type "399" :type (int-enum :sector 1
                                              :country 2
                                              :index 3))
  (:bid-descriptor "400" :type string)
  (:side-value-ind "401" :type (int-enum :sidevalue1 1
                                         :sidevalue-2 2))
  (:liquidity-pct-low "402" :type cl-fix:percentage)
  (:liquidity-pct-high "403" :type cl-fix:percentage)
  (:liquidity-value "404" :type cl-fix:amt)
  (:efptracking-error "405" :type cl-fix:percentage)
  (:fair-value "406" :type cl-fix:amt)
  (:outside-index-pct "407" :type cl-fix:percentage)
  (:value-futures "408" :type cl-fix:amt)
  (:liquidity-ind-type "409" :type (int-enum :5day-moving-average 1
                                             :20-day-moving-average 2
                                             :normal-market-size 3
                                             :other 4))
  (:wt-average-liquidity "410" :type cl-fix:percentage)
  (:exchange-for-physical "411" :type boolean)
  (:out-main-cntryuindex "412" :type cl-fix:amt)
  (:cross-percent "413" :type cl-fix:percentage)
  (:prog-rpt-reqs "414" :type (int-enum :buyside-explicitly-requests-status-using-statusrequest 1
                                        :sellside-periodically-sends-status-using-liststatus-period-optionally-specified-in-progressperiod 2
                                        :real-time-execution-reports 3))
  (:prog-period-interval "415" :type cl-fix:int)
  (:inc-tax-ind "416" :type (int-enum :net 1
                                      :gross 2))
  (:num-bidders "417" :type cl-fix:int)
  (:bid-trade-type "418" :type (char-enum :risk-trade #\R
                                          :vwap-guarantee #\G
                                          :agency #\A
                                          :guaranteed-close #\J))
  (:basis-type "419" :type (char-enum :closing-price-at-morning-session #\2
                                      :closing-price #\3
                                      :current-price #\4
                                      :sq #\5
                                      :vwap-through-a-day #\6
                                      :vwap-through-a-morning-session #\7
                                      :vwap-through-an-afternoon-session #\8
                                      :vwap-through-a-day-except-yori #\9
                                      :vwap-through-a-morning-session-except-yori #\A
                                      :vwap-through-an-afternoon-session-except-yori #\B
                                      :strike #\C
                                      :open #\D
                                      :others #\Z))
  (:no-bid-components "420" :type cl-fix:numingroup)
  (:country "421" :type cl-fix:country)
  (:tot-strikes "422" :type cl-fix:int)
  (:price-type "423" :type (int-enum :percentage 1
                                     :per-unit 2
                                     :fixed-amount 3
                                     :discount-percentage-points-below-par 4
                                     :premium-percentage-points-over-par 5
                                     :spread 6
                                     :ted-price 7
                                     :ted-yield 8
                                     :yield 9
                                     :fixed-cabinet-trade-price 10
                                     :variable-cabinet-trade-price 11))
  (:day-order-qty "424" :type cl-fix:qty)
  (:day-cum-qty "425" :type cl-fix:qty)
  (:day-avg-px "426" :type cl-fix:price)
  (:gtbooking-inst "427" :type (int-enum :book-out-all-trades-on-day-of-execution 0
                                         :accumulate-executions-until-order-is-filled-or-expires 1
                                         :accumulate-until-verbally-notified-otherwise 2))
  (:no-strikes "428" :type cl-fix:numingroup)
  (:list-status-type "429" :type (int-enum :ack 1
                                           :response 2
                                           :timed 3
                                           :execstarted 4
                                           :alldone 5
                                           :alert 6))
  (:net-gross-ind "430" :type (int-enum :net 1
                                        :gross 2))
  (:list-order-status "431" :type (int-enum :inbiddingprocess 1
                                            :receivedforexecution 2
                                            :executing 3
                                            :canceling 4
                                            :alert 5
                                            :all-done 6
                                            :reject 7))
  (:expire-date "432" :type cl-fix:localmktdate)
  (:list-exec-inst-type "433" :type (char-enum :immediate #\1
                                               :wait-for-execute-instruction #\2
                                               :exchange-switch-civ-order-sell-driven #\3
                                               :exchange-switch-civ-order-buy-driven-cash-top-up #\4
                                               :exchange-switch-civ-order-buy-driven-cash-withdraw #\5))
  (:cxl-rej-response-to "434" :type (char-enum :order-cancel-request #\1
                                               :order-cancel-replace-request #\2))
  (:underlying-coupon-rate "435" :type cl-fix:percentage)
  (:underlying-contract-multiplier "436" :type double-float)
  (:contra-trade-qty "437" :type cl-fix:qty)
  (:contra-trade-time "438" :type cl-fix:utctimestamp)
  (:liquidity-num-securities "441" :type cl-fix:int)
  (:multi-leg-reporting-type "442" :type (char-enum :single-security #\1
                                                    :individual-leg-of-a-multi-leg-security #\2
                                                    :multi-leg-security #\3))
  (:strike-time "443" :type cl-fix:utctimestamp)
  (:list-status-text "444" :type string)
  (:encoded-list-status-text-len "445" :type cl-fix:len :data-field :encoded-list-status-text)
  (:encoded-list-status-text "446" :type cl-fix:data)
  (:party-id-source "447" :type (char-enum :bic #\B
                                           :generally-accepted-market-participant-identifier #\C
                                           :proprietary-custom-code #\D
                                           :iso-country-code #\E
                                           :settlement-entity-location #\F
                                           :mic #\G
                                           :csd-participant-member-code #\H
                                           :korean-investor-id #\1
                                           :taiwanese-qualified-foreign-investor-id-qfii #\2
                                           :taiwanese-trading-account #\3
                                           :malaysian-central-depository #\4
                                           :chinese-b-share #\5
                                           :uk-national-insurance-or-pension-number #\6
                                           :us-social-security-number #\7
                                           :us-employer-identification-number #\8
                                           :australian-business-number #\9
                                           :australian-tax-file-number #\A
                                           :directed-broker-three-character-acronym-as-defined-in-isitc-etc-best-practice-guidelines-document #\I))
  (:party-id "448" :type string)
  (:net-chg-prev-day "451" :type cl-fix:priceoffset)
  (:party-role "452" :type (int-enum :executing-firm 1
                                     :broker-of-credit 2
                                     :client-id 3
                                     :clearing-firm 4
                                     :investor-id 5
                                     :introducing-firm 6
                                     :entering-firm 7
                                     :locate-lending-firm 8
                                     :fund-manager-client-id 9
                                     :settlement-location 10
                                     :order-origination-trader 11
                                     :executing-trader 12
                                     :order-origination-firm 13
                                     :giveup-clearing-firm 14
                                     :correspondant-clearing-firm 15
                                     :executing-system 16
                                     :contra-firm 17
                                     :contra-clearing-firm 18
                                     :sponsoring-firm 19
                                     :underlying-contra-firm 20
                                     :clearing-organization 21
                                     :exchange 22
                                     :customer-account 24
                                     :correspondent-clearing-organization 25
                                     :correspondent-broker 26
                                     :buyer-seller 27
                                     :custodian 28
                                     :intermediary 29
                                     :agent 30
                                     :sub-custodian 31
                                     :beneficiary 32
                                     :interested-party 33
                                     :regulatory-body 34
                                     :liquidity-provider 35
                                     :entering-trader 36
                                     :contra-trader 37
                                     :position-account 38))
  (:no-party-ids "453" :type cl-fix:numingroup)
  (:no-security-alt-id "454" :type cl-fix:numingroup)
  (:security-alt-id "455" :type string)
  (:security-alt-id-source "456" :type string)
  (:no-underlying-security-alt-id "457" :type cl-fix:numingroup)
  (:underlying-security-alt-id "458" :type string)
  (:underlying-security-alt-id-source "459" :type string)
  (:product "460" :type (int-enum :agency 1
                                  :commodity 2
                                  :corporate 3
                                  :currency 4
                                  :equity 5
                                  :government 6
                                  :index 7
                                  :loan 8
                                  :moneymarket 9
                                  :mortgage 10
                                  :municipal 11
                                  :other 12
                                  :financing 13))
  (:cficode "461" :type string)
  (:underlying-product "462" :type cl-fix:int)
  (:underlyingcficode "463" :type string)
  (:test-message-indicator "464" :type boolean)
  (:booking-ref-id "466" :type string)
  (:individual-alloc-id "467" :type string)
  (:rounding-direction "468" :type (char-enum :round-to-nearest #\0
                                              :round-down #\1
                                              :round-up #\2))
  (:rounding-modulus "469" :type double-float)
  (:country-issue "470" :type cl-fix:country)
  (:state-province-issue "471" :type string)
  (:locale-issue "472" :type string)
  (:no-regist-dtls "473" :type cl-fix:numingroup)
  (:mailing-dtls "474" :type string)
  (:investor-country-residence "475" :type cl-fix:country)
  (:payment-ref "476" :type string)
  (:distrib-payment-method "477" :type (int-enum :crest 1
                                                 :nscc 2
                                                 :euroclear 3
                                                 :clearstream 4
                                                 :cheque 5
                                                 :telegraphic-transfer 6
                                                 :fedwire 7
                                                 :direct-credit 8
                                                 :ach-credit 9
                                                 :bpay 10
                                                 :high-value-clearing-system 11
                                                 :reinvest-in-fund 12))
  (:cash-distrib-curr "478" :type cl-fix:currency)
  (:comm-currency "479" :type cl-fix:currency)
  (:cancellation-rights "480" :type (char-enum :yes #\Y
                                               :no-execution-only #\N
                                               :no-waiver-agreement #\M
                                               :no-institutional #\O))
  (:money-laundering-status "481" :type (char-enum :passed #\Y
                                                   :not-checked #\N
                                                   :exempt-below-the-limit #\1
                                                   :exempt-client-money-type-exemption #\2
                                                   :exempt-authorised-credit-or-financial-institution #\3))
  (:mailing-inst "482" :type string)
  (:trans-bkd-time "483" :type cl-fix:utctimestamp)
  (:exec-price-type "484" :type (char-enum :bid-price #\B
                                           :creation-price #\C
                                           :creation-price-plus-adjustment #\D
                                           :creation-price-plus-adjustment-amount #\E
                                           :offer-price #\O
                                           :offer-price-minus-adjustment #\P
                                           :offer-price-minus-adjustment-amount #\Q
                                           :single-price #\S))
  (:exec-price-adjustment "485" :type double-float)
  (:date-birth "486" :type cl-fix:localmktdate)
  (:trade-report-trans-type "487" :type cl-fix:int)
  (:card-holder-name "488" :type string)
  (:card-number "489" :type string)
  (:card-exp-date "490" :type cl-fix:localmktdate)
  (:card-iss-num "491" :type string)
  (:payment-method "492" :type (int-enum :crest 1
                                         :nscc 2
                                         :euroclear 3
                                         :clearstream 4
                                         :cheque 5
                                         :telegraphic-transfer 6
                                         :fedwire 7
                                         :debit-card 8
                                         :direct-debit 9
                                         :direct-credit 10
                                         :credit-card 11
                                         :ach-debit 12
                                         :ach-credit 13
                                         :bpay 14
                                         :high-value-clearing-system 15))
  (:regist-acct-type "493" :type string)
  (:designation "494" :type string)
  (:tax-advantage-type "495" :type (int-enum :none-not-applicable 0
                                             :maxi-isa 1
                                             :tessa 2
                                             :mini-cash-isa 3
                                             :mini-stocks-and-shares-isa 4
                                             :mini-insurance-isa 5
                                             :current-year-payment 6
                                             :prior-year-payment 7
                                             :asset-transfer 8
                                             :employee 9
                                             :employee-current-year 10
                                             :employer 11
                                             :employer-current-year 12
                                             :non-fund-prototype-ira 13
                                             :non-fund-qualified-plan 14
                                             :defined-contribution-plan 15
                                             :individual-retirement-account 16
                                             :individual-retirement-account-rollover 17
                                             :keogh 18
                                             :profit-sharing-plan 19
                                             :401k 20
                                             :self-directed-ira 21
                                             :403 22
                                             :457 23
                                             :roth-ira-24 24
                                             :roth-ira-25 25
                                             :roth-conversion-ira-26 26
                                             :roth-conversion-ira-27 27
                                             :education-ira-28 28
                                             :education-ira-29 29))
  (:regist-rej-reason-text "496" :type string)
  (:fund-renew-waiv "497" :type (char-enum :yes #\Y
                                           :no #\N))
  (:cash-distrib-agent-name "498" :type string)
  (:cash-distrib-agent-code "499" :type string)
  (:cash-distrib-agent-acct-number "500" :type string)
  (:cash-distrib-pay-ref "501" :type string)
  (:cash-distrib-agent-acct-name "502" :type string)
  (:card-start-date "503" :type cl-fix:localmktdate)
  (:payment-date "504" :type cl-fix:localmktdate)
  (:payment-remitter-id "505" :type string)
  (:regist-status "506" :type (char-enum :accepted #\A
                                         :rejected #\R
                                         :held #\H
                                         :reminder-ie-registration-instructions-are-still-outstanding #\N))
  (:regist-rej-reason-code "507" :type (int-enum :invalid-unacceptable-account-type 1
                                                 :invalid-unacceptable-tax-exempt-type 2
                                                 :invalid-unacceptable-ownership-type 3
                                                 :invalid-unacceptable-no-reg-detls 4
                                                 :invalid-unacceptable-reg-seq-no 5
                                                 :invalid-unacceptable-reg-dtls 6
                                                 :invalid-unacceptable-mailing-dtls 7
                                                 :invalid-unacceptable-mailing-inst 8
                                                 :invalid-unacceptable-investor-id 9
                                                 :invalid-unacceptable-investor-id-source 10
                                                 :invalid-unacceptable-date-of-birth 11
                                                 :invalid-unacceptable-investor-country-of-residence 12
                                                 :invalid-unacceptable-nodistribinstns 13
                                                 :invalid-unacceptable-distrib-percentage 14
                                                 :invalid-unacceptable-distrib-payment-method 15
                                                 :invalid-unacceptable-cash-distrib-agent-acct-name 16
                                                 :invalid-unacceptable-cash-distrib-agent-code 17
                                                 :invalid-unacceptable-cash-distrib-agent-acct-num 18
                                                 :other 99))
  (:regist-ref-id "508" :type string)
  (:regist-dtls "509" :type string)
  (:no-distrib-insts "510" :type cl-fix:numingroup)
  (:regist-email "511" :type string)
  (:distrib-percentage "512" :type cl-fix:percentage)
  (:regist-id "513" :type string)
  (:regist-trans-type "514" :type (char-enum :new #\0
                                             :replace #\1
                                             :cancel #\2))
  (:exec-valuation-point "515" :type cl-fix:utctimestamp)
  (:order-percent "516" :type cl-fix:percentage)
  (:ownership-type "517" :type (char-enum :joint-investors #\J
                                          :tenants-in-common #\T
                                          :joint-trustees #\2))
  (:no-cont-amts "518" :type cl-fix:numingroup)
  (:cont-amt-type "519" :type (int-enum :commission-amount 1
                                        :commission 2
                                        :initial-charge-amount 3
                                        :initial-charge 4
                                        :discount-amount 5
                                        :discount 6
                                        :dilution-levy-amount 7
                                        :dilution-levy 8
                                        :exit-charge-amount 9
                                        :exit-charge 10
                                        :fund-based-renewal-commission 11
                                        :projected-fund-value 12
                                        :fund-based-renewal-commission-amount-13 13
                                        :fund-based-renewal-commission-amount-14 14
                                        :net-settlement-amount 15))
  (:cont-amt-value "520" :type double-float)
  (:cont-amt-curr "521" :type cl-fix:currency)
  (:owner-type "522" :type (int-enum :individual-investor 1
                                     :public-company 2
                                     :private-company 3
                                     :individual-trustee 4
                                     :company-trustee 5
                                     :pension-plan 6
                                     :custodian-under-gifts-to-minors-act 7
                                     :trusts 8
                                     :fiduciaries 9
                                     :networking-sub-account 10
                                     :non-profit-organization 11
                                     :corporate-body 12
                                     :nominee 13))
  (:party-sub-id "523" :type string)
  (:nested-party-id "524" :type string)
  (:nested-party-id-source "525" :type character)
  (:secondary-ord-id "526" :type string)
  (:secondary-exec-id "527" :type string)
  (:order-capacity "528" :type (char-enum :agency #\A
                                          :proprietary #\G
                                          :individual #\I
                                          :principal #\P
                                          :riskless-principal #\R
                                          :agent-for-other-member #\W))
  (:order-restrictions "529" :type (multiple-value-string-enum :program-trade "1"
                                                               :index-arbitrage "2"
                                                               :non-index-arbitrage "3"
                                                               :competing-market-maker "4"
                                                               :acting-as-market-maker-or-specialist-in-the-security "5"
                                                               :acting-as-market-maker-or-specialist-in-the-underlying-security-of-a-derivative-security "6"
                                                               :foreign-entity "7"
                                                               :external-market-participant "8"
                                                               :external-inter-connected-market-linkage "9"
                                                               :riskless-arbitrage "A"))
  (:mass-cancel-request-type "530" :type (char-enum :cancel-orders-for-a-security #\1
                                                    :cancel-orders-for-an-underlying-security #\2
                                                    :cancel-orders-for-a-product #\3
                                                    :cancel-orders-for-a-cficode #\4
                                                    :cancel-orders-for-a-securitytype #\5
                                                    :cancel-orders-for-a-trading-session #\6
                                                    :cancel-all-orders #\7))
  (:mass-cancel-response "531" :type (char-enum :cancel-request-rejected #\0
                                                :cancel-orders-for-a-security #\1
                                                :cancel-orders-for-an-underlying-security #\2
                                                :cancel-orders-for-a-product #\3
                                                :cancel-orders-for-a-cficode #\4
                                                :cancel-orders-for-a-securitytype #\5
                                                :cancel-orders-for-a-trading-session #\6
                                                :cancel-all-orders #\7))
  (:mass-cancel-reject-reason "532" :type (char-enum :mass-cancel-not-supported "0"
                                                     :invalid-or-unknown-security "1"
                                                     :invalid-or-unknown-underlying "2"
                                                     :invalid-or-unknown-product "3"
                                                     :invalid-or-unknown-cficode "4"
                                                     :invalid-or-unknown-security-type "5"
                                                     :invalid-or-unknown-trading-session "6"
                                                     :other "99"))
  (:total-affected-orders "533" :type cl-fix:int)
  (:no-affected-orders "534" :type cl-fix:numingroup)
  (:affected-order-id "535" :type string)
  (:affected-secondary-order-id "536" :type string)
  (:quote-type "537" :type (int-enum :indicative 0
                                     :tradeable 1
                                     :restricted-tradeable 2
                                     :counter 3))
  (:nested-party-role "538" :type cl-fix:int)
  (:no-nested-party-ids "539" :type cl-fix:numingroup)
  (:total-accrued-interest-amt "540" :type cl-fix:amt)
  (:maturity-date "541" :type cl-fix:localmktdate)
  (:underlying-maturity-date "542" :type cl-fix:localmktdate)
  (:instr-registry "543" :type string)
  (:cash-margin "544" :type (char-enum :cash #\1
                                       :margin-open #\2
                                       :margin-close #\3))
  (:nested-party-sub-id "545" :type string)
  (:scope "546" :type (multiple-value-string-enum :local "1"
                                                  :national "2"
                                                  :global "3"))
  (:md-implicit-delete "547" :type boolean)
  (:cross-id "548" :type string)
  (:cross-type "549" :type (int-enum :cross-trade-which-is-executed-completely-or-not-both-sides-are-treated-in-the-same-manner-this-is-equivalent-to-an-all-or-none 1
                                     :cross-trade-which-is-executed-partially-and-the-rest-is-cancelled-one-side-is-fully-executed-the-other-side-is-partially-executed-with-the-remainder-being-cancelled-this-is-equivalent-to-an-immediate-or-cancel-on-the-other-side-note-the-crossprioritzation 2
                                     :cross-trade-which-is-partially-executed-with-the-unfilled-portions-remaining-active-one-side-of-the-cross-is-fully-executed 3
                                     :cross-trade-is-executed-with-existing-orders-with-the-same-price-in-the-case-other-orders-exist-with-the-same-price-the-quantity-of-the-cross-is-executed-against-the-existing-orders-and-quotes-the-remainder-of-the-cross-is-executed-against-the-other-side-of-the-cross-the-two-sides-potentially-have-different-quantities 4))
  (:cross-prioritization "550" :type (int-enum :none 0
                                               :buy-side-is-prioritized 1
                                               :sell-side-is-prioritized 2))
  (:orig-cross-id "551" :type string)
  (:no-sides "552" :type cl-fix:numingroup)
  (:username "553" :type string)
  (:password "554" :type string)
  (:no-legs "555" :type cl-fix:numingroup)
  (:leg-currency "556" :type cl-fix:currency)
  (:tot-security-types "557" :type cl-fix:int)
  (:no-security-types "558" :type cl-fix:numingroup)
  (:security-list-request-type "559" :type (int-enum :symbol 0
                                                     :securitytype-and-or-cficode 1
                                                     :product 2
                                                     :tradingsessionid 3
                                                     :all-securities 4))
  (:security-request-result "560" :type (int-enum :valid-request 0
                                                  :invalid-or-unsupported-request 1
                                                  :no-instruments-found-that-match-selection-criteria 2
                                                  :not-authorized-to-retrieve-instrument-data 3
                                                  :instrument-data-temporarily-unavailable 4
                                                  :request-for-instrument-data-not-supported 5))
  (:round-lot "561" :type cl-fix:qty)
  (:min-trade-vol "562" :type cl-fix:qty)
  (:multi-leg-rpt-type-req "563" :type (int-enum :report-by-mulitleg-security-only 0
                                                 :report-by-multileg-security-and-by-instrument-legs-belonging-to-the-multileg-security 1
                                                 :report-by-instrument-legs-belonging-to-the-multileg-security-only 2))
  (:leg-position-effect "564" :type character)
  (:leg-covered-uncovered "565" :type cl-fix:int)
  (:leg-price "566" :type cl-fix:price)
  (:trad-ses-status-rej-reason "567" :type (int-enum :unknown-or-invalid-tradingsessionid 1
                                                     :other 99))
  (:trade-request-id "568" :type string)
  (:trade-request-type "569" :type (int-enum :all-trades 0
                                             :matched-trades-matching-criteria-provided-on-request 1
                                             :unmatched-trades-that-match-criteria 2
                                             :unreported-trades-that-match-criteria 3
                                             :advisories-that-match-criteria 4))
  (:previously-reported "570" :type boolean)
  (:trade-report-id "571" :type string)
  (:trade-report-ref-id "572" :type string)
  (:match-status "573" :type (char-enum :compared-matched-or-affirmed #\0
                                        :uncompared-unmatched-or-unaffirmed #\1
                                        :advisory-or-alert #\2))
  (:match-type "574" :type (string-enum :exact-match-on-trade-date-stock-symbol-quantity-price-trade-type-and-special-trade-indicator-plus-four-badges-and-execution-time "A1"
                                        :exact-match-on-trade-date-stock-symbol-quantity-price-trade-type-and-special-trade-indicator-plus-four-badges "A2"
                                        :exact-match-on-trade-date-stock-symbol-quantity-price-trade-type-and-special-trade-indicator-plus-two-badges-and-execution-time "A3"
                                        :exact-match-on-trade-date-stock-symbol-quantity-price-trade-type-and-special-trade-indicator-plus-two-badges "A4"
                                        :exact-match-on-trade-date-stock-symbol-quantity-price-trade-type-and-special-trade-indicator-plus-execution-time "A5"
                                        :compared-records-resulting-from-stamped-advisories-or-specialist-accepts-pair-offs "AQ"
                                        :summarized-match-using-a1-exact-match-criteria-except-quantity-is-summarized "S1"
                                        :summarized-match-using-a2-exact-match-criteria-except-quantity-is-summarized "S2"
                                        :summarized-match-using-a3-exact-match-criteria-except-quantity-is-summarized "S3"
                                        :summarized-match-using-a4-exact-match-criteria-except-quantity-is-summarized "S4"
                                        :summarized-match-using-a5-exact-match-criteria-except-quantity-is-summarized "S5"
                                        :exact-match-on-trade-date-stock-symbol-quantity-price-trade-type-and-special-trade-indicator-minus-badges-and-times-act-m1-match "M1"
                                        :summarized-match-minus-badges-and-times-act-m2-match "M2"
                                        :ocs-locked-in-non-act "MT"
                                        :act-accepted-trade "M3"
                                        :act-default-trade "M4"
                                        :act-default-after-m2 "M5"
                                        :act-m6-match "M6"))
  (:odd-lot "575" :type boolean)
  (:no-clearing-instructions "576" :type cl-fix:numingroup)
  (:clearing-instruction "577" :type (int-enum :process-normally 0
                                               :exclude-from-all-netting 1
                                               :bilateral-netting-only 2
                                               :ex-clearing 3
                                               :special-trade 4
                                               :multilateral-netting 5
                                               :clear-against-central-counterparty 6
                                               :exclude-from-central-counterparty 7
                                               :manual-mode 8
                                               :automatic-posting-mode 9
                                               :automatic-give-up-mode 10
                                               :qualified-service-representative 11
                                               :customer-trade 12
                                               :self-clearing 13))
  (:trade-input-source "578" :type string)
  (:trade-input-device "579" :type string)
  (:no-dates "580" :type cl-fix:numingroup)
  (:account-type "581" :type (int-enum :account-is-carried-on-customer-side-of-books 1
                                       :account-is-carried-on-non-customer-side-of-books 2
                                       :house-trader 3
                                       :floor-trader 4
                                       :account-is-carried-on-non-customer-side-of-books-and-is-cross-margined 6
                                       :account-is-house-trader-and-is-cross-margined 7
                                       :joint-backoffice-account 8))
  (:cust-order-capacity "582" :type (int-enum :member-trading-for-their-own-account 1
                                              :clearing-firm-trading-for-its-proprietary-account 2
                                              :member-trading-for-another-member 3
                                              :all-other 4))
  (:cl-ord-link-id "583" :type string)
  (:mass-status-req-id "584" :type string)
  (:mass-status-req-type "585" :type (int-enum :status-for-orders-for-a-security 1
                                               :status-for-orders-for-an-underlying-security 2
                                               :status-for-orders-for-a-product 3
                                               :status-for-orders-for-a-cficode 4
                                               :status-for-orders-for-a-securitytype 5
                                               :status-for-orders-for-a-trading-session 6
                                               :status-for-all-orders 7
                                               :status-for-orders-for-a-partyid 8))
  (:orig-ord-mod-time "586" :type cl-fix:utctimestamp)
  (:leg-settl-type "587" :type character)
  (:leg-settl-date "588" :type cl-fix:localmktdate)
  (:day-booking-inst "589" :type (char-enum :can-trigger-booking-without-reference-to-the-order-initiator #\0
                                            :speak-with-order-initiator-before-booking #\1
                                            :accumulate #\2))
  (:booking-unit "590" :type (char-enum :each-partial-execution-is-a-bookable-unit #\0
                                        :aggregate-partial-executions-on-this-order-and-book-one-trade-per-order #\1
                                        :aggregate-executions-for-this-symbol-side-and-settlement-date #\2))
  (:prealloc-method "591" :type (char-enum :pro-rata #\0
                                           :do-not-pro-rata-discuss-first #\1))
  (:underlying-country-issue "592" :type cl-fix:country)
  (:underlying-state-province-issue "593" :type string)
  (:underlying-locale-issue "594" :type string)
  (:underlying-instr-registry "595" :type string)
  (:leg-country-issue "596" :type cl-fix:country)
  (:leg-state-province-issue "597" :type string)
  (:leg-locale-issue "598" :type string)
  (:leg-instr-registry "599" :type string)
  (:leg-symbol "600" :type string)
  (:leg-symbol-sfx "601" :type string)
  (:leg-security-id "602" :type string)
  (:leg-security-id-source "603" :type string)
  (:no-leg-security-alt-id "604" :type cl-fix:numingroup)
  (:leg-security-alt-id "605" :type string)
  (:leg-security-alt-id-source "606" :type string)
  (:leg-product "607" :type cl-fix:int)
  (:legcficode "608" :type string)
  (:leg-security-type "609" :type string)
  (:leg-maturity-month-year "610" :type cl-fix:monthyear)
  (:leg-maturity-date "611" :type cl-fix:localmktdate)
  (:leg-strike-price "612" :type cl-fix:price)
  (:leg-opt-attribute "613" :type character)
  (:leg-contract-multiplier "614" :type double-float)
  (:leg-coupon-rate "615" :type cl-fix:percentage)
  (:leg-security-exchange "616" :type cl-fix:exchange)
  (:leg-issuer "617" :type string)
  (:encoded-leg-issuer-len "618" :type cl-fix:len :data-field :encoded-leg-issuer)
  (:encoded-leg-issuer "619" :type cl-fix:data)
  (:leg-security-desc "620" :type string)
  (:encoded-leg-security-desc-len "621" :type cl-fix:len :data-field :encoded-leg-security-desc)
  (:encoded-leg-security-desc "622" :type cl-fix:data)
  (:leg-ratio-qty "623" :type double-float)
  (:leg-side "624" :type character)
  (:trading-session-sub-id "625" :type string)
  (:alloc-type "626" :type (int-enum :calculated 1
                                     :preliminary 2
                                     :ready-to-book 5
                                     :warehouse-instruction 7
                                     :request-to-intermediary 8))
  (:no-hops "627" :type cl-fix:numingroup)
  (:hop-comp-id "628" :type string)
  (:hop-sending-time "629" :type cl-fix:utctimestamp)
  (:hop-ref-id "630" :type cl-fix:seqnum)
  (:mid-px "631" :type cl-fix:price)
  (:bid-yield "632" :type cl-fix:percentage)
  (:mid-yield "633" :type cl-fix:percentage)
  (:offer-yield "634" :type cl-fix:percentage)
  (:clearing-fee-indicator "635" :type (string-enum :cboe-member "B"
                                                    :non-member-and-customer "C"
                                                    :equity-member-and-clearing-member "E"
                                                    :full-and-associate-member-trading-for-own-account-and-as-floor-brokers "F"
                                                    :106h-and-106j-firms "H"
                                                    :gim-idem-and-com-membership-interest-holders "I"
                                                    :lessee-and-106f-employees "L"
                                                    :all-other-ownership-types "M"
                                                    :1st-year-delegate-trading-for-his-own-account "1"
                                                    :2nd-year-delegate-trading-for-his-own-account "2"
                                                    :3rd-year-delegate-trading-for-his-own-account "3"
                                                    :4th-year-delegate-trading-for-his-own-account "4"
                                                    :5th-year-delegate-trading-for-his-own-account "5"
                                                    :6th-year-and-beyond-delegate-trading-for-his-own-account "9"))
  (:working-indicator "636" :type boolean)
  (:leg-last-px "637" :type cl-fix:price)
  (:priority-indicator "638" :type (int-enum :priority-unchanged 0
                                             :lost-priority-as-result-of-order-change 1))
  (:price-improvement "639" :type cl-fix:priceoffset)
  (:price2 "640" :type cl-fix:price)
  (:last-forward-points2 "641" :type cl-fix:priceoffset)
  (:bid-forward-points2 "642" :type cl-fix:priceoffset)
  (:offer-forward-points2 "643" :type cl-fix:priceoffset)
  (:rfqreq-id "644" :type string)
  (:mkt-bid-px "645" :type cl-fix:price)
  (:mkt-offer-px "646" :type cl-fix:price)
  (:min-bid-size "647" :type cl-fix:qty)
  (:min-offer-size "648" :type cl-fix:qty)
  (:quote-status-req-id "649" :type string)
  (:legal-confirm "650" :type boolean)
  (:underlying-last-px "651" :type cl-fix:price)
  (:underlying-last-qty "652" :type cl-fix:qty)
  (:leg-ref-id "654" :type string)
  (:contra-leg-ref-id "655" :type string)
  (:settl-curr-bid-rate "656" :type double-float)
  (:settl-curr-offer-rate "657" :type double-float)
  (:quote-request-reject-reason "658" :type (int-enum :unknown-symbol 1
                                                      :exchange 2
                                                      :quote-request-exceeds-limit 3
                                                      :too-late-to-enter 4
                                                      :invalid-price 5
                                                      :not-authorized-to-request-quote 6
                                                      :no-match-for-inquiry 7
                                                      :no-market-for-instrument 8
                                                      :no-inventory 9
                                                      :pass 10
                                                      :other 99))
  (:side-compliance-id "659" :type string)
  (:acct-id-source "660" :type (int-enum :bic 1
                                         :sid-code 2
                                         :tfm 3
                                         :omgeo 4
                                         :dtcc-code 5
                                         :other 99))
  (:alloc-acct-id-source "661" :type cl-fix:int)
  (:benchmark-price "662" :type cl-fix:price)
  (:benchmark-price-type "663" :type cl-fix:int)
  (:confirm-id "664" :type string)
  (:confirm-status "665" :type (int-enum :received 1
                                         :mismatched-account 2
                                         :missing-settlement-instructions 3
                                         :confirmed 4
                                         :request-rejected 5))
  (:confirm-trans-type "666" :type (int-enum :new 0
                                             :replace 1
                                             :cancel 2))
  (:contract-settl-month "667" :type cl-fix:monthyear)
  (:delivery-form "668" :type (int-enum :bookentry 1
                                        :bearer 2))
  (:last-par-px "669" :type cl-fix:price)
  (:no-leg-allocs "670" :type cl-fix:numingroup)
  (:leg-alloc-account "671" :type string)
  (:leg-individual-alloc-id "672" :type string)
  (:leg-alloc-qty "673" :type cl-fix:qty)
  (:leg-alloc-acct-id-source "674" :type string)
  (:leg-settl-currency "675" :type cl-fix:currency)
  (:leg-benchmark-curve-currency "676" :type cl-fix:currency)
  (:leg-benchmark-curve-name "677" :type string)
  (:leg-benchmark-curve-point "678" :type string)
  (:leg-benchmark-price "679" :type cl-fix:price)
  (:leg-benchmark-price-type "680" :type cl-fix:int)
  (:leg-bid-px "681" :type cl-fix:price)
  (:leg-ioi-qty "682" :type string)
  (:no-leg-stipulations "683" :type cl-fix:numingroup)
  (:leg-offer-px "684" :type cl-fix:price)
  (:leg-price-type "686" :type cl-fix:int)
  (:leg-qty "687" :type cl-fix:qty)
  (:leg-stipulation-type "688" :type string)
  (:leg-stipulation-value "689" :type string)
  (:leg-swap-type "690" :type (int-enum :par-for-par 1
                                        :modified-duration 2
                                        :risk 4
                                        :proceeds 5))
  (:pool "691" :type string)
  (:quote-price-type "692" :type (int-enum :percent 1
                                           :per-share 2
                                           :fixed-amount 3
                                           :discount-percentage-points-below-par 4
                                           :premium-percentage-points-over-par 5
                                           :basis-points-relative-to-benchmark 6
                                           :ted-price 7
                                           :ted-yield 8
                                           :yield-spread 9
                                           :yield 10))
  (:quote-resp-id "693" :type string)
  (:quote-resp-type "694" :type (int-enum :hit-lift 1
                                          :counter 2
                                          :expired 3
                                          :cover 4
                                          :done-away 5
                                          :pass 6))
  (:quote-qualifier "695" :type character)
  (:yield-redemption-date "696" :type cl-fix:localmktdate)
  (:yield-redemption-price "697" :type cl-fix:price)
  (:yield-redemption-price-type "698" :type cl-fix:int)
  (:benchmark-security-id "699" :type string)
  (:reversal-indicator "700" :type boolean)
  (:yield-calc-date "701" :type cl-fix:localmktdate)
  (:no-positions "702" :type cl-fix:numingroup)
  (:pos-type "703" :type (string-enum :transaction-quantity "TQ"
                                      :intra-spread-qty "IAS"
                                      :inter-spread-qty "IES"
                                      :end-of-day-qty "FIN"
                                      :start-of-day-qty "SOD"
                                      :option-exercise-qty "EX"
                                      :option-assignment "AS"
                                      :transaction-from-exercise "TX"
                                      :transaction-from-assignment "TA"
                                      :pit-trade-qty "PIT"
                                      :transfer-trade-qty "TRF"
                                      :electronic-trade-qty "ETR"
                                      :allocation-trade-qty "ALC"
                                      :adjustment-qty "PA"
                                      :as-of-trade-qty "ASF"
                                      :delivery-qty "DLV"
                                      :total-transaction-qty "TOT"
                                      :cross-margin-qty "XM"
                                      :integral-split "SPL"))
  (:long-qty "704" :type cl-fix:qty)
  (:short-qty "705" :type cl-fix:qty)
  (:pos-qty-status "706" :type (int-enum :submitted 0
                                         :accepted 1
                                         :rejected 2))
  (:pos-amt-type "707" :type (string-enum :final-mark-to-market-amount "FMTM"
                                          :incremental-mark-to-market-amount "IMTM"
                                          :trade-variation-amount "TVAR"
                                          :start-of-day-mark-to-market-amount "SMTM"
                                          :premium-amount "PREM"
                                          :cash-residual-amount "CRES"
                                          :cash-amount "CASH"
                                          :value-adjusted-amount "VADJ"))
  (:pos-amt "708" :type cl-fix:amt)
  (:pos-trans-type "709" :type (int-enum :exercise 1
                                         :do-not-exercise 2
                                         :position-adjustment 3
                                         :position-change-submission-margin-disposition 4
                                         :pledge 5))
  (:pos-req-id "710" :type string)
  (:no-underlyings "711" :type cl-fix:numingroup)
  (:pos-maint-action "712" :type (int-enum :new-used-to-increment-the-overall-transaction-quantity 1
                                           :replace-used-to-override-the-overall-transaction-quantity-or-specific-add-messages-based-on-the-reference-id 2
                                           :cancel-used-to-remove-the-overall-transaction-or-specific-add-messages-based-on-reference-id 3))
  (:orig-pos-req-ref-id "713" :type string)
  (:pos-maint-rpt-ref-id "714" :type string)
  (:clearing-business-date "715" :type cl-fix:localmktdate)
  (:settl-sess-id "716" :type (string-enum :intraday "ITD"
                                           :regular-trading-hours "RTH"
                                           :electronic-trading-hours "ETH"))
  (:settl-sess-sub-id "717" :type string)
  (:adjustment-type "718" :type (int-enum :process-request-as-margin-disposition 0
                                          :delta-plus 1
                                          :delta-minus 2
                                          :final 3))
  (:contrary-instruction-indicator "719" :type boolean)
  (:prior-spread-indicator "720" :type boolean)
  (:pos-maint-rpt-id "721" :type string)
  (:pos-maint-status "722" :type (int-enum :accepted 0
                                           :accepted-with-warnings 1
                                           :rejected 2
                                           :completed 3
                                           :completed-with-warnings 4))
  (:pos-maint-result "723" :type (int-enum :successful-completion 0
                                           :rejected 1
                                           :other 99))
  (:pos-req-type "724" :type (int-enum :positions 0
                                       :trades 1
                                       :exercises 2
                                       :assignments 3))
  (:response-transport-type "725" :type (int-enum :inband-transport-the-request-was-sent-over 0
                                                  :out-of-band-pre-arranged-out-of-band-delivery-mechanism 1))
  (:response-destination "726" :type string)
  (:total-num-pos-reports "727" :type cl-fix:int)
  (:pos-req-result "728" :type (int-enum :valid-request 0
                                         :invalid-or-unsupported-request 1
                                         :no-positions-found-that-match-criteria 2
                                         :not-authorized-to-request-positions 3
                                         :request-for-position-not-supported 4
                                         :other 99))
  (:pos-req-status "729" :type (int-enum :completed 0
                                         :completed-with-warnings 1
                                         :rejected 2))
  (:settl-price "730" :type cl-fix:price)
  (:settl-price-type "731" :type (int-enum :final 1
                                           :theoretical 2))
  (:underlying-settl-price "732" :type cl-fix:price)
  (:underlying-settl-price-type "733" :type cl-fix:int)
  (:prior-settl-price "734" :type cl-fix:price)
  (:no-quote-qualifiers "735" :type cl-fix:numingroup)
  (:alloc-settl-currency "736" :type cl-fix:currency)
  (:alloc-settl-curr-amt "737" :type cl-fix:amt)
  (:interest-maturity "738" :type cl-fix:amt)
  (:leg-dated-date "739" :type cl-fix:localmktdate)
  (:leg-pool "740" :type string)
  (:alloc-interest-maturity "741" :type cl-fix:amt)
  (:alloc-accrued-interest-amt "742" :type cl-fix:amt)
  (:delivery-date "743" :type cl-fix:localmktdate)
  (:assignment-method "744" :type (char-enum :random #\R
                                             :prorata #\P))
  (:assignment-unit "745" :type cl-fix:qty)
  (:open-interest "746" :type cl-fix:amt)
  (:exercise-method "747" :type (char-enum :automatic #\A
                                           :manual #\M))
  (:tot-num-trade-reports "748" :type cl-fix:int)
  (:trade-request-result "749" :type (int-enum :successful 0
                                               :invalid-or-unknown-instrument 1
                                               :invalid-type-of-trade-requested 2
                                               :invalid-parties 3
                                               :invalid-transport-type-requested 4
                                               :invalid-destination-requested 5
                                               :traderequesttype-not-supported 8
                                               :unauthorized-for-trade-capture-report-request 9
                                               :other 99))
  (:trade-request-status "750" :type (int-enum :accepted 0
                                               :completed 1
                                               :rejected 2))
  (:trade-report-reject-reason "751" :type (int-enum :successful 0
                                                     :invalid-party-information 1
                                                     :unknown-instrument 2
                                                     :unauthorized-to-report-trades 3
                                                     :invalid-trade-type 4
                                                     :other 99))
  (:side-multi-leg-reporting-type "752" :type (int-enum :single-security 1
                                                        :individual-leg-of-a-multi-leg-security 2
                                                        :multi-leg-security 3))
  (:no-pos-amt "753" :type cl-fix:numingroup)
  (:auto-accept-indicator "754" :type boolean)
  (:alloc-report-id "755" :type string)
  (:no-nested2party-ids "756" :type cl-fix:numingroup)
  (:nested2party-id "757" :type string)
  (:nested2party-id-source "758" :type character)
  (:nested2party-role "759" :type cl-fix:int)
  (:nested2party-sub-id "760" :type string)
  (:benchmark-security-id-source "761" :type string)
  (:security-sub-type "762" :type string)
  (:underlying-security-sub-type "763" :type string)
  (:leg-security-sub-type "764" :type string)
  (:allowable-one-sidedness-pct "765" :type cl-fix:percentage)
  (:allowable-one-sidedness-value "766" :type cl-fix:amt)
  (:allowable-one-sidedness-curr "767" :type cl-fix:currency)
  (:no-trd-reg-timestamps "768" :type cl-fix:numingroup)
  (:trd-reg-timestamp "769" :type cl-fix:utctimestamp)
  (:trd-reg-timestamp-type "770" :type (int-enum :execution-time 1
                                                 :time-in 2
                                                 :time-out 3
                                                 :broker-receipt 4
                                                 :broker-execution 5))
  (:trd-reg-timestamp-origin "771" :type string)
  (:confirm-ref-id "772" :type string)
  (:confirm-type "773" :type (int-enum :status 1
                                       :confirmation 2
                                       :confirmation-request-rejected 3))
  (:confirm-rej-reason "774" :type (int-enum :mismatched-account 1
                                             :missing-settlement-instructions 2
                                             :other 99))
  (:booking-type "775" :type (int-enum :regular-booking 0
                                       :cfd 1
                                       :total-return-swap 2))
  (:individual-alloc-rej-code "776" :type cl-fix:int)
  (:settl-inst-msg-id "777" :type string)
  (:no-settl-inst "778" :type cl-fix:numingroup)
  (:last-update-time "779" :type cl-fix:utctimestamp)
  (:alloc-settl-inst-type "780" :type (int-enum :use-default-instructions 0
                                                :derive-from-parameters-provided 1
                                                :full-details-provided 2
                                                :ssi-db-ids-provided 3
                                                :phone-for-instructions 4))
  (:no-settl-party-ids "781" :type cl-fix:numingroup)
  (:settl-party-id "782" :type string)
  (:settl-party-id-source "783" :type character)
  (:settl-party-role "784" :type cl-fix:int)
  (:settl-party-sub-id "785" :type string)
  (:settl-party-sub-id-type "786" :type cl-fix:int)
  (:dlvy-inst-type "787" :type (char-enum :securities #\S
                                          :cash #\C))
  (:termination-type "788" :type (int-enum :overnight 1
                                           :term 2
                                           :flexible 3
                                           :open 4))
  (:next-expected-msg-seq-num "789" :type cl-fix:seqnum)
  (:ord-status-req-id "790" :type string)
  (:settl-inst-req-id "791" :type string)
  (:settl-inst-req-rej-code "792" :type (int-enum :unable-to-process-request 0
                                                  :unknown-account 1
                                                  :no-matching-settlement-instructions-found 2
                                                  :other 99))
  (:secondary-alloc-id "793" :type string)
  (:alloc-report-type "794" :type (int-enum :sellside-calculated-using-preliminary 3
                                            :sellside-calculated-without-preliminary 4
                                            :warehouse-recap 5
                                            :request-to-intermediary 8))
  (:alloc-report-ref-id "795" :type string)
  (:alloc-canc-replace-reason "796" :type (int-enum :original-details-incomplete-incorrect 1
                                                    :change-in-underlying-order-details 2
                                                    :other 99))
  (:copy-msg-indicator "797" :type boolean)
  (:alloc-account-type "798" :type (int-enum :account-is-carried-on-customer-side-of-books 1
                                             :account-is-carried-on-non-customer-side-of-books 2
                                             :house-trader 3
                                             :floor-trader 4
                                             :account-is-carried-on-non-customer-side-of-books-and-is-cross-margined 6
                                             :account-is-house-trader-and-is-cross-margined 7
                                             :joint-backoffice-account 8))
  (:order-avg-px "799" :type cl-fix:price)
  (:order-booking-qty "800" :type cl-fix:qty)
  (:no-settl-party-sub-ids "801" :type cl-fix:numingroup)
  (:no-party-sub-ids "802" :type cl-fix:numingroup)
  (:party-sub-id-type "803" :type (int-enum :firm 1
                                            :person 2
                                            :system 3
                                            :application 4
                                            :full-legal-name-of-firm 5
                                            :postal-address 6
                                            :phone-number 7
                                            :email-address 8
                                            :contact-name 9
                                            :securities-account-number 10
                                            :registration-number 11
                                            :registered-address-12 12
                                            :regulatory-status 13
                                            :registration-name 14
                                            :cash-account-number 15
                                            :bic 16
                                            :csd-participant-member-code 17
                                            :registered-address-18 18
                                            :fund-account-name 19
                                            :telex-number 20
                                            :fax-number 21
                                            :securities-account-name 22
                                            :cash-account-name 23
                                            :department 24
                                            :location 25
                                            :position-account-type 26))
  (:no-nested-party-sub-ids "804" :type cl-fix:numingroup)
  (:nested-party-sub-id-type "805" :type cl-fix:int)
  (:no-nested2party-sub-ids "806" :type cl-fix:numingroup)
  (:nested2party-sub-id-type "807" :type cl-fix:int)
  (:alloc-intermed-req-type "808" :type (int-enum :pending-accept 1
                                                  :pending-release 2
                                                  :pending-reversal 3
                                                  :accept 4
                                                  :block-level-reject 5
                                                  :account-level-reject 6))
  (:underlying-px "810" :type cl-fix:price)
  (:price-delta "811" :type double-float)
  (:appl-queue-max "812" :type cl-fix:int)
  (:appl-queue-depth "813" :type cl-fix:int)
  (:appl-queue-resolution "814" :type (int-enum :no-action-taken 0
                                                :queue-flushed 1
                                                :overlay-last 2
                                                :end-session 3))
  (:appl-queue-action "815" :type (int-enum :no-action-taken 0
                                            :queue-flushed 1
                                            :overlay-last 2
                                            :end-session 3))
  (:no-altmdsource "816" :type cl-fix:numingroup)
  (:altmdsource-id "817" :type string)
  (:secondary-trade-report-id "818" :type string)
  (:avg-indicator "819" :type (int-enum :no-average-pricing 0
                                        :trade-is-part-of-an-average-price-group-identified-by-the-tradelinkid 1
                                        :last-trade-in-the-average-price-group-identified-by-the-tradelinkid 2))
  (:trade-link-id "820" :type string)
  (:order-input-device "821" :type string)
  (:underlying-trading-session-id "822" :type string)
  (:underlying-trading-session-sub-id "823" :type string)
  (:trade-leg-ref-id "824" :type string)
  (:exchange-rule "825" :type string)
  (:trade-alloc-indicator "826" :type (int-enum :allocation-not-required 0
                                                :allocation-required 1
                                                :use-allocation-provided-with-the-trade 2))
  (:expiration-cycle "827" :type (int-enum :expire-on-trading-session-close 0
                                           :expire-on-trading-session-open 1))
  (:trd-type "828" :type (int-enum :regular-trade 0
                                   :block-trade 1
                                   :efp 2
                                   :transfer 3
                                   :late-trade 4
                                   :t-trade 5
                                   :weighted-average-price-trade 6
                                   :bunched-trade 7
                                   :late-bunched-trade 8
                                   :prior-reference-price-trade 9
                                   :after-hours-trade 10))
  (:trd-sub-type "829" :type cl-fix:int)
  (:transfer-reason "830" :type string)
  (:tot-num-assignment-reports "832" :type cl-fix:int)
  (:asgn-rpt-id "833" :type string)
  (:threshold-amount "834" :type cl-fix:priceoffset)
  (:peg-move-type "835" :type (int-enum :floating 0
                                        :fixed 1))
  (:peg-offset-type "836" :type (int-enum :price 0
                                          :basis-points 1
                                          :ticks 2
                                          :price-tier 3))
  (:peg-limit-type "837" :type (int-enum :or-better 0
                                         :strict-limit-is-a-strict-limit 1
                                         :or-worse-for-a-buy-the-peg-limit-is-a-minimum-and-for-a-sell-the-peg-limit-is-a-maximum 2))
  (:peg-round-direction "838" :type (int-enum :more-aggressive-on-a-buy-order-round-the-price-up-round-up-to-the-nearest-tick-on-a-sell-round-down-to-the-nearest-tick 1
                                              :more-passive-on-a-buy-order-round-down-to-nearest-tick-on-a-sell-order-round-up-to-nearest-tick 2))
  (:pegged-price "839" :type cl-fix:price)
  (:peg-scope "840" :type (int-enum :local 1
                                    :national 2
                                    :global 3
                                    :national-excluding-local 4))
  (:discretion-move-type "841" :type (int-enum :floating 0
                                               :fixed 1))
  (:discretion-offset-type "842" :type (int-enum :price 0
                                                 :basis-points 1
                                                 :ticks 2
                                                 :price-tier 3))
  (:discretion-limit-type "843" :type (int-enum :or-better 0
                                                :strict-limit-is-a-strict-limit 1
                                                :or-worse-for-a-buy-the-discretion-price-is-a-minimum-and-for-a-sell-the-discretion-price-is-a-maximum 2))
  (:discretion-round-direction "844" :type (int-enum :more-aggressive-on-a-buy-order-round-the-price-up-round-up-to-the-nearest-tick-on-a-sell-round-down-to-the-nearest-tick 1
                                                     :more-passive-on-a-buy-order-round-down-to-nearest-tick-on-a-sell-order-round-up-to-nearest-tick 2))
  (:discretion-price "845" :type cl-fix:price)
  (:discretion-scope "846" :type (int-enum :local 1
                                           :national 2
                                           :global 3
                                           :national-excluding-local 4))
  (:target-strategy "847" :type (int-enum :vwap 1
                                          :participate 2
                                          :mininize-market-impact 3))
  (:target-strategy-parameters "848" :type string)
  (:participation-rate "849" :type cl-fix:percentage)
  (:target-strategy-performance "850" :type double-float)
  (:last-liquidity-ind "851" :type (int-enum :added-liquidity 1
                                             :removed-liquidity 2
                                             :liquidity-routed-out 3))
  (:publish-trd-indicator "852" :type boolean)
  (:short-sale-reason "853" :type (int-enum :dealer-sold-short 0
                                            :dealer-sold-short-exempt 1
                                            :selling-customer-sold-short 2
                                            :selling-customer-sold-short-exempt 3
                                            :qualifed-service-representative 4
                                            :qsr-or-agu-contra-side-sold-short-exempt 5))
  (:qty-type "854" :type (int-enum :units 0
                                   :contracts 1))
  (:secondary-trd-type "855" :type cl-fix:int)
  (:trade-report-type "856" :type (int-enum :submit 0
                                            :alleged 1
                                            :accept 2
                                            :decline 3
                                            :addendum 4
                                            :no-was 5
                                            :trade-report-cancel 6
                                            :locked-in-trade-break 7))
  (:alloc-orders-type "857" :type (int-enum :not-specified 0
                                            :explicit-list-provided 1))
  (:shared-commission "858" :type cl-fix:amt)
  (:confirm-req-id "859" :type string)
  (:avg-par-px "860" :type cl-fix:price)
  (:reported-px "861" :type cl-fix:price)
  (:no-capacities "862" :type cl-fix:numingroup)
  (:order-capacity-qty "863" :type cl-fix:qty)
  (:no-events "864" :type cl-fix:numingroup)
  (:event-type "865" :type (int-enum :put 1
                                     :call 2
                                     :tender 3
                                     :sinking-fund-call 4
                                     :other 99))
  (:event-date "866" :type cl-fix:localmktdate)
  (:event-px "867" :type cl-fix:price)
  (:event-text "868" :type string)
  (:pct-risk "869" :type cl-fix:percentage)
  (:no-instr-attrib "870" :type cl-fix:numingroup)
  (:instr-attrib-type "871" :type (int-enum :flat 1
                                            :zero-coupon 2
                                            :interest-bearing 3
                                            :no-periodic-payments 4
                                            :variable-rate 5
                                            :less-fee-for-put 6
                                            :stepped-coupon 7
                                            :coupon-period 8
                                            :when-and-if-issued 9
                                            :original-issue-discount 10
                                            :callable-puttable 11
                                            :escrowed-to-maturity 12
                                            :escrowed-to-redemption-date-callable-supply-redemption-date-in-the-instrattribvalue 13
                                            :prerefunded 14
                                            :in-default 15
                                            :unrated 16
                                            :taxable 17
                                            :indexed 18
                                            :subject-to-alternative-minimum-tax 19
                                            :original-issue-discount-price-supply-price-in-the-instrattribvalue 20
                                            :callable-below-maturity-value 21
                                            :callable-without-notice-by-mail-to-holder-unless-registered 22
                                            :text-supply-the-text-of-the-attribute-or-disclaimer-in-the-instrattribvalue 99))
  (:instr-attrib-value "872" :type string)
  (:dated-date "873" :type cl-fix:localmktdate)
  (:interest-accrual-date "874" :type cl-fix:localmktdate)
  (:cp-program "875" :type (int-enum :3 1
                                     :4 2
                                     :other 99))
  (:cp-reg-type "876" :type string)
  (:underlying-cp-program "877" :type string)
  (:underlying-cp-reg-type "878" :type string)
  (:underlying-qty "879" :type cl-fix:qty)
  (:trd-match-id "880" :type string)
  (:secondary-trade-report-ref-id "881" :type string)
  (:underlying-dirty-price "882" :type cl-fix:price)
  (:underlying-end-price "883" :type cl-fix:price)
  (:underlying-start-value "884" :type cl-fix:amt)
  (:underlying-current-value "885" :type cl-fix:amt)
  (:underlying-end-value "886" :type cl-fix:amt)
  (:no-underlying-stips "887" :type cl-fix:numingroup)
  (:underlying-stip-type "888" :type string)
  (:underlying-stip-value "889" :type string)
  (:maturity-net-money "890" :type cl-fix:amt)
  (:misc-fee-basis "891" :type (int-enum :absolute 0
                                         :per-unit 1
                                         :percentage 2))
  (:tot-allocs "892" :type cl-fix:int)
  (:last-fragment "893" :type boolean)
  (:coll-req-id "894" :type string)
  (:coll-asgn-reason "895" :type (int-enum :initial 0
                                           :scheduled 1
                                           :time-warning 2
                                           :margin-deficiency 3
                                           :margin-excess 4
                                           :forward-collateral-demand 5
                                           :event-of-default 6
                                           :adverse-tax-event 7))
  (:coll-inquiry-qualifier "896" :type (int-enum :tradedate 0
                                                 :gc-instrument 1
                                                 :collateralinstrument 2
                                                 :substitution-eligible 3
                                                 :not-assigned 4
                                                 :partially-assigned 5
                                                 :fully-assigned 6
                                                 :outstanding-trades 7))
  (:no-trades "897" :type cl-fix:numingroup)
  (:margin-ratio "898" :type cl-fix:percentage)
  (:margin-excess "899" :type cl-fix:amt)
  (:total-net-value "900" :type cl-fix:amt)
  (:cash-outstanding "901" :type cl-fix:amt)
  (:coll-asgn-id "902" :type string)
  (:coll-asgn-trans-type "903" :type (int-enum :new 0
                                               :replace 1
                                               :cancel 2
                                               :release 3
                                               :reverse 4))
  (:coll-resp-id "904" :type string)
  (:coll-asgn-resp-type "905" :type (int-enum :received 0
                                              :accepted 1
                                              :declined 2
                                              :rejected 3))
  (:coll-asgn-reject-reason "906" :type (int-enum :unknown-deal 0
                                                  :unknown-or-invalid-instrument 1
                                                  :unauthorized-transaction 2
                                                  :insufficient-collateral 3
                                                  :invalid-type-of-collateral 4
                                                  :excessive-substitution 5
                                                  :other 99))
  (:coll-asgn-ref-id "907" :type string)
  (:coll-rpt-id "908" :type string)
  (:coll-inquiry-id "909" :type string)
  (:coll-status "910" :type (int-enum :unassigned 0
                                      :partially-assigned 1
                                      :assignment-proposed 2
                                      :assigned 3
                                      :challenged 4))
  (:tot-num-reports "911" :type cl-fix:int)
  (:last-rpt-requested "912" :type boolean)
  (:agreement-desc "913" :type string)
  (:agreement-id "914" :type string)
  (:agreement-date "915" :type cl-fix:localmktdate)
  (:start-date "916" :type cl-fix:localmktdate)
  (:end-date "917" :type cl-fix:localmktdate)
  (:agreement-currency "918" :type cl-fix:currency)
  (:delivery-type "919" :type (int-enum :versus-payment-deliver 0
                                        :free-deliver 1
                                        :tri-party 2
                                        :hold-in-custody 3))
  (:end-accrued-interest-amt "920" :type cl-fix:amt)
  (:start-cash "921" :type cl-fix:amt)
  (:end-cash "922" :type cl-fix:amt)
  (:user-request-id "923" :type string)
  (:user-request-type "924" :type (int-enum :logonuser 1
                                            :logoffuser 2
                                            :changepasswordforuser 3
                                            :request-individual-user-status 4))
  (:new-password "925" :type string)
  (:user-status "926" :type (int-enum :logged-in 1
                                      :not-logged-in 2
                                      :user-not-recognised 3
                                      :password-incorrect 4
                                      :password-changed 5
                                      :other 6))
  (:user-status-text "927" :type string)
  (:status-value "928" :type (int-enum :connected 1
                                       :not-connected-down-expected-up 2
                                       :not-connected-down-expected-down 3
                                       :in-process 4))
  (:status-text "929" :type string)
  (:ref-comp-id "930" :type string)
  (:ref-sub-id "931" :type string)
  (:network-response-id "932" :type string)
  (:network-request-id "933" :type string)
  (:last-network-response-id "934" :type string)
  (:network-request-type "935" :type (int-enum :snapshot 1
                                               :subscribe 2
                                               :stop-subscribing 4
                                               :level-of-detail-then-nocompids-becomes-required 8))
  (:no-comp-ids "936" :type cl-fix:numingroup)
  (:network-status-response-type "937" :type (int-enum :full 1
                                                       :incremental-update 2))
  (:no-coll-inquiry-qualifier "938" :type cl-fix:numingroup)
  (:trd-rpt-status "939" :type (int-enum :accepted 0
                                         :rejected 1))
  (:affirm-status "940" :type (int-enum :received 1
                                        :confirm-rejected-ie-not-affirmed 2
                                        :affirmed 3))
  (:underlying-strike-currency "941" :type cl-fix:currency)
  (:leg-strike-currency "942" :type cl-fix:currency)
  (:time-bracket "943" :type string)
  (:coll-action "944" :type (int-enum :retain 0
                                      :add 1
                                      :remove 2))
  (:coll-inquiry-status "945" :type (int-enum :accepted 0
                                              :accepted-with-warnings 1
                                              :completed 2
                                              :completed-with-warnings 3
                                              :rejected 4))
  (:coll-inquiry-result "946" :type (int-enum :successful 0
                                              :invalid-or-unknown-instrument 1
                                              :invalid-or-unknown-collateral-type 2
                                              :invalid-parties 3
                                              :invalid-transport-type-requested 4
                                              :invalid-destination-requested 5
                                              :no-collateral-found-for-the-trade-specified 6
                                              :no-collateral-found-for-the-order-specified 7
                                              :collateral-inquiry-type-not-supported 8
                                              :unauthorized-for-collateral-inquiry 9
                                              :other 99))
  (:strike-currency "947" :type cl-fix:currency)
  (:no-nested3party-ids "948" :type cl-fix:numingroup)
  (:nested3party-id "949" :type string)
  (:nested3party-id-source "950" :type character)
  (:nested3party-role "951" :type cl-fix:int)
  (:no-nested3party-sub-ids "952" :type cl-fix:numingroup)
  (:nested3party-sub-id "953" :type string)
  (:nested3party-sub-id-type "954" :type cl-fix:int)
  (:leg-contract-settl-month "955" :type cl-fix:monthyear)
  (:leg-interest-accrual-date "956" :type cl-fix:localmktdate))

(defcomponent commission-data ()
  :commission
  :comm-type
  :comm-currency
  :fund-renew-waiv)

(defcomponent discretion-instructions ()
  :discretion-inst
  :discretion-offset-value
  :discretion-move-type
  :discretion-offset-type
  :discretion-limit-type
  :discretion-round-direction
  :discretion-scope)

(defcomponent financing-details ()
  :agreement-desc
  :agreement-id
  :agreement-date
  :agreement-currency
  :termination-type
  :start-date
  :end-date
  :delivery-type
  :margin-ratio)

(defcomponent instrument ()
  :symbol
  :symbol-sfx
  :security-id
  :security-id-source
  (component :sec-alt-id-grp)
  :product
  :cficode
  :security-type
  :security-sub-type
  :maturity-month-year
  :maturity-date
  :put-call
  :coupon-payment-date
  :issue-date
  :repo-collateral-security-type
  :repurchase-term
  :repurchase-rate
  :factor
  :credit-rating
  :instr-registry
  :country-issue
  :state-province-issue
  :locale-issue
  :redemption-date
  :strike-price
  :strike-currency
  :opt-attribute
  :contract-multiplier
  :coupon-rate
  :security-exchange
  :issuer
  :encoded-issuer-len
  :encoded-issuer
  :security-desc
  :encoded-security-desc-len
  :encoded-security-desc
  :pool
  :contract-settl-month
  :cp-program
  :cp-reg-type
  (component :evnt-grp)
  :dated-date
  :interest-accrual-date)

(defcomponent instrument-extension ()
  :delivery-form
  :pct-risk
  (component :attrb-grp))

(defcomponent instrument-leg ()
  :leg-symbol
  :leg-symbol-sfx
  :leg-security-id
  :leg-security-id-source
  (component :leg-sec-alt-id-grp)
  :leg-product
  :legcficode
  :leg-security-type
  :leg-security-sub-type
  :leg-maturity-month-year
  :leg-maturity-date
  :leg-coupon-payment-date
  :leg-issue-date
  :leg-repo-collateral-security-type
  :leg-repurchase-term
  :leg-repurchase-rate
  :leg-factor
  :leg-credit-rating
  :leg-instr-registry
  :leg-country-issue
  :leg-state-province-issue
  :leg-locale-issue
  :leg-redemption-date
  :leg-strike-price
  :leg-strike-currency
  :leg-opt-attribute
  :leg-contract-multiplier
  :leg-coupon-rate
  :leg-security-exchange
  :leg-issuer
  :encoded-leg-issuer-len
  :encoded-leg-issuer
  :leg-security-desc
  :encoded-leg-security-desc-len
  :encoded-leg-security-desc
  :leg-ratio-qty
  :leg-side
  :leg-currency
  :leg-pool
  :leg-dated-date
  :leg-contract-settl-month
  :leg-interest-accrual-date)

(defcomponent leg-benchmark-curve-data ()
  :leg-benchmark-curve-currency
  :leg-benchmark-curve-name
  :leg-benchmark-curve-point
  :leg-benchmark-price
  :leg-benchmark-price-type)

(defcomponent leg-stipulations ()
  (group 
    :no-leg-stipulations
    :leg-stipulation-type
    :leg-stipulation-value))

(defcomponent nested-parties ()
  (group 
    :no-nested-party-ids
    :nested-party-id
    :nested-party-id-source
    :nested-party-role
    (component :nstd-ptys-sub-grp)))

(defcomponent order-qty-data ()
  :order-qty
  :cash-order-qty
  :order-percent
  :rounding-direction
  :rounding-modulus)

(defcomponent parties ()
  (group 
    :no-party-ids
    :party-id
    :party-id-source
    :party-role
    (component :ptys-sub-grp)))

(defcomponent peg-instructions ()
  :peg-offset-value
  :peg-move-type
  :peg-offset-type
  :peg-limit-type
  :peg-round-direction
  :peg-scope)

(defcomponent position-amount-data ()
  (group 
    :no-pos-amt
    :pos-amt-type
    :pos-amt))

(defcomponent position-qty ()
  (group 
    :no-positions
    :pos-type
    :long-qty
    :short-qty
    :pos-qty-status
    (component :nested-parties)))

(defcomponent settl-instructions-data ()
  :settl-delivery-type
  :stand-inst-type
  :stand-inst-name
  :stand-inst-db-id
  (component :dlvy-inst-grp))

(defcomponent settl-parties ()
  (group 
    :no-settl-party-ids
    :settl-party-id
    :settl-party-id-source
    :settl-party-role
    (component :settl-ptys-sub-grp)))

(defcomponent spread-benchmark-curve-data ()
  :spread
  :benchmark-curve-currency
  :benchmark-curve-name
  :benchmark-curve-point
  :benchmark-price
  :benchmark-price-type
  :benchmark-security-id
  :benchmark-security-id-source)

(defcomponent stipulations ()
  (group 
    :no-stipulations
    :stipulation-type
    :stipulation-value))

(defcomponent trd-reg-timestamps ()
  (group 
    :no-trd-reg-timestamps
    :trd-reg-timestamp
    :trd-reg-timestamp-type
    :trd-reg-timestamp-origin))

(defcomponent underlying-instrument ()
  :underlying-symbol
  :underlying-symbol-sfx
  :underlying-security-id
  :underlying-security-id-source
  (component :und-sec-alt-id-grp)
  :underlying-product
  :underlyingcficode
  :underlying-security-type
  :underlying-security-sub-type
  :underlying-maturity-month-year
  :underlying-maturity-date
  :underlying-put-call
  :underlying-coupon-payment-date
  :underlying-issue-date
  :underlying-repo-collateral-security-type
  :underlying-repurchase-term
  :underlying-repurchase-rate
  :underlying-factor
  :underlying-credit-rating
  :underlying-instr-registry
  :underlying-country-issue
  :underlying-state-province-issue
  :underlying-locale-issue
  :underlying-redemption-date
  :underlying-strike-price
  :underlying-strike-currency
  :underlying-opt-attribute
  :underlying-contract-multiplier
  :underlying-coupon-rate
  :underlying-security-exchange
  :underlying-issuer
  :encoded-underlying-issuer-len
  :encoded-underlying-issuer
  :underlying-security-desc
  :encoded-underlying-security-desc-len
  :encoded-underlying-security-desc
  :underlying-cp-program
  :underlying-cp-reg-type
  :underlying-currency
  :underlying-qty
  :underlying-px
  :underlying-dirty-price
  :underlying-end-price
  :underlying-start-value
  :underlying-current-value
  :underlying-end-value
  (component :underlying-stipulations))

(defcomponent yield-data ()
  :yield-type
  :yield
  :yield-calc-date
  :yield-redemption-date
  :yield-redemption-price
  :yield-redemption-price-type)

(defcomponent underlying-stipulations ()
  (group 
    :no-underlying-stips
    :underlying-stip-type
    :underlying-stip-value))

(defcomponent nested-parties2 ()
  (group 
    :no-nested2party-ids
    :nested2party-id
    :nested2party-id-source
    :nested2party-role
    (component :nstd-ptys2sub-grp)))

(defcomponent nested-parties3 ()
  (group 
    :no-nested3party-ids
    :nested3party-id
    :nested3party-id-source
    :nested3party-role
    (component :nstd-ptys3sub-grp)))

(defcomponent affected-ord-grp ()
  (group 
    :no-affected-orders
    :orig-ord-id
    :affected-order-id
    :affected-secondary-order-id))

(defcomponent alloc-ack-grp ()
  (group 
    :no-allocs
    :alloc-account
    :alloc-acct-id-source
    :alloc-price
    :individual-alloc-id
    :individual-alloc-rej-code
    :alloc-text
    :encoded-alloc-text-len
    :encoded-alloc-text))

(defcomponent alloc-grp ()
  (group 
    :no-allocs
    :alloc-account
    :alloc-acct-id-source
    :match-status
    :alloc-price
    :alloc-qty
    :individual-alloc-id
    :process-code
    (component :nested-parties)
    :notify-broker-credit
    :alloc-handl-inst
    :alloc-text
    :encoded-alloc-text-len
    :encoded-alloc-text
    (component :commission-data)
    :alloc-avg-px
    :alloc-net-money
    :settl-curr-amt
    :alloc-settl-curr-amt
    :settl-currency
    :alloc-settl-currency
    :settl-curr-rate
    :settl-curr-rate-calc
    :alloc-accrued-interest-amt
    :alloc-interest-maturity
    (component :misc-fees-grp)
    (component :clr-inst-grp)
    :alloc-settl-inst-type
    (component :settl-instructions-data)))

(defcomponent bid-comp-req-grp ()
  (group 
    :no-bid-components
    :list-id
    :side
    :trading-session-id
    :trading-session-sub-id
    :net-gross-ind
    :settl-type
    :settl-date
    :account
    :acct-id-source))

(defcomponent bid-comp-rsp-grp ()
  (group 
    (:no-bid-components :required t)
    (component :commission-data :required t)
    :list-id
    :country
    :side
    :price
    :price-type
    :fair-value
    :net-gross-ind
    :settl-type
    :settl-date
    :trading-session-id
    :trading-session-sub-id
    :text
    :encoded-text-len
    :encoded-text))

(defcomponent bid-desc-req-grp ()
  (group 
    :no-bid-descriptors
    :bid-descriptor-type
    :bid-descriptor
    :side-value-ind
    :liquidity-value
    :liquidity-num-securities
    :liquidity-pct-low
    :liquidity-pct-high
    :efptracking-error
    :fair-value
    :outside-index-pct
    :value-futures))

(defcomponent clr-inst-grp ()
  (group 
    :no-clearing-instructions
    :clearing-instruction))

(defcomponent coll-inq-qual-grp ()
  (group 
    :no-coll-inquiry-qualifier
    :coll-inquiry-qualifier))

(defcomponent comp-id-req-grp ()
  (group 
    :no-comp-ids
    :ref-comp-id
    :ref-sub-id
    :location-id
    :desk-id))

(defcomponent comp-id-stat-grp ()
  (group 
    (:no-comp-ids :required t)
    :ref-comp-id
    :ref-sub-id
    :location-id
    :desk-id
    :status-value
    :status-text))

(defcomponent cont-amt-grp ()
  (group 
    :no-cont-amts
    :cont-amt-type
    :cont-amt-value
    :cont-amt-curr))

(defcomponent contra-grp ()
  (group 
    :no-contra-brokers
    :contra-broker
    :contra-trader
    :contra-trade-qty
    :contra-trade-time
    :contra-leg-ref-id))

(defcomponent cpcty-conf-grp ()
  (group 
    (:no-capacities :required t)
    (:order-capacity :required t)
    :order-restrictions
    (:order-capacity-qty :required t)))

(defcomponent exec-alloc-grp ()
  (group 
    :no-execs
    :last-qty
    :exec-id
    :secondary-exec-id
    :last-px
    :last-par-px
    :last-capacity))

(defcomponent exec-coll-grp ()
  (group 
    :no-execs
    :exec-id))

(defcomponent execs-grp ()
  (group 
    :no-execs
    :exec-id))

(defcomponent instrmt-grp ()
  (group 
    :no-related-sym
    (component :instrument)))

(defcomponent instrmt-leg-exec-grp ()
  (group 
    :no-legs
    (component :instrument-leg)
    :leg-qty
    :leg-swap-type
    (component :leg-stipulations)
    :leg-position-effect
    :leg-covered-uncovered
    (component :nested-parties)
    :leg-ref-id
    :leg-price
    :leg-settl-type
    :leg-settl-date
    :leg-last-px))

(defcomponent instrmt-leg-grp ()
  (group 
    :no-legs
    (component :instrument-leg)))

(defcomponent instrmt-leg-ioi-grp ()
  (group 
    :no-legs
    (component :instrument-leg)
    :leg-ioi-qty
    (component :leg-stipulations)))

(defcomponent instrmt-leg-sec-list-grp ()
  (group 
    :no-legs
    (component :instrument-leg)
    :leg-swap-type
    :leg-settl-type
    (component :leg-stipulations)
    (component :leg-benchmark-curve-data)))

(defcomponent instrmtmdreq-grp ()
  (group 
    (:no-related-sym :required t)
    (component :instrument :required t)
    (component :und-instrmt-grp)
    (component :instrmt-leg-grp)))

(defcomponent instrmt-strk-grp ()
  (group 
    (:no-strikes :required t)
    (component :instrument :required t)))

(defcomponent ioi-qual-grp ()
  (group 
    :no-ioi-qualifiers
    :ioi-qualifier))

(defcomponent leg-ord-grp ()
  (group 
    (:no-legs :required t)
    (component :instrument-leg)
    :leg-qty
    :leg-swap-type
    (component :leg-stipulations)
    (component :leg-pre-alloc-grp)
    :leg-position-effect
    :leg-covered-uncovered
    (component :nested-parties)
    :leg-ref-id
    :leg-price
    :leg-settl-type
    :leg-settl-date))

(defcomponent leg-pre-alloc-grp ()
  (group 
    :no-leg-allocs
    :leg-alloc-account
    :leg-individual-alloc-id
    (component :nested-parties2)
    :leg-alloc-qty
    :leg-alloc-acct-id-source
    :leg-settl-currency))

(defcomponent leg-quot-grp ()
  (group 
    :no-legs
    (component :instrument-leg)
    :leg-qty
    :leg-swap-type
    :leg-settl-type
    :leg-settl-date
    (component :leg-stipulations)
    (component :nested-parties)
    :leg-price-type
    :leg-bid-px
    :leg-offer-px
    (component :leg-benchmark-curve-data)))

(defcomponent leg-quot-stat-grp ()
  (group 
    :no-legs
    (component :instrument-leg)
    :leg-qty
    :leg-swap-type
    :leg-settl-type
    :leg-settl-date
    (component :leg-stipulations)
    (component :nested-parties)))

(defcomponent lines-text-grp ()
  (group 
    (:no-lines-text :required t)
    (:text :required t)
    :encoded-text-len
    :encoded-text))

(defcomponent list-ord-grp ()
  (group 
    (:no-orders :required t)
    (:cl-ord-id :required t)
    :secondary-ord-id
    (:list-seq-no :required t)
    :cl-ord-link-id
    :settl-inst-mode
    (component :parties)
    :trade-origination-date
    :trade-date
    :account
    :acct-id-source
    :account-type
    :day-booking-inst
    :booking-unit
    :alloc-id
    :prealloc-method
    (component :pre-alloc-grp)
    :settl-type
    :settl-date
    :cash-margin
    :clearing-fee-indicator
    :handl-inst
    :exec-inst
    :min-qty
    :max-floor
    :ex-destination
    (component :trdg-ses-grp)
    :process-code
    (component :instrument :required t)
    (component :und-instrmt-grp)
    :prev-close-px
    (:side :required t)
    :side-value-ind
    :locate-reqd
    :transact-time
    (component :stipulations)
    :qty-type
    (component :order-qty-data :required t)
    :ord-type
    :price-type
    :price
    :stop-px
    (component :spread-benchmark-curve-data)
    (component :yield-data)
    :currency
    :compliance-id
    :solicited-flag
    :ioi-id
    :quote-id
    :time-force
    :effective-time
    :expire-date
    :expire-time
    :gtbooking-inst
    (component :commission-data)
    :order-capacity
    :order-restrictions
    :cust-order-capacity
    :forex-req
    :settl-currency
    :booking-type
    :text
    :encoded-text-len
    :encoded-text
    :settl-date2
    :order-qty2
    :price2
    :position-effect
    :covered-uncovered
    :max-show
    (component :peg-instructions)
    (component :discretion-instructions)
    :target-strategy
    :target-strategy-parameters
    :participation-rate
    :designation))

(defcomponent md-full-grp ()
  (group 
    (:no-md-entries :required t)
    (:md-entry-type :required t)
    :md-entry-px
    :currency
    :md-entry-size
    :md-entry-date
    :md-entry-time
    :tick-direction
    :md-mkt
    :trading-session-id
    :trading-session-sub-id
    :quote-condition
    :trade-condition
    :md-entry-originator
    :location-id
    :desk-id
    :open-close-settl-flag
    :time-force
    :expire-date
    :expire-time
    :min-qty
    :exec-inst
    :seller-days
    :order-id
    :quote-entry-id
    :md-entry-buyer
    :md-entry-seller
    :number-orders
    :md-entry-position-no
    :scope
    :price-delta
    :text
    :encoded-text-len
    :encoded-text))

(defcomponent md-inc-grp ()
  (group 
    (:no-md-entries :required t)
    (:md-update-action :required t)
    :delete-reason
    :md-entry-type
    :md-entry-id
    :md-entry-ref-id
    (component :instrument)
    (component :und-instrmt-grp)
    (component :instrmt-leg-grp)
    :financial-status
    :corporate-action
    :md-entry-px
    :currency
    :md-entry-size
    :md-entry-date
    :md-entry-time
    :tick-direction
    :md-mkt
    :trading-session-id
    :trading-session-sub-id
    :quote-condition
    :trade-condition
    :md-entry-originator
    :location-id
    :desk-id
    :open-close-settl-flag
    :time-force
    :expire-date
    :expire-time
    :min-qty
    :exec-inst
    :seller-days
    :order-id
    :quote-entry-id
    :md-entry-buyer
    :md-entry-seller
    :number-orders
    :md-entry-position-no
    :scope
    :price-delta
    :net-chg-prev-day
    :text
    :encoded-text-len
    :encoded-text))

(defcomponent md-req-grp ()
  (group 
    (:no-md-entry-types :required t)
    (:md-entry-type :required t)))

(defcomponent md-rjct-grp ()
  (group 
    :no-altmdsource
    :altmdsource-id))

(defcomponent misc-fees-grp ()
  (group 
    :no-misc-fees
    :misc-fee-amt
    :misc-fee-curr
    :misc-fee-type
    :misc-fee-basis))

(defcomponent ord-alloc-grp ()
  (group 
    :no-orders
    :cl-ord-id
    :order-id
    :secondary-order-id
    :secondary-ord-id
    :list-id
    (component :nested-parties2)
    :order-qty
    :order-avg-px
    :order-booking-qty))

(defcomponent ord-list-stat-grp ()
  (group 
    (:no-orders :required t)
    (:cl-ord-id :required t)
    :secondary-ord-id
    (:cum-qty :required t)
    (:ord-status :required t)
    :working-indicator
    (:leaves-qty :required t)
    (:cxl-qty :required t)
    (:avg-px :required t)
    :ord-rej-reason
    :text
    :encoded-text-len
    :encoded-text))

(defcomponent pos-und-instrmt-grp ()
  (group 
    :no-underlyings
    (component :underlying-instrument)
    (:underlying-settl-price :required t)
    (:underlying-settl-price-type :required t)))

(defcomponent pre-alloc-grp ()
  (group 
    :no-allocs
    :alloc-account
    :alloc-acct-id-source
    :alloc-settl-currency
    :individual-alloc-id
    (component :nested-parties)
    :alloc-qty))

(defcomponent pre-alloc-mleg-grp ()
  (group 
    :no-allocs
    :alloc-account
    :alloc-acct-id-source
    :alloc-settl-currency
    :individual-alloc-id
    (component :nested-parties3)
    :alloc-qty))

(defcomponent quot-cxl-entries-grp ()
  (group 
    :no-quote-entries
    (component :instrument)
    (component :financing-details)
    (component :und-instrmt-grp)
    (component :instrmt-leg-grp)))

(defcomponent quot-entry-ack-grp ()
  (group 
    :no-quote-entries
    :quote-entry-id
    (component :instrument)
    (component :instrmt-leg-grp)
    :bid-px
    :offer-px
    :bid-size
    :offer-size
    :valid-until-time
    :bid-spot-rate
    :offer-spot-rate
    :bid-forward-points
    :offer-forward-points
    :mid-px
    :bid-yield
    :mid-yield
    :offer-yield
    :transact-time
    :trading-session-id
    :trading-session-sub-id
    :settl-date
    :ord-type
    :settl-date2
    :order-qty2
    :bid-forward-points2
    :offer-forward-points2
    :currency
    :quote-entry-reject-reason))

(defcomponent quot-entry-grp ()
  (group 
    (:no-quote-entries :required t)
    (:quote-entry-id :required t)
    (component :instrument)
    (component :instrmt-leg-grp)
    :bid-px
    :offer-px
    :bid-size
    :offer-size
    :valid-until-time
    :bid-spot-rate
    :offer-spot-rate
    :bid-forward-points
    :offer-forward-points
    :mid-px
    :bid-yield
    :mid-yield
    :offer-yield
    :transact-time
    :trading-session-id
    :trading-session-sub-id
    :settl-date
    :ord-type
    :settl-date2
    :order-qty2
    :bid-forward-points2
    :offer-forward-points2
    :currency))

(defcomponent quot-qual-grp ()
  (group 
    :no-quote-qualifiers
    :quote-qualifier))

(defcomponent quot-req-grp ()
  (group 
    (:no-related-sym :required t)
    (component :instrument :required t)
    (component :financing-details)
    (component :und-instrmt-grp)
    :prev-close-px
    :quote-request-type
    :quote-type
    :trading-session-id
    :trading-session-sub-id
    :trade-origination-date
    :side
    :qty-type
    (component :order-qty-data)
    :settl-type
    :settl-date
    :settl-date2
    :order-qty2
    :currency
    (component :stipulations)
    :account
    :acct-id-source
    :account-type
    (component :quot-req-legs-grp)
    (component :quot-qual-grp)
    :quote-price-type
    :ord-type
    :valid-until-time
    :expire-time
    :transact-time
    (component :spread-benchmark-curve-data)
    :price-type
    :price
    :price2
    (component :yield-data)
    (component :parties)))

(defcomponent quot-req-legs-grp ()
  (group 
    :no-legs
    (component :instrument-leg)
    :leg-qty
    :leg-swap-type
    :leg-settl-type
    :leg-settl-date
    (component :leg-stipulations)
    (component :nested-parties)
    (component :leg-benchmark-curve-data)))

(defcomponent quot-req-rjct-grp ()
  (group 
    (:no-related-sym :required t)
    (component :instrument :required t)
    (component :financing-details)
    (component :und-instrmt-grp)
    :prev-close-px
    :quote-request-type
    :quote-type
    :trading-session-id
    :trading-session-sub-id
    :trade-origination-date
    :side
    :qty-type
    (component :order-qty-data)
    :settl-type
    :settl-date
    :settl-date2
    :order-qty2
    :currency
    (component :stipulations)
    :account
    :acct-id-source
    :account-type
    (component :quot-req-legs-grp)
    (component :quot-qual-grp)
    :quote-price-type
    :ord-type
    :expire-time
    :transact-time
    (component :spread-benchmark-curve-data)
    :price-type
    :price
    :price2
    (component :yield-data)
    (component :parties)))

(defcomponent quot-set-ack-grp ()
  (group 
    :no-quote-sets
    :quote-set-id
    (component :underlying-instrument)
    :tot-quote-entries
    :last-fragment
    (component :quot-entry-ack-grp)))

(defcomponent quot-set-grp ()
  (group 
    (:no-quote-sets :required t)
    (:quote-set-id :required t)
    (component :underlying-instrument)
    :quote-set-valid-until-time
    (:tot-quote-entries :required t)
    :last-fragment
    (component :quot-entry-grp :required t)))

(defcomponent rel-sym-deriv-sec-grp ()
  (group 
    :no-related-sym
    (component :instrument)
    :currency
    :expiration-cycle
    (component :instrument-extension)
    (component :instrmt-leg-grp)
    :trading-session-id
    :trading-session-sub-id
    :text
    :encoded-text-len
    :encoded-text))

(defcomponent rfqreq-grp ()
  (group 
    (:no-related-sym :required t)
    (component :instrument :required t)
    (component :und-instrmt-grp)
    (component :instrmt-leg-grp)
    :prev-close-px
    :quote-request-type
    :quote-type
    :trading-session-id
    :trading-session-sub-id))

(defcomponent rgst-dist-inst-grp ()
  (group 
    :no-distrib-insts
    :distrib-payment-method
    :distrib-percentage
    :cash-distrib-curr
    :cash-distrib-agent-name
    :cash-distrib-agent-code
    :cash-distrib-agent-acct-number
    :cash-distrib-pay-ref
    :cash-distrib-agent-acct-name))

(defcomponent rgst-dtls-grp ()
  (group 
    :no-regist-dtls
    :regist-dtls
    :regist-email
    :mailing-dtls
    :mailing-inst
    (component :nested-parties)
    :owner-type
    :date-birth
    :investor-country-residence))

(defcomponent routing-grp ()
  (group 
    :no-routing-ids
    :routing-type
    :routing-id))

(defcomponent sec-list-grp ()
  (group 
    :no-related-sym
    (component :instrument)
    (component :instrument-extension)
    (component :financing-details)
    (component :und-instrmt-grp)
    :currency
    (component :stipulations)
    (component :instrmt-leg-sec-list-grp)
    (component :spread-benchmark-curve-data)
    (component :yield-data)
    :round-lot
    :min-trade-vol
    :trading-session-id
    :trading-session-sub-id
    :expiration-cycle
    :text
    :encoded-text-len
    :encoded-text))

(defcomponent sec-types-grp ()
  (group 
    :no-security-types
    :security-type
    :security-sub-type
    :product
    :cficode))

(defcomponent settl-inst-grp ()
  (group 
    :no-settl-inst
    :settl-inst-id
    :settl-inst-trans-type
    :settl-inst-ref-id
    (component :parties)
    :side
    :product
    :security-type
    :cficode
    :effective-time
    :expire-time
    :last-update-time
    (component :settl-instructions-data)
    :payment-method
    :payment-ref
    :card-holder-name
    :card-number
    :card-start-date
    :card-exp-date
    :card-iss-num
    :payment-date
    :payment-remitter-id))

(defcomponent side-cross-ord-cxl-grp ()
  (group 
    (:no-sides :required t)
    (:side :required t)
    (:orig-ord-id :required t)
    (:cl-ord-id :required t)
    :secondary-ord-id
    :cl-ord-link-id
    :orig-ord-mod-time
    (component :parties)
    :trade-origination-date
    :trade-date
    (component :order-qty-data :required t)
    :compliance-id
    :text
    :encoded-text-len
    :encoded-text))

(defcomponent side-cross-ord-mod-grp ()
  (group 
    (:no-sides :required t)
    (:side :required t)
    (:cl-ord-id :required t)
    :secondary-ord-id
    :cl-ord-link-id
    (component :parties)
    :trade-origination-date
    :trade-date
    :account
    :acct-id-source
    :account-type
    :day-booking-inst
    :booking-unit
    :prealloc-method
    :alloc-id
    (component :pre-alloc-grp)
    :qty-type
    (component :order-qty-data :required t)
    (component :commission-data)
    :order-capacity
    :order-restrictions
    :cust-order-capacity
    :forex-req
    :settl-currency
    :booking-type
    :text
    :encoded-text-len
    :encoded-text
    :position-effect
    :covered-uncovered
    :cash-margin
    :clearing-fee-indicator
    :solicited-flag
    :side-compliance-id))

(defcomponent trd-alloc-grp ()
  (group 
    :no-allocs
    :alloc-account
    :alloc-acct-id-source
    :alloc-settl-currency
    :individual-alloc-id
    (component :nested-parties2)
    :alloc-qty))

(defcomponent trd-cap-rpt-side-grp ()
  (group 
    (:no-sides :required t)
    (:side :required t)
    (:order-id :required t)
    :secondary-order-id
    :cl-ord-id
    :secondary-ord-id
    :list-id
    (component :parties)
    :account
    :acct-id-source
    :account-type
    :process-code
    :odd-lot
    (component :clr-inst-grp)
    :trade-input-source
    :trade-input-device
    :order-input-device
    :currency
    :compliance-id
    :solicited-flag
    :order-capacity
    :order-restrictions
    :cust-order-capacity
    :ord-type
    :exec-inst
    :trans-bkd-time
    :trading-session-id
    :trading-session-sub-id
    :time-bracket
    (component :commission-data)
    :gross-trade-amt
    :num-days-interest
    :ex-date
    :accrued-interest-rate
    :accrued-interest-amt
    :interest-maturity
    :end-accrued-interest-amt
    :start-cash
    :end-cash
    :concession
    :total-takedown
    :net-money
    :settl-curr-amt
    :settl-currency
    :settl-curr-rate
    :settl-curr-rate-calc
    :position-effect
    :text
    :encoded-text-len
    :encoded-text
    :side-multi-leg-reporting-type
    (component :cont-amt-grp)
    (component :stipulations)
    (component :misc-fees-grp)
    :exchange-rule
    :trade-alloc-indicator
    :prealloc-method
    :alloc-id
    (component :trd-alloc-grp)))

(defcomponent trd-coll-grp ()
  (group 
    :no-trades
    :trade-report-id
    :secondary-trade-report-id))

(defcomponent trd-instrmt-leg-grp ()
  (group 
    :no-legs
    (component :instrument-leg)
    :leg-qty
    :leg-swap-type
    (component :leg-stipulations)
    :leg-position-effect
    :leg-covered-uncovered
    (component :nested-parties)
    :leg-ref-id
    :leg-price
    :leg-settl-type
    :leg-settl-date
    :leg-last-px))

(defcomponent trdg-ses-grp ()
  (group 
    :no-trading-sessions
    :trading-session-id
    :trading-session-sub-id))

(defcomponent und-instrmt-coll-grp ()
  (group 
    :no-underlyings
    (component :underlying-instrument)
    :coll-action))

(defcomponent und-instrmt-grp ()
  (group 
    :no-underlyings
    (component :underlying-instrument)))

(defcomponent und-instrmt-strk-grp ()
  (group 
    :no-underlyings
    (component :underlying-instrument)
    :prev-close-px
    :cl-ord-id
    :secondary-ord-id
    :side
    (:price :required t)
    :currency
    :text
    :encoded-text-len
    :encoded-text))

(defcomponent trd-cap-grp ()
  (group 
    :no-dates
    :trade-date
    :transact-time))

(defcomponent evnt-grp ()
  (group 
    :no-events
    :event-type
    :event-date
    :event-px
    :event-text))

(defcomponent sec-alt-id-grp ()
  (group 
    :no-security-alt-id
    :security-alt-id
    :security-alt-id-source))

(defcomponent leg-sec-alt-id-grp ()
  (group 
    :no-leg-security-alt-id
    :leg-security-alt-id
    :leg-security-alt-id-source))

(defcomponent und-sec-alt-id-grp ()
  (group 
    :no-underlying-security-alt-id
    :underlying-security-alt-id
    :underlying-security-alt-id-source))

(defcomponent attrb-grp ()
  (group 
    :no-instr-attrib
    :instr-attrib-type
    :instr-attrib-value))

(defcomponent dlvy-inst-grp ()
  (group 
    :no-dlvy-inst
    :settl-inst-source
    :dlvy-inst-type
    (component :settl-parties)))

(defcomponent settl-ptys-sub-grp ()
  (group 
    :no-settl-party-sub-ids
    :settl-party-sub-id
    :settl-party-sub-id-type))

(defcomponent ptys-sub-grp ()
  (group 
    :no-party-sub-ids
    :party-sub-id
    :party-sub-id-type))

(defcomponent nstd-ptys-sub-grp ()
  (group 
    :no-nested-party-sub-ids
    :nested-party-sub-id
    :nested-party-sub-id-type))

(defcomponent hop ()
  (group 
    :no-hops
    :hop-comp-id
    :hop-sending-time
    :hop-ref-id))

(defcomponent nstd-ptys2sub-grp ()
  (group 
    :no-nested2party-sub-ids
    :nested2party-sub-id
    :nested2party-sub-id-type))

(defcomponent nstd-ptys3sub-grp ()
  (group 
    :no-nested3party-sub-ids
    :nested3party-sub-id
    :nested3party-sub-id-type))

(defheader
  (:begin-string :required t)
  (:body-length :required t)
  (:msg-type :required t)
  (:sender-comp-id :required t)
  (:target-comp-id :required t)
  :on-behalf-comp-id
  :deliver-comp-id
  :secure-data-len
  :secure-data
  (:msg-seq-num :required t)
  :sender-sub-id
  :sender-location-id
  :target-sub-id
  :target-location-id
  :on-behalf-sub-id
  :on-behalf-location-id
  :deliver-sub-id
  :deliver-location-id
  :poss-dup-flag
  :poss-resend
  (:sending-time :required t)
  :orig-sending-time
  :xml-data-len
  :xml-data
  :message-encoding
  :last-msg-seq-num-processed
  (component :hop))

(deftrailer
  :signature-length
  :signature
  (:check-sum :required t))

(defmessage heartbeat "0" ()
  :test-req-id)

(defmessage test-request "1" ()
  (:test-req-id :required t))

(defmessage resend-request "2" ()
  (:begin-seq-no :required t)
  (:end-seq-no :required t))

(defmessage reject "3" ()
  (:ref-seq-num :required t)
  :ref-tag-id
  :ref-msg-type
  :session-reject-reason
  :text
  :encoded-text-len
  :encoded-text)

(defmessage sequence-reset "4" ()
  :gap-fill-flag
  (:new-seq-no :required t))

(defmessage logout "5" ()
  :text
  :encoded-text-len
  :encoded-text)

(defmessage ioi "6" ()
  (:ioi-id :required t)
  (:ioi-trans-type :required t)
  :ioi-ref-id
  (component :instrument :required t)
  (component :financing-details)
  (component :und-instrmt-grp)
  (:side :required t)
  :qty-type
  (component :order-qty-data)
  (:ioi-qty :required t)
  :currency
  (component :stipulations)
  (component :instrmt-leg-ioi-grp)
  :price-type
  :price
  :valid-until-time
  :ioi-qlty-ind
  :ioi-natural-flag
  (component :ioi-qual-grp)
  :text
  :encoded-text-len
  :encoded-text
  :transact-time
  :urllink
  (component :routing-grp)
  (component :spread-benchmark-curve-data)
  (component :yield-data))

(defmessage advertisement "7" ()
  (:adv-id :required t)
  (:adv-trans-type :required t)
  :adv-ref-id
  (component :instrument :required t)
  (component :instrmt-leg-grp)
  (component :und-instrmt-grp)
  (:adv-side :required t)
  (:quantity :required t)
  :qty-type
  :price
  :currency
  :trade-date
  :transact-time
  :text
  :encoded-text-len
  :encoded-text
  :urllink
  :last-mkt
  :trading-session-id
  :trading-session-sub-id)

(defmessage execution-report "8" ()
  (:order-id :required t)
  :secondary-order-id
  :secondary-ord-id
  :secondary-exec-id
  :cl-ord-id
  :orig-ord-id
  :cl-ord-link-id
  :quote-resp-id
  :ord-status-req-id
  :mass-status-req-id
  :tot-num-reports
  :last-rpt-requested
  (component :parties)
  :trade-origination-date
  (component :contra-grp)
  :list-id
  :cross-id
  :orig-cross-id
  :cross-type
  (:exec-id :required t)
  :exec-ref-id
  (:exec-type :required t)
  (:ord-status :required t)
  :working-indicator
  :ord-rej-reason
  :exec-restatement-reason
  :account
  :acct-id-source
  :account-type
  :day-booking-inst
  :booking-unit
  :prealloc-method
  :settl-type
  :settl-date
  :cash-margin
  :clearing-fee-indicator
  (component :instrument :required t)
  (component :financing-details)
  (component :und-instrmt-grp)
  (:side :required t)
  (component :stipulations)
  :qty-type
  (component :order-qty-data)
  :ord-type
  :price-type
  :price
  :stop-px
  (component :peg-instructions)
  (component :discretion-instructions)
  :pegged-price
  :discretion-price
  :target-strategy
  :target-strategy-parameters
  :participation-rate
  :target-strategy-performance
  :currency
  :compliance-id
  :solicited-flag
  :time-force
  :effective-time
  :expire-date
  :expire-time
  :exec-inst
  :order-capacity
  :order-restrictions
  :cust-order-capacity
  :last-qty
  :underlying-last-qty
  :last-px
  :underlying-last-px
  :last-par-px
  :last-spot-rate
  :last-forward-points
  :last-mkt
  :trading-session-id
  :trading-session-sub-id
  :time-bracket
  :last-capacity
  (:leaves-qty :required t)
  (:cum-qty :required t)
  (:avg-px :required t)
  :day-order-qty
  :day-cum-qty
  :day-avg-px
  :gtbooking-inst
  :trade-date
  :transact-time
  :report-exch
  (component :commission-data)
  (component :spread-benchmark-curve-data)
  (component :yield-data)
  :gross-trade-amt
  :num-days-interest
  :ex-date
  :accrued-interest-rate
  :accrued-interest-amt
  :interest-maturity
  :end-accrued-interest-amt
  :start-cash
  :end-cash
  :traded-flat-switch
  :basis-feature-date
  :basis-feature-price
  :concession
  :total-takedown
  :net-money
  :settl-curr-amt
  :settl-currency
  :settl-curr-rate
  :settl-curr-rate-calc
  :handl-inst
  :min-qty
  :max-floor
  :position-effect
  :max-show
  :booking-type
  :text
  :encoded-text-len
  :encoded-text
  :settl-date2
  :order-qty2
  :last-forward-points2
  :multi-leg-reporting-type
  :cancellation-rights
  :money-laundering-status
  :regist-id
  :designation
  :trans-bkd-time
  :exec-valuation-point
  :exec-price-type
  :exec-price-adjustment
  :priority-indicator
  :price-improvement
  :last-liquidity-ind
  (component :cont-amt-grp)
  (component :instrmt-leg-exec-grp)
  :copy-msg-indicator
  (component :misc-fees-grp))

(defmessage order-cancel-reject "9" ()
  (:order-id :required t)
  :secondary-order-id
  :secondary-ord-id
  (:cl-ord-id :required t)
  :cl-ord-link-id
  (:orig-ord-id :required t)
  (:ord-status :required t)
  :working-indicator
  :orig-ord-mod-time
  :list-id
  :account
  :acct-id-source
  :account-type
  :trade-origination-date
  :trade-date
  :transact-time
  (:cxl-rej-response-to :required t)
  :cxl-rej-reason
  :text
  :encoded-text-len
  :encoded-text)

(defmessage logon "A" ()
  (:encrypt-method :required t)
  (:heart-int :required t)
  :raw-data-length
  :raw-data
  :reset-seq-num-flag
  :next-expected-msg-seq-num
  :max-message-size
  (group 
    :no-msg-types
    :ref-msg-type
    :msg-direction)
  :test-message-indicator
  :username
  :password)

(defmessage news "B" ()
  :orig-time
  :urgency
  (:headline :required t)
  :encoded-headline-len
  :encoded-headline
  (component :routing-grp)
  (component :instrmt-grp)
  (component :instrmt-leg-grp)
  (component :und-instrmt-grp)
  (component :lines-text-grp :required t)
  :urllink
  :raw-data-length
  :raw-data)

(defmessage email "C" ()
  (:email-thread-id :required t)
  (:email-type :required t)
  :orig-time
  (:subject :required t)
  :encoded-subject-len
  :encoded-subject
  (component :routing-grp)
  (component :instrmt-grp)
  (component :und-instrmt-grp)
  (component :instrmt-leg-grp)
  :order-id
  :cl-ord-id
  (component :lines-text-grp :required t)
  :raw-data-length
  :raw-data)

(defmessage new-order-single "D" ()
  (:cl-ord-id :required t)
  :secondary-ord-id
  :cl-ord-link-id
  (component :parties)
  :trade-origination-date
  :trade-date
  :account
  :acct-id-source
  :account-type
  :day-booking-inst
  :booking-unit
  :prealloc-method
  :alloc-id
  (component :pre-alloc-grp)
  :settl-type
  :settl-date
  :cash-margin
  :clearing-fee-indicator
  :handl-inst
  :exec-inst
  :min-qty
  :max-floor
  :ex-destination
  (component :trdg-ses-grp)
  :process-code
  (component :instrument :required t)
  (component :financing-details)
  (component :und-instrmt-grp)
  :prev-close-px
  (:side :required t)
  :locate-reqd
  (:transact-time :required t)
  (component :stipulations)
  :qty-type
  (component :order-qty-data :required t)
  (:ord-type :required t)
  :price-type
  :price
  :stop-px
  (component :spread-benchmark-curve-data)
  (component :yield-data)
  :currency
  :compliance-id
  :solicited-flag
  :ioi-id
  :quote-id
  :time-force
  :effective-time
  :expire-date
  :expire-time
  :gtbooking-inst
  (component :commission-data)
  :order-capacity
  :order-restrictions
  :cust-order-capacity
  :forex-req
  :settl-currency
  :booking-type
  :text
  :encoded-text-len
  :encoded-text
  :settl-date2
  :order-qty2
  :price2
  :position-effect
  :covered-uncovered
  :max-show
  (component :peg-instructions)
  (component :discretion-instructions)
  :target-strategy
  :target-strategy-parameters
  :participation-rate
  :cancellation-rights
  :money-laundering-status
  :regist-id
  :designation)

(defmessage new-order-list "E" ()
  (:list-id :required t)
  :bid-id
  :client-bid-id
  :prog-rpt-reqs
  (:bid-type :required t)
  :prog-period-interval
  :cancellation-rights
  :money-laundering-status
  :regist-id
  :list-exec-inst-type
  :list-exec-inst
  :encoded-list-exec-inst-len
  :encoded-list-exec-inst
  :allowable-one-sidedness-pct
  :allowable-one-sidedness-value
  :allowable-one-sidedness-curr
  (:tot-orders :required t)
  :last-fragment
  (component :list-ord-grp :required t))

(defmessage order-cancel-request "F" ()
  (:orig-ord-id :required t)
  :order-id
  (:cl-ord-id :required t)
  :secondary-ord-id
  :cl-ord-link-id
  :list-id
  :orig-ord-mod-time
  :account
  :acct-id-source
  :account-type
  (component :parties)
  (component :instrument :required t)
  (component :financing-details)
  (component :und-instrmt-grp)
  (:side :required t)
  (:transact-time :required t)
  (component :order-qty-data :required t)
  :compliance-id
  :text
  :encoded-text-len
  :encoded-text)

(defmessage order-cancel-replace-request "G" ()
  :order-id
  (component :parties)
  :trade-origination-date
  :trade-date
  (:orig-ord-id :required t)
  (:cl-ord-id :required t)
  :secondary-ord-id
  :cl-ord-link-id
  :list-id
  :orig-ord-mod-time
  :account
  :acct-id-source
  :account-type
  :day-booking-inst
  :booking-unit
  :prealloc-method
  :alloc-id
  (component :pre-alloc-grp)
  :settl-type
  :settl-date
  :cash-margin
  :clearing-fee-indicator
  :handl-inst
  :exec-inst
  :min-qty
  :max-floor
  :ex-destination
  (component :trdg-ses-grp)
  (component :instrument :required t)
  (component :financing-details)
  (component :und-instrmt-grp)
  (:side :required t)
  (:transact-time :required t)
  :qty-type
  (component :order-qty-data :required t)
  (:ord-type :required t)
  :price-type
  :price
  :stop-px
  (component :spread-benchmark-curve-data)
  (component :yield-data)
  (component :peg-instructions)
  (component :discretion-instructions)
  :target-strategy
  :target-strategy-parameters
  :participation-rate
  :compliance-id
  :solicited-flag
  :currency
  :time-force
  :effective-time
  :expire-date
  :expire-time
  :gtbooking-inst
  (component :commission-data)
  :order-capacity
  :order-restrictions
  :cust-order-capacity
  :forex-req
  :settl-currency
  :booking-type
  :text
  :encoded-text-len
  :encoded-text
  :settl-date2
  :order-qty2
  :price2
  :position-effect
  :covered-uncovered
  :max-show
  :locate-reqd
  :cancellation-rights
  :money-laundering-status
  :regist-id
  :designation)

(defmessage order-status-request "H" ()
  :order-id
  (:cl-ord-id :required t)
  :secondary-ord-id
  :cl-ord-link-id
  (component :parties)
  :ord-status-req-id
  :account
  :acct-id-source
  (component :instrument :required t)
  (component :financing-details)
  (component :und-instrmt-grp)
  (:side :required t))

(defmessage allocation-instruction "J" ()
  (:alloc-id :required t)
  (:alloc-trans-type :required t)
  (:alloc-type :required t)
  :secondary-alloc-id
  :ref-alloc-id
  :alloc-canc-replace-reason
  :alloc-intermed-req-type
  :alloc-link-id
  :alloc-link-type
  :booking-ref-id
  (:alloc-orders-type :required t)
  (component :ord-alloc-grp)
  (component :exec-alloc-grp)
  :previously-reported
  :reversal-indicator
  :match-type
  (:side :required t)
  (component :instrument :required t)
  (component :instrument-extension)
  (component :financing-details)
  (component :und-instrmt-grp)
  (component :instrmt-leg-grp)
  (:quantity :required t)
  :qty-type
  :last-mkt
  :trade-origination-date
  :trading-session-id
  :trading-session-sub-id
  :price-type
  (:avg-px :required t)
  :avg-par-px
  (component :spread-benchmark-curve-data)
  :currency
  :avg-precision
  (component :parties)
  (:trade-date :required t)
  :transact-time
  :settl-type
  :settl-date
  :booking-type
  :gross-trade-amt
  :concession
  :total-takedown
  :net-money
  :position-effect
  :auto-accept-indicator
  :text
  :encoded-text-len
  :encoded-text
  :num-days-interest
  :accrued-interest-rate
  :accrued-interest-amt
  :total-accrued-interest-amt
  :interest-maturity
  :end-accrued-interest-amt
  :start-cash
  :end-cash
  :legal-confirm
  (component :stipulations)
  (component :yield-data)
  :tot-allocs
  :last-fragment
  (component :alloc-grp))

(defmessage list-cancel-request "K" ()
  (:list-id :required t)
  (:transact-time :required t)
  :trade-origination-date
  :trade-date
  :text
  :encoded-text-len
  :encoded-text)

(defmessage list-execute "L" ()
  (:list-id :required t)
  :client-bid-id
  :bid-id
  (:transact-time :required t)
  :text
  :encoded-text-len
  :encoded-text)

(defmessage list-status-request "M" ()
  (:list-id :required t)
  :text
  :encoded-text-len
  :encoded-text)

(defmessage list-status "N" ()
  (:list-id :required t)
  (:list-status-type :required t)
  (:no-rpts :required t)
  (:list-order-status :required t)
  (:rpt-seq :required t)
  :list-status-text
  :encoded-list-status-text-len
  :encoded-list-status-text
  :transact-time
  (:tot-orders :required t)
  :last-fragment
  (component :ord-list-stat-grp :required t))

(defmessage allocation-instruction-ack "P" ()
  (:alloc-id :required t)
  (component :parties)
  :secondary-alloc-id
  :trade-date
  (:transact-time :required t)
  (:alloc-status :required t)
  :alloc-rej-code
  :alloc-type
  :alloc-intermed-req-type
  :match-status
  :product
  :security-type
  :text
  :encoded-text-len
  :encoded-text
  (component :alloc-ack-grp))

(defmessage dont-know-trade "Q" ()
  (:order-id :required t)
  :secondary-order-id
  (:exec-id :required t)
  (:dkreason :required t)
  (component :instrument :required t)
  (component :und-instrmt-grp)
  (component :instrmt-leg-grp)
  (:side :required t)
  (component :order-qty-data :required t)
  :last-qty
  :last-px
  :text
  :encoded-text-len
  :encoded-text)

(defmessage quote-request "R" ()
  (:quote-req-id :required t)
  :rfqreq-id
  :cl-ord-id
  :order-capacity
  (component :quot-req-grp :required t)
  :text
  :encoded-text-len
  :encoded-text)

(defmessage quote "S" ()
  :quote-req-id
  (:quote-id :required t)
  :quote-resp-id
  :quote-type
  (component :quot-qual-grp)
  :quote-response-level
  (component :parties)
  :trading-session-id
  :trading-session-sub-id
  (component :instrument :required t)
  (component :financing-details)
  (component :und-instrmt-grp)
  :side
  (component :order-qty-data)
  :settl-type
  :settl-date
  :settl-date2
  :order-qty2
  :currency
  (component :stipulations)
  :account
  :acct-id-source
  :account-type
  (component :leg-quot-grp)
  :bid-px
  :offer-px
  :mkt-bid-px
  :mkt-offer-px
  :min-bid-size
  :bid-size
  :min-offer-size
  :offer-size
  :valid-until-time
  :bid-spot-rate
  :offer-spot-rate
  :bid-forward-points
  :offer-forward-points
  :mid-px
  :bid-yield
  :mid-yield
  :offer-yield
  :transact-time
  :ord-type
  :bid-forward-points2
  :offer-forward-points2
  :settl-curr-bid-rate
  :settl-curr-offer-rate
  :settl-curr-rate-calc
  :comm-type
  :commission
  :cust-order-capacity
  :ex-destination
  :order-capacity
  :price-type
  (component :spread-benchmark-curve-data)
  (component :yield-data)
  :text
  :encoded-text-len
  :encoded-text)

(defmessage settlement-instructions "T" ()
  (:settl-inst-msg-id :required t)
  :settl-inst-req-id
  (:settl-inst-mode :required t)
  :settl-inst-req-rej-code
  :text
  :encoded-text-len
  :encoded-text
  :cl-ord-id
  (:transact-time :required t)
  (component :settl-inst-grp))

(defmessage market-data-request "V" ()
  (:md-req-id :required t)
  (:subscription-request-type :required t)
  (:market-depth :required t)
  :md-update-type
  :aggregated-book
  :open-close-settl-flag
  :scope
  :md-implicit-delete
  (component :md-req-grp :required t)
  (component :instrmtmdreq-grp :required t)
  (component :trdg-ses-grp)
  :appl-queue-action
  :appl-queue-max)

(defmessage market-data-snapshot-full-refresh "W" ()
  :md-req-id
  (component :instrument :required t)
  (component :und-instrmt-grp)
  (component :instrmt-leg-grp)
  :financial-status
  :corporate-action
  :net-chg-prev-day
  (component :md-full-grp :required t)
  :appl-queue-depth
  :appl-queue-resolution)

(defmessage market-data-incremental-refresh "X" ()
  :md-req-id
  (component :md-inc-grp :required t)
  :appl-queue-depth
  :appl-queue-resolution)

(defmessage market-data-request-reject "Y" ()
  (:md-req-id :required t)
  :md-req-rej-reason
  (component :md-rjct-grp)
  :text
  :encoded-text-len
  :encoded-text)

(defmessage quote-cancel "Z" ()
  :quote-req-id
  (:quote-id :required t)
  (:quote-cancel-type :required t)
  :quote-response-level
  (component :parties)
  :account
  :acct-id-source
  :account-type
  :trading-session-id
  :trading-session-sub-id
  (component :quot-cxl-entries-grp))

(defmessage quote-status-request "a" ()
  :quote-status-req-id
  :quote-id
  (component :instrument :required t)
  (component :financing-details)
  (component :und-instrmt-grp)
  (component :instrmt-leg-grp)
  (component :parties)
  :account
  :acct-id-source
  :account-type
  :trading-session-id
  :trading-session-sub-id
  :subscription-request-type)

(defmessage mass-quote-acknowledgement "b" ()
  :quote-req-id
  :quote-id
  (:quote-status :required t)
  :quote-reject-reason
  :quote-response-level
  :quote-type
  (component :parties)
  :account
  :acct-id-source
  :account-type
  :text
  :encoded-text-len
  :encoded-text
  (component :quot-set-ack-grp))

(defmessage security-definition-request "c" ()
  (:security-req-id :required t)
  (:security-request-type :required t)
  (component :instrument)
  (component :instrument-extension)
  (component :und-instrmt-grp)
  :currency
  :text
  :encoded-text-len
  :encoded-text
  :trading-session-id
  :trading-session-sub-id
  (component :instrmt-leg-grp)
  :expiration-cycle
  :subscription-request-type)

(defmessage security-definition "d" ()
  (:security-req-id :required t)
  (:security-response-id :required t)
  (:security-response-type :required t)
  (component :instrument)
  (component :instrument-extension)
  (component :und-instrmt-grp)
  :currency
  :trading-session-id
  :trading-session-sub-id
  :text
  :encoded-text-len
  :encoded-text
  (component :instrmt-leg-grp)
  :expiration-cycle
  :round-lot
  :min-trade-vol)

(defmessage security-status-request "e" ()
  (:security-status-req-id :required t)
  (component :instrument :required t)
  (component :instrument-extension)
  (component :und-instrmt-grp)
  (component :instrmt-leg-grp)
  :currency
  (:subscription-request-type :required t)
  :trading-session-id
  :trading-session-sub-id)

(defmessage security-status "f" ()
  :security-status-req-id
  (component :instrument :required t)
  (component :instrument-extension)
  (component :und-instrmt-grp)
  (component :instrmt-leg-grp)
  :currency
  :trading-session-id
  :trading-session-sub-id
  :unsolicited-indicator
  :security-trading-status
  :financial-status
  :corporate-action
  :halt-reason-char
  :in-view-common
  :due-related
  :buy-volume
  :sell-volume
  :high-px
  :low-px
  :last-px
  :transact-time
  :adjustment
  :text
  :encoded-text-len
  :encoded-text)

(defmessage trading-session-status-request "g" ()
  (:trad-ses-req-id :required t)
  :trading-session-id
  :trading-session-sub-id
  :trad-ses-method
  :trad-ses-mode
  (:subscription-request-type :required t))

(defmessage trading-session-status "h" ()
  :trad-ses-req-id
  (:trading-session-id :required t)
  :trading-session-sub-id
  :trad-ses-method
  :trad-ses-mode
  :unsolicited-indicator
  (:trad-ses-status :required t)
  :trad-ses-status-rej-reason
  :trad-ses-start-time
  :trad-ses-open-time
  :trad-ses-pre-close-time
  :trad-ses-close-time
  :trad-ses-end-time
  :total-volume-traded
  :text
  :encoded-text-len
  :encoded-text)

(defmessage mass-quote "i" ()
  :quote-req-id
  (:quote-id :required t)
  :quote-type
  :quote-response-level
  (component :parties)
  :account
  :acct-id-source
  :account-type
  :def-bid-size
  :def-offer-size
  (component :quot-set-grp :required t))

(defmessage business-message-reject "j" ()
  :ref-seq-num
  (:ref-msg-type :required t)
  :business-reject-ref-id
  (:business-reject-reason :required t)
  :text
  :encoded-text-len
  :encoded-text)

(defmessage bid-request "k" ()
  :bid-id
  (:client-bid-id :required t)
  (:bid-request-trans-type :required t)
  :list-name
  (:tot-related-sym :required t)
  (:bid-type :required t)
  :num-tickets
  :currency
  :side-value1
  :side-value2
  (component :bid-desc-req-grp)
  (component :bid-comp-req-grp)
  :liquidity-ind-type
  :wt-average-liquidity
  :exchange-for-physical
  :out-main-cntryuindex
  :cross-percent
  :prog-rpt-reqs
  :prog-period-interval
  :inc-tax-ind
  :forex-req
  :num-bidders
  :trade-date
  (:bid-trade-type :required t)
  (:basis-type :required t)
  :strike-time
  :text
  :encoded-text-len
  :encoded-text)

(defmessage bid-response "l" ()
  :bid-id
  :client-bid-id
  (component :bid-comp-rsp-grp :required t))

(defmessage list-strike-price "m" ()
  (:list-id :required t)
  (:tot-strikes :required t)
  :last-fragment
  (component :instrmt-strk-grp :required t)
  (component :und-instrmt-strk-grp))

(defmessage xmlnonfix "n" ()
)

(defmessage registration-instructions "o" ()
  (:regist-id :required t)
  (:regist-trans-type :required t)
  (:regist-ref-id :required t)
  :cl-ord-id
  (component :parties)
  :account
  :acct-id-source
  :regist-acct-type
  :tax-advantage-type
  :ownership-type
  (component :rgst-dtls-grp)
  (component :rgst-dist-inst-grp))

(defmessage registration-instructions-response "p" ()
  (:regist-id :required t)
  (:regist-trans-type :required t)
  (:regist-ref-id :required t)
  :cl-ord-id
  (component :parties)
  :account
  :acct-id-source
  (:regist-status :required t)
  :regist-rej-reason-code
  :regist-rej-reason-text)

(defmessage order-mass-cancel-request "q" ()
  (:cl-ord-id :required t)
  :secondary-ord-id
  (:mass-cancel-request-type :required t)
  :trading-session-id
  :trading-session-sub-id
  (component :instrument)
  (component :underlying-instrument)
  :side
  (:transact-time :required t)
  :text
  :encoded-text-len
  :encoded-text)

(defmessage order-mass-cancel-report "r" ()
  :cl-ord-id
  :secondary-ord-id
  (:order-id :required t)
  :secondary-order-id
  (:mass-cancel-request-type :required t)
  (:mass-cancel-response :required t)
  :mass-cancel-reject-reason
  :total-affected-orders
  (component :affected-ord-grp)
  :trading-session-id
  :trading-session-sub-id
  (component :instrument)
  (component :underlying-instrument)
  :side
  :transact-time
  :text
  :encoded-text-len
  :encoded-text)

(defmessage new-order-cross "s" ()
  (:cross-id :required t)
  (:cross-type :required t)
  (:cross-prioritization :required t)
  (component :side-cross-ord-mod-grp :required t)
  (component :instrument :required t)
  (component :und-instrmt-grp)
  (component :instrmt-leg-grp)
  :settl-type
  :settl-date
  :handl-inst
  :exec-inst
  :min-qty
  :max-floor
  :ex-destination
  (component :trdg-ses-grp)
  :process-code
  :prev-close-px
  :locate-reqd
  (:transact-time :required t)
  (component :stipulations)
  (:ord-type :required t)
  :price-type
  :price
  :stop-px
  (component :spread-benchmark-curve-data)
  (component :yield-data)
  :currency
  :compliance-id
  :ioi-id
  :quote-id
  :time-force
  :effective-time
  :expire-date
  :expire-time
  :gtbooking-inst
  :max-show
  (component :peg-instructions)
  (component :discretion-instructions)
  :target-strategy
  :target-strategy-parameters
  :participation-rate
  :cancellation-rights
  :money-laundering-status
  :regist-id
  :designation)

(defmessage cross-order-cancel-replace-request "t" ()
  :order-id
  (:cross-id :required t)
  (:orig-cross-id :required t)
  (:cross-type :required t)
  (:cross-prioritization :required t)
  (component :side-cross-ord-mod-grp :required t)
  (component :instrument :required t)
  (component :und-instrmt-grp)
  (component :instrmt-leg-grp)
  :settl-type
  :settl-date
  :handl-inst
  :exec-inst
  :min-qty
  :max-floor
  :ex-destination
  (component :trdg-ses-grp)
  :process-code
  :prev-close-px
  :locate-reqd
  (:transact-time :required t)
  (component :stipulations)
  (:ord-type :required t)
  :price-type
  :price
  :stop-px
  (component :spread-benchmark-curve-data)
  (component :yield-data)
  :currency
  :compliance-id
  :ioi-id
  :quote-id
  :time-force
  :effective-time
  :expire-date
  :expire-time
  :gtbooking-inst
  :max-show
  (component :peg-instructions)
  (component :discretion-instructions)
  :target-strategy
  :target-strategy-parameters
  :participation-rate
  :cancellation-rights
  :money-laundering-status
  :regist-id
  :designation)

(defmessage cross-order-cancel-request "u" ()
  :order-id
  (:cross-id :required t)
  (:orig-cross-id :required t)
  (:cross-type :required t)
  (:cross-prioritization :required t)
  (component :side-cross-ord-cxl-grp :required t)
  (component :instrument :required t)
  (component :und-instrmt-grp)
  (component :instrmt-leg-grp)
  (:transact-time :required t))

(defmessage security-type-request "v" ()
  (:security-req-id :required t)
  :text
  :encoded-text-len
  :encoded-text
  :trading-session-id
  :trading-session-sub-id
  :product
  :security-type
  :security-sub-type)

(defmessage security-types "w" ()
  (:security-req-id :required t)
  (:security-response-id :required t)
  (:security-response-type :required t)
  :tot-security-types
  :last-fragment
  (component :sec-types-grp)
  :text
  :encoded-text-len
  :encoded-text
  :trading-session-id
  :trading-session-sub-id
  :subscription-request-type)

(defmessage security-list-request "x" ()
  (:security-req-id :required t)
  (:security-list-request-type :required t)
  (component :instrument)
  (component :instrument-extension)
  (component :financing-details)
  (component :und-instrmt-grp)
  (component :instrmt-leg-grp)
  :currency
  :text
  :encoded-text-len
  :encoded-text
  :trading-session-id
  :trading-session-sub-id
  :subscription-request-type)

(defmessage security-list "y" ()
  (:security-req-id :required t)
  (:security-response-id :required t)
  (:security-request-result :required t)
  :tot-related-sym
  :last-fragment
  (component :sec-list-grp))

(defmessage derivative-security-list-request "z" ()
  (:security-req-id :required t)
  (:security-list-request-type :required t)
  (component :underlying-instrument)
  :security-sub-type
  :currency
  :text
  :encoded-text-len
  :encoded-text
  :trading-session-id
  :trading-session-sub-id
  :subscription-request-type)

(defmessage derivative-security-list "AA" ()
  (:security-req-id :required t)
  (:security-response-id :required t)
  (:security-request-result :required t)
  (component :underlying-instrument)
  :tot-related-sym
  :last-fragment
  (component :rel-sym-deriv-sec-grp))

(defmessage new-order-multileg "AB" ()
  (:cl-ord-id :required t)
  :secondary-ord-id
  :cl-ord-link-id
  (component :parties)
  :trade-origination-date
  :trade-date
  :account
  :acct-id-source
  :account-type
  :day-booking-inst
  :booking-unit
  :prealloc-method
  :alloc-id
  (component :pre-alloc-mleg-grp)
  :settl-type
  :settl-date
  :cash-margin
  :clearing-fee-indicator
  :handl-inst
  :exec-inst
  :min-qty
  :max-floor
  :ex-destination
  (component :trdg-ses-grp)
  :process-code
  (:side :required t)
  (component :instrument :required t)
  (component :und-instrmt-grp)
  :prev-close-px
  (component :leg-ord-grp :required t)
  :locate-reqd
  (:transact-time :required t)
  :qty-type
  (component :order-qty-data :required t)
  (:ord-type :required t)
  :price-type
  :price
  :stop-px
  :currency
  :compliance-id
  :solicited-flag
  :ioi-id
  :quote-id
  :time-force
  :effective-time
  :expire-date
  :expire-time
  :gtbooking-inst
  (component :commission-data)
  :order-capacity
  :order-restrictions
  :cust-order-capacity
  :forex-req
  :settl-currency
  :booking-type
  :text
  :encoded-text-len
  :encoded-text
  :position-effect
  :covered-uncovered
  :max-show
  (component :peg-instructions)
  (component :discretion-instructions)
  :target-strategy
  :target-strategy-parameters
  :participation-rate
  :cancellation-rights
  :money-laundering-status
  :regist-id
  :designation
  :multi-leg-rpt-type-req)

(defmessage multileg-order-cancel-replace "AC" ()
  :order-id
  (:orig-ord-id :required t)
  (:cl-ord-id :required t)
  :secondary-ord-id
  :cl-ord-link-id
  :orig-ord-mod-time
  (component :parties)
  :trade-origination-date
  :trade-date
  :account
  :acct-id-source
  :account-type
  :day-booking-inst
  :booking-unit
  :prealloc-method
  :alloc-id
  (component :pre-alloc-mleg-grp)
  :settl-type
  :settl-date
  :cash-margin
  :clearing-fee-indicator
  :handl-inst
  :exec-inst
  :min-qty
  :max-floor
  :ex-destination
  (component :trdg-ses-grp)
  :process-code
  (:side :required t)
  (component :instrument :required t)
  (component :und-instrmt-grp)
  :prev-close-px
  (component :leg-ord-grp :required t)
  :locate-reqd
  (:transact-time :required t)
  :qty-type
  (component :order-qty-data :required t)
  (:ord-type :required t)
  :price-type
  :price
  :stop-px
  :currency
  :compliance-id
  :solicited-flag
  :ioi-id
  :quote-id
  :time-force
  :effective-time
  :expire-date
  :expire-time
  :gtbooking-inst
  (component :commission-data)
  :order-capacity
  :order-restrictions
  :cust-order-capacity
  :forex-req
  :settl-currency
  :booking-type
  :text
  :encoded-text-len
  :encoded-text
  :position-effect
  :covered-uncovered
  :max-show
  (component :peg-instructions)
  (component :discretion-instructions)
  :target-strategy
  :target-strategy-parameters
  :participation-rate
  :cancellation-rights
  :money-laundering-status
  :regist-id
  :designation
  :multi-leg-rpt-type-req)

(defmessage trade-capture-report-request "AD" ()
  (:trade-request-id :required t)
  (:trade-request-type :required t)
  :subscription-request-type
  :trade-report-id
  :secondary-trade-report-id
  :exec-id
  :exec-type
  :order-id
  :cl-ord-id
  :match-status
  :trd-type
  :trd-sub-type
  :transfer-reason
  :secondary-trd-type
  :trade-link-id
  :trd-match-id
  (component :parties)
  (component :instrument)
  (component :instrument-extension)
  (component :financing-details)
  (component :und-instrmt-grp)
  (component :instrmt-leg-grp)
  (component :trd-cap-grp)
  :clearing-business-date
  :trading-session-id
  :trading-session-sub-id
  :time-bracket
  :side
  :multi-leg-reporting-type
  :trade-input-source
  :trade-input-device
  :response-transport-type
  :response-destination
  :text
  :encoded-text-len
  :encoded-text)

(defmessage trade-capture-report "AE" ()
  (:trade-report-id :required t)
  :trade-report-trans-type
  :trade-report-type
  :trade-request-id
  :trd-type
  :trd-sub-type
  :secondary-trd-type
  :transfer-reason
  :exec-type
  :tot-num-trade-reports
  :last-rpt-requested
  :unsolicited-indicator
  :subscription-request-type
  :trade-report-ref-id
  :secondary-trade-report-ref-id
  :secondary-trade-report-id
  :trade-link-id
  :trd-match-id
  :exec-id
  :ord-status
  :secondary-exec-id
  :exec-restatement-reason
  (:previously-reported :required t)
  :price-type
  (component :instrument :required t)
  (component :financing-details)
  (component :order-qty-data)
  :qty-type
  (component :yield-data)
  (component :und-instrmt-grp)
  :underlying-trading-session-id
  :underlying-trading-session-sub-id
  (:last-qty :required t)
  (:last-px :required t)
  :last-par-px
  :last-spot-rate
  :last-forward-points
  :last-mkt
  (:trade-date :required t)
  :clearing-business-date
  :avg-px
  (component :spread-benchmark-curve-data)
  :avg-indicator
  (component :position-amount-data)
  :multi-leg-reporting-type
  :trade-leg-ref-id
  (component :trd-instrmt-leg-grp)
  (:transact-time :required t)
  (component :trd-reg-timestamps)
  :settl-type
  :settl-date
  :match-status
  :match-type
  (component :trd-cap-rpt-side-grp :required t)
  :copy-msg-indicator
  :publish-trd-indicator
  :short-sale-reason)

(defmessage order-mass-status-request "AF" ()
  (:mass-status-req-id :required t)
  (:mass-status-req-type :required t)
  (component :parties)
  :account
  :acct-id-source
  :trading-session-id
  :trading-session-sub-id
  (component :instrument)
  (component :underlying-instrument)
  :side)

(defmessage quote-request-reject "AG" ()
  (:quote-req-id :required t)
  :rfqreq-id
  (:quote-request-reject-reason :required t)
  (component :quot-req-rjct-grp :required t)
  :text
  :encoded-text-len
  :encoded-text)

(defmessage rfqrequest "AH" ()
  (:rfqreq-id :required t)
  (component :rfqreq-grp :required t)
  :subscription-request-type)

(defmessage quote-status-report "AI" ()
  :quote-status-req-id
  :quote-req-id
  (:quote-id :required t)
  :quote-resp-id
  :quote-type
  (component :parties)
  :trading-session-id
  :trading-session-sub-id
  (component :instrument :required t)
  (component :financing-details)
  (component :und-instrmt-grp)
  :side
  (component :order-qty-data)
  :settl-type
  :settl-date
  :settl-date2
  :order-qty2
  :currency
  (component :stipulations)
  :account
  :acct-id-source
  :account-type
  (component :leg-quot-stat-grp)
  (component :quot-qual-grp)
  :expire-time
  :price
  :price-type
  (component :spread-benchmark-curve-data)
  (component :yield-data)
  :bid-px
  :offer-px
  :mkt-bid-px
  :mkt-offer-px
  :min-bid-size
  :bid-size
  :min-offer-size
  :offer-size
  :valid-until-time
  :bid-spot-rate
  :offer-spot-rate
  :bid-forward-points
  :offer-forward-points
  :mid-px
  :bid-yield
  :mid-yield
  :offer-yield
  :transact-time
  :ord-type
  :bid-forward-points2
  :offer-forward-points2
  :settl-curr-bid-rate
  :settl-curr-offer-rate
  :settl-curr-rate-calc
  :comm-type
  :commission
  :cust-order-capacity
  :ex-destination
  :quote-status
  :text
  :encoded-text-len
  :encoded-text)

(defmessage quote-response "AJ" ()
  (:quote-resp-id :required t)
  :quote-id
  (:quote-resp-type :required t)
  :cl-ord-id
  :order-capacity
  :ioi-id
  :quote-type
  (component :quot-qual-grp)
  (component :parties)
  :trading-session-id
  :trading-session-sub-id
  (component :instrument :required t)
  (component :financing-details)
  (component :und-instrmt-grp)
  :side
  (component :order-qty-data)
  :settl-type
  :settl-date
  :settl-date2
  :order-qty2
  :currency
  (component :stipulations)
  :account
  :acct-id-source
  :account-type
  (component :leg-quot-grp)
  :bid-px
  :offer-px
  :mkt-bid-px
  :mkt-offer-px
  :min-bid-size
  :bid-size
  :min-offer-size
  :offer-size
  :valid-until-time
  :bid-spot-rate
  :offer-spot-rate
  :bid-forward-points
  :offer-forward-points
  :mid-px
  :bid-yield
  :mid-yield
  :offer-yield
  :transact-time
  :ord-type
  :bid-forward-points2
  :offer-forward-points2
  :settl-curr-bid-rate
  :settl-curr-offer-rate
  :settl-curr-rate-calc
  :commission
  :comm-type
  :cust-order-capacity
  :ex-destination
  :text
  :encoded-text-len
  :encoded-text
  :price
  :price-type
  (component :spread-benchmark-curve-data)
  (component :yield-data))

(defmessage confirmation "AK" ()
  (:confirm-id :required t)
  :confirm-ref-id
  :confirm-req-id
  (:confirm-trans-type :required t)
  (:confirm-type :required t)
  :copy-msg-indicator
  :legal-confirm
  (:confirm-status :required t)
  (component :parties)
  (component :ord-alloc-grp)
  :alloc-id
  :secondary-alloc-id
  :individual-alloc-id
  (:transact-time :required t)
  (:trade-date :required t)
  (component :trd-reg-timestamps)
  (component :instrument :required t)
  (component :instrument-extension)
  (component :financing-details)
  (component :und-instrmt-grp :required t)
  (component :instrmt-leg-grp :required t)
  (component :yield-data)
  (:alloc-qty :required t)
  :qty-type
  (:side :required t)
  :currency
  :last-mkt
  (component :cpcty-conf-grp :required t)
  (:alloc-account :required t)
  :alloc-acct-id-source
  :alloc-account-type
  (:avg-px :required t)
  :avg-precision
  :price-type
  :avg-par-px
  (component :spread-benchmark-curve-data)
  :reported-px
  :text
  :encoded-text-len
  :encoded-text
  :process-code
  (:gross-trade-amt :required t)
  :num-days-interest
  :ex-date
  :accrued-interest-rate
  :accrued-interest-amt
  :interest-maturity
  :end-accrued-interest-amt
  :start-cash
  :end-cash
  :concession
  :total-takedown
  (:net-money :required t)
  :maturity-net-money
  :settl-curr-amt
  :settl-currency
  :settl-curr-rate
  :settl-curr-rate-calc
  :settl-type
  :settl-date
  (component :settl-instructions-data)
  (component :commission-data)
  :shared-commission
  (component :stipulations)
  (component :misc-fees-grp))

(defmessage position-maintenance-request "AL" ()
  (:pos-req-id :required t)
  (:pos-trans-type :required t)
  (:pos-maint-action :required t)
  :orig-pos-req-ref-id
  :pos-maint-rpt-ref-id
  (:clearing-business-date :required t)
  :settl-sess-id
  :settl-sess-sub-id
  (component :parties :required t)
  (:account :required t)
  :acct-id-source
  (:account-type :required t)
  (component :instrument :required t)
  :currency
  (component :instrmt-leg-grp)
  (component :und-instrmt-grp)
  (component :trdg-ses-grp)
  (:transact-time :required t)
  (component :position-qty :required t)
  :adjustment-type
  :contrary-instruction-indicator
  :prior-spread-indicator
  :threshold-amount
  :text
  :encoded-text-len
  :encoded-text)

(defmessage position-maintenance-report "AM" ()
  (:pos-maint-rpt-id :required t)
  (:pos-trans-type :required t)
  :pos-req-id
  (:pos-maint-action :required t)
  (:orig-pos-req-ref-id :required t)
  (:pos-maint-status :required t)
  :pos-maint-result
  (:clearing-business-date :required t)
  :settl-sess-id
  :settl-sess-sub-id
  (component :parties)
  (:account :required t)
  :acct-id-source
  (:account-type :required t)
  (component :instrument :required t)
  :currency
  (component :instrmt-leg-grp)
  (component :und-instrmt-grp)
  (component :trdg-ses-grp)
  (:transact-time :required t)
  (component :position-qty :required t)
  (component :position-amount-data :required t)
  :adjustment-type
  :threshold-amount
  :text
  :encoded-text-len
  :encoded-text)

(defmessage request-for-positions "AN" ()
  (:pos-req-id :required t)
  (:pos-req-type :required t)
  :match-status
  :subscription-request-type
  (component :parties :required t)
  (:account :required t)
  :acct-id-source
  (:account-type :required t)
  (component :instrument)
  :currency
  (component :instrmt-leg-grp)
  (component :und-instrmt-grp)
  (:clearing-business-date :required t)
  :settl-sess-id
  :settl-sess-sub-id
  (component :trdg-ses-grp)
  (:transact-time :required t)
  :response-transport-type
  :response-destination
  :text
  :encoded-text-len
  :encoded-text)

(defmessage request-for-positions-ack "AO" ()
  (:pos-maint-rpt-id :required t)
  :pos-req-id
  :total-num-pos-reports
  :unsolicited-indicator
  (:pos-req-result :required t)
  (:pos-req-status :required t)
  (component :parties :required t)
  (:account :required t)
  :acct-id-source
  (:account-type :required t)
  (component :instrument)
  :currency
  (component :instrmt-leg-grp)
  (component :und-instrmt-grp)
  :response-transport-type
  :response-destination
  :text
  :encoded-text-len
  :encoded-text)

(defmessage position-report "AP" ()
  (:pos-maint-rpt-id :required t)
  :pos-req-id
  :pos-req-type
  :subscription-request-type
  :total-num-pos-reports
  :unsolicited-indicator
  (:pos-req-result :required t)
  (:clearing-business-date :required t)
  :settl-sess-id
  :settl-sess-sub-id
  (component :parties :required t)
  (:account :required t)
  :acct-id-source
  (:account-type :required t)
  (component :instrument)
  :currency
  (:settl-price :required t)
  (:settl-price-type :required t)
  (:prior-settl-price :required t)
  (component :instrmt-leg-grp)
  (component :pos-und-instrmt-grp)
  (component :position-qty :required t)
  (component :position-amount-data :required t)
  :regist-status
  :delivery-date
  :text
  :encoded-text-len
  :encoded-text)

(defmessage trade-capture-report-request-ack "AQ" ()
  (:trade-request-id :required t)
  (:trade-request-type :required t)
  :subscription-request-type
  :tot-num-trade-reports
  (:trade-request-result :required t)
  (:trade-request-status :required t)
  (component :instrument :required t)
  (component :und-instrmt-grp)
  (component :instrmt-leg-grp)
  :multi-leg-reporting-type
  :response-transport-type
  :response-destination
  :text
  :encoded-text-len
  :encoded-text)

(defmessage trade-capture-report-ack "AR" ()
  (:trade-report-id :required t)
  :trade-report-trans-type
  :trade-report-type
  :trd-type
  :trd-sub-type
  :secondary-trd-type
  :transfer-reason
  (:exec-type :required t)
  :trade-report-ref-id
  :secondary-trade-report-ref-id
  :trd-rpt-status
  :trade-report-reject-reason
  :secondary-trade-report-id
  :subscription-request-type
  :trade-link-id
  :trd-match-id
  :exec-id
  :secondary-exec-id
  (component :instrument :required t)
  :transact-time
  (component :trd-reg-timestamps)
  :response-transport-type
  :response-destination
  :text
  :encoded-text-len
  :encoded-text
  (component :trd-instrmt-leg-grp)
  :clearing-fee-indicator
  :order-capacity
  :order-restrictions
  :cust-order-capacity
  :account
  :acct-id-source
  :account-type
  :position-effect
  :prealloc-method
  (component :trd-alloc-grp))

(defmessage allocation-report "AS" ()
  (:alloc-report-id :required t)
  :alloc-id
  (:alloc-trans-type :required t)
  :alloc-report-ref-id
  :alloc-canc-replace-reason
  :secondary-alloc-id
  (:alloc-report-type :required t)
  (:alloc-status :required t)
  :alloc-rej-code
  :ref-alloc-id
  :alloc-intermed-req-type
  :alloc-link-id
  :alloc-link-type
  :booking-ref-id
  (:alloc-orders-type :required t)
  (component :ord-alloc-grp)
  (component :exec-alloc-grp)
  :previously-reported
  :reversal-indicator
  :match-type
  (:side :required t)
  (component :instrument :required t)
  (component :instrument-extension)
  (component :financing-details)
  (component :und-instrmt-grp)
  (component :instrmt-leg-grp)
  (:quantity :required t)
  :qty-type
  :last-mkt
  :trade-origination-date
  :trading-session-id
  :trading-session-sub-id
  :price-type
  (:avg-px :required t)
  :avg-par-px
  (component :spread-benchmark-curve-data)
  :currency
  :avg-precision
  (component :parties)
  (:trade-date :required t)
  :transact-time
  :settl-type
  :settl-date
  :booking-type
  :gross-trade-amt
  :concession
  :total-takedown
  :net-money
  :position-effect
  :auto-accept-indicator
  :text
  :encoded-text-len
  :encoded-text
  :num-days-interest
  :accrued-interest-rate
  :accrued-interest-amt
  :total-accrued-interest-amt
  :interest-maturity
  :end-accrued-interest-amt
  :start-cash
  :end-cash
  :legal-confirm
  (component :stipulations)
  (component :yield-data)
  :tot-allocs
  :last-fragment
  (component :alloc-grp))

(defmessage allocation-report-ack "AT" ()
  (:alloc-report-id :required t)
  (:alloc-id :required t)
  (component :parties)
  :secondary-alloc-id
  :trade-date
  (:transact-time :required t)
  (:alloc-status :required t)
  :alloc-rej-code
  :alloc-report-type
  :alloc-intermed-req-type
  :match-status
  :product
  :security-type
  :text
  :encoded-text-len
  :encoded-text
  (component :alloc-ack-grp))

(defmessage confirmation-ack "AU" ()
  (:confirm-id :required t)
  (:trade-date :required t)
  (:transact-time :required t)
  (:affirm-status :required t)
  :confirm-rej-reason
  :match-status
  :text
  :encoded-text-len
  :encoded-text)

(defmessage settlement-instruction-request "AV" ()
  (:settl-inst-req-id :required t)
  (:transact-time :required t)
  (component :parties)
  :alloc-account
  :alloc-acct-id-source
  :side
  :product
  :security-type
  :cficode
  :effective-time
  :expire-time
  :last-update-time
  :stand-inst-type
  :stand-inst-name
  :stand-inst-db-id)

(defmessage assignment-report "AW" ()
  (:asgn-rpt-id :required t)
  :tot-num-assignment-reports
  :last-rpt-requested
  (component :parties :required t)
  :account
  (:account-type :required t)
  (component :instrument)
  :currency
  (component :instrmt-leg-grp)
  (component :und-instrmt-grp)
  (component :position-qty :required t)
  (component :position-amount-data :required t)
  :threshold-amount
  (:settl-price :required t)
  (:settl-price-type :required t)
  (:underlying-settl-price :required t)
  :expire-date
  (:assignment-method :required t)
  :assignment-unit
  (:open-interest :required t)
  (:exercise-method :required t)
  (:settl-sess-id :required t)
  (:settl-sess-sub-id :required t)
  (:clearing-business-date :required t)
  :text
  :encoded-text-len
  :encoded-text)

(defmessage collateral-request "AX" ()
  (:coll-req-id :required t)
  (:coll-asgn-reason :required t)
  (:transact-time :required t)
  :expire-time
  (component :parties)
  :account
  :account-type
  :cl-ord-id
  :order-id
  :secondary-order-id
  :secondary-ord-id
  (component :exec-coll-grp)
  (component :trd-coll-grp)
  (component :instrument)
  (component :financing-details)
  :settl-date
  :quantity
  :qty-type
  :currency
  (component :instrmt-leg-grp)
  (component :und-instrmt-coll-grp)
  :margin-excess
  :total-net-value
  :cash-outstanding
  (component :trd-reg-timestamps)
  :side
  (component :misc-fees-grp)
  :price
  :price-type
  :accrued-interest-amt
  :end-accrued-interest-amt
  :start-cash
  :end-cash
  (component :spread-benchmark-curve-data)
  (component :stipulations)
  :trading-session-id
  :trading-session-sub-id
  :settl-sess-id
  :settl-sess-sub-id
  :clearing-business-date
  :text
  :encoded-text-len
  :encoded-text)

(defmessage collateral-assignment "AY" ()
  (:coll-asgn-id :required t)
  :coll-req-id
  (:coll-asgn-reason :required t)
  (:coll-asgn-trans-type :required t)
  :coll-asgn-ref-id
  (:transact-time :required t)
  :expire-time
  (component :parties)
  :account
  :account-type
  :cl-ord-id
  :order-id
  :secondary-order-id
  :secondary-ord-id
  (component :exec-coll-grp)
  (component :trd-coll-grp)
  (component :instrument)
  (component :financing-details)
  :settl-date
  :quantity
  :qty-type
  :currency
  (component :instrmt-leg-grp)
  (component :und-instrmt-coll-grp)
  :margin-excess
  :total-net-value
  :cash-outstanding
  (component :trd-reg-timestamps)
  :side
  (component :misc-fees-grp)
  :price
  :price-type
  :accrued-interest-amt
  :end-accrued-interest-amt
  :start-cash
  :end-cash
  (component :spread-benchmark-curve-data)
  (component :stipulations)
  (component :settl-instructions-data)
  :trading-session-id
  :trading-session-sub-id
  :settl-sess-id
  :settl-sess-sub-id
  :clearing-business-date
  :text
  :encoded-text-len
  :encoded-text)

(defmessage collateral-response "AZ" ()
  (:coll-resp-id :required t)
  (:coll-asgn-id :required t)
  :coll-req-id
  (:coll-asgn-reason :required t)
  :coll-asgn-trans-type
  (:coll-asgn-resp-type :required t)
  :coll-asgn-reject-reason
  (:transact-time :required t)
  (component :parties)
  :account
  :account-type
  :cl-ord-id
  :order-id
  :secondary-order-id
  :secondary-ord-id
  (component :exec-coll-grp)
  (component :trd-coll-grp)
  (component :instrument)
  (component :financing-details)
  :settl-date
  :quantity
  :qty-type
  :currency
  (component :instrmt-leg-grp)
  (component :und-instrmt-coll-grp)
  :margin-excess
  :total-net-value
  :cash-outstanding
  (component :trd-reg-timestamps)
  :side
  (component :misc-fees-grp)
  :price
  :price-type
  :accrued-interest-amt
  :end-accrued-interest-amt
  :start-cash
  :end-cash
  (component :spread-benchmark-curve-data)
  (component :stipulations)
  :text
  :encoded-text-len
  :encoded-text)

(defmessage collateral-report "BA" ()
  (:coll-rpt-id :required t)
  :coll-inquiry-id
  (:coll-status :required t)
  :tot-num-reports
  :last-rpt-requested
  (component :parties)
  :account
  :account-type
  :cl-ord-id
  :order-id
  :secondary-order-id
  :secondary-ord-id
  (component :exec-coll-grp)
  (component :trd-coll-grp)
  (component :instrument)
  (component :financing-details)
  :settl-date
  :quantity
  :qty-type
  :currency
  (component :instrmt-leg-grp)
  (component :und-instrmt-grp)
  :margin-excess
  :total-net-value
  :cash-outstanding
  (component :trd-reg-timestamps)
  :side
  (component :misc-fees-grp)
  :price
  :price-type
  :accrued-interest-amt
  :end-accrued-interest-amt
  :start-cash
  :end-cash
  (component :spread-benchmark-curve-data)
  (component :stipulations)
  (component :settl-instructions-data)
  :trading-session-id
  :trading-session-sub-id
  :settl-sess-id
  :settl-sess-sub-id
  :clearing-business-date
  :text
  :encoded-text-len
  :encoded-text)

(defmessage collateral-inquiry "BB" ()
  :coll-inquiry-id
  (component :coll-inq-qual-grp)
  :subscription-request-type
  :response-transport-type
  :response-destination
  (component :parties)
  :account
  :account-type
  :cl-ord-id
  :order-id
  :secondary-order-id
  :secondary-ord-id
  (component :exec-coll-grp)
  (component :trd-coll-grp)
  (component :instrument)
  (component :financing-details)
  :settl-date
  :quantity
  :qty-type
  :currency
  (component :instrmt-leg-grp)
  (component :und-instrmt-grp)
  :margin-excess
  :total-net-value
  :cash-outstanding
  (component :trd-reg-timestamps)
  :side
  :price
  :price-type
  :accrued-interest-amt
  :end-accrued-interest-amt
  :start-cash
  :end-cash
  (component :spread-benchmark-curve-data)
  (component :stipulations)
  (component :settl-instructions-data)
  :trading-session-id
  :trading-session-sub-id
  :settl-sess-id
  :settl-sess-sub-id
  :clearing-business-date
  :text
  :encoded-text-len
  :encoded-text)

(defmessage network-counterparty-system-status-request "BC" ()
  (:network-request-type :required t)
  (:network-request-id :required t)
  (component :comp-id-req-grp))

(defmessage network-counterparty-system-status-response "BD" ()
  (:network-status-response-type :required t)
  :network-request-id
  (:network-response-id :required t)
  :last-network-response-id
  (component :comp-id-stat-grp :required t))

(defmessage user-request "BE" ()
  (:user-request-id :required t)
  (:user-request-type :required t)
  (:username :required t)
  :password
  :new-password
  :raw-data-length
  :raw-data)

(defmessage user-response "BF" ()
  (:user-request-id :required t)
  (:username :required t)
  :user-status
  :user-status-text)

(defmessage collateral-inquiry-ack "BG" ()
  (:coll-inquiry-id :required t)
  (:coll-inquiry-status :required t)
  :coll-inquiry-result
  (component :coll-inq-qual-grp)
  :tot-num-reports
  (component :parties)
  :account
  :account-type
  :cl-ord-id
  :order-id
  :secondary-order-id
  :secondary-ord-id
  (component :exec-coll-grp)
  (component :trd-coll-grp)
  (component :instrument)
  (component :financing-details)
  :settl-date
  :quantity
  :qty-type
  :currency
  (component :instrmt-leg-grp)
  (component :und-instrmt-grp)
  :trading-session-id
  :trading-session-sub-id
  :settl-sess-id
  :settl-sess-sub-id
  :clearing-business-date
  :response-transport-type
  :response-destination
  :text
  :encoded-text-len
  :encoded-text)

(defmessage confirmation-request "BH" ()
  (:confirm-req-id :required t)
  (:confirm-type :required t)
  (component :ord-alloc-grp)
  :alloc-id
  :secondary-alloc-id
  :individual-alloc-id
  (:transact-time :required t)
  :alloc-account
  :alloc-acct-id-source
  :alloc-account-type
  :text
  :encoded-text-len
  :encoded-text)

;; Defines the READ-FIX function. Enables you to read a fix message from a stream. The
;; reader is blocking - that is, it uses (read-char) under the hood directly against a
;; stream. Again, you're encouraged to macro-expand to see what it is doing.
(defreader)

;; Defines the SERIALIZE-FIX function, which allows you to serialize a MSG into a stream.
;; Also defines a convenience method - STR - which does the above into a string.
(defserializer)

;; Defines the VALIDATE function. This performs a basic check on the top level of a message to
;; ensure that: (1) all required fields are present, (2) the values are the correct type, and (3)
;; fields actually belong to this message.
(defvalidator)
